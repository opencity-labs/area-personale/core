
BASIC_COMPOSE_CONFIG := -f docker-compose.yml -f docker-compose.override.yml

upd:
	docker compose $(BASIC_COMPOSE_CONFIG) up -d

down:
	docker compose $(BASIC_COMPOSE_CONFIG) down --remove-orphans

restart: down upd

enter:
	docker compose $(BASIC_COMPOSE_CONFIG) exec php /bin/bash

enter-root:
	docker compose $(BASIC_COMPOSE_CONFIG) exec -uroot php /bin/bash

postgres:
	docker compose $(BASIC_COMPOSE_CONFIG) up -d postgres

test:
	docker compose $(BASIC_COMPOSE_CONFIG) exec -t php bin/phpunit --stop-on-error --stop-on-failure

test-unit:
	docker compose $(BASIC_COMPOSE_CONFIG) exec -t php bin/phpunit --stop-on-error --stop-on-failure --group unit

test-functional:
	docker compose $(BASIC_COMPOSE_CONFIG) exec -t php bin/phpunit --stop-on-error --stop-on-failure --group functional

logs:
	docker compose $(BASIC_COMPOSE_CONFIG) logs -f php
