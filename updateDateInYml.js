const fs = require('fs');
const yaml = require('yaml');

// Function to read YAML file
function readYamlFile(filePath) {
  const fileContents = fs.readFileSync(filePath, 'utf8');
  return yaml.parse(fileContents);
}

// Function to write YAML file
function writeYamlFile(filePath, data) {
  const yamlString = yaml.stringify(data);
  fs.writeFileSync(filePath, yamlString, 'utf8');
}

// Function to update a line in YAML with the current date in 'Y-m-d' format
function updateYamlLineWithCurrentDate(filePath, keyPath) {
  const data = readYamlFile(filePath);

  // Get the current date in 'Y-m-d' format
  const currentDate = new Date().toISOString().split('T')[0];

  // Navigate to the correct keyPath
  const keys = keyPath.split('.');
  let current = data;
  for (let i = 0; i < keys.length - 1; i++) {
    current = current[keys[i]];
  }

  // Update the value with the current date
  current[keys[keys.length - 1]] = currentDate;

  // Write the updated data back to the YAML file
  writeYamlFile(filePath, data);
}

// Extract command line arguments
const [, , filePath, keyPath] = process.argv;

// Validate command line arguments
if (!filePath || !keyPath) {
  console.error('Usage: node updateYaml.js <filePath> <keyPath>');
  process.exit(1);
}

// Update YAML file with current date
updateYamlLineWithCurrentDate(filePath, keyPath);
console.log(`Updated ${keyPath} with the current date in ${filePath}`);
