<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Set\ValueObject\SetList;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Symfony\Set\SymfonySetList;
use Rector\Symfony\Set\SymfonyLevelSetList;
use Rector\TypeDeclaration\Rector\StmtsAwareInterface\DeclareStrictTypesRector;

return static function (RectorConfig $rectorConfig): void
{
  $rectorConfig->symfonyContainerXml(__DIR__ . '/var/cache/dev/App_KernelDevDebugContainer.xml');
  //$rectorConfig->rule(TypedPropertyRector::class);
  $rectorConfig->phpVersion(PhpVersion::PHP_74);
  //$rectorConfig->phpstanConfig(__DIR__ . '/phpstan.neon');
  //$rectorConfig->disableParallel();

  $rectorConfig->importNames();
  $rectorConfig->importShortClasses();
  $rectorConfig->rule(DeclareStrictTypesRector::class);

  $rectorConfig->sets([
    SetList::CODE_QUALITY,
    SetList::CODING_STYLE,
    SetList::DEAD_CODE,
    SetList::PHP_74,
    DoctrineSetList::DOCTRINE_CODE_QUALITY,
    //SymfonySetList::ANNOTATIONS_TO_ATTRIBUTES,
    SymfonySetList::SYMFONY_CODE_QUALITY,
    SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,
    SymfonySetList::SYMFONY_54,
    SymfonyLevelSetList::UP_TO_SYMFONY_54,
    //SymfonySetList::SYMFONY_STRICT,
  ]);


};
