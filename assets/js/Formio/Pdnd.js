import moment from 'moment/moment';
import {Utils} from 'formiojs';
import axios from 'axios';
import Auth from '../rest/auth/Auth';
import Swal from 'sweetalert2/src/sweetalert2.js';
import Confirmation from "bootstrap-confirmation2/src/confirmation";

class Pdnd {

  static async setComponentFieldsReadonly(component) {
    const successIcon = `<small class="text-success pdnd-success-icon"><i class="fa fa-check-circle"></i></small>`;

    if (component.type === 'select') {
      $(component.element).find('select').each((i, e) => {
        const selectValue = $(e).val();
        if (selectValue && $(e).attr('readonly') !== 'readonly') {
          $(component.element).find('label').each((il, el) => {
            $(el).prepend(successIcon);
          });
          $(e).attr('readonly', 'readonly');
          $(component.element).find('.dropdown').each((il, el) => {
            $(el).addClass('d-none');
            $(el).parent().append(`<input type="text" class="form-control" disabled="disabled" value="${selectValue}">`);
          });
        }
      });
      // Da valutare il disabled
      //component.disabled = true
    } else if (component.type === 'radio') {
      if ($(component.element).find('.pdnd-success-icon').length <= 0) {
        $(component.element).find('.col-form-label').each((i, e) => {
          $(e).prepend(successIcon);
        });
      }
      $(component.element).find('input').each((i, e) => {
        $(e).attr('disabled', 'disabled');
      });
    } else {
      $(component.element).find('input').each((i, e) => {
        if ($(e).val() && $(e).attr('readonly') !== 'readonly') {
          $(component.element).find('label').each((il, el) => {
            $(el).prepend(successIcon);
          });
          $(e).attr('readonly', 'readonly');
        }
      });
    }
  }

  static isObjectEmpty(obj) {

    // Check if obj is a non-null object or array, then evaluate all values
    if (obj && typeof obj === 'object') {

      // Skip property unique_id of the passed object
      for (const [key, value] of Object.entries(obj)) {
        if (key !== 'unique_id' && !Pdnd.isObjectEmpty(value)) {
          return false;
        }
      }
      return true;
    }

    // Base case: true if the value is an empty string, null, or an empty array
    return obj === '' || obj === null || (Array.isArray(obj) && obj.length === 0);
  }

  static isDatagridEmpty(data) {
    // Check if data array exists and has elements
    if (Array.isArray(data) && data.length > 0) {
      // Check if the first row is empty
      return Pdnd.isObjectEmpty(data[0]);
    }
    // Empty if no rows exist
    return true;
  }

  static async setPartOfComponentFieldsReadonly(component) {

    const language = document.documentElement.lang.toString();

    const componentData = component.getValue();
    const formattedDate = moment(componentData.meta.created_at).format('L');
    const pdndAlert = `<p class="mt-n4 pdnd-alert">${Translator.trans('pdnd.success_badge', {
      'source': componentData.meta.source,
      'date': formattedDate
    }, 'messages', language)}</p>`;

    // Append alert if it doesn't already exist
    if ($(component.element).find('.pdnd-alert').length === 0) {
      $(component.element).append(pdndAlert);
    }

    // Wait for subform to be ready
    component.subFormReady.then((subForm) => {
      if (!subForm || !subForm.components || subForm.components.length === 0) {
        console.warn('Subform or components are not available');
        return;
      }

      // Traverse through subform components
      Pdnd.traverseComponents(subForm.components, (subComponent) => {
        // Disable add/remove buttons for datagrid
        if (subComponent.type === 'datagrid') {
          $(subComponent.element).find('.formio-button-remove-row').addClass('d-none').attr('disabled', true);
          $(subComponent.element).find('.formio-button-add-row').addClass('d-none').attr('disabled', true);

          //subComponent.disableAddingRemovingRows = true
          // Non posso usare il disabled perchè anche i componenti all'interno non pdnd_field vengono disabilitati
          //subComponent.disabled = true

          // Verifico che la submission del datagrid sia vuota, in tal caso elimino la riga
          if (Pdnd.isDatagridEmpty(componentData.data[subComponent.component.key])) {
            $(subComponent.element).find('tr').remove();
            $(subComponent.element).find('tbody').html(`<tr><td><span class="text-success"><i class="fa fa-check-circle"></i></span> ${Translator.trans('none', {}, 'messages', language)}</td></tr>`);
          }
        }

        // Set readonly if component has 'pdnd_field' property
        if (subComponent.component.properties?.pdnd_field) {
          Pdnd.setComponentFieldsReadonly(subComponent);
        }
      });
    });
  }

  static async traverseComponents(components, callback) {
    // Iterate over each component in the array
    components.forEach(component => {
      // Execute the callback on the current component
      callback(component);

      // If the component has nested components, recursively traverse them
      if (component.components && Array.isArray(component.components)) {
        Pdnd.traverseComponents(component.components, callback);
      }

      // If the component is a DataGrid, each row can contain components as well
      if (component.rows && Array.isArray(component.rows)) {
        component.rows.forEach(row => {
          if (Array.isArray(component.row)) {
            row.forEach(col => {
              if (col.components) {
                Pdnd.traverseComponents(col.components, callback);
              }
            });
          }
        });
      }

      // If the component is a Panel, FieldSet, or Column, they may contain components
      if (component.columns && Array.isArray(component.columns)) {
        component.columns.forEach(column => {
          Pdnd.traverseComponents(column.components || [], callback);
        });
      }

      // If the component is a Tab, each tab can contain components
      if (component.type === 'tabs' && component.components) {
        component.components.forEach(tab => {
          Pdnd.traverseComponents(tab.components || [], callback);
        });
      }

      // If the component is a nested form, retrieve and traverse its components
      if (component.type === 'form' && component.components) {
        Pdnd.traverseComponents(component.components, callback);
      }

      if (component.type === 'form' && component.subForm) {
        Pdnd.traverseComponents(component.subForm.components, callback);
      }
    });
  }


  static async initEditablePdnd(form, pdndConfigs) {

    const auth = new Auth();

    //if (!_.isEmpty(pdndConfigs)) {
    const applicant = Utils.getComponent(form.components, 'applicant');
    const applicantData = applicant.getValue();
    const fiscalCode = applicantData.data.fiscal_code.data.fiscal_code;
    let profileBlockComponents = [];

    // Devo gestire i pdnd blocks all'interno dell'applicant diversamente
    applicant.subFormReady.then((subForm) => {
      Pdnd.traverseComponents(subForm.components, component => {
        if (component && 'eservice' in component.component.properties && 'format' in component.component.properties) {
          profileBlockComponents.push(component.parent.parent);
        }
      });
    });


    await Pdnd.traverseComponents(form.components, component => {
      if ('profile_block' in component.component.properties) {
        profileBlockComponents.push(component);
      }
    });

    if (profileBlockComponents.length > 0) {
      profileBlockComponents.forEach(component => {
        component.subFormReady.then((subForm) => {
          // Se nella submission c'è un campo meta questo è stato inserito dalla PDND
          if (component.getValue().meta !== undefined) {
            Pdnd.setPartOfComponentFieldsReadonly(component);
          } else {
            try {
              if (subForm && subForm.components && subForm.components.length > 0) {
                let pdndComponent = subForm.components[0];
                if (pdndComponent && 'eservice' in pdndComponent.component.properties && 'format' in pdndComponent.component.properties) {
                  const eserviceId = pdndComponent.component.properties.eservice;
                  const format = pdndComponent.component.properties.format || 'default';

                  if (!_.isEmpty(pdndConfigs) && pdndConfigs[eserviceId]) {
                    const componentElement = $(component.element);

                    if (!component.rendered) {
                      return;
                    }

                    // Verifico tramite la presenza di questo elemento se già fatto l'inizializzazione
                    if (componentElement.find('.pdnd-call').length > 0) {
                      return;
                    }
                    componentElement.prepend('<div class="pdnd-call d-none"></div>')

                    let callUrl = pdndConfigs[eserviceId].call_url;
                    callUrl += '&fiscal_code=' + fiscalCode + '&format=' + format;

                    auth.execAuthenticatedCall((token) => {
                      const authConfig = {
                        headers: {
                          Authorization: `Bearer ${token}`
                        }
                      };
                      axios.get(callUrl, authConfig)
                        .then((response) => {
                          component.setValue(response.data);
                          Pdnd.setPartOfComponentFieldsReadonly(component);
                        })
                        .catch((error) => {
                          console.error(error)
                          Pdnd.initOnceOnly(component, pdndComponent);
                        });
                    });
                  } else {
                    // Sono in un profile block ma la pdnd non risulta abilitata
                    Pdnd.initOnceOnly(component, pdndComponent);
                  }
                } else {
                  // Sono in un profile block ma non ha la pdnd agganciata
                  Pdnd.initOnceOnly(component, pdndComponent);
                }
              }
            } catch (e) {
              console.error(e);
            }
          }
        });
      });
    }
    //}
  }

  static initOnceOnly(component, nestedFormComponent) {

    if (!component.rendered) {
      return;
    }

    // Se l'applicanto o l'address nell'applicant esco
    if (component.path === 'applicant.data.address' || component.component.key === 'applicant') {
      return;
    }

    const componentKey = component.component.key
    const componentElement = $(component.element);

    // Verifico tramite la presenza di questo elemento se già fatto l'inizializzazione
    if (componentElement.find('.once-only').length > 0) {
      return;
    }

    const language = document.documentElement.lang.toString();
    const ctaId = component.id + '-cta'

    if (nestedFormComponent.type === 'datagrid' || (nestedFormComponent.components?.length && nestedFormComponent.components[0].type === 'datagrid')) {
      componentElement.prepend(`<p id="${ctaId}" class="once-only d-none"></p>`);

      const datagridComponent = nestedFormComponent.type === 'datagrid' ? nestedFormComponent : nestedFormComponent.components[0];
      const componentData = component.getValue()
      // Verifico che il componente non sia già valorizzato (bozze o tasto indietro)
      // Todo: migliorare il controllo
      if (Pdnd.isDatagridEmpty(componentData.data[datagridComponent.component.key])) {
        Pdnd.getProfileBlocksData(componentKey, (response) => {
          if (response.data.data.length > 0) {
            component.setValue(response.data.data[0].value);
          }
        });
      }
    } else {
      componentElement.prepend(`<p id="${ctaId}" class="once-only d-none">${Translator.trans('profile_block.has_entry', {}, 'messages', language)} <i class="fa fa-external-link"></i></a></p>`);
      const cta = $('#' + ctaId)

      Pdnd.getProfileBlocksData(componentKey, (response) => {
        if (response.data.data.length > 0) {
          cta.removeClass('d-none');
          let entry = []
          response.data.data.forEach((d) => {
            entry[d.id] = d;
          });

          cta.on('click', (e) => {
            e.preventDefault();
            Swal.fire({
              title: `${Translator.trans('profile_block.modal.title', {}, 'messages', language)}`,
              showCloseButton: true,
              html: '<div></div>',
              showConfirmButton: false,
              showCancelButton: false,
              didOpen: () => {
                Pdnd.generateModalContent(component, entry)
              }
            });
          });
        }
      })
    }

  }

  static generateModalContent(component, profileBlocks) {
    const language = document.documentElement.lang.toString();
    let html = '<div class="it-list-wrapper"><ul class="it-list">';
    for (let id in profileBlocks) {
      const d = profileBlocks[id];
      let name = d.unique_id;
      let subtitle = Translator.trans('profile_block.modal.empty_entry_title', {
        'day': moment(d.updated_at).format('DD/MM/YYYY'),
        'hour': moment(d.updated_at).format('HH:mm'),
      }, 'messages', language)
      if (name === null || name === 'undefined' || name === '') {
        name = subtitle;
        subtitle = '';
      }
      html += `<li>
                <div class="list-item">
                    <button class="btn btn-link btn-xs text-left select-once-only-entry" data-content='${JSON.stringify(d.value)}' type="button">
                      <span class="text">${name}</span>
                      <span class="text-info">${subtitle}</span>
                    </button>
                    <div class="it-right-zone justify-content-end">
                      <button id="${(d.id)}" class="btn btn-outline-danger btn-xs mr-1 delete-once-only-entry" data-toggle="confirmation" data-id="${(d.id)}" type="button"><i class="fa fa-trash-o px-1"></i></button>
                    </div>
               </div>
             </li>`;
    };
    html += '</ul></div>';
    $(Swal.getHtmlContainer()).html(html)
    // Add complention event
    $(Swal.getPopup()).find('.select-once-only-entry').each((i, el) => {
      const content = $(el).data('content');
      $(el).on('click', (e) => {
        e.preventDefault();
        component.setValue(content);
        Swal.close();
      });
    });
    // Add delete event
    $(Swal.getPopup()).find('.delete-once-only-entry').each((i, el) => {
      const id = $(el).data('id');
      const $el = $(el);
      $el.confirmation(
        {
          title: `${Translator.trans('sure_to_proceed', {}, 'messages', language)}`,
          btnOkLabel: `${Translator.trans('elimina', {}, 'messages', language)}`,
          btnOkClass: 'btn btn-sm btn-danger',
          btnCancelLabel: `${Translator.trans('annulla', {}, 'messages', language)}`,
          rootSelector: '#' + id,
          popout: true,
          placement: 'left',
          singleton: true,
          onConfirm: () => {
            $el.on('click', (e) => {
              e.preventDefault();
              Pdnd.deleteProfileBlocks($el, id, () => {
                delete profileBlocks[id]
              });
            });
          }
        }
      )
    });
  }

  static getProfileBlocksData(componentKey, callback) {
    const auth = new Auth();
    const profileBlockApiUrl = $('#formio').data('profile-blocks-url');
    auth.execAuthenticatedCall((token) => {
      const authConfig = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      };

      axios.get(profileBlockApiUrl + '?keys=' + componentKey, authConfig)
        .then((response) => {
          callback(response)
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }

  static deleteProfileBlocks($el, id, callback) {
    const auth = new Auth();
    const profileBlockApiUrl = $('#formio').data('profile-blocks-url');
    $el.parents('li').remove();
    auth.execAuthenticatedCall((token) => {
      const authConfig = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      };
      axios.delete(profileBlockApiUrl + '/' + id, authConfig)
        .then((response) => {
          $el.parents('li').remove();
          if ($(Swal.getPopup()).find('.delete-once-only-entry').length === 0) {
            Swal.close()
          }
          callback()
        })
        .catch((error) => {
          console.error(error);
        });
    })
  }

  static async initSummaryPdnd(form) {
    let profileBlockComponents = [];

    // Devo gestire i pdnd blocks all'interno dell'applicant diversamente
    const applicant = Utils.getComponent(form.components, 'applicant');
    await Pdnd.traverseComponents(applicant.subForm.components, component => {
      if (component && 'eservice' in component.component.properties && 'format' in component.component.properties) {
        profileBlockComponents.push(component.parent.parent);
      }
    });

    await Pdnd.traverseComponents(form.components, component => {
      if ('profile_block' in component.component.properties) {
        profileBlockComponents.push(component);
      }
    });

    if (profileBlockComponents.length > 0) {
      profileBlockComponents.forEach(component => {
        const componentData = component.getValue();
        if (componentData.meta && componentData.meta.is_valid === true) {
          Pdnd.setPartOfComponentFieldsReadonly(component);
        }
      });
    }
  }

}

export default Pdnd;
