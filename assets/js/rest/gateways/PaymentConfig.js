import axios from "axios";
import 'formiojs/dist/formio.form.min.css';
import {Formio} from "formiojs";
import Gateways from "./Gateways";
import Swal from "sweetalert2/src/sweetalert2.js";
import Auth from "../auth/Auth";
import Services from "../services/Services";


class PaymentConfig {

  static $gatewayIdentifierField;
  static currentGatewayValue;
  static $formContainer;

  static gatewayUrl = '';
  static language;
  static tenantId;
  static paymentConfigIds = [];
  static _configs = [];

  static serviceID;
  static serviceApiUtils;

  static auth;

  static init() {

    const $multiplePaymentsContainer = $('#multiple-payments');

    this.auth = new Auth();

    this.serviceApiUtils = new Services();
    this.tenantId = $multiplePaymentsContainer.data('tenant-id');
    this.paymentConfigIds = $multiplePaymentsContainer.data('payment-configs');
    this.serviceID = $multiplePaymentsContainer.data('service-id')
    this.language = document.documentElement.lang.toString();

    this.$formContainer = $('#form');

    this.$gatewayIdentifierField = $('#payment_data_paymentGatewayIdentifier');
    this.currentGatewayValue = this.$gatewayIdentifierField.val();
    this.$gatewayIdentifierField.on('change', (e) => {
      this.handleGatewayChange(e)
    });
    this.$gatewayIdentifierField.trigger('change');

    $('.add-payment').on('click', (e) => {
      this.handleAddPayment(e)
    })

    $(document).on('click', '.edit-payment', (e) => {
      this.handleEditPayment(e)
    });

    $(document).on('click', '.delete-payment', (e) => {
      this.handleDeletePayment(e)
    });

    this.loadPaymentConfigs()
  }

  static handleGatewayChange(e) {

    let target = $(e.target);

    if (this.$gatewayIdentifierField.val() !== this.currentGatewayValue && this.currentGatewayValue) {
      Swal.fire({
        title: `Modifica del gateway di pagamento`,
        text: `Cambiando il gateway di pagamento tutte le configurazioni associate andranno perse, sei sicuro di voler continuare?`,
        showCloseButton: false,
        showCancelButton: true,
        showConfirmButton: true
      })
        .then((result) => {
          // Cambio il gateway perdendo le configurazioni
          if (result.isConfirmed) {
            this.currentGatewayValue = this.$gatewayIdentifierField.val();
            this.updateServicePaymentGateway()
            this.paymentConfigIds = {};
            this._configs = [];
            this.updateServicePaymentConfigs();
          } else  {
            this.$gatewayIdentifierField.val(this.currentGatewayValue)
          }

          let selected = this.$gatewayIdentifierField.find(':selected')[0];
          this.gatewayUrl = $(selected).data('url');
        });
    } else {
      this.currentGatewayValue = this.$gatewayIdentifierField.val();
      let selected = this.$gatewayIdentifierField.find(':selected')[0];
      this.gatewayUrl = $(selected).data('url');
      if (this.currentGatewayValue) {
        $('.payment-configs').addClass('d-block').removeClass('d-none')
      } else {
        $('.payment-configs').addClass('d-none').removeClass('d-block')
      }
    }


  }

  static handleAddPayment(e) {
    const target = $(e.target);
    const phase = target.data('phase');
    let data = {
      tenant_id: this.tenantId,
      active: true
    }

    Swal.fire({
      title: `Aggiungi pagamento`,
      width: '80%',
      html: '<div class="overflow-hidden"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>',
      showCloseButton: false,
      showConfirmButton: false,
      didOpen: () => {
        const html = '<div id="form"></div>';
        $(Swal.getPopup()).children('.swal2-html-container').html(html);
        Gateways.getTenantsSchema(this.gatewayUrl + '/configs/schema').then((result) => {
          Formio.createForm(document.getElementById('form'), result, {
            noAlerts: true,
            buttonSettings: {showCancel: false},
          })
            .then(function (form) {
              form.submission = {
                data: data
              };
              form.nosubmit = true;
              form.on('submit', function (submission) {

                submission.data.remote_collection = {
                  id: PaymentConfig.serviceID,
                  type: 'service'
                }

                // Creo la configurazione
                PaymentConfig.auth.execAuthenticatedCall((token) => {
                  const authConfig = {
                    headers: {
                      Authorization: `Bearer ${token}`
                    }
                  };
                  axios.post(PaymentConfig.gatewayUrl + '/configs', submission.data, authConfig)
                    .then(function (response) {

                      // Aggiorno il servizio
                      PaymentConfig.paymentConfigIds[response.data.id] = {
                        'id': response.data.id,
                        'phase': phase
                      };
                      PaymentConfig._configs[response.data.id] = submission.data;
                      PaymentConfig.updateServicePaymentConfigs();

                      // Aggiorno il template
                      PaymentConfig.renderPaymentConfig(response.data, phase);

                      Swal.fire({
                        title: `Pagamento creato correttamente`,
                        icon: "success",
                        showConfirmButton: true
                      });
                    })
                    .finally(() => {
                      PaymentConfig.updatePaymentConfigsCounter(phase)
                    })
                    .catch(function (error) {
                      console.log(error);
                    });
                })
              });
            })

        }).catch(err => {
          if (err.status === 404) {
            $('form').html(`<div>${Translator.trans('iscrizioni.no_payments_config', {}, 'messages', this.language)}</div>`)
          } else {
            $('form').html('<div>' + err.statusText + '</div>')
          }
        })
      },
    })

  }

  static handleEditPayment(e) {
    const target = $(e.target);
    const parent = $(target.parents('.payment-config')[0]);
    const id = parent.data('id');
    const data = this._configs[id];

    Swal.fire({
      title: `Modifica pagamento`,
      width: '80%',
      html: '<div class="overflow-hidden"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>',
      showCloseButton: false,
      showConfirmButton: false,
      didOpen: () => {
        const html = '<div id="form"></div>';
        $(Swal.getPopup()).children('.swal2-html-container').html(html);
        Gateways.getTenantsSchema(this.gatewayUrl + '/configs/schema').then((result) => {
          Formio.createForm(document.getElementById('form'), result, {
            noAlerts: true,
            buttonSettings: {showCancel: false},
          })
            .then(function (form) {
              form.submission = {
                data: data
              };
              form.nosubmit = true;
              form.on('submit', function (submission) {
                // Creo la configurazione
                PaymentConfig.auth.execAuthenticatedCall((token) => {
                  const authConfig = {
                    headers: {
                      Authorization: `Bearer ${token}`
                    }
                  };
                  axios.put(PaymentConfig.gatewayUrl + '/configs/' + id, submission.data, authConfig)
                    .then(function (response) {
                      Swal.fire({
                        title: `Pagamento modificato correttamente`,
                        icon: "success",
                        showConfirmButton: true
                      });
                    })
                    .catch(function (error) {
                      console.log(error);
                    });
                })
              });
            })

        }).catch(err => {
          if (err.status === 404) {
            $('form').html(`<div>${Translator.trans('iscrizioni.no_payments_config', {}, 'messages', this.language)}</div>`)
          } else {
            $('form').html('<div>' + err.statusText + '</div>')
          }
        })
      },
    })
  }

  static handleDeletePayment(e) {
    const target = $(e.target);
    const parent = $(target.parents('.payment-config')[0]);
    const id = parent.data('id');

    PaymentConfig.auth.execAuthenticatedCall((token) => {
      const authConfig = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      };
      axios.delete(PaymentConfig.gatewayUrl + '/configs/' + id, authConfig)
        .then(function (response) {

          PaymentConfig.updatePaymentConfigsCounter(PaymentConfig.paymentConfigIds[id].phase)

          delete (PaymentConfig.paymentConfigIds[id]);
          PaymentConfig._configs.splice(id, 1);
          PaymentConfig.updateServicePaymentConfigs()
          parent.remove()
          Swal.fire({
            title: `Pagamento eliminato correttamente`,
            icon: "success",
            showConfirmButton: true
          });
        })
        .catch(function (error) {
          console.log(error);
        });
    })
  }

  static loadPaymentConfigs() {
    $.each(this.paymentConfigIds, (index, item) => {
      PaymentConfig.auth.execAuthenticatedCall((token) => {
        const authConfig = {
          headers: {
            Authorization: `Bearer ${token}`
          }
        };
        axios.get(this.gatewayUrl + '/configs/' + item.id, authConfig)
          .then((response) => {
            this._configs[response.data.id] = response.data;
            this.renderPaymentConfig(response.data, item.phase);
          })
          .catch((error) => {
            console.error(error);
            Swal.fire('Error', 'Failed to load payment config', 'error');
          });
      })
    })
  }

  static renderPaymentConfig(config, phase) {
    this.removeNoPaymentConfigsAlert(phase);
    let paymentTitle = config.reason || (config.items && config.items[0] && config.items[0].reason);
    if (!paymentTitle) {
      paymentTitle = 'Pagamento';
    }

    let paymentStatus = config.active ? '<span class="badge badge-success">Abilitato</span>' : '<span class="badge badge-success">Disabilitato</span>';
    // Todo: Calcolare l'importo, gestire gli importi gestiti da Mypay (multiple items) o degli altri proxy (Single item)
    let paymentAmount = '';

    $('#' + phase + '-payment-configs-table').append(
      `<tr class="payment-config" data-id="${config.id}" data-config="${config}">
        <td class="w-75 pl-2">
            <a href="#" class="text-decoration-none edit-payment small">${paymentTitle}</a>
        </td>
        <td>${paymentStatus}</td>
        <td><span class="small text-primary">${paymentAmount}</span></td>
        <td class="text-right py-2">
          <a href="#" class="text-secondary text-decoration-none delete-payment small"><i class="fa fa-trash-o"></i> ${Translator.trans('elimina', {}, 'messages', this.language)}</a>
        </td>
      </tr>`
    )
  }

  static removeNoPaymentConfigsAlert(phase) {
    $('#' + phase + '-payment-configs-table').find('tr.no-payment-configs').remove();
  }

  static updatePaymentConfigsCounter(phase) {
    let count = $('#' + phase + '-payment-configs-table').find('payment-config').length;
    $('#' + phase + '-payment-configs-counter').text(count)
  }

  static updateServicePaymentGateway()
  {
    PaymentConfig.serviceApiUtils.patch(PaymentConfig.serviceID, {
      "payment_gateway_identifier": this.currentGatewayValue
    });
  }

  static updateServicePaymentConfigs()
  {
    let data = [];
    Object.entries(this.paymentConfigIds).forEach(entry => {
      const [key, value] = entry;
      data.push(value)
    });
    PaymentConfig.serviceApiUtils.patch(PaymentConfig.serviceID, {
      "payment_configs": data
    });
  }


}

export default PaymentConfig;
