import Auth from "../auth/Auth";
import axios from "axios";
import 'formiojs/dist/formio.form.min.css';
import {Formio} from "formiojs";
import Swal from 'sweetalert2/src/sweetalert2.js'

class Gateways {

  $token; // Auth token
  $language; // Browser language
  $alertError; // Html element alert errors
  $tenant; // Tenant Slug
  $apiService; // API Class

  static init() {

    // Init value variables
    Gateways.$language = document.documentElement.lang.toString();
    Gateways.$alertError = $('.save-alert');
    Gateways.$apiService = new Auth();

    // Get tenant slug
    Gateways.$tenant = window.location.pathname.split('/')[1];

    // Get Auth token
    Gateways.$apiService.getSessionAuthTokenPromise().then((data) => {
      Gateways.$token = data.token;

      Gateways.hideSubmitTabPayments()
      Gateways.createPopulateExternalPayChoice()
      // Mypay Payment gateways show form
      Gateways.detectChangeLegacyGateway();
      Gateways.handleActionButton()
    })

  }

  static hideSubmitTabPayments() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      if (e.target.id === 'payments-tab') {
        $('#ente_save_container').hide()
      } else {
        $('#ente_save_container').show()
      }
    })
  }


  //MyPay
  static detectChangeLegacyGateway() {
    const myPayElement = $('input[type="checkbox"][value="mypay"]')

    if (myPayElement.is(':checked')) {
      $(`[id$="_${myPayElement.val()}"]`).removeClass('d-none');
      $(`[id$="heading-${myPayElement.val()}"]`).removeClass('d-none');
    } else {
      $(`[id$="_${myPayElement.val()}"]`).addClass('d-none');
      $(`[id$="heading-${myPayElement.val()}"]`).addClass('d-none');
    }
  }


  // Enable or Disable hidden checkbox gateways on click Enable/Disable button
  static handleActionButton() {
    $('button[data-parent*="gateways_"]').click(function (e) {
      const parentId = $(this).data('parent');
      let input = $(`input[id$="${parentId}"]`)
      const checked = !input.is(':checked')
      input.prop("checked", checked);
      input.attr('checked', checked);
      if (input.val() === 'mypay') {
        if (checked) {
          $(`[id$="_${input.val()}"]`).removeClass('d-none');
        } else {
          $(`[id$="_${input.val()}"]`).addClass('d-none');
        }
      }

      if (input.val() !== 'mypay' && input.val() !== 'bollo') {
        $("form").first().trigger("submit");
      }
    })
  }


  static handleDeleteExternalPayChoice(identifier, url) {
    $(`button[data-identifier="ente_gateways_${identifier}"]`).click(function () {
      const id = $(this).data('identifier');
      if (id) {
        Gateways.deleteTenantId(url).then(() => {
          $("form").first().trigger("submit");
        }).catch((e) => {
          Swal.fire(
            `${Translator.trans('servizio.error_gateway_config', {}, 'messages', self.$language)}`,
            `${Translator.trans('servizio.error_gateway_config_message', {'identifier': identifier}, 'messages', self.$language)}`,
            'error'
          );
        })
      }
    })
  }

  static handleCreateExternalPayChoice() {
    $("form").first().trigger("submit");
  }

  static createPopulateExternalPayChoice() {
    $('.external-pay-choice').each((i, e) => {
      const gatewayIdentifier = $(e).data('identifier');
      const tenantId = $(e).data('tenant');
      const serviceId = $(e).data('service');
      const servicesCount = $(e).data('services-count');
      const gatewayType = serviceId ? 'services' : 'tenants'
      let isFirstConf = false;
      const url = $(e).data('url') + '/' + gatewayType + '/schema';

      // Sanitized for "." in name
      const sanitizeIdentifier = gatewayIdentifier.replace(/\./g, '');

      const $gatewaySettingsContainer = $('<div id="ente_' + sanitizeIdentifier + '" class="gateway-form-type"></div>');
      let settings = {
        "id": serviceId ? serviceId : tenantId,
        ...serviceId && {tenant_id: tenantId},
      }

      // Verifico che sia presente la configurazione del tenant prima di salvare la configurazione del servizio
      if (serviceId) {
        let tenantUrl = $(e).data('url') + '/tenants/' + tenantId;
        let btn = $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`)
        Gateways.getTenantsSchema(tenantUrl).then((res) => {
          if (!res.active) {
            // La configurazione è attiva sul servizio, ma è stata disattivata a livello di tenant
            Swal.fire(
              `${Translator.trans('servizio.incomplete_gateway_config', {}, 'messages', this.$language)}`,
              `${Translator.trans('servizio.disabled_gateway_tenant', {'identifier': gatewayIdentifier}, 'messages', this.$language)}`,
              'error'
            );
          }
          btn.attr("disabled", false);
        }).catch((e) => {
          // Disabilito il pulsante per l'abilitazione dell'intermediario se non è presente la configurazione dell'ente
          if (!btn.hasClass('inactive')) {
            btn.attr("disabled", true);
          }

          Swal.fire(
            `${Translator.trans('servizio.error_gateway_config', {}, 'messages', this.$language)}`,
            `${Translator.trans('servizio.missing_gateway_tenant', {'identifier': gatewayIdentifier}, 'messages', this.$language)}`,
            'error'
          );
        });
      }

      Gateways.getTenantsSchema(url).then((result) => {
        // Creo l'elemento a cui appendere il form
        $('#card-collapse-' + sanitizeIdentifier).html($gatewaySettingsContainer)
        if (document.getElementById('ente_' + sanitizeIdentifier)) {
          Formio.createForm(document.getElementById('ente_' + sanitizeIdentifier), result, {
            noAlerts: true,
            buttonSettings: {showCancel: false},
          })
            .then(function (form) {

              if (result.data) {
                settings = result.data;
              }
              form.submission = {
                data: settings
              };

              let initForm
              let isReady = false

              form.nosubmit = true;
              form.on('submit', function (submission) {
                const url = $(e).data('url') + '/' + gatewayType + '/' + settings.id;
                const urlPost = $(e).data('url') + '/' + gatewayType
                Gateways.getTenantId(url).then((res) => {

                  if (res) {
                    Gateways.putTenantId(url, JSON.stringify({...submission.data, active: true}))
                      .then(function (response) {
                        form.emit('submitDone', submission)
                        if (isFirstConf || !submission.data.active) {
                          Gateways.handleCreateExternalPayChoice(sanitizeIdentifier)
                        } else {
                          $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).removeAttr("type").attr("type", "button");
                        }
                      }).catch((error) => {
                      form.emit('submitError')
                    });
                  } else {
                    Gateways.postTenants(url, JSON.stringify({...submission.data, active: true}))
                      .then(function (response) {
                        form.emit('submitDone', submission)
                        if (isFirstConf || !submission.data.active) {
                          Gateways.handleCreateExternalPayChoice(sanitizeIdentifier)
                        } else {
                          $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).removeAttr("type").attr("type", "button");
                        }
                      }).catch((error) => {
                      console.error(error)
                      form.emit('submitError')
                    });
                  }
                }).catch(e => {
                  if (e.response.status === 404) {
                    Gateways.postTenants(urlPost, JSON.stringify({...submission.data, active: true}))
                      .then(function (response) {
                        form.emit('submitDone', submission)
                        if (isFirstConf || !submission.data.active) {
                          Gateways.handleCreateExternalPayChoice(sanitizeIdentifier)
                        } else {
                          $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).removeAttr("type").attr("type", "button");
                        }
                      }).catch((error) => {
                      form.emit('submitError')
                    });
                  } else {
                    form.emit('submitError')
                  }
                })
              });
              form.ready.then((event) => {
                const url = $(e).data('url') + '/' + gatewayType + '/' + settings.id;
                Gateways.getTenantId(url).then((res) => {
                  form.submission = {
                    data: res
                  };
                  initForm = {...res, submit: false}
                  isReady = true

                  if (!servicesCount) {
                    // Non ci sono servizi collegati
                    // Prepare button for delete
                    if (res.active) {
                      $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).removeAttr("type").attr("type", "button");
                      Gateways.handleDeleteExternalPayChoice(sanitizeIdentifier, url)
                    }
                  } else {
                    // Non è possibile eliminare a livello di tenant perchè ci sono servizi che utilizzano questa configurazione
                    $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).off('click')
                    $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).click((e) => {
                      Swal.fire(
                        `${Translator.trans('servizio.error_delete_gateway', {}, 'messages', self.language)}`,
                        `${Translator.trans('servizio.error_gateway_linked_services', {'identifier': sanitizeIdentifier}, 'messages', self.language)}`,
                        'error'
                      );
                    })
                  }
                }).catch((error) => {
                  // If first config don't exist
                  initForm = {...form.submission.data}
                  isReady = true
                  if (error.response.status === 404) {
                    isFirstConf = true;
                    $(`button[data-identifier="ente_gateways_${sanitizeIdentifier}"]`).removeAttr("type").attr("type", "submit");
                  }
                });
              });
            })
        }
      }).catch(err => {
        if (err.response.status === 404) {
          $('#card-collapse-' + sanitizeIdentifier).html(`<div>${Translator.trans('iscrizioni.no_payments_config', {}, 'messages', this.$language)}</div>`)
        } else {
          $('#card-collapse-' + sanitizeIdentifier).html('<div>' + err.response.statusText + '</div>')
        }
      })
    })
  }


  static getTenantsSchema(url) {
    return new Promise((resolve, reject) => {
      axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Gateways.$token}`
        }
      })
        .then((res) => {
          resolve(res.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  static getTenantId(url) {
    return new Promise((resolve, reject) => {
      axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Gateways.$token}`
        }
      })
        .then((res) => {
          resolve(res.data)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }


  static putTenantId(url, data) {
    return new Promise((resolve, reject) => {
      axios.put(url, data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Gateways.$token}`
        }
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  static patchTenantId(url, data) {
    return new Promise((resolve, reject) => {
      axios.patch(url, data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Gateways.$token}`
        }
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  static postTenants(url, data) {
    return new Promise((resolve, reject) => {
      axios.post(url, data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Gateways.$token}`
        }
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  static deleteTenantId(url) {
    return new Promise((resolve, reject) => {

      axios.delete(url, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Gateways.$token}`
        }
      }).then((res) => {
        resolve(res)
      }).catch((err) => {
        reject(err)
      })
    })
  }


}

export default Gateways;
