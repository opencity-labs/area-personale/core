import Auth from '../auth/Auth';
import axios from 'axios';


class Payment {

  $token; // Auth token
  $spinner; // Html Element Spinner
  $spinnerContainer; // Html Element Wrapper Spinner
  $callToActionButtons; // Html Element Wrapper Button Call to Actions
  $statusPayment; // Html Element Status payment
  $pollInterval; // Instance poll interval
  $language; // Browser language
  $alertError; // Html element alert errors
  $tenant; // Tenant Slug
  $apiService; // API Class
  paymentType;
  checkoutType;
  checkoutApiUrl;
  callbackUrl;

  static maxpollAttemps = 5;
  static pollAttemps = 1;

  static init() {

    // Init value variables
    Payment.$spinner = $('.progress-spinner');
    Payment.$spinnerContainer = $('.spinner-container');
    Payment.$callToActionButtons = $('.actions-container');
    Payment.$statusPayment = $('.status');
    Payment.$language = document.documentElement.lang.toString();
    Payment.$alertError = $('.alert-error');
    Payment.$apiService = new Auth();
    Payment.paymentType = Payment.$callToActionButtons.data('payment-type');
    Payment.checkoutType = Payment.$callToActionButtons.data('checkout-type');
    Payment.checkoutApiUrl = Payment.$callToActionButtons.data('checkout-api-url');
    Payment.callbackUrl = Payment.$callToActionButtons.data('callback-url');


    // Active spinner animations
    Payment.$spinner.addClass('progress-spinner-active');
    Payment.$statusPayment.html(Translator.trans('payment.creation_pending', {}, 'messages', Payment.$language));

    // Get tenant slug
    Payment.$tenant = window.location.pathname.split('/')[1];
    // Get Auth token
    Payment.$apiService.getSessionAuthTokenPromise().then((data) => {
      Payment.$token = data.token;
      Payment.poolingPayment();
    }).catch(() => {
      Payment.handleErrors(Translator.trans('payment.unauth', {}, 'messages', Payment.$language));
    });
  }

  static updateProgressBar() {
    const progressValue = Math.max(0, Math.min(100, (100 / Payment.maxpollAttemps * Payment.pollAttemps)));

    const progressBar = document.querySelector('.progress-bar');
    const progressLabel = document.querySelector('.progress-bar-label');

    if (progressBar && progressLabel) {
      progressBar.style.width = `${progressValue}%`;
      progressBar.setAttribute('aria-valuenow', progressValue);
      progressLabel.innerHTML = `<span class="sr-only">Progresso </span>${progressValue}%`;
    }
  }

  static isMultiplePayment() {
    return Payment.paymentType === 'multiple';
  }

  static isCheckoutPayment() {
    return Payment.checkoutType === 'checkout';
  }

  static handleErrors(errorMessage) {
    Payment.$spinnerContainer.removeClass('d-none').addClass('d-flex');
    Payment.$callToActionButtons.removeClass('d-flex').addClass('d-none');
    $('.progress-bar-wrapper').addClass('d-none');
    $('.waiting-content').addClass('d-none');
    $('.ellipse').addClass('d-none');
    $('.step-error').find('h5').text(errorMessage)
    $('.step-error').removeClass('d-none');

    /*Payment.$alertError.html(errorMessage);
    Payment.$alertError.removeClass('d-none').addClass('d-block fade show');*/
  }

  static showSecondWaitingStep() {
    $('.waiting-content').addClass('d-none');
    $('.ellipse').addClass('d-none');
    $('.step-2').removeClass('d-none');
  }

  static showThirdWaitingStep() {
    $('.progress-bar-wrapper').addClass('d-none');
    $('.waiting-content').addClass('d-none');
    $('.ellipse').addClass('d-none');
    $('.step-3').removeClass('d-none');
  }

  static handleSwitchStatus(data, self) {
    switch (data.status) {
      case 'CREATION_PENDING':
        Payment.$statusPayment.html(Translator.trans('STATUS_PAYMENT_PENDING', {}, 'messages', Payment.$language));
        Payment.retryPooling(self);
        break;
      case 'PAYMENT_PENDING':
        Payment.hideSpinnerContainer();

        if (Payment.isCheckoutPayment()) {
          Payment.initCheckout([data.id], [data]);
        } else {
          const onlinePaymentUrl = data.links.online_payment_begin.url;
          const offlinePaymentUrl = data.links.offline_payment.url;

          if (onlinePaymentUrl) {
            $('#sezione-online').addClass('d-block');
            $('.online_payment_begin').attr('href', onlinePaymentUrl);
            if (!offlinePaymentUrl) {
              $('.card-pay-offline').addClass('d-none');
            }
          } else {
            $('#sezione-online, .online_payment_begin').addClass('d-none');
          }

          if (offlinePaymentUrl) {
            $('#sezione-offline').addClass('d-block');
            $('.offline_payment').attr('href', offlinePaymentUrl);
            if (!onlinePaymentUrl) {
              $('.card-pay-online').removeClass('col-md-6');
            }
          } else {
            $('#sezione-offline, .offline_payment').addClass('d-none');
          }
        }

        Payment.$callToActionButtons.removeClass('d-none').addClass('d-flex');
        break;
      case 'CREATION_FAILED':
        Payment.hideSpinnerContainer();
        Payment.handleErrors(Translator.trans('payment.creation_failed_text', {}, 'messages', Payment.$language));
        break;
      case 'PAYMENT_STARTED':
        Payment.hideSpinnerContainer();
        Payment.$statusPayment.html(Translator.trans('payment.payment_started', {}, 'messages', Payment.$language));
        break;
      default:
        console.log(`Status not found - ${data.status}.`);
        break;
    }
  }

  static hideSpinnerContainer() {
    Payment.$spinnerContainer.removeClass('d-flex').addClass('d-none');
  }

  static handlePayments(paymentsData, self) {
    const CreationPendingException = {};
    const CreationFailedException = {};

    let paymentNotices = [];
    try {
      paymentsData.data.forEach(payment => {
        if (payment.status === 'CREATION_PENDING') {
          throw CreationPendingException;
        }

        if (payment.status === 'CREATION_FAILED') {
          throw CreationPendingException;
        }
        paymentNotices.push(payment.id);
      });

      Payment.initCheckout(paymentNotices, paymentsData.data);

    } catch (e) {
      if (e === CreationPendingException) {
        Payment.$statusPayment.html(Translator.trans('STATUS_PAYMENT_PENDING', {}, 'messages', Payment.$language));
        Payment.retryPooling(self);
      }

      if (e === CreationFailedException) {
        Payment.hideSpinnerContainer();
        Payment.handleErrors(Translator.trans('payment.creation_failed_text', {}, 'messages', Payment.$language));
      }
      console.error(e);
    }
  }

  static initCheckout(paymentNotices, payments) {

    const payload = {
      'payment_notices': paymentNotices,
      'return_urls': {
        'return_ok_url': Payment.callbackUrl + '?esito=OK',
        'return_cancel_url': window.location.href,
        'return_error_url': Payment.callbackUrl + '?esito=KO'
      }
    };

    axios.post(Payment.checkoutApiUrl, payload)
      .then((response) => {

        const onlinePaymentUrl = response.data.location;
        $('.online_payment_begin').attr('href', onlinePaymentUrl);
        //$('.card-pay-online').removeClass('col-md-6');
        //$('.card-pay-offline').addClass('d-none');
        Payment.createDropdown(payments)
        Payment.hideSpinnerContainer();
        Payment.$callToActionButtons.removeClass('d-none').addClass('d-flex');

      })
      .catch(function (error) {
        console.error(error);
        Payment.handleErrors(Translator.trans('payment.creation_failed_text', {}, 'messages', Payment.$language));
      })
    ;
  }

  static createDropdown(payments) {

    // Create the dropdown structure
    const dropdown = document.createElement('div');
    dropdown.className = 'btn-group w-100';

    // Create the button
    const button = document.createElement('button');
    button.className = 'btn btn-primary dropdown-toggle';
    button.type = 'button';
    button.setAttribute('data-toggle', 'dropdown');
    button.setAttribute('aria-haspopup', 'true');
    button.setAttribute('aria-expanded', 'false');
    button.innerHTML = `
        ${Translator.trans('pratica.payment.paga_offline', {}, 'messages', Payment.$language)}
        <svg class="icon-expand icon icon-sm icon-light">
          <use href="/bootstrap-italia/dist/svg/sprite.svg#it-expand"></use>
        </svg>
      `;

    // Create the dropdown menu
    const dropdownMenu = document.createElement('div');
    dropdownMenu.className = 'dropdown-menu w-100';

    // Create the link list wrapper
    const linkListWrapper = document.createElement('div');
    linkListWrapper.className = 'link-list-wrapper';

    // Create the link list
    const linkList = document.createElement('ul');
    linkList.className = 'link-list';

    // Populate the link list with items from the URLs array
    payments.forEach((payment) => {
      const listItem = document.createElement('li');
      const link = document.createElement('a');
      link.className = 'list-item';
      link.target = '_blank';
      link.href = payment.links.offline_payment.url;
      link.innerHTML = `<span>${payment.reason ?? Translator.trans('gateway.mypay.download_button', {}, 'messages', Payment.$language)}</span>`;
      listItem.appendChild(link);
      linkList.appendChild(listItem);
    });

    // Assemble the dropdown
    linkListWrapper.appendChild(linkList);
    dropdownMenu.appendChild(linkListWrapper);
    dropdown.appendChild(button);
    dropdown.appendChild(dropdownMenu);

    // Append the dropdown to the container
    $('.card-pay-offline .card-body').append(dropdown);
    $('.offline_payment').addClass('d-none');
    Payment.hideSpinnerContainer();
    Payment.$callToActionButtons.removeClass('d-none').addClass('d-flex');
  }

  static retryPooling(self) {
    Payment.pollAttemps++;
    if (Payment.pollAttemps <= self.retryLimit) {

      if (Payment.pollAttemps >= 3) {
        Payment.showSecondWaitingStep();
      }

      const timeout = self.retryTimeout * Payment.pollAttemps;
      setTimeout(() => {
        $.ajax(self);
      }, timeout);
    } else {
      // if I exceed the timeout I show a message
      Payment.showThirdWaitingStep();
    }
  }

  static poolingPayment() {

    function poolingAjaxRequest() {
      $.ajax({
        url: Payment.$callToActionButtons.data('api'),
        dataType: 'json',
        type: 'get',
        // timeout: 100, // enable for simulate timeout
        //tryCount: 0,
        retryLimit: Payment.maxpollAttemps,
        retryTimeout: 2000,
        limitTimeout: 60000, // 1 minutes - 30 retry
        beforeSend: function (xhr) {
          Payment.updateProgressBar();
          xhr.setRequestHeader('Authorization', `Bearer ${Payment.$token}`);
        },
        success: function (data) {
          console.log(data, this);
          // Il nuovo sistema di pagamenti potrebbe ritornare un array
          if (Payment.isMultiplePayment()) {
            Payment.handlePayments(data, this);
          } else {
            Payment.handleSwitchStatus(data, this);
          }
        },
        error: function (xmlhttprequest, textstatus, message) { // error logging
          if (textstatus === 'timeout') {
            Payment.handleErrors(Translator.trans('payment.timeout', {}, 'messages', Payment.$language));
          } else if (xmlhttprequest.status === 401) {
            Payment.handleErrors(Translator.trans('payment.timeout', {}, 'messages', Payment.$language));
          } else if (xmlhttprequest.status === 404) {
            Payment.retryPooling(this);
            Payment.$statusPayment.html(Translator.trans('payment.not_found', {}, 'messages', Payment.$language));
          } else {
            Payment.handleErrors(Translator.trans('payment.creation_failed_text', {}, 'messages', Payment.$language));
          }
        }
      });
    }

    // Call pooling request
    poolingAjaxRequest();
  }

}

export default Payment;
