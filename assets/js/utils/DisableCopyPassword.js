window.addEventListener('copy', function(e) {
  if (e.target.className.includes('no-copy') || e.target.type === 'password') {
    e.preventDefault();
  }
});
window.addEventListener('drag', function(e) {
  if (e.target.className.includes('no-copy') || e.target.type === 'password') {
    e.preventDefault();
  }
});
window.addEventListener('drop', function(e) {
  if (e.target.className.includes('no-copy') || e.target.type === 'password') {
    e.preventDefault();
  }
});
window.addEventListener('cut', function(e) {
  if (e.target.className.includes('no-copy') || e.target.type === 'password') {
    e.preventDefault();
  }
});
