import {
  getStatus,
  deleteDraftModal,
  getCookie,
  setCookie,
  exportCalendarEventsToICS,
  enableDisableAllInput,
  isMeetingOutdated,
  printCalendar,
  disableBtnOnClick,
  enableBtn,
} from "./fullcalendar-common";
import { Calendar } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import listPlugin from "@fullcalendar/list";
import interactionPlugin from "@fullcalendar/interaction";
import bootstrapPlugin from "@fullcalendar/bootstrap";
import itLocale from "@fullcalendar/core/locales/it";
import enLocale from "@fullcalendar/core/locales/en-gb";
import deLocale from "@fullcalendar/core/locales/de";

require("@fullcalendar/core/main.min.css");
require("@fullcalendar/daygrid/main.min.css");
require("@fullcalendar/timegrid/main.min.css");
require("@fullcalendar/list/main.min.css");
require("@fullcalendar/bootstrap/main.min.css");

import "../styles/components/_calendars.scss";

const language = document.documentElement.lang.toString();

$(document).ready(function () {

  // Hide all buttons
  $("#edit_alert").hide();
  $("#no_slots_edit_alert").hide();
  $("#no_slots_new_alert").hide();
  $("#modalApprove").hide();
  $("#modalRefuse").hide();
  $("#modalMissed").hide();
  $("#modalComplete").hide();
  $("#modalStatusHelper").hide();
  $("#modalSlot").click(function () {
    $("#edit_alert").show();
  });

  var view_cookie = getCookie("d_view_type");
  var date_cookie = getCookie("d_date_view");

  // Fullcalendar initialization
  const language = document.documentElement.lang.toString();
  const calendarEl = document.getElementById("fullcalendar");
  const calendarID = $("#hidden").attr("data-calendar");
  var calendar = new Calendar(calendarEl, {
    plugins: [
      dayGridPlugin,
      timeGridPlugin,
      listPlugin,
      interactionPlugin,
      bootstrapPlugin,
    ],
    themeSystem: "bootstrap",
    locale: language,
    locales: [itLocale, enLocale, deLocale],
    timeZone: "Europe/Rome",
    nowIndicator: true,
    eventColor: "#3478BD",
    events: calendarID + '/fullcalendar',
    allDaySlot: false,
    defaultView: view_cookie
      ? view_cookie
      : $(window).width() < 765
        ? "timeGridDay"
        : "dayGridMonth",
    defaultDate: date_cookie ? new Date(date_cookie) : new Date(),
    header: {
      left: "prev,today,next",
      center: "title",
      right:
        "dayGridMonth,timeGridWeek,timeGridDay,listMonth,listWeek,listDay",
    },
    selectable: true,
    slotDuration: "00:05:00",
    contentHeight: 600,
    minTime: JSON.parse($("#hidden").attr("data-range-time-event")).min,
    maxTime: JSON.parse($("#hidden").attr("data-range-time-event")).max,
    select(selectionInfo) {
      newModal(selectionInfo);
    },
    selectAllow: function (selectInfo) {
      var someEvents = calendar.getEvents().filter(function (evt) {
        return (
          evt.start <= selectInfo.start &&
          evt.end >= selectInfo.end &&
          evt.resourceId === selectInfo.resourceId &&
          evt.title === "Apertura"
        );
      });
      return someEvents.length > 0;
    },
    eventRender: function (info) {
      if (info.event.extendedProps.status === 0) {

        const dotEl = info.el.getElementsByClassName("fc-event-dot")[0];
        if (dotEl) {
          dotEl.style.backgroundColor = "var(--primary)";
        }
      }
      const textEl = info.el.getElementsByClassName("fc-list-item-title")[0];
      if (textEl && info.event.extendedProps.description) {
        textEl.append(`: ${info.event.extendedProps.description}`);
      } else if (textEl) {
        textEl.append(` Occupato`);
      }
      if (info.event.title === "OpeningDay") {
        if (info.view.type !== "dayGridMonth") return false;
      }
    },
    editable: true,
    eventDurationEditable: true,
    eventAllow: function (dropInfo, draggedEvent) {
      var someEvents = calendar.getEvents().filter(function (evt) {
        return (
          evt.start <= dropInfo.start &&
          evt.end >= dropInfo.end &&
          evt.resourceId === dropInfo.resourceId
        );
      });
      return someEvents.length > 0;
    },
    eventDrop: function (info) {
      if (
        !info.event.extendedProps.uid &&
        info.event.extendedProps.status !== 6
      ) {
        compileModal(info);
        $("#edit_alert").show();
      } else {
        info.revert();
      }
    },
    eventResize: function (info) {
      if (
        !info.event.extendedProps.uid &&
        info.event.extendedProps.status !== 6
      ) {
        compileModal(info);
        $("#edit_alert").show();
      } else {
        info.revert();
      }
    },
    eventClick: function (info) {
      setCookie("d_view_type", info.view.type);
      if (info.event.extendedProps.status === 6) {
        deleteDraftModal(info);
      } else if (info.event.id) compileModal(info);
    },
    dateClick: function (info) {
      setCookie("d_date_view", info.dateStr);
      if (info.view.type === "dayGridMonth") {
        this.changeView("timeGridDay", info.dateStr);
        setCookie("d_view_type", info.view.type);
      }
    },
    datesRender: function (info) {
      date_cookie
        ? setCookie("d_date_view", new Date(date_cookie))
        : setCookie("d_date_view", new Date());
    },
  });

  calendar.render();

  $(".fc-listDay-button, .fc-listWeek-button").hide();

  $("#print").on("click", () => {
    printCalendar()
  });

  $('#export-ics').on("click", function () {
    exportCalendarEventsToICS(calendar);
  })
});

$(document).on('click', '.fc-dayGridMonth-button, .fc-listMonth-button', function() {
  $(".fc-listDay-button, .fc-listWeek-button").hide();
  $(".fc-listMonth-button").show();
});

$(document).on('click', '.fc-timeGridWeek-button, .fc-listWeek-button', function() {
  $(".fc-listDay-button, .fc-listMonth-button").hide();
  $(".fc-listWeek-button").show();
});

$(document).on('click', '.fc-timeGridDay-button, .fc-listDay-button', function() {
  $(".fc-listWeek-button, .fc-listMonth-button").hide();
  $(".fc-listDay-button").show();
});

$(document).on('click', 'td, .fc-bgevent-skeleton, .fc-bgevent, .fc-day-top, .fc-content-skeleton td, td.fc-bgevent.fc-allow-mouse-resize', function(event) {
  setTimeout(() => {
    $(".fc-listWeek-button, .fc-listMonth-button").hide();
    $(".fc-listDay-button").show();
  },50)
});

$(document).on('click', '.fc-next-button, .fc-prev-button, .fc-today-button', function () {
  if ($('.fc-dayGridMonth-button.active').length) {
    $(".fc-listDay-button, .fc-listWeek-button").hide();
    $(".fc-listMonth-button").show();
  } else if ($('.fc-timeGridWeek-button.active').length) {
    $(".fc-listDay-button, .fc-listMonth-button").hide();
    $(".fc-listWeek-button").show();
  } else if ($('.fc-timeGridDay-button.active')) {
    $(".fc-listWeek-button, .fc-listMonth-button").hide();
    $(".fc-listDay-button").show();
  }
});

/**
 * Fills modal data
 * @param info event
 */
function compileModal(info) {
  $("#edit_alert").hide();
  $("#no_slots_edit_alert").hide();
  $("#modalApprove").hide();
  $("#modalRefuse").hide();
  $("#modalMissed").hide();
  $("#modalCancel").hide();
  $("#modalComplete").hide();
  $("#modalStatusHelper").hide();
  $("#modalReschedule").hide();

  let date = new Date(info.event.start).toISOString().slice(0, 10);
  let start = new Date(info.event.start).toISOString().slice(11, 16);
  let end = new Date(info.event.end).toISOString().slice(11, 16);
  let title = `[${getStatus(info.event.extendedProps.status)}] ${info.event.extendedProps.name || Translator.trans("meetings.modal.no_name", {}, "messages", language)}`
  if (info.event.extendedProps.code) {
    title += ` (${info.event.extendedProps.code})`;
  }

  //Set cookie
  setCookie("d_date_view", new Date(info.event.start));
  setCookie("d_view_type", info.view.type);

  // Populate modal
  $("#modalId").html(info.event.id);
  $("#modalName").val(info.event.extendedProps.name);
  $("#modalDate").val(date);
  $("#modalStart").val(start);
  $("#modalEnd").val(end);
  $("#modalOpeningHour").val(info.event.extendedProps.opening_hour);
  $("#modalTitle").html(title);
  $("#modalReason").val(info.event.extendedProps.reason);
  $("#modalDescription").val(info.event.extendedProps.description);
  $("#modalLocation").html(info.event.extendedProps.location);
  $("#modalMotivationOutcome").val(info.event.extendedProps.motivation_outcome);
  $("#modalVideoconferenceLink").val(
    info.event.extendedProps.videoconferenceLink
  );
  $("#modalPhone").val(info.event.extendedProps.phoneNumber);
  $("#modalEmail").val(info.event.extendedProps.email);
  $("#modalStatus").html(info.event.extendedProps.status);

  $("#modalLocale").val(info.event.extendedProps.locale)
  $('#modalLocale option').each(function() {
    if($(this).val() == info.event.extendedProps.locale) {
      $(this).attr('selected','selected');
    }
  });

  if (info.event.extendedProps.rescheduled === 1) {
    $("#modalRescheduleText").html(
      `${Translator.trans(
        "meetings.error.one_moved",
        {},
        "messages",
        language
      )}`
    );
    $("#modalReschedule").show();
  } else if (info.event.extendedProps.rescheduled !== 0) {
    $("#modalRescheduleText").html(
      `${Translator.trans("meetings.error.moved", {}, "messages", language)} ${
        info.event.extendedProps.rescheduled
      } ${Translator.trans("meetings.error.times", {}, "messages", language)}`
    );
    $("#modalReschedule").show();
  }
  const meetingEnd = info.event.end;
  const isOutdated = isMeetingOutdated(meetingEnd);

  enableDisableAllInput(isOutdated)

  const meetingStatus = $("#modalStatus").html();
  switch (meetingStatus) {
    case "0": // Attesa

      if (isOutdated) {
        $("#modalComplete").show();
        $("#modalMissed").show();
      } else {
        $("#modalApprove").show();
        $("#modalRefuse").show();
      }

      break;

    case "1": // Confermato

      $("#modalComplete").show();
      $("#modalMissed").show();
      if (!isOutdated) {
        $("#modalCancel").show();
      }
      $("#modalStatusHelper").show();

      break;

    case "2": // Rifiutato

      $("#modalApprove").show();
      $("#modalRefuse").show();

      break;

    case "3": // Assente

      $("#modalComplete").show();
      $("#modalMissed").show();
      $("#modalMissed").prop("disabled", true);

      break;

    case "4": // Concluso

      $("#modalComplete").show();
      $("#modalComplete").prop("disabled", true);
      $("#modalMissed").show();

      break;
  }

  $("#modalError").html("");
  $("#modalCenter").modal("show");
  $("#no_slots_edit_alert").hide();

  $("#modalClose").click(info.revert);
}

/**
 * Fills modal data
 * @param info event
 */
function newModal(info) {
  let date = new Date(info.start).toISOString().slice(0, 10);
  let start = new Date(info.start).toISOString().slice(11, 16);
  let end = new Date(info.end).toISOString().slice(11, 16);

  //Set cookie
  setCookie("d_date_view", new Date(info.start));
  setCookie("d_view_type", info.view.type);

  $("#modalNewDate").val(date);
  $("#modalNewStart").val(start);
  $("#modalNewEnd").val(end);
  $("#modalNewStatus").html(1);

  $("#modalNew").modal("show");
}

function parseMeetingsApiResponse(responseJSON) {

  if (responseJSON.type && responseJSON.type === "validation_error") {

    return responseJSON.errors[0]
  }

  return responseJSON.description
    ? responseJSON.description
    : `${Translator.trans("meetings.error.save_slot", {}, "messages", language)}`;
}

function prepareModalErrorMessage(errorText) {
  return `<span><span class="badge badge-danger mr-2">${Translator.trans("status_error", {}, "messages", language)}</span>${errorText}</span>`;
}

$(".modal-edit").on("click", function editMeeting() {
  if (!confirm($(this).data("confirm"))) {
    return;
  }
  let status = $(this).attr("data-status")
    ? $(this).attr("data-status")
    : $("#modalStatus").html();

  let date = $("#modalDate").val();
  let start = $("#modalStart").val();
  let end = $("#modalEnd").val();
  let id = $("#modalId").html();

  if (!start || !end) {
    const modalError = prepareModalErrorMessage(
      Translator.trans("meetings.error.slot_hours_invalid", {}, "messages", language)
    )
    return $("#modalError").html(modalError);
  }

  setCookie("d_date_view", new Date(date));

  // disabilito il pulsante per evitare chiamate parallele
  let btn = this;
  disableBtnOnClick(btn);

  let data = {
    status: status,
    from_time: new Date(`${date}T${start}`).toISOString(),
    to_time: new Date(`${date}T${end}`).toISOString(),
    name: $("#modalName").val(),
    email: $("#modalEmail").val(),
    phone_number: $("#modalPhone").val(),
    reason: $("#modalReason").val(),
    user_message: $("#modalDescription").val(),
    motivation_outcome: $("#modalMotivationOutcome").val(),
    videoconference_link: $("#modalVideoconferenceLink").val(),
    opening_hour: $("#modalOpeningHour").val(),
    locale: $("#modalLocale").val()
  };
  let url = $(this).attr("data-url");
  let token = $("#hidden").attr("data-token");
  url = url.replace("meeting_id", id);
  $.ajax({
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    url: url,
    type: "PATCH",
    data: JSON.stringify(data),
    success: function (response, textStatus, jqXhr) {
      location.reload();
    },
    error: function (jqXHR, textStatus, errorThrown) {

      const error = parseMeetingsApiResponse(jqXHR.responseJSON)
      const errorMessage = prepareModalErrorMessage(error)
      $("#modalError").html(errorMessage);

      // Riabilito il pulsante in caso di errore
      enableBtn(btn);
    },
  });
});

$(".modal-delete").on("click", function () {
  // disabilito il pulsante per evitare chiamate parallele
  let btn = this;
  disableBtnOnClick(btn);

  let id = $("#modalDraftId").html();
  let url = $(this).attr("data-url");
  let token = $("#hidden").attr("data-token");
  url = url.replace("meeting_id", id);
  $.ajax({
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    url: url,
    type: "DELETE",
    success: function (response, textStatus, jqXhr) {
      location.reload();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      const error = parseMeetingsApiResponse(jqXHR.responseJSON)
      const errorMessage = prepareModalErrorMessage(error)
      $("#modalDraftError").html(errorMessage);

      // Riabilito il pulsante in caso di errore
      enableBtn(btn);
    },
  });
});

$(".modal-create").on("click", function () {
  let calendar = $(this).attr("data-calendar");
  let date = $("#modalNewDate").val();
  let start = $("#modalNewStart").val();
  let end = $("#modalNewEnd").val();
  const email = $("#modalNewEmail").val().trim()

  if (!start || !end) {
    const errorMessage = prepareModalErrorMessage(
      Translator.trans("meetings.error.slot_hours_invalid", {}, "messages", language)
    )
    return $("#modalNewError").html(errorMessage);
  }

  if (!email) {
    const modalError = prepareModalErrorMessage(
      Translator.trans("meetings.error.email_is_required", {}, "messages", language)
    )
    return $("#modalNewError").html(modalError)
  }

  setCookie("d_date_view", new Date(date));

  // disabilito il pulsante per evitare chiamate parallele
  let btn = this;
  disableBtnOnClick(btn);

  let data = {
    status: 1,
    from_time: new Date(`${date}T${start}`).toISOString(),
    to_time: new Date(`${date}T${end}`).toISOString(),
    reason: $("#modalNewReason").val(),
    user_message: $("#modalNewDescription").val(),
    motivation_outcome: $("#modalNewMotivationOutcome").val(),
    fiscal_code: $("#modalNewFiscalCode").val(),
    videoconference_link: $("#modalNewVideoconferenceLink").val(),
    email: $("#modalNewEmail").val(),
    name: $("#modalNewName").val(),
    phone_number: $("#modalNewPhone").val(),
    calendar: calendar,
    opening_hour: $("#modalNewOpeningHour").val(),
    locale: $('#modalNewLocale').val()
  };

  let url = $(this).attr("data-url");
  let token = $("#hidden").attr("data-token");
  $.ajax({
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    url: url,
    type: "POST",
    data: JSON.stringify(data),
    success: function (response, textStatus, jqXhr) {
      location.reload();
    },
    error: function (jqXHR, textStatus, errorThrown) {

      const error = parseMeetingsApiResponse(jqXHR.responseJSON)
      let errorMessage = prepareModalErrorMessage(error)

      $("#modalNewError").html(errorMessage);

      // Riabilito il pulsante in caso di errore
      enableBtn(btn);
    },
  });
});
