import React, {useEffect, useState} from 'react';
import axios from "axios";
import {notify} from "design-react-kit";
import {useLanguage} from "../contexts/LanguageContext";
import {useAuth} from "../contexts/AuthContext";
import BasePath from "../../utils/BasePath";

export default function useCheckout({payment,executeOnMount = false}) {
  const [apiUrl] = useState(()=>new BasePath().getBasePath())
  const element = document.getElementById('payments').getAttribute('data-checkout-api-url');
  const  {
    userLanguage: lang
  } = useLanguage()

  const  {
    authUser,
  } = useAuth()


  const config = {
    headers: {
      Authorization: `Bearer ${authUser?.token}`,
    }
  };
  const data = {
    "payment_notices": [payment.id],
    "return_urls": {
      "return_ok_url": `${apiUrl}/${lang}/user/payments/${payment.id}`,
      'return_cancel_url': `${apiUrl}/${lang}/user/payments/${payment.id}`,
      'return_error_url':`${apiUrl}/${lang}/user/payments/${payment.id}`
    }
  }

  const handleData = () => {
    axios.post(`${element}`,data, config)
      .then(response => {
        window.open(response.data.location, '_blank', 'noopener,noreferrer');
      }).catch((e) => {
      notify(
        Translator.trans('payment.warning', {}, 'messages', lang),
        <p>
          {Translator.trans('pratica.payment_error', {}, 'messages', lang)} {' '}  {Translator.trans('general.retry_after', {}, 'messages', lang)}
        </p>,{
          dismissable: true,
          state: 'error',
          duration: 6000
        }
      )
    })
  }

  useEffect(() => {
    if (executeOnMount) {
      handleData();
    }
  }, []);


  return {execute: handleData};
}
