import React, {useId, useState} from 'react';
import pagopa from "../../../images/pagopa.svg";
import {useLanguage} from "../contexts/LanguageContext";
import {ifPaymentToPay} from "../../utils/PaymentStatus";
import axios from "axios";
import {Button, NotificationManager, notify} from "design-react-kit";
import BasePath from "../../utils/BasePath";
import {useAuth} from "../contexts/AuthContext";
import useCheckout from "../hooks/useCheckout";

function PaymentButtons({payment}) {

  const {execute} = useCheckout({payment})

  const  {
    userLanguage: lang
  } = useLanguage()


  const handleCheckout = () =>{
    execute()
  }

    return (
        <div className="cmp-text-button mb-40 mb-lg-80 mt-lg-40 d-flex">
            {ifPaymentToPay(payment?.status) && payment?.links?.online_payment_begin?.url !== null && payment?.links?.online_payment_begin?.url !== ''  ?
                (  <div className="button-wrapper mr-3">
                  <Button size={"md"} type={"button"} color={"primary"} onClick={()=> handleCheckout()} className="btn btn-icon btn-re  btn-primary mb-2 justify-content-center w-auto" role="button" aria-pressed="true">
                        <img src={pagopa} alt="paga con pagoPA" className="me-2 icon"/>
                        <span className="text-capitalize">{Translator.trans('payment.payment_pagopa', {}, 'messages', lang)}</span>
                  </Button>
                </div>) :null}
            {ifPaymentToPay(payment?.status) && payment?.links?.offline_payment?.url !== null && payment?.links?.offline_payment?.url !== '' ?
                (  <div className="button-wrapper mr-3">
                    <a target="_blank" href={payment?.links?.offline_payment?.url} className="btn btn-outline-primary " role="button" aria-pressed="true" download> {Translator.trans('payment.paga_offline', {}, 'messages', lang)}</a></div>) : null}

            {payment?.status === 'COMPLETE' && payment?.links?.receipt.url !== null ? (
                <div className="button-wrapper mr-3">
                    <a aria-label="Scarica la Ricevuta richiesta" download target={'_blank'} href={payment?.links?.receipt.url} className="btn btn-re btn-outline-primary mb-2 justify-content-center" role="button" aria-pressed="true"> {Translator.trans('payment.download_receipt', {}, 'messages', lang)}</a></div>) : null}

        </div>
    )
}

export default PaymentButtons;
