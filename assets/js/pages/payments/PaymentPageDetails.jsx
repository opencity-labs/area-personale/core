import React, {useEffect, useState} from 'react';
import {useParams, useSearchParams} from 'react-router-dom';
import {Spinner, notify,NotificationManager, Icon} from "design-react-kit"
import axios from 'axios';
import {useAuth} from "../contexts/AuthContext";
import {useLanguage} from "../contexts/LanguageContext";
import PaymentNavSteps from "./PaymentNavSteps";
import PaymentButtons from "./PaymentButtons";
import PaymentDetails from "./PaymentDetails";
import BasePath from "../../utils/BasePath";
import Swal from 'sweetalert2/src/sweetalert2.js'


function PaymentPageDetails() {

  const [payment, setPayment] = useState([])
  const [loading, setLoading] = useState(true);
  const { id } = useParams();
  const [searchParams, setSearchParams] = useSearchParams();
  const [apiUrl] = useState(()=>new BasePath().getBasePath())

  const  {
    authUser,
  } = useAuth()

  const  {
    userLanguage: lang
  } = useLanguage()

  useEffect(() => {
    if (
      (searchParams.get('esito') && searchParams.get('esito') === 'KO')
      || (searchParams.get('payment') && searchParams.get('payment') === 'KO')
    ) {
      Swal.fire(
        `${Translator.trans('error_payment_generic', {}, 'messages', lang)}`,
        '',
        'error'
      );
    }
  }, [])

  useEffect( () => {
    if (authUser?.token && id) {
      getPayments()
    }
  }, [authUser?.token]);

  const getPayments = () => {
    const config = {
      headers: {
        Authorization: `Bearer ${authUser?.token}`,
      }
    };
     axios.get(`${apiUrl}/api/payments/${id}`, config)
      .then(list => {
        setPayment(list.data)
        setLoading(false);
    }).catch((e) => {
      notify(
        Translator.trans('payment.warning', {}, 'messages', lang),
        <p>
          {e.message}
        </p>,{
          dismissable: true,
          state: 'error',
          duration: 3000
        }
      )
      setLoading(false);
    })

    setLoading(true);
  }


  return (

    <div> {loading ?
      (<div className="d-flex justify-content-center mb-5 mx-2">
        <Spinner small active  /><span className={'mx-2'}>{Translator.trans('loading', {}, 'messages', lang)}</span>
      </div>)
      : !loading && payment?.status  ? (
       <div>
        <h2 className="title-xxlarge mb-4 mt-40">{Translator.trans('operatori.dati_generali', {}, 'messages', lang)}</h2>
        <div className="cmp-card mb-4">
          <div className="card has-bkg-grey shadow-sm mb-0">
            <div className="card-header border-0 p-0 mb-lg-30">
              <div className="d-flex">
                <h3 className="subtitle-large mb-0">{Translator.trans('pratica.richiedente', {}, 'messages', lang)}</h3>
              </div>
            </div>
            <div className="card-body p-0">
              <div className="cmp-info-summary bg-white mb-4 mb-lg-30 p-3 p-lg-4">
                <div className="card">
                  <div
                    className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                    <h4 className="title-large-semi-bold mb-3">{payment?.payer?.name} {payment?.payer?.family_name}</h4>
                  </div>
                  <div className="card-body p-0">
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{Translator.trans('user.profile.codice_fiscale', {}, 'messages', lang)}</div>
                      <div className="border-light border-0">
                        <p className="data-text">
                          <strong>{payment?.payer?.tax_identification_number}</strong>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer p-0 d-none">
                  </div>
                </div>
              </div>
              <div className="cmp-info-summary bg-white p-3 p-lg-4">
                <div className="card">
                  <div
                    className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                    <h4 className="title-large-semi-bold mb-3">{Translator.trans('user.profile.indirizzo', {}, 'messages', lang)}</h4>
                  </div>
                  <div className="card-body p-0">
                    <div className="single-line-info border-light">
                      <div className="border-light border-0">
                        <p className="data-text">
                          <strong>{payment?.payer?.street_name} {payment?.payer?.building_number ? payment?.payer?.building_number +',' : '--'} {payment?.payer?.postal_code} {payment?.payer?.town_name ? payment?.payer?.town_name+',' : ''} {payment?.payer?.country_subdivision}</strong>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer p-0 d-none">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <h2 className="title-xxlarge mb-4 pt-3 pt-lg-0">{Translator.trans('data_services', {}, 'messages', lang)}</h2>

        <div>
         <PaymentDetails payment={payment}></PaymentDetails>
        <PaymentButtons payment={payment}></PaymentButtons>

       <PaymentNavSteps goBack={`${apiUrl}/${lang}/user/payments/`}></PaymentNavSteps>
        </div>
       <NotificationManager></NotificationManager>
       </div>
      ) : <div className="d-flex justify-content-center mb-5 mx-2">{Translator.trans('iscrizioni.no_pagamento', {}, 'messages', lang)}</div>}
      </div>
  );
}

export default PaymentPageDetails;
