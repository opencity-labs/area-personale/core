import React, {useEffect, useState} from 'react';
import PaymentListItem from "./PaymentListItem";
import {
  Spinner,
  notify,
  NotificationManager,
  Button,
} from "design-react-kit"
import axios from 'axios';
import {useAuth} from "../contexts/AuthContext";
import {useLanguage} from "../contexts/LanguageContext";
import DateRangePickerWrapper from "../../components/DateRangePicker/DateRangePicker";
import {useLocation, useSearchParams} from "react-router-dom";
import moment from "moment/moment";
import BasePath from "../../utils/BasePath";
import {QUERY_STATUS_PAID, QUERY_STATUS_TO_PAY, STATUS_ALL, STATUS_PAID, STATUS_TO_PAY} from "./Main";


function PaymentList() {

  const [paymentList, setPaymentList] = useState([])
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState(null);
  const [query, setQuery] = useState(null);
  const [searchParams] = useSearchParams();
  const [startDateRead, setStartDateRead] = useState(null);
  const [endDateRead, setEndDateRead] = useState(null);

  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const queryStatus = queryParams.get("status");

  const [apiUrl] = useState(()=>new BasePath().getBasePath())


  const {
    authUser
  } = useAuth()

  const  {
    lang
  } = useLanguage()


  const getPayments = () => {
    setLoading(true);
    const config = {
      headers: {
        Authorization: `Bearer ${authUser?.token}`
      }
    };

       axios.get(`${apiUrl}/api/payments/${status === STATUS_PAID ? QUERY_STATUS_PAID : QUERY_STATUS_TO_PAY}?${searchParams.get('service_id') ? `service_id=${searchParams.get('service_id')}` : ''}${query ? `&${query}` : ''}`, config)
      .then(list => {
        list.data.data = list.data.data.filter(payment => payment.status !== 'CREATION_PENDING')

        if(status === STATUS_PAID){
          const sortedDate =  list.data.data.sort(function(a,b){
            return moment(b.payment.paid_at).valueOf() - moment(a.payment.paid_at).valueOf();
          });
          setPaymentList(sortedDate)
        }else if(status === STATUS_TO_PAY){
          const sortedDate =  list.data.data.sort(function(a,b){
            return moment(a.payment.expire_at).valueOf() - moment(b.payment.expire_at).valueOf();
          });
          setPaymentList(sortedDate)
        }else{
          const sortedDate =  list.data.data.sort(function(a,b){
            return moment(b.created_at).valueOf() - moment(a.created_at).valueOf();
          });
          setPaymentList(sortedDate)
        }
        setLoading(false);
      }).catch((e) => {
      if (e.response.status !== 404) {
        notify(
          Translator.trans('payment.warning', {}, 'messages', lang),
          <p>
            {e.message}
          </p>, {
            dismissable: true,
            state: 'error',
            duration: 6000
          }
        )
      }else{
        setPaymentList([])
      }
      setLoading(false);
    })
  }

  useEffect(() => {
    if (authUser?.token && (status || status === null || query)) {
      getPayments()
    }
  }, [query,authUser?.token]);

  useEffect(() => {
    if((queryStatus === QUERY_STATUS_PAID || queryStatus === null || queryStatus === '')){
      setStatus(STATUS_PAID)
    }else if(queryStatus === QUERY_STATUS_TO_PAY){
      setStatus(STATUS_TO_PAY)
    }else{
      setStatus(STATUS_PAID)
     // setStatus(STATUS_ALL)
    }


  }, []);


  useEffect(() => {

    if(status === STATUS_PAID && (queryStatus === QUERY_STATUS_PAID || queryStatus === null || queryStatus === '' || queryStatus)){
      setQuery(`paid_at[after]=${moment().subtract(6, 'months').format('YYYY-MM-DD')}&paid_at[before]=${encodeURIComponent(moment().format("YYYY-MM-DDT23:59:00Z"))}`)
    }else if(status === STATUS_TO_PAY || queryStatus === QUERY_STATUS_TO_PAY){
      setQuery(`expire_at[after]=${moment().subtract(3, 'months').format('YYYY-MM-DD')}&expire_at[before]=${encodeURIComponent(moment().add('3','months').format("YYYY-MM-DDT23:59:00Z"))}`)
    }else{
      setQuery(`created_at[after]=${moment().subtract(6, 'months').format('YYYY-MM-DD')}&created_at[before]=${encodeURIComponent(moment().format("YYYY-MM-DDT23:59:00Z"))}`)
    }
  }, [status]);


  const childToParent = ({ startDate, endDate }) => {

    if(startDate && endDate){
      setStartDateRead((startDate).format('L'))
      setEndDateRead((endDate).format('L'))
      if(status === STATUS_PAID){
        setQuery(`paid_at[after]=${moment(startDate).format('YYYY-MM-DD')}&paid_at[before]=${encodeURIComponent(moment(endDate).format("YYYY-MM-DDT23:59:00Z"))}`)
      }else if(status === STATUS_TO_PAY){
        setQuery(`expire_at[after]=${moment(startDate).format('YYYY-MM-DD')}&expire_at[before]=${encodeURIComponent(moment(endDate).format("YYYY-MM-DDT23:59:00Z"))}`)
      }else{
        setQuery(`created_at[after]=${moment(startDate).format('YYYY-MM-DD')}&created_at[before]=${encodeURIComponent(moment(endDate).format("YYYY-MM-DDT23:59:00Z"))}`)
      }
    }
  }

  const handleStatusPayments = (status) => {
    setStatus(status)
    if(status === STATUS_PAID){
      setQuery(`paid_at[after]=${moment().subtract(6, 'months').format('YYYY-MM-DD')}&paid_at[before]=${encodeURIComponent(moment().format("YYYY-MM-DDT23:59:00Z"))}`)
    }else if(status === STATUS_TO_PAY){
      setQuery(`expire_at[after]=${moment().subtract(3, 'months').format('YYYY-MM-DD')}&expire_at[before]=${encodeURIComponent(moment().add(3,'months').format("YYYY-MM-DDT23:59:00Z"))}`)
    }else{
      setQuery(`created_at[after]=${moment().subtract(6, 'months').format('YYYY-MM-DD')}&created_at[before]=${encodeURIComponent(moment().format("YYYY-MM-DDT23:59:00Z"))}`)
    }
  }


  return (
    <section className="mb-50 mb-lg-90">
      <div className="">
        <div className="filter-section">
          <div
            className="filter-wrapper payments d-flex flex-column flex-md-row justify-content-between align-items-start my-3">
            <div className='mb-2'>
              <Button className={'min-w-120'} color='primary' onClick={() => handleStatusPayments(STATUS_PAID)} outline={status !== STATUS_PAID} disabled={loading}>
                {Translator.trans('paid', {}, 'messages', lang)}
              </Button>{' '}
              <Button className={'min-w-120'} color='primary' outline={status !== STATUS_TO_PAY} onClick={() => handleStatusPayments(STATUS_TO_PAY)} disabled={loading}>
                {Translator.trans('user.dashboard.pratiche_payment_pending', {}, 'messages', lang)}
              </Button>{' '}
{/*              <Button className={'min-w-120'} color='primary' outline={status !== STATUS_ALL} onClick={() => handleStatusPayments('')} disabled={loading}>
                {Translator.trans('tutti', {}, 'messages', lang)}
              </Button>*/}
            </div>
            <DateRangePickerWrapper childToParent={childToParent} status={status} loading={loading}></DateRangePickerWrapper>
          </div>
        </div>
      </div>
      <div className="cmp-accordion">
        <div className="accordion">
          {paymentList.length === 0 && loading ? <span className="d-flex justify-content-center">
              <Spinner small active/><span className={'mx-2'}>{Translator.trans('loading', {}, 'messages', lang)}</span> </span> :
            paymentList.length > 0 && loading ? <span className="d-flex justify-content-center">
                <Spinner small active/><span
                className={'mx-2'}>{Translator.trans('loading', {}, 'messages', lang)}</span> </span> :
              paymentList.map((payment, index) => (
                <PaymentListItem key={payment.id} payment={payment} status={status}></PaymentListItem>
              ))
          }
          {paymentList.length === 0 && !loading ? `${Translator.trans('backoffice.integration.subscription_service.payment.no_payments', {}, 'messages', lang)} ${Translator.trans('from_date', {}, 'messages', lang).toLowerCase()}  ${startDateRead} ${Translator.trans('from_to', {}, 'messages', lang)} ${endDateRead}`: ''}
        </div>
      </div>
      <NotificationManager></NotificationManager>
    </section>
  );
}

export default PaymentList;
