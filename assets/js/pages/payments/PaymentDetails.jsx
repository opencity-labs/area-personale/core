import React from 'react';
import moment from "moment";
import {getPaymentStatus} from "../../utils/PaymentStatus";
import {useLanguage} from "../contexts/LanguageContext";

function PaymentDetails({payment}) {

  const  {
    lang
  } = useLanguage()

  return (
      <div className="cmp-card mb-4">
        <div className="card has-bkg-grey shadow-sm mb-0">
          <div className="card-header border-0 p-0 mb-lg-30">
            <div className="d-flex">
              <h3 className="subtitle-large mb-0">{Translator.trans('payment_details', {}, 'messages', lang)}</h3>
            </div>
          </div>
          <div className="card-body p-0">
            <div className="cmp-info-summary bg-white p-3 p-lg-4">
              <div className="card">
                <div className="card-body p-0">
                  {payment?.status ? (
                      <div className="single-line-info border-light">
                        <div className="text-paragraph-small">{Translator.trans('meetings.labels.status', {}, 'messages', lang)}</div>
                        <div className="border-light">
                          <p className="data-text">
                            <strong>{getPaymentStatus(payment?.status)}</strong>
                          </p>
                        </div>
                      </div>
                  ) : null}
                  <div className="single-line-info border-light">
                    <div className="text-paragraph-small">{Translator.trans('payment.iud', {}, 'messages', lang)}</div>
                    <div className="border-light">
                      <p className="data-text">
                        <strong>{payment?.payment?.iud}</strong>
                      </p>
                    </div>
                  </div>
                  {payment?.payment?.iuv ? (
                      <div className="single-line-info border-light">
                        <div className="text-paragraph-small">{Translator.trans('payment.iuv', {}, 'messages', lang)}</div>
                        <div className="border-light">
                          <p className="data-text">
                            <strong>{payment?.payment?.iuv}</strong>
                          </p>
                        </div>
                      </div>
                  ) : null}
                  <div className="single-line-info border-light">
                    <div className="text-paragraph-small">{Translator.trans('payment.event_created_at', {}, 'messages', lang)}</div>
                    <div className="border-light">
                      <p className="data-text">
                        <strong>{moment(moment.utc(payment?.created_at).toDate()).local(lang).format('L LTS')}</strong>
                      </p>
                    </div>
                  </div>
                  { payment?.payment?.paid_at ?
                      <div className="single-line-info border-light">
                        <div className="text-paragraph-small">{Translator.trans('payment.payment_data', {}, 'messages', lang)}</div>
                        <div className="border-light">
                          <p className="data-text">
                            <strong> { payment?.payment?.paid_at ? moment(moment.utc(payment?.payment?.paid_at).toDate()).local(lang).format('L LTS') : '--' }</strong>
                          </p>
                        </div>
                      </div>
                      : null}
                  <div className="single-line-info border-light">
                    <div className="text-paragraph-small">{Translator.trans('payment.expiration_date', {}, 'messages', lang)}</div>
                    <div className="border-light">
                      <p className="data-text">
                        <strong>{ payment?.payment?.expire_at ? moment(moment.utc(payment?.payment?.expire_at).toDate()).local(lang).format('L LTS') : '--' }</strong>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="card-footer p-0 d-none">
                </div>
              </div>
            </div>
            <div className="cmp-info-summary bg-white mt-3 p-3 p-lg-4">
              <p className="title-large-semi-bold card-white mb-0">{Translator.trans('payment.payment_amount', {}, 'messages', lang)}           {new Intl.NumberFormat("it-IT", { style: "currency", currency: "EUR" }).format(
                payment?.payment?.amount,
              )}</p>
            </div>
          </div>
        </div>
      </div>
  );
}

export default PaymentDetails;
