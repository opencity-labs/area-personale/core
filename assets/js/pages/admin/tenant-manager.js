import '../../core';
import Gateways from "../../rest/gateways/Gateways";
import GraphicAspectTenant from "../../utils/GraphicAspectTenant";
import HtmlEditor from "../../utils/HtmlEditor";
import Swal from 'sweetalert2/src/sweetalert2.js';

const language = document.documentElement.lang.toString();

$(document).ready(function () {

  GraphicAspectTenant.init();
  HtmlEditor.init();

  const calendars_integration_checkbox = $('.operatori_calendars_index');
  if (calendars_integration_checkbox.prop('checked')) {
    $('#linkable_application_meetings').show();
  } else {
    $('#ente_linkable_application_meetings').prop("checked", false);
    $('#linkable_application_meetings').hide();
  }

  calendars_integration_checkbox.change(function () {
    if (calendars_integration_checkbox.prop('checked')) {
      $('#linkable_application_meetings').show();
    } else {
      $('#ente_linkable_application_meetings').prop("checked", false);
      $('#linkable_application_meetings').hide();
    }
  });

  const ioEnabled = $('#ente_io_enabled');
  if (ioEnabled.prop('checked')) {
    $('#io_helper').show();
  } else {
    $('#io_helper').hide();
  }

  ioEnabled.change(function () {
    if (ioEnabled.prop('checked')) {
      $('#io_helper').show();
    } else {
      $('#io_helper').hide();
    }
  });


  if( $('#payments-tab').length > 0){
    Gateways.init();
  }

// Mailers
  $('#add-mailer').click(function (e) {
    e.preventDefault();
    let list = $('#current-mailers');
    // Try to find the counter of the list or use the length of the list
    let counter = list.data('widget-counter') || list.children().length;

    if ($('#no-mailers').length) {
      $('#no-mailers').remove();
    }

    // grab the prototype template
    let newWidget = $('#mailer-item-template').text();
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    newWidget = newWidget.replace(/__name__/g, new Date().getTime());
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data('widget-counter', counter);

    // create a new list element and add it to the list
    let newElem = $(list.attr('data-widget-mailer')).html(newWidget);
    console.log(newElem);
    newElem.appendTo(list);
  });

  $("#current-mailers").on("click", "a.js-remove-mailer", function (e) {
    e.preventDefault();
    $(this).closest('.js-mailer-item').remove();

    if ($('.js-mailer-item').length === 0) {
      $('#current-mailers').append('<p class="text-info" id="no-mailers"><i class="fa fa-info-circle"></i> Non sono ancora stati impostati mailer per l\'ente, i messaggi verranno inviati via e-mail dall\'indirizzo di default del sistema</p>');
    }
  });

  let url = location.href.replace(/\/$/, "");

  if (location.hash) {
    const hash = url.split("#");
    $('#myTab a[href="#' + hash[1] + '"]').tab("show");
    url = location.href.replace(/\/#/, "#");
    history.replaceState(null, null, url);
    setTimeout(() => {
      $(window).scrollTop(0);
    }, 400);
  }

  $('#myTab a[data-toggle="tab"]').on("click", function () {
    let newUrl;
    const hash = $(this).attr("href");
    newUrl = url.split("#")[0] + hash;
    history.replaceState(null, null, newUrl);
  });

  // Theme
  const $previewBox = $('#theme-preview');
  const $themeOptions = $('#ente_theme_options_identifier');
  const $lightTopHeader = $('#ente_theme_options_light_top_header');
  const $lightCenterHeader = $('#ente_theme_options_light_center_header');
  const $lightNavbarHeader = $('#ente_theme_options_light_navbar_header');
  const previewTheme = function () {

    let previewUrl = $previewBox.data('preview-url')+ '?theme=' + $themeOptions.val();
    if ($lightTopHeader.is(":checked")) {
      previewUrl += '&light-top-header=true'
    }

    if ($lightCenterHeader.is(":checked")) {
      previewUrl += '&light-center-header=true'
    }

    if ($lightNavbarHeader.is(":checked")) {
      previewUrl += '&light-navbar-header=true'
    }

    $previewBox.attr('src', previewUrl)
  }
  $('.theme-option').change(function () {
    previewTheme();
  })

  if( $('#templates-pdf').length > 0){
    let preview = $('#preview');

    preview.find('a').click(function (e) {
      Swal.fire(
        {
          title:`${Translator.trans('ente.custom_template.download_pdf', {}, 'messages', language)}`,
          text: `${Translator.trans('ente.custom_template.wait_for_download', {}, 'messages', language)}`,
          icon: 'info',
          showConfirmButton: false,
          timer: 5000
        });
    });
  }

  // Periodi di chiusura
  $(".add-another-closing_period-widget").click(function (e) {
    let list = $($(this).attr("data-list-selector"));
    // Try to find the counter of the list or use the length of the list
    let counter = list.data("widget-counter") || list.children().length;

    if ($("#no-closing_periods").length) {
      $("#no-closing_periods").remove();
    }

    // grab the prototype template
    let newWidget = list.attr("data-prototype");
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    newWidget = newWidget.replace(/__name__/g, new Date().getTime());
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data("widget-counter", counter);

    // create a new list element and add it to the list
    let newElem = $(list.attr("data-widget-closing_period")).html(newWidget);
    newElem.appendTo(list);
  });

  $("#closing_periods").on("click", "a.js-remove-closing_period", function (e) {
    e.preventDefault();
    $(this).closest(".js-closing_period-item").remove();

    if ($(".js-closing_period-item").length === 0) {
      $("#closing_periods").append(
        `<div class="alert alert-info" id="no-closing_periods">${Translator.trans(
          "backoffice.integration.calendars.no_closure_days",
          {},
          "messages",
          language
        )}</div>`
      );
    }
  });
});
