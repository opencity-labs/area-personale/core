import "./../styles/components/_calendars.scss";
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import './utils/datatables'
import {TextEditor} from "./utils/TextEditor";
import Swal from 'sweetalert2/src/sweetalert2.js'
import Auth from "./rest/auth/Auth";
import axios from "axios";

$(document).ready(function () {
  const language = document.documentElement.lang.toString();

  const auth = new Auth();
  const $tableContainer = $('#calendars-table');
  if ($tableContainer.length > 0) {
    let table; //Table reference
    const datatableSetting = JSON.parse(decodeURIComponent($tableContainer.data('config')));
    const datatableOptions = {
      dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 text-right'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      searching: true,
      paging: true,
      pagingType: 'simple_numbers',
      drawCallback: function (settings) {

        const api = new $.fn.dataTable.Api(settings);
        $('.toggle-notifications').on('click', function (e) {
          const el = $(this);
          const checked = el.prop('checked')
          auth.execAuthenticatedCall((token) => {
            axios.patch(el.data('api-url'), {
              "disable_notifications": Boolean(!checked),
            }, {
              headers: {
                Authorization: `Bearer ${token}`
              }
            })
              .then(function (response) {})
              .catch(function (error) {
                el.prop('checked', !checked)
                console.log(error);
                Swal.fire({
                  icon: 'error',
                  title: `${Translator.trans("status_error", {}, "messages", language)}`,
                  text: `${Translator.trans("generic_error", {}, "messages", language)}`
                })
              });

          });
        });


        $('input.disablepaste').bind('paste', function (e) {
          e.preventDefault();
        });

        $('.delete-btn').on('click', function () {
          if (checkName($(this).attr('data-id')))
            window.location.assign($(this).attr('data-url'));
        })

        $('.name-input').on('input', function () {
          let id = $(this).attr('data-id')
          let confirmBtn = $(`#confirm-delete-btn-${id}`)
          checkName(id) ? confirmBtn.attr('disabled', false) : confirmBtn.attr('disabled', true);
        })

        function checkName(id) {
          let input = $(`#input-calendar-name-${id}`).val();
          let check = $(`#check-calendar-name-${id}`).val();
          return !!(check && input && check.toLowerCase() === input.toLowerCase());
        }


      }
    };

    $tableContainer.initDataTables(datatableSetting, datatableOptions)
      .then(function (dt) {
        // dt contains the initialized instance of DataTables
        table = dt.api();
      });
  }


  //Used by tinymce for location field
  let messageTextEditor = null;
  TextEditor.init((editor,messageText)  => {
    messageTextEditor = messageText;
  });



  $(".add-another-closing_period-widget").click(function (e) {
    let list = $($(this).attr("data-list-selector"));
    // Try to find the counter of the list or use the length of the list
    let counter = list.data("widget-counter") || list.children().length;

    if ($("#no-closing_periods").length) {
      $("#no-closing_periods").remove();
    }

    // grab the prototype template
    let newWidget = list.attr("data-prototype");
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    newWidget = newWidget.replace(/__name__/g, new Date().getTime());
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data("widget-counter", counter);

    // create a new list element and add it to the list
    let newElem = $(list.attr("data-widget-closing_period")).html(newWidget);
    newElem.appendTo(list);
  });

  $("#closing_periods").on("click", "a.js-remove-closing_period", function (e) {
    e.preventDefault();
    $(this).closest(".js-closing_period-item").remove();

    if ($(".js-closing_period-item").length === 0) {
      $("#closing_periods").append(
        `<div class="alert alert-info" id="no-closing_periods">${Translator.trans(
          "backoffice.integration.calendars.no_closure_days",
          {},
          "messages",
          language
        )}</div>`
      );
    }
  });

  $(".add-another-opening_hour-widget").click(function (e) {
    let list = $($(this).attr("data-list-selector"));
    // Try to find the counter of the list or use the length of the list
    let counter = list.data("widget-counter") || list.children().length;

    if ($("#no-opening_hours").length) {
      $("#no-opening_hours").remove();
    }

    // grab the prototype template
    let newWidget = list.attr("data-prototype");
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    newWidget = newWidget.replace(/__name__/g, new Date().getTime());
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data("widget-counter", counter);

    // create a new list element and add it to the list
    let newElem = $(list.attr("data-widget-opening_hour")).html(newWidget);
    newElem.appendTo(list);
  });

  $("#opening_hours").on("click", "a.js-remove-opening_hour", function (e) {
    e.preventDefault();
    $(this).closest(".js-opening_hour-item").remove();

    if ($(".js-opening_hour-item").length === 0) {
      $("#no-opening_hours").append(
        `<div class="alert alert-info" id="no-opening_hours">${Translator.trans(
          "calendars.opening_hours.no_opening_hours",
          {},
          "messages",
          language
        )}</div>`
      );
    }
  });

  $(".add-another-external_calendar-widget").click(function (e) {
    let list = $($(this).attr("data-list-selector"));
    // Try to find the counter of the list or use the length of the list
    let counter = list.data("widget-counter") || list.children().length;

    if ($("#no-external_calendars").length) {
      $("#no-external_calendars").remove();
    }

    // grab the prototype template
    let newWidget = list.attr("data-prototype");
    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"
    newWidget = newWidget.replace(/__name__/g, new Date().getTime());
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data("widget-counter", counter);

    // create a new list element and add it to the list
    let newElem = $(list.attr("data-widget-external_calendar")).html(newWidget);
    newElem.appendTo(list);
  });

  $("#external_calendars").on(
    "click",
    "a.js-remove-external_calendar",
    function (e) {
      e.preventDefault();
      $(this).closest(".js-external_calendar-item").remove();

      if ($(".js-external_calendar-item").length === 0) {
        $("#no-external_calendars").append(
          `<div class="alert alert-info" id="no-external_calendars">${Translator.trans(
            "backoffice.integration.calendars.no_external_calendar",
            {},
            "messages",
            language
          )}</div>`
        );
      }
    }
  );

  $(".clone").click(function (e) {
    e.preventDefault();
    let button = $(this);
    let temp = $("<input>");
    $("body").append(temp);
    temp.val($("#App_calendar_id").val()).select();
    document.execCommand("copy");
    button
      .find("span")
      .text(`${Translator.trans("copied_id", {}, "messages", language)}`);
    temp.remove();
    setTimeout(function () {
      button
        .find("span")
        .text(`${Translator.trans("copy_id", {}, "messages", language)}`);
    }, 2000);
  });


  // Show/Hide reservation limits
  const $reservationLimitsCheckbox = $('#enable-reservation-limits');
  const $reservationLimitsContainer = $('#reservation-limits-container');
  const hideReservationLimits = function () {
    if ($reservationLimitsCheckbox.is(":checked")) {
      $reservationLimitsContainer.show();
    } else {
      $reservationLimitsContainer.hide();
      $('#App_calendar_reservationLimits_max_meetings_per_interval').val(0);
      $('#App_calendar_reservationLimits_interval').val('');
    }
  };
  hideReservationLimits()
  $reservationLimitsCheckbox.click(function () {
    hideReservationLimits()
  });
});
