export function Zoom($) {
  const overlay = $('#overlay')
  $('.it-thumb-container').on('click', function(e) {
    const elImg = $(this).find('img')
    e.preventDefault();
    e.stopPropagation();
    if(elImg.length){
      if(elImg[0].dataset.zoom){
        overlay
          .css({backgroundImage: `url(${elImg[0].dataset.zoom})`})
          .addClass('open');
      }else{
        overlay
          .css({backgroundImage: `url(${elImg[0].src})`})
          .addClass('open');
      }
    }
  })


  $('#overlay .btn').on('click', function() {
    $(overlay).removeClass('open');
  });
}
