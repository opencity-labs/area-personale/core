import "../styles/vendor/formio.scss";
import Form from './Formio/Form';
import Payment from "./rest/payment/Payment";
import FormIoHelper from "./utils/FormIoHelper";
import axios from "axios";
import {PrintButton} from "./utils/PrintButton";
require("jsrender")(); // Load JsRender as jQuery plugin (jQuery instance as parameter)
const $language = document.documentElement.lang.toString();

$(document).ready(function () {

  function confirmAndSendApplication($form) {
    $('#confirm').modal({backdrop: 'static', keyboard: false})
      .one('click', '#ok', function () {
        let profileBlocks = [];
        $("input.profile_blocks").each(function () {
          let el = $(this);
          if (el.is(':checked')) {
            profileBlocks.push(el.val())
          }
        });

        $('#profile_blocks').val(profileBlocks.join(','));
        $form.trigger('submit'); // submit the form
      });
  }

  // Modal conferma ed invia alla fine della compilazione di una pratica con pagamento
  if ($('#pratica_summary_flow_formIO_step').length) {
    $('button.craue_formflow_button_class_next').on('click', function (e) {
      const $form = $(this).closest('form');
      e.preventDefault();
      confirmAndSendApplication($form);
    });
  }

  // Modal conferma ed invia alla fine della compilazione di una pratica
  $('button.craue_formflow_button_last').on('click', function (e) {
    const $form = $(this).closest('form');
    e.preventDefault();
    confirmAndSendApplication($form);
  });

  // Init formIo
  if ($('#formio').length > 0) {
    Form.init('formio');
  }

  //
  if ($('form[name="pratica_payment_gateway"]').length > 0) {
    Form.getStoredSteps()
    Form.createStepsMobile()
    Payment.init();
  }

  // Bollo e MyPay
  if ($('form[name="pratica_select_payment_gateway"]').length > 0 || $('form[name="pratica_stamps"]').length > 0) {
    Form.getStoredSteps()
    Form.createStepsMobile()
  }

  if ($('#step-payments').length > 0) {
    Form.init('step-payments');
  }

  //Modal add form note
  if ($('#modalNote').length > 0) {
    const inputs = $('#textareaDescription'), submitBtn = $('#textareaSaveButton'), modalNote = $('#modalNote');
    const endpointUrl = modalNote.data('endpoint-url')

    $('.note-text').html(`<span>${Translator.trans("loading", {}, "messages", $language)}</span>`);

    axios.get(endpointUrl)
      .then(function (res) {
        if (res.data) {
          $('.note-text').html(`${Translator.trans("update_note", {}, "messages", $language)}`);
          inputs.val(res.data);
        } else {
          $('.note-text').html(`${Translator.trans("add_note", {}, "messages", $language)}`);
        }
      })

    inputs.on('input', function (e) {
      let invalid = inputs.is(function (index, element) {
        return !$(element).val().trim();
      });
      if (invalid) {
        submitBtn.addClass("isDisabled").prop("disabled", true);
      } else {
        submitBtn.removeClass("isDisabled").prop("disabled", false);
      }
    });

    modalNote.on('show.bs.modal', function (e) {
      // Get note if exist
      axios.get(endpointUrl)
        .then(function (res) {
          if (res.data) {
            inputs.val(res.data);
          }
        })
    })

    submitBtn.on('click', (e) => {
      e.preventDefault()
      const options = {
        method: 'POST',
        data: inputs.val(),
        url: endpointUrl
      };
      axios(options).then((res) => {
        modalNote.modal('hide')
        $('.note-text').html(`${Translator.trans("update_note", {}, "messages", $language)}`);
      })
    })
  }

  if ($('#download_module').length > 0) {
    PrintButton.init()
  }

});

window.FormioHelper = new FormIoHelper()
