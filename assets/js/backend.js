import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';

import Swal from 'sweetalert2/src/sweetalert2.js';
import BasePath from "./utils/BasePath";
import './utils/datatables'
import moment from "moment/moment";

$(document).ready(function () {
  let table; //Table reference
  const tableContainer = $('#datatable');
  if (tableContainer.length > 0) {
    const datatableSetting = JSON.parse(decodeURIComponent(tableContainer.data('config')));
    const datatableOptions = {
      dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      searching: true,
      paging: true,
      pagingType: 'simple_numbers',
      drawCallback: ( settings ) => {
        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();
      },
    };

    tableContainer.initDataTables(datatableSetting, datatableOptions)
      .then(function (dt) {
        // dt contains the initialized instance of DataTables
        table = dt.api();
      });
  }

});

document.addEventListener("DOMContentLoaded", function () {

  const basePath = new BasePath().getBasePath();
  const language = document.documentElement.lang.toString();

  fetch(basePath + '/api/session/lifetime')
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json()
    })
    .then(data => {
      const sessionTimeout = data.session_lifetime * 1000; // Convert to milliseconds
      const warningTime = 5 * 60 * 1000; // Set warning 5 minutes before expiration
      const timeoutWarning = sessionTimeout - warningTime;

      setTimeout(showSessionWarning, timeoutWarning);
      setTimeout(expireSession, sessionTimeout);
    })
    .catch(error => console.log('Error fetching session lifetime:', error));

  function showSessionWarning() {
    Swal.fire({
      title: `${Translator.trans('session.expire_warning', {}, 'messages', language)}`,
      icon: "info",
      showConfirmButton: true
    });
  }

  function expireSession() {
    Swal.fire({
      title: `${Translator.trans('session.expired', {}, 'messages', language)}`,
      icon: "warning",
      showConfirmButton: true
    }).then(() => {
      window.location.href = basePath + '/operatori/logout';
    });

  }
});
