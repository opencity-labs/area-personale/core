<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250123162858 extends AbstractMigration
{
  public function getDescription(): string
  {
    return 'Add PEC field to utente';
  }

  public function up(Schema $schema): void
  {
    $this->addSql('ALTER TABLE utente ADD pec VARCHAR(255) DEFAULT NULL');
  }

  public function down(Schema $schema): void
  {
    $this->addSql('ALTER TABLE utente DROP pec');
  }
}
