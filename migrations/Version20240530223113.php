<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240530223113 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE code_counter (id UUID NOT NULL, code_generation_strategy_id UUID NOT NULL, key VARCHAR(255) DEFAULT NULL, counter INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F985D0E4B08C65E8 ON code_counter (code_generation_strategy_id)');
        $this->addSql('CREATE UNIQUE INDEX code_counter_unique ON code_counter (code_generation_strategy_id, key)');
        $this->addSql('CREATE TABLE code_generation_strategy (id UUID NOT NULL, name VARCHAR(255) NOT NULL, prefix VARCHAR(255) DEFAULT NULL, postfix VARCHAR(255) DEFAULT NULL, counter_digit_length INT NOT NULL, temporal_reset VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE code_counter ADD CONSTRAINT FK_F985D0E4B08C65E8 FOREIGN KEY (code_generation_strategy_id) REFERENCES code_generation_strategy (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE calendar ADD code_generation_strategy_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE calendar ADD CONSTRAINT FK_6EA9A146B08C65E8 FOREIGN KEY (code_generation_strategy_id) REFERENCES code_generation_strategy (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6EA9A146B08C65E8 ON calendar (code_generation_strategy_id)');
        $this->addSql('ALTER TABLE meeting ADD code VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE calendar DROP CONSTRAINT FK_6EA9A146B08C65E8');
        $this->addSql('ALTER TABLE code_counter DROP CONSTRAINT FK_F985D0E4B08C65E8');
        $this->addSql('DROP TABLE code_counter');
        $this->addSql('DROP TABLE code_generation_strategy');
        $this->addSql('ALTER TABLE meeting DROP code');
        $this->addSql('DROP INDEX IDX_6EA9A146B08C65E8');
        $this->addSql('ALTER TABLE calendar DROP code_generation_strategy_id');
    }
}
