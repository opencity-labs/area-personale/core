<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250208134833 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE administrative_identifier (id UUID NOT NULL, identifies UUID NOT NULL, type VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A446D52329B3FC ON administrative_identifier (identifies)');
        $this->addSql('CREATE UNIQUE INDEX administrative_identifier_unique ON administrative_identifier (identifies, type, identifier)');
        $this->addSql('ALTER TABLE administrative_identifier ADD CONSTRAINT FK_A446D52329B3FC FOREIGN KEY (identifies) REFERENCES utente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrative_identifier DROP CONSTRAINT FK_A446D52329B3FC');
        $this->addSql('DROP TABLE administrative_identifier');
    }
}
