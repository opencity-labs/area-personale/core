<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231212173814 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql("UPDATE servizio SET receipt = 'compiled_module_on_demand' WHERE pratica_fcqn = '\App\Entity\BuiltIn'");
    $this->addSql("UPDATE servizio SET workflow = 0 WHERE identifier = 'helpdesk'");
    $this->addSql("UPDATE servizio SET workflow = 0 WHERE identifier = 'inefficiencies'");
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql("UPDATE servizio SET receipt = 'compiled_module' WHERE pratica_fcqn = '\App\Entity\BuiltIn'");
    $this->addSql("UPDATE servizio SET workflow = 1 WHERE identifier = 'helpdesk'");
    $this->addSql("UPDATE servizio SET workflow = 1 WHERE identifier = 'inefficiencies'");
  }
}
