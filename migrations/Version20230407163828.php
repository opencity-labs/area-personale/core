<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230407163828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pratica_geographic_area (pratica_id UUID NOT NULL, geographic_area_id UUID NOT NULL, PRIMARY KEY(pratica_id, geographic_area_id))');
        $this->addSql('CREATE INDEX IDX_D536EC2924038DEB ON pratica_geographic_area (pratica_id)');
        $this->addSql('CREATE INDEX IDX_D536EC297A617856 ON pratica_geographic_area (geographic_area_id)');
        $this->addSql('ALTER TABLE pratica_geographic_area ADD CONSTRAINT FK_D536EC2924038DEB FOREIGN KEY (pratica_id) REFERENCES pratica (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pratica_geographic_area ADD CONSTRAINT FK_D536EC297A617856 FOREIGN KEY (geographic_area_id) REFERENCES geographic_area (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE pratica_geographic_area');
    }
}
