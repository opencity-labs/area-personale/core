#!/bin/bash

[[ $DEBUG == 1 ]] && set -x

echo "==> Validate app setup"

bin/console ocsdc:validate-setup --no-interaction
