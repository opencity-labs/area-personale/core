<?php

declare(strict_types=1);

namespace Tests\Functional\Applications\Application;

use App\Applications\Application\ApplicationLiteService;
use App\Applications\Application\DTOs\ApplicantData;
use App\Applications\Application\DTOs\ApplicationLiteData;
use App\Applications\Application\DTOs\BeneficiaryData;
use App\Applications\Application\DTOs\ContactsData;
use App\Applications\Application\DTOs\IdentifiersData;
use App\Applications\Exceptions\ApplicationLiteException;
use App\Entity\ApplicationLite;
use App\Entity\Categoria;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Servizio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\Builders\ServizioBuilder;

/**
 * @group functional
 * @group ApplicationLite
 */
class ApplicationLiteServiceFunctionalTest extends KernelTestCase
{
  private ?ApplicationLiteService $applicationLiteService;
  private ?EntityManagerInterface $entityManager;
  private Ente $ente;

  protected function setUp(): void
  {
    $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
    $this->applicationLiteService = static::getContainer()->get(ApplicationLiteService::class);

    $this->entityManager->beginTransaction();

    $ente = new Ente();
    $ente->setName('comune-di-bugliano');
    $ente->setSlug('comune-di-bugliano');
    $this->entityManager->persist($ente);
    $this->ente = $ente;
  }

  protected function tearDown(): void
  {
    if ($this->entityManager->getConnection()->isTransactionActive()) {
      // Rollback della transazione alla fine del test
      $this->entityManager->rollback();
    }

    $this->entityManager->close();
    $this->entityManager = null;

    parent::tearDown();
  }

  public function createAndPersistService(): Servizio
  {
    $topics = new Categoria();
    $topics->setName('generic_category');

    $service = ServizioBuilder::createAsApplicationLiteService()
      ->withTopic($topics)
      ->build();

    $this->entityManager->persist($topics);
    $this->entityManager->persist($service);
    $this->entityManager->flush();

    return $service;
  }

  /** @throws ApplicationLiteException */
  public function testShouldCorrectlyCreateNewApplicationLiteWhitApplicant(): void
  {
    $admIds = new IdentifiersData();
    $admIds->taxCode = 'CCCSSS81M11H123U';

    $contacts = new ContactsData();
    $contacts->email = 'user@email.me';
    $contacts->pec = 'user@pec.email.me';

    $applicant = new ApplicantData();
    $applicant->givenName = 'Applicant';
    $applicant->familyName = 'User';
    $applicant->identifiers = $admIds;
    $applicant->contacts = $contacts;

    $data = new ApplicationLiteData();
    $data->applicant = $applicant;
    $data->externalId = 'ext-123';
    $data->subject = 'Test Subject';
    $data->status = 2000;

    $service = $this->createAndPersistService();

    $applicationLite = $this->applicationLiteService->createOrUpdate($data, $service, $this->ente);

    $this->assertInstanceOf(ApplicationLite::class, $applicationLite);
    $this->assertEquals('ext-123', $applicationLite->getExternalId());
    $this->assertEquals('Test Subject', $applicationLite->subject());
    $this->assertInstanceOf(CPSUser::class, $applicationLite->applicant());
    $this->assertNull($applicationLite->beneficiary());
    $this->assertEquals(2000, $applicationLite->status());
    $this->assertEquals('user@pec.email.me', $applicationLite->applicant()->getPec());
  }

  /** @throws ApplicationLiteException */
  public function testShouldCorrectlyCreateNewApplicationLiteWhitApplicantAndBeneficiary(): void
  {
    $admIds = new IdentifiersData();
    $admIds->taxCode = 'CCCSSS81M11H123U';

    $contacts = new ContactsData();
    $contacts->email = 'user@email.me';

    $applicant = new ApplicantData();
    $applicant->givenName = 'Applicant';
    $applicant->familyName = 'User';
    $applicant->identifiers = $admIds;
    $applicant->contacts = $contacts;

    $admIds = new IdentifiersData();
    $admIds->taxCode = 'CCCSSS81M11H222U';

    $beneficiary = new BeneficiaryData();
    $beneficiary->givenName = 'Beneficiary';
    $beneficiary->familyName = 'User';
    $beneficiary->identifiers = $admIds;
    $beneficiary->contacts = $contacts;

    $data = new ApplicationLiteData();
    $data->applicant = $applicant;
    $data->beneficiary = $beneficiary;
    $data->externalId = 'ext-123';
    $data->subject = 'Test Subject';
    $data->status = 2000;

    $service = $this->createAndPersistService();

    $applicationLite = $this->applicationLiteService->createOrUpdate($data, $service, $this->ente);

    $this->assertInstanceOf(ApplicationLite::class, $applicationLite);
    $this->assertEquals('ext-123', $applicationLite->getExternalId());
    $this->assertEquals('Test Subject', $applicationLite->subject());
    $this->assertInstanceOf(CPSUser::class, $applicationLite->applicant());
    $this->assertInstanceOf(CPSUser::class, $applicationLite->beneficiary());
    $this->assertEquals(2000, $applicationLite->status());
  }

  /** @throws ApplicationLiteException */
  public function testShouldCorrectlyUpdateExistingApplicationLite(): void
  {
    $data = new ApplicationLiteData();
    $data->externalId = 'ext-123';
    $data->subject = 'Test Subject';
    $data->status = 2000;

    $service = $this->createAndPersistService();

    $applicant = new CPSUser();
    $applicant->setUsername('applicant');
    $this->entityManager->persist($applicant);

    $applicationLite = ApplicationLite::createAsExternal(
      $data->externalId,
      $service,
      $applicant,
      $data->subject,
      $data->status,
      $this->ente,
    );

    $this->entityManager->persist($applicationLite);
    $this->entityManager->flush();

    // Aggiorna l'ApplicationLite con un nuovo status
    $data->status = 7000;
    $updatedApplicationLite = $this->applicationLiteService->createOrUpdate($data, $service, $this->ente);

    // Verifica che l'aggiornamento sia stato applicato correttamente
    $this->assertInstanceOf(ApplicationLite::class, $updatedApplicationLite);
    $this->assertEquals(7000, $updatedApplicationLite->status());
  }

  public function testShouldThrowsExceptionForInvalidStatus(): void
  {
    $invalidStatus = 1234;

    $this->expectException(ApplicationLiteException::class);
    $this->expectExceptionMessage("ApplicationLite status is invalid: {$invalidStatus}");

    $admIds = new IdentifiersData();
    $admIds->taxCode = 'CCCSSS81M11H123U';

    $contacts = new ContactsData();
    $contacts->email = 'user@email.me';

    $applicant = new ApplicantData();
    $applicant->givenName = 'Applicant';
    $applicant->familyName = 'User';
    $applicant->identifiers = $admIds;
    $applicant->contacts = $contacts;

    $data = new ApplicationLiteData();
    $data->applicant = $applicant;
    $data->externalId = 'ext-999';
    $data->subject = 'Invalid Status Test';
    $data->status = $invalidStatus;

    $service = $this->createAndPersistService();

    $this->applicationLiteService->createOrUpdate($data, $service, $this->ente);
  }
}
