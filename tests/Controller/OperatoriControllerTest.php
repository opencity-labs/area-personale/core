<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\Entity\Allegato;
use App\Entity\ComponenteNucleoFamiliare;
use App\Entity\CPSUser;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Logging\LogConstants;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Tests\Base\AbstractAppTestCase;

/**
 * Class OperatoriControllerTest.
 */
class OperatoriControllerTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();
    $this->cleanDb(ComponenteNucleoFamiliare::class);
    $this->cleanDb(Allegato::class);
    $this->cleanDb(Pratica::class);
    $this->cleanDb(OperatoreUser::class);
    $this->cleanDb(CPSUser::class);
  }

  /**
   * @test
   */
  public function testICannotAccessOperatoriHomePageAsAnonymousUser(): void
  {
    $operatoriHome = $this->router->generate('operatori_index');
    $this->client->request('GET', $operatoriHome);
    $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICanAccessOperatoriHomePageAsLoggedInOperatore(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $user = $this->createOperatoreUser($username, $password, $this->createEnti()[0]);

    $operatoriHome = $this->router->generate('operatori_index');
    $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

    $this->doTestISeeMyNameAsLoggedInUser($user, $this->client->getResponse());
  }

  /**
   * @test
   */
  public function testICanSeeMyPraticheWhenAccessingOperatoriHomePageAsLoggedInOperatore(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $operatore = $this->createOperatoreUser($username, $password, $this->createEnti()[0]);
    $altroOperatore = $this->createOperatoreUser($username . '2', $password);
    $user = $this->createCPSUser();

    $praticaSubmitted = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_SUBMITTED);
    $praticaRegistered = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_REGISTERED);
    $praticaPending = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_PENDING);
    $praticaSubmittedMaAltroOperatore = $this->setupPraticheForUserWithOperatoreAndStatus($user, $altroOperatore, Pratica::STATUS_SUBMITTED);

    $operatoriHome = $this->router->generate('operatori_index');
    $crawler = $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

    $this->doTestISeeMyNameAsLoggedInUser($operatore, $this->client->getResponse());

    $praticheCount = $crawler->filter('.list.mie')->filter('.pratica')->count();
    $this->assertEquals(3, $praticheCount);

    $expectedPratiche = [
      $praticaSubmitted,
      $praticaRegistered,
      $praticaPending,
    ];

    $unexpectedPratiche = [
      $praticaSubmittedMaAltroOperatore,
    ];

    foreach ($expectedPratiche as $pratica) {
      $this->assertEquals(1, $crawler->filterXPath('//*[@data-pratica="' . $pratica->getId() . '"]')->count());
    }

    foreach ($unexpectedPratiche as $pratica) {
      $this->assertEquals(0, $crawler->filterXPath('//*[@data-pratica="' . $pratica->getId() . '"]')->count());
    }
  }

  /**
   * @test
   */
  public function testICanSeeUnassignedPraticheForMyEnteWhenAccessingOperatoriHomePageAsLoggedInOperatore(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $enti = $this->createEnti();
    $ente1 = $enti[0];
    $ente2 = $enti[1];

    $this->createOperatoreUser($username, $password, $ente1);
    $user = $this->createCPSUser();


    $praticaSubmitted = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_SUBMITTED);
    $praticaRegistered = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_REGISTERED);
    $praticaPending = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_PENDING);
    $praticaPendingMaAltroEnte = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_PENDING);

    $operatoriHome = $this->router->generate('operatori_index');
    $crawler = $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

    $praticheCount = $crawler->filter('.list.libere')->filter('.pratica')->count();
    $this->assertEquals(3, $praticheCount);

    $expectedPratiche = [
      $praticaSubmitted,
      $praticaRegistered,
      $praticaPending,
    ];

    $unexpectedPratiche = [
      $praticaPendingMaAltroEnte,
    ];

    foreach ($expectedPratiche as $pratica) {
      $this->assertEquals(1, $crawler->filterXPath('//*[@data-pratica="' . $pratica->getId() . '"]')->count());
    }

    foreach ($unexpectedPratiche as $pratica) {
      $this->assertEquals(0, $crawler->filterXPath('//*[@data-pratica="' . $pratica->getId() . '"]')->count());
    }
  }

  /**
   * @test
   */
  public function testICanSeeMyCompletedPraticheWhenAccessingOperatoriHomePageAsLoggedInOperatore(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $operatore = $this->createOperatoreUser($username, $password, $this->createEnti()[0]);
    $altroOperatore = $this->createOperatoreUser($username . '2', $password);
    $user = $this->createCPSUser();

    $praticaSubmitted = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_SUBMITTED);
    $praticaRegistered = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_REGISTERED);
    $praticaPending = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_PENDING);
    $praticaComplete = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_COMPLETE);

    $praticaCompletedMaAltroOperatore = $this->setupPraticheForUserWithOperatoreAndStatus($user, $altroOperatore, Pratica::STATUS_COMPLETE);

    $operatoriHome = $this->router->generate('operatori_index');
    $crawler = $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

    $praticheCount = $crawler->filter('.list.mie')->filter('.pratica')->count();
    $this->assertEquals(3, $praticheCount);

    $expectedPratiche = [
      $praticaSubmitted,
      $praticaRegistered,
      $praticaPending,
      $praticaComplete,
    ];

    $unexpectedPratiche = [
      $praticaCompletedMaAltroOperatore,
    ];

    foreach ($expectedPratiche as $pratica) {
      $this->assertEquals(1, $crawler->filterXPath('//*[@data-pratica="' . $pratica->getId() . '"]')->count());
    }

    foreach ($unexpectedPratiche as $pratica) {
      $this->assertEquals(0, $crawler->filterXPath('//*[@data-pratica="' . $pratica->getId() . '"]')->count());
    }
  }

  /**
   * @test
   */
  public function testICanAccessToMyAssignedPraticaDetail(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $operatore = $this->createOperatoreUser($username, $password, $this->createEnti()[0]);
    $altroOperatore = $this->createOperatoreUser($username . '2', $password);
    $user = $this->createCPSUser();

    $pratica = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_PENDING);
    $detailPraticaUrl = $this->router->generate('operatori_show_pratica', ['pratica' => $pratica->getId()]);

    $operatoriHome = $this->router->generate('operatori_index');
    $crawler = $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    $this->assertContains($detailPraticaUrl, $this->client->getResponse()->getContent());

    $this->client->request('GET', $detailPraticaUrl, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

  }

  /**
   * @test
   */
  public function testICannotAccessToUnassignedPraticaDetail(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $enti = $this->createEnti();
    $ente1 = $enti[0];

    $operatore = $this->createOperatoreUser($username, $password, $ente1);
    $user = $this->createCPSUser();

    $pratica = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_SUBMITTED);
    $detailPraticaUrl = $this->router->generate('operatori_show_pratica', ['pratica' => $pratica->getId()]);

    $operatoriHome = $this->router->generate('operatori_index');
    $crawler = $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    $this->assertNotContains($detailPraticaUrl, $this->client->getResponse()->getContent());

    $this->client->request('GET', $detailPraticaUrl, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICanAssignToMyselfAUnassignedPratica(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $enti = $this->createEnti();
    $ente1 = $enti[0];

    $operatore = $this->createOperatoreUser($username, $password, $ente1);
    $user = $this->createCPSUser();
    $pratica = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_SUBMITTED);
    $pratica->setNumeroProtocollo('test');
    $pratica->setNumeroFascicolo('test');
    $pratica->setStatus(Pratica::STATUS_REGISTERED);
    $this->em->flush($pratica);

    $mockLogger = $this->getMockLogger();
    $expectedArgs = [
      LogConstants::PRATICA_ASSIGNED,
      LogConstants::PRATICA_CHANGED_STATUS,
    ];
    $mockLogger->expects($this->exactly(count($expectedArgs)))
      ->method('info')
      ->with($this->callback(fn ($subject) => in_array($subject, $expectedArgs)));

    $mockMailer = $this->setupSwiftmailerMock([$user, $operatore]);

    static::$kernel->setKernelModifier(function (KernelInterface $kernel) use ($mockLogger, $mockMailer): void {
      $kernel->getContainer()->set('logger', $mockLogger);
      $kernel->getContainer()->set('swiftmailer.mailer.default', $mockMailer);
    });

    $autoassignPraticaUrl = $this->router->generate('operatori_autoassing_pratica', ['pratica' => $pratica->getId()]);

    $this->client->followRedirects();
    $this->client->request('GET', $autoassignPraticaUrl, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);

    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

    $pratica = $this->em->getRepository('App:Pratica')->find($pratica->getId());
    $this->assertEquals(Pratica::STATUS_PENDING, $pratica->getStatus());
  }

  /**
   * @test
   */
  public function testICanNotAssignToMyselfAUnassignedPraticaWithoutProtocollo(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $enti = $this->createEnti();
    $ente1 = $enti[0];

    $operatore = $this->createOperatoreUser($username, $password, $ente1);
    $user = $this->createCPSUser();
    $pratica = $this->setupPraticheForUserWithStatus($user, Pratica::STATUS_SUBMITTED);

    $autoassignPraticaUrl = $this->router->generate('operatori_autoassing_pratica', ['pratica' => $pratica->getId()]);

    $this->client->followRedirects();
    $this->client->request('GET', $autoassignPraticaUrl, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICannotAssignToMyselfAnAssignedPratica(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $enti = $this->createEnti();
    $ente1 = $enti[0];

    $operatore = $this->createOperatoreUser($username, $password, $ente1);
    $user = $this->createCPSUser();

    $pratica = $this->setupPraticheForUserWithOperatoreAndStatus($user, $operatore, Pratica::STATUS_PENDING);
    $autoassignPraticaUrl = $this->router->generate('operatori_autoassing_pratica', ['pratica' => $pratica->getId()]);

    $this->client->followRedirects();
    $this->client->request('GET', $autoassignPraticaUrl, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICannotAccessOperatoriListAsNormalOperatore(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $this->createOperatoreUser($username, $password);

    $operatoriList = $this->router->generate('operatori_list_by_ente');
    $this->client->request('GET', $operatoriList);
    $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    $this->assertMatchesRegularExpression('/\/login$/', $this->client->getResponse()->headers->get('location'));
  }

  /**
   * @test
   */
  public function testICanDoLogoutAsOperatore(): void
  {
    $password = 'pa$$word';
    $username = 'username';

    $operatore = $this->createOperatoreUser($username, $password, $this->createEnti()[0]);

    $operatoriHome = $this->router->generate('operatori_index');
    $crawler = $this->client->request('GET', $operatoriHome, [], [], [
      'PHP_AUTH_USER' => $username,
      'PHP_AUTH_PW' => $password,
    ]);
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

    $logout = $crawler->selectLink($this->translator->trans('logout'))->link();
    $this->client->click($logout);
    $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode(), 'Unexpected HTTP status code');
    $this->assertMatchesRegularExpression('/\/operatori\/$/', $this->client->getResponse()->headers->get('location'));

    $this->client->request('GET', $operatoriHome);
    $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    $this->assertMatchesRegularExpression('/\/login$/', $this->client->getResponse()->headers->get('location'));
  }
}
