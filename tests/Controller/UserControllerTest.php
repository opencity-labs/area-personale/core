<?php

declare(strict_types=1);

namespace Tests\Controller;

use App\Logging\LogConstants;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Tests\Base\AbstractAppTestCase;

/**
 * Class UserControllerTest.
 */
class UserControllerTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();
    /*$this->em->getConnection()->executeQuery('DELETE FROM servizio_erogatori')->execute();
    $this->cleanDb(ComponenteNucleoFamiliare::class);
    $this->cleanDb(Allegato::class);
    $this->cleanDb(Pratica::class);
    $this->cleanDb(CPSUser::class);
    $this->cleanDb(Servizio::class);
    $this->cleanDb(Ente::class);*/
  }

  /**
   * @test
   */
  public function testICannotAccessUserDashboardAsAnonymousUser(): void
  {
    $this->client->followRedirects();
    $this->client->request('GET', $this->router->generate('user_dashboard'));
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICanAccessUserDashboardAsLoggedUser(): void
  {
    $user = $this->createCPSUser();
    $this->client->followRedirects();
    $this->clientRequestAsCPSUser(
      $user,
      'GET',
      $this->router->generate(
        'login_pat'
      )
    );
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICannotAccessUserProfileAsAnonymousUser(): void
  {
    $this->client->followRedirects();
    $this->client->request('GET', $this->router->generate('user_profile'));
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
  }

  /**
   * @test
   */
  public function testICanAccessUserProfileAsLoggedUser(): void
  {
    $user = $this->createCPSUser();
    $this->client->followRedirects();
    $this->client->request('GET', $this->router->generate('user_profile'));
    $this->clientRequestAsCPSUser(
      $user,
      'GET',
      $this->router->generate(
        'user_dashboard'
      )
    );
    $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
  }

  public function testUseCPSValuesAsDefaultContactInfoWhenCreatingUser(): void
  {
    $user = $this->createCPSUser();
    $this->assertEquals($user->getCellulare(), $user->getCellulareContatto());
    $this->assertEquals($user->getEmail(), $user->getEmailContatto());
  }

  public function testICanChangeMyContactInfoAsLoggedUser(): void
  {
    $mockLogger = $this->getMockLogger();
    $mockLogger->expects($this->exactly(1))
      ->method('info')
      ->with(LogConstants::USER_HAS_CHANGED_CONTACTS_INFO);
    static::$kernel->setKernelModifier(function (KernelInterface $kernel) use ($mockLogger): void {
      $kernel->getContainer()->set('logger', $mockLogger);
    });

    $user = $this->createCPSUser();

    $testEmail = random_int(1, 10) . '@example.com';
    $testCellulare = random_int(1, 10);

    $crawler = $this->clientRequestAsCPSUser($user, 'GET', $this->router->generate('user_profile'));
    $form = $crawler->selectButton('form[save]')->form([
      'form[email_contatto]' => $testEmail,
      'form[cellulare_contatto]' => $testCellulare,
    ]);
    $this->client->submit($form);
    $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode(), 'Unexpected HTTP status code');

    $this->em->refresh($user);
    $this->assertEquals($testCellulare, $user->getCellulareContatto());
    $this->assertEquals($testEmail, $user->getEmailContatto());
  }
}
