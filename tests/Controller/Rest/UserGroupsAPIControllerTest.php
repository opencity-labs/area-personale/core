<?php

declare(strict_types=1);

namespace Tests\Controller\Rest;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\Helpers\ApiHelper;
use JsonException;

class UserGroupsAPIControllerTest extends TestCase
{
  protected ?string $token = null;

  /**
   * @throws GuzzleException
   * @throws JsonException
   */
  protected function setUp(): void
  {
    parent::setUp();
    $this->token = ApiHelper::getAdminJwtToken();
  }

  /**
   * @throws GuzzleException
   */
  public function testPostUserGroupAction()
  {
    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];

    $data = [
      'name' => 'test' . date('Y-m-d H:i:s'),
      'short_description' => 'short description',
      'main_function' => 'main function',
      'core_contact_point' => [
        'name' => 'test',
        'pec' => 'pec@sdc.it',
        'email' => 'email@sdc.it',
        'phone_number' => '33312312323',
      ],
    ];

    $request = new Request(
      'POST',
      ApiHelper::API_BASE_URL . '/api/user-groups',
      $headers,
      json_encode($data, JSON_THROW_ON_ERROR)
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    $responseData = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    $this->assertArrayHasKey('id', $responseData);

    return $responseData['id'];
  }

  /**
   * @throws GuzzleException|JsonException
   *
   * @depends testPostUserGroupAction
   */
  public function testPutUserGroupAction($id): void
  {
    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];

    $data = [
      'name' => 'test' . date('Y-m-d H:i:s'),
      'short_description' => 'short description',
      'main_function' => 'main function',
      'core_contact_point' => [
        'name' => 'test',
        'pec' => 'pec@sdc.it',
        'email' => 'email@sdc.it',
        'phone_number' => '33312312323',
      ],
    ];

    $request = new Request(
      'PUT',
      ApiHelper::API_BASE_URL . '/api/user-groups/' . $id,
      $headers,
      json_encode($data, JSON_THROW_ON_ERROR)
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
  }

  /**
   * @throws GuzzleException
   *
   * @depends testPostUserGroupAction
   */
  public function testPatchUserGroupAction($id): void
  {
    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];

    $data = [
      'name' => 'test' . date('Y-m-d H:i:s'),
    ];

    $request = new Request(
      'PATCH',
      ApiHelper::API_BASE_URL . '/api/user-groups/' . $id,
      $headers,
      json_encode($data)
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
  }

  /**
   * @throws GuzzleException
   */
  public function testGetUserGroupsAction(): void
  {
    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];

    $request = new Request(
      'GET',
      ApiHelper::API_BASE_URL . '/api/user-groups',
      $headers
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
  }

  /**
   * @depends testPostUserGroupAction
   *
   * @throws GuzzleException
   */
  public function testGetUserGroupAction($id): void
  {
    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];

    $request = new Request(
      'GET',
      ApiHelper::API_BASE_URL . '/api/user-groups/' . $id,
      $headers
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    $responseData = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    $this->assertEquals($id, $responseData['id']);
  }

  /**
   * @depends testPostUserGroupAction
   *
   * @throws GuzzleException
   */
  public function testDeleteUserGroupAction($id): void
  {
    $client = new Client();
    $headers = [
      'Authorization' => 'Bearer ' . $this->token,
      'Content-Type' => 'application/json',
    ];

    $request = new Request(
      'DELETE',
      ApiHelper::API_BASE_URL . '/api/user-groups/' . $id,
      $headers
    );

    $response = $client->send($request);
    $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
  }
}
