<?php

declare(strict_types=1);

namespace Tests\EventListener;

use App\Event\ProtocollaPraticaSuccessEvent;
use App\EventListener\GiscomSendPraticaListener;
use App\Services\GiscomAPIAdapterService;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\NullLogger;
use Tests\Base\AbstractAppTestCase;

class GiscomPraticaListenerTest extends AbstractAppTestCase
{
  public function testGiscomSendPraticaListenerSendsPratica(): void
  {
    $giscomAPIAdapter = $this->mockGiscomAPIAdapterService();
    $listener = new GiscomSendPraticaListener($giscomAPIAdapter, new NullLogger());

    $pratica = $this->setupPraticaScia();
    $event = new ProtocollaPraticaSuccessEvent($pratica);

    $giscomAPIAdapter->expects($this->once())
      ->method('sendPraticaToGiscom')
      ->with($this->equalTo($pratica));

    $listener->onPraticaProtocollata($event);
  }

  public function testGiscomSendPraticaListenerIgnoresPraticaThatAreNotManagedByGiscom(): void
  {
    $mockGiscomAdapter = $this->mockGiscomAPIAdapterService();
    $listener = new GiscomSendPraticaListener($mockGiscomAdapter, new NullLogger());

    $pratica = $this->createPratica($this->createCPSUser());
    $event = new ProtocollaPraticaSuccessEvent($pratica);

    $mockGiscomAdapter->expects($this->never())
      ->method('sendPraticaToGiscom')
      ->with($this->equalTo($pratica));

    $listener->onPraticaProtocollata($event);
  }

  /** @return (GiscomAPIAdapterService&MockObject)|MockObject */
  public function mockGiscomAPIAdapterService(): GiscomAPIAdapterService
  {
    return $this->getMockBuilder(GiscomAPIAdapterService::class)
      ->disableOriginalConstructor()
      ->getMock();
  }
}
