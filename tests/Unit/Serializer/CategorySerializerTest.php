<?php

declare(strict_types=1);

namespace Tests\Unit\Serializer;

use JMS\Serializer\SerializationContext;
use Tests\Helpers\EntitiesHelper;
use JsonException;

class CategorySerializerTest extends AbstractSerializerTest
{
  /**
   * @throws JsonException
   */
  public function testSerializeCategory(): void
  {

    $category = EntitiesHelper::createCategory();

    $context = new SerializationContext();
    $context->setSerializeNull(true);
    $jsonContent = $this->serializer->serialize($category, 'json', $context);

    $jsonToValidateObject = json_decode($jsonContent, false, 512, JSON_THROW_ON_ERROR);
    $jsonSchemaObject = $this->getJsonSchemaObject();

    // Provide $schemaStorage to the Validator so that references can be resolved during validation
    $jsonValidator = $this->createJsonValidator($jsonSchemaObject);

    // Do validation (use isValid() and getErrors() to check the result)
    $jsonValidator->validate($jsonToValidateObject, $jsonSchemaObject);

    // Per vedere l'output con gli errori il testa va lanciato con l'opzione --debug. Es. php bin/phpunit --debug  tests/Serializer/
    $this->assertTrue($jsonValidator->isValid(), $this->getValidationErrors($jsonValidator));
  }

  public function getSchema(): string
  {
    return <<<'JSON'
      {
        "type": "object",
        "properties": {
          "parent_id": {},
          "id": {
            "type": "string"
          },
          "name": {
            "type": "string"
          },
          "slug": {
            "type": "string"
          },
          "description": {
            "type": "string"
          }
        },
        "required": [
          "parent_id",
          "id",
          "name",
          "slug",
          "description"
        ]
      }
      JSON;
  }
}
