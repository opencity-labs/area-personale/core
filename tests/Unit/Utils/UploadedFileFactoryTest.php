<?php

declare(strict_types=1);

namespace Tests\Unit\Utils;

use App\Utils\UploadedFileFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**  @group unit */
class UploadedFileFactoryTest extends TestCase
{
  private ?UploadedFile $uploadedFile = null;

  public function setUp(): void
  {
    parent::setUp();

    $this->uploadedFile = null;
  }

  public function tearDown(): void
  {
    parent::tearDown();

    if ($this->uploadedFile) {
      @unlink($this->uploadedFile->getRealPath());
    }
  }

  public function testFromJsonStringCreatesUploadedFile(): void
  {
    $jsonContent = '{"key": "value"}';
    $fileName = 'test.json';

    $this->uploadedFile = UploadedFileFactory::fromJsonString($jsonContent, $fileName);

    $this->assertInstanceOf(UploadedFile::class, $this->uploadedFile);
    $this->assertSame('application/json', $this->uploadedFile->getMimeType());
    $this->assertSame($fileName, $this->uploadedFile->getClientOriginalName());
    $this->assertFileExists($this->uploadedFile->getPathname());
    $this->assertSame($jsonContent, file_get_contents($this->uploadedFile->getPathname()));
  }

  public function testFromStringCreatesUploadedFile(): void
  {
    $content = 'Sample text content';
    $mimeType = 'text/plain';
    $fileName = 'test.txt';

    $this->uploadedFile = UploadedFileFactory::fromString($content, $mimeType, $fileName);

    $this->assertInstanceOf(UploadedFile::class, $this->uploadedFile);
    $this->assertSame($mimeType, $this->uploadedFile->getMimeType());
    $this->assertSame($fileName, $this->uploadedFile->getClientOriginalName());
    $this->assertFileExists($this->uploadedFile->getPathname());
    $this->assertSame($content, file_get_contents($this->uploadedFile->getPathname()));
  }

  public function testFromBase64StringCreatesUploadedFileAsUri(): void
  {
    $content = 'Sample base64 content';
    $base64Content = 'data:text/plain;base64,' . base64_encode($content);
    $mimeType = 'text/plain';
    $fileName = 'test.txt';

    $this->uploadedFile = UploadedFileFactory::fromBase64String($base64Content, $mimeType, $fileName);

    $this->assertInstanceOf(UploadedFile::class, $this->uploadedFile);
    $this->assertSame($mimeType, $this->uploadedFile->getMimeType());
    $this->assertSame($fileName, $this->uploadedFile->getClientOriginalName());
    $this->assertFileExists($this->uploadedFile->getPathname());
    $this->assertSame($content, file_get_contents($this->uploadedFile->getPathname()));
  }

  public function testFromBase64StringCreatesUploadedFile(): void
  {
    $content = 'Sample base64 content';
    $base64Content = base64_encode($content);
    $mimeType = 'text/plain';
    $fileName = 'test.txt';

    $this->uploadedFile = UploadedFileFactory::fromBase64String($base64Content, $mimeType, $fileName);

    $this->assertInstanceOf(UploadedFile::class, $this->uploadedFile);
    $this->assertSame($mimeType, $this->uploadedFile->getMimeType());
    $this->assertSame($fileName, $this->uploadedFile->getClientOriginalName());
    $this->assertFileExists($this->uploadedFile->getPathname());
    $this->assertSame($content, file_get_contents($this->uploadedFile->getPathname()));
  }

  public function testFromBase64StringWithNoMimeType(): void
  {
    $content = 'Sample base64 content';
    $base64Content = 'data:text/plain;base64,' . base64_encode($content);

    $this->uploadedFile = UploadedFileFactory::fromBase64String($base64Content);

    $this->assertInstanceOf(UploadedFile::class, $this->uploadedFile);
    $this->assertSame('text/plain', $this->uploadedFile->getMimeType());
    $this->assertFileExists($this->uploadedFile->getPathname());
    $this->assertSame($content, file_get_contents($this->uploadedFile->getPathname()));
  }
}
