<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\OperatoreUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

/**
 * Class OperatoreUserTest.
 */
class OperatoreUserTest extends TestCase
{
  /** @test */
  public function testCanStoreServicesIds(): void
  {
    $operatore = new OperatoreUser();
    $operatore->setServiziAbilitati(new ArrayCollection([
      Uuid::uuid4() . '',
      Uuid::uuid4() . '',
    ]));

    $operatore->setUsername('pippo')
      ->setPlainPassword('pippo')
      ->setEmail(md5(random_int(0, 1000) . microtime()) . 'some@fake.email')
      ->setNome('a')
      ->setCognome('b')
      ->setEnabled(true);

    $this->assertInstanceOf(Collection::class, $operatore->getServiziAbilitati());
  }
}
