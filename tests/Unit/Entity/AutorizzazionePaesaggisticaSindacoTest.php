<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\AutorizzazionePaesaggisticaSindaco;
use App\Entity\Pratica;
use PHPUnit\Framework\TestCase;

class AutorizzazionePaesaggisticaSindacoTest extends TestCase
{
  public function testConstructorInitializesFields(): void
  {
    $pratica = new AutorizzazionePaesaggisticaSindaco();

    // Assert that the type is set correctly
    $this->assertEquals(Pratica::TYPE_AUTORIZZAZIONE_PAESAGGISTICA_SINDACO, $pratica->getType());

    // Assert that dematerializedForms is set correctly
    $this->assertIsArray($pratica->getDematerializedForms());
  }
}
