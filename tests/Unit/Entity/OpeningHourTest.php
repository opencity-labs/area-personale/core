<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Calendar;
use App\Entity\Meeting;
use App\Entity\OpeningHour;
use DateTime;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;
use ReflectionClass;

class OpeningHourTest extends TestCase
{
  public function testConstructor(): void
  {
    $openingHour = new OpeningHour();

    $this->assertNotNull($openingHour->getId());
    $this->assertInstanceOf(UuidInterface::class, $openingHour->getId());
    $this->assertEquals(30, $openingHour->getMeetingMinutes());
    $this->assertEquals(0, $openingHour->getIntervalMinutes());
    $this->assertFalse($openingHour->getIsModerated());
    $this->assertEquals(1, $openingHour->getMeetingQueue());
    $this->assertInstanceOf(Collection::class, $openingHour->getMeetings());
  }

  public function testSetAndGetName(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setName('Test Name');

    $this->assertEquals('Test Name', $openingHour->getName());
  }

  public function testSetAndGetCalendar(): void
  {
    $openingHour = new OpeningHour();
    $calendar = $this->createMock(Calendar::class);
    $openingHour->setCalendar($calendar);

    $this->assertSame($calendar, $openingHour->getCalendar());
  }

  public function testSetAndGetStartDate(): void
  {
    $openingHour = new OpeningHour();
    $startDate = new DateTime('2024-09-01');
    $openingHour->setStartDate($startDate);

    $this->assertSame($startDate, $openingHour->getStartDate());
  }

  public function testSetAndGetEndDate(): void
  {
    $openingHour = new OpeningHour();
    $endDate = new DateTime('2024-09-02');
    $openingHour->setEndDate($endDate);

    $this->assertSame($endDate, $openingHour->getEndDate());
  }

  public function testSetAndGetDaysOfWeek(): void
  {
    $openingHour = new OpeningHour();
    $daysOfWeek = [1, 3, 5];
    $openingHour->setDaysOfWeek($daysOfWeek);

    $this->assertEquals($daysOfWeek, $openingHour->getDaysOfWeek());
  }

  public function testSetAndGetBeginHour(): void
  {
    $openingHour = new OpeningHour();
    $beginHour = new DateTime('09:00');
    $openingHour->setBeginHour($beginHour);

    $this->assertSame($beginHour, $openingHour->getBeginHour());
  }

  public function testSetAndGetEndHour(): void
  {
    $openingHour = new OpeningHour();
    $endHour = new DateTime('17:00');
    $openingHour->setEndHour($endHour);

    $this->assertSame($endHour, $openingHour->getEndHour());
  }

  public function testSetAndGetIsModerated(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setIsModerated(true);

    $this->assertTrue($openingHour->getIsModerated());
  }

  public function testSetAndGetMeetingMinutes(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setMeetingMinutes(45);

    $this->assertEquals(45, $openingHour->getMeetingMinutes());
  }

  public function testSetAndGetIntervalMinutes(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setIntervalMinutes(15);

    $this->assertEquals(15, $openingHour->getIntervalMinutes());
  }

  public function testSetAndGetMeetingQueue(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setMeetingQueue(5);

    $this->assertEquals(5, $openingHour->getMeetingQueue());
  }

  public function testAddAndRemoveMeetings(): void
  {
    $openingHour = new OpeningHour();
    $meeting = $this->createMock(Meeting::class);
    $meeting->expects($this->once())->method('setOpeningHour')->with($openingHour);

    $openingHour->addMeetings($meeting);
    $this->assertCount(1, $openingHour->getMeetings());
    $this->assertSame($meeting, $openingHour->getMeetings()->first());

    // $meeting->expects($this->once())->method('setOpeningHour')->with(null);
    $openingHour->removeMeetings($meeting);
    $this->assertCount(0, $openingHour->getMeetings());
  }

  public function testGetDefaultName(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setDaysOfWeek([1, 2, 3]);
    $openingHour->setBeginHour(new DateTime('09:00'));
    $openingHour->setEndHour(new DateTime('17:00'));

    $expectedName = 'Lu, Ma, Me | 09:00 - 17:00';
    $this->assertEquals($expectedName, $openingHour->getDefaultName());
  }

  public function testToString(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setName('Opening Hour Test');

    $this->assertEquals('Opening Hour Test', (string) $openingHour);
  }

  public function testSetAndGetCreatedAt(): void
  {
    $openingHour = new OpeningHour();
    $date = new DateTime('2024-09-01 12:00:00');
    $openingHour->setCreatedAt($date);

    $this->assertSame($date, $openingHour->getCreatedAt());
  }

  public function testSetAndGetUpdatedAt(): void
  {
    $openingHour = new OpeningHour();
    $date = new DateTime('2024-09-01 12:00:00');
    $openingHour->setUpdatedAt($date);

    $this->assertSame($date, $openingHour->getUpdatedAt());
  }

  public function testUpdatedTimestamps(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->updatedTimestamps();

    $this->assertInstanceOf(DateTime::class, $openingHour->getCreatedAt());
    $this->assertInstanceOf(DateTime::class, $openingHour->getUpdatedAt());
    $this->assertEquals($openingHour->getCreatedAt(), $openingHour->getUpdatedAt());
  }

  public function testSetTranslatableLocale(): void
  {
    $openingHour = new OpeningHour();
    $openingHour->setTranslatableLocale('en');

    $reflection = new ReflectionClass($openingHour);
    $property = $reflection->getProperty('locale');
    $property->setAccessible(true);

    $this->assertEquals('en', $property->getValue($openingHour));
  }
}
