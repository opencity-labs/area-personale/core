<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\BuiltIn;
use App\Entity\Pratica;
use PHPUnit\Framework\TestCase;

class BuiltInTest extends TestCase
{
  public function testConstructorInitializesType(): void
  {
    $builtIn = new BuiltIn();

    // Assert that the type is initialized correctly
    $this->assertEquals(Pratica::TYPE_BUILTIN, $builtIn->getType());
  }

  public function testGetType(): void
  {
    $builtIn = new BuiltIn();

    // Assert that the getType method returns the correct type
    $this->assertEquals(Pratica::TYPE_BUILTIN, $builtIn->getType());
  }
}
