<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Document;
use App\Entity\CPSUser;
use App\Entity\Folder;
use App\Entity\Ente;
use App\Entity\Categoria;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use DateTime;

class DocumentTest extends TestCase
{
  public function testConstructorInitialization(): void
  {
    $document = new Document();

    $this->assertInstanceOf(Document::class, $document);
    $this->assertInstanceOf(UuidInterface::class, $document->getId());
    $this->assertEquals(1, $document->getVersion());
    $this->assertEquals(0, $document->getDownloadsCounter());
    $this->assertFalse($document->isStore());
    $this->assertInstanceOf(ArrayCollection::class, $document->getTopics());
    $this->assertInstanceOf(ArrayCollection::class, $document->getCorrelatedServices());
  }

  public function testSetAndGetOwner(): void
  {
    $document = new Document();
    $owner = $this->createMock(CPSUser::class);
    $owner->method('getId')->willReturn(Uuid::uuid4()->toString());

    $document->setOwner($owner);

    $this->assertEquals($owner, $document->getOwner());
    $this->assertEquals($owner->getId(), $document->getOwnerId());
  }

  public function testSetAndGetFolder(): void
  {
    $document = new Document();
    $folder = $this->createMock(Folder::class);
    $folder->method('getId')->willReturn(Uuid::uuid4()->toString());

    $document->setFolder($folder);

    $this->assertEquals($folder, $document->getFolder());
    $this->assertEquals($folder->getId(), $document->getFolderId());
  }

  public function testSetAndGetTenant(): void
  {
    $document = new Document();
    $tenant = $this->createMock(Ente::class);
    $tenant->method('getId')->willReturn(Uuid::uuid4()->toString());

    $document->setTenant($tenant);

    $this->assertEquals($tenant, $document->getTenant());
    $this->assertEquals($tenant->getId(), $document->getTenantId());
  }

  public function testSetAndGetTitle(): void
  {
    $document = new Document();
    $title = 'Test Title';

    $document->setTitle($title);

    $this->assertEquals($title, $document->getTitle());
  }

  public function testSetAndGetRecipientType(): void
  {
    $document = new Document();
    $recipientType = Document::RECIPIENT_USER;

    $document->setRecipientType($recipientType);

    $this->assertEquals($recipientType, $document->getRecipientType());
  }

  public function testSetAndGetMd5(): void
  {
    $document = new Document();
    $md5 = 'md5string';

    $document->setMd5($md5);

    $this->assertEquals($md5, $document->getMd5());
  }

  public function testSetAndGetOriginalFilename(): void
  {
    $document = new Document();
    $originalFilename = 'file.pdf';

    $document->setOriginalFilename($originalFilename);

    $this->assertEquals($originalFilename, $document->getOriginalFilename());
  }

  public function testSetAndGetMimeType(): void
  {
    $document = new Document();
    $mimeType = 'application/pdf';

    $document->setMimeType($mimeType);

    $this->assertEquals($mimeType, $document->getMimeType());
  }

  public function testSetAndGetAddress(): void
  {
    $document = new Document();
    $address = 'https://www.example.com/file.pdf';

    $document->setAddress($address);

    $this->assertEquals($address, $document->getAddress());
  }

  public function testSetAndGetDownloadLink(): void
  {
    $document = new Document();
    $downloadLink = 'https://www.example.com/download-file';

    $document->setDownloadLink($downloadLink);

    $this->assertEquals($downloadLink, $document->getDownloadLink());
  }

  public function testSetAndGetDescription(): void
  {
    $document = new Document();
    $description = 'This is a test document';

    $document->setDescription($description);

    $this->assertEquals($description, $document->getDescription());
  }

  public function testSetAndGetReadersAllowed(): void
  {
    $document = new Document();
    $readersAllowed = json_encode(['user1', 'user2']);

    $document->setReadersAllowed($readersAllowed);

    $this->assertEquals($readersAllowed, $document->getReadersAllowed());
  }

  public function testSetAndGetLastReadAt(): void
  {
    $document = new Document();
    $dateTime = new DateTime();

    $document->setLastReadAt($dateTime);

    $this->assertEquals($dateTime, $document->getLastReadAt());
  }

  public function testSetAndGetDownloadsCounter(): void
  {
    $document = new Document();
    $downloadsCounter = 5;

    $document->setDownloadsCounter($downloadsCounter);

    $this->assertEquals($downloadsCounter, $document->getDownloadsCounter());
  }

  public function testSetAndGetValidityBegin(): void
  {
    $document = new Document();
    $dateTime = new DateTime();

    $document->setValidityBegin($dateTime);

    $this->assertEquals($dateTime, $document->getValidityBegin());
  }

  public function testSetAndGetValidityEnd(): void
  {
    $document = new Document();
    $dateTime = new DateTime();

    $document->setValidityEnd($dateTime);

    $this->assertEquals($dateTime, $document->getValidityEnd());
  }

  public function testSetAndGetExpireAt(): void
  {
    $document = new Document();
    $dateTime = new DateTime();

    $document->setExpireAt($dateTime);

    $this->assertEquals($dateTime, $document->getExpireAt());
  }

  public function testSetAndGetDueDate(): void
  {
    $document = new Document();
    $dateTime = new DateTime();

    $document->setDueDate($dateTime);

    $this->assertEquals($dateTime, $document->getDueDate());
  }

  public function testSetAndGetCorrelatedServices(): void
  {
    $document = new Document();
    $service = $this->createMock(\App\Entity\Servizio::class);

    $document->setCorrelatedServices(new ArrayCollection([$service]));

    $this->assertCount(1, $document->getCorrelatedServices());
  }

  public function testSetAndGetTopics(): void
  {
    $document = new Document();
    $topic = $this->createMock(Categoria::class);
    $topic->method('getId')->willReturn(Uuid::uuid4()->toString());

    $document->setTopics(new ArrayCollection([$topic]));

    $this->assertCount(1, $document->getTopics());
    $this->assertEquals([$topic->getId()], $document->getTopicsId());
  }

  public function testSetAndGetStore(): void
  {
    $document = new Document();

    $document->setStore(true);

    $this->assertTrue($document->isStore());
  }

  public function testGetFile(): void
  {
    $document = new Document();

    $this->assertEquals('', $document->getFile());
  }
}
