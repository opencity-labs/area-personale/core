<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Servizio;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;
use Tests\Helpers\EntitiesHelper;

/** @covers \App\Entity\Servizio */
class ServizioTest extends TestCase
{
  public function testFormIOServiceCreate(): void
  {
    $ente = EntitiesHelper::createEnte();
    $category = EntitiesHelper::createCategoria();
    $servizio = EntitiesHelper::createFormIOService($ente, $category);

    $this->assertInstanceOf(UuidInterface::class, $servizio->getId());

    $this->assertEquals('Servizio di test', $servizio->getName());
    $this->assertEquals($ente->getId(), $servizio->getEnte()->getId());
    $this->assertEquals($category->getId(), $servizio->getTopics()->getId());
    $this->assertEquals('App\Entity\FormIO', $servizio->getPraticaFCQN());
    $this->assertEquals('ocsdc.form.flow.formio', $servizio->getPraticaFlowServiceName());

    // Defaults of service    $this->assertInstanceOf(Collection::class, $servizio->getFeedbackMessages());
    $this->assertInstanceOf(Collection::class, $servizio->getRecipients());
    $this->assertInstanceOf(Collection::class, $servizio->getGeographicAreas());

    $this->assertIsArray($servizio->getCoverage());

    $this->assertEquals(Servizio::STATUS_AVAILABLE, $servizio->getStatus());
    $this->assertEquals(Servizio::ACCESS_LEVEL_SPID_L2, $servizio->getAccessLevel());
    $this->assertFalse($servizio->isLoginSuggested());
    $this->assertFalse($servizio->isProtocolRequired());
    $this->assertFalse($servizio->isAllowReopening());
    $this->assertTrue($servizio->isAllowWithdraw());
    $this->assertTrue($servizio->isAllowIntegrationRequest());


    $this->assertEquals('Service lorem ipsum', $servizio->getDescription());
    $this->assertEquals('Service lorem ipsum', $servizio->getHowto());
    $this->assertEquals('Service lorem ipsum', $servizio->getWho());
    $this->assertEquals('Service lorem ipsum', $servizio->getSpecialCases());
    $this->assertEquals('Service lorem ipsum', $servizio->getMoreInfo());
    $this->assertEquals('Service lorem ipsum', $servizio->getCompilationInfo());
    $this->assertEquals('Service lorem ipsum', $servizio->getFinalIndications());

  }

  public function testServiceHasSameContentsOfServiceGroupIfShared(): void
  {
    $ente = EntitiesHelper::createEnte();
    $category = EntitiesHelper::createCategoria();
    $servizio = EntitiesHelper::createFormIOService($ente, $category);

    $serviceGroup = EntitiesHelper::createServiceGroup();
    $servizio->setServiceGroup($serviceGroup);
    $servizio->setSharedWithGroup(true);

    $this->assertEquals($serviceGroup->getDescription(), $servizio->getDescription());
    $this->assertEquals($serviceGroup->getHowto(), $servizio->getHowto());
    $this->assertEquals($serviceGroup->getWho(), $servizio->getWho());
    $this->assertEquals($serviceGroup->getSpecialCases(), $servizio->getSpecialCases());
    $this->assertEquals($serviceGroup->getMoreInfo(), $servizio->getMoreInfo());
    $this->assertEquals($serviceGroup->getTopics(), $servizio->getTopics());

  }
}
