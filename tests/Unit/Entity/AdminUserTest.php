<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\AdminUser;
use App\Entity\Ente;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

/** @covers \App\Entity\AdminUser */
class AdminUserTest extends TestCase
{
  public function testConstructor(): void
  {
    $adminUser = new AdminUser();

    // Test that the parent constructor is called and the type is set
    $this->assertEquals(User::USER_TYPE_ADMIN, $adminUser->getType());

    // Test that the role ROLE_ADMIN is added
    $this->assertSame([User::ROLE_ADMIN, User::ROLE_USER], $adminUser->getRoles());
  }

  public function testGetAndSetEnte(): void
  {
    $adminUser = new AdminUser();
    $ente = $this->createMock(Ente::class);

    $adminUser->setEnte($ente);

    // Test that the Ente is set correctly
    $this->assertSame($ente, $adminUser->getEnte());
  }
}
