<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Folder;
use App\Entity\CPSUser;
use App\Entity\Ente;
use App\Entity\Servizio;
use App\Entity\Document;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use DateTime;
use ReflectionClass;

class FolderTest extends TestCase
{
  private Folder $folder;

  protected function setUp(): void
  {
    $this->folder = new Folder();
  }

  public function testConstructor(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->folder->getId());
    $this->assertEmpty($this->folder->getCorrelatedServices());
    $this->assertEmpty($this->folder->getDocuments());
  }

  public function testSetAndGetTitle(): void
  {
    $title = 'Test Folder';
    $this->folder->setTitle($title);
    $this->assertEquals($title, $this->folder->getTitle());
  }

  public function testSetAndGetDescription(): void
  {
    $description = 'This is a test folder description';
    $this->folder->setDescription($description);
    $this->assertEquals($description, $this->folder->getDescription());
  }

  public function testSetAndGetOwner(): void
  {
    $owner = $this->createMock(CPSUser::class);
    $owner->method('getId')->willReturn(Uuid::uuid4()->toString());

    $this->folder->setOwner($owner);
    $this->assertSame($owner, $this->folder->getOwner());
    $this->assertEquals($owner->getId(), $this->folder->getOwnerId());
  }

  public function testSetAndGetTenant(): void
  {
    $tenant = $this->createMock(Ente::class);
    $tenant->method('getId')->willReturn(Uuid::uuid4()->toString());

    $this->folder->setTenant($tenant);
    $this->assertSame($tenant, $this->folder->getTenant());
    $this->assertEquals($tenant->getId(), $this->folder->getTenantId());
  }

  public function testSetAndGetCorrelatedServices(): void
  {
    $service1 = $this->createMock(Servizio::class);
    $service1->method('getId')->willReturn(Uuid::uuid4()->toString());
    $service2 = $this->createMock(Servizio::class);
    $service2->method('getId')->willReturn(Uuid::uuid4()->toString());

    $this->folder->serCorrelatedServices([$service1, $service2]);
    $this->assertCount(2, $this->folder->getCorrelatedServices());
    $this->assertCount(2, $this->folder->getServicesId());
    $this->assertEquals([$service1->getId(), $service2->getId()], $this->folder->getServicesId());
  }

  public function testGetDocuments(): void
  {
    $document = $this->createMock(Document::class);
    $reflection = new ReflectionClass($this->folder);
    $property = $reflection->getProperty('documents');
    $property->setAccessible(true);
    $property->setValue($this->folder, new \Doctrine\Common\Collections\ArrayCollection([$document]));

    $this->assertCount(1, $this->folder->getDocuments());
    $this->assertSame($document, $this->folder->getDocuments()->first());
  }

  public function testSetAndGetCreatedAt(): void
  {
    $dateTime = new DateTime();
    $this->folder->setCreatedAt($dateTime);
    $this->assertEquals($dateTime, $this->folder->getCreatedAt());
  }

  public function testSetAndGetUpdatedAt(): void
  {
    $dateTime = new DateTime();
    $this->folder->setUpdatedAt($dateTime);
    $this->assertEquals($dateTime, $this->folder->getUpdatedAt());
  }

  public function testUpdatedTimestamps(): void
  {
    $this->folder->updatedTimestamps();
    $this->assertInstanceOf(DateTime::class, $this->folder->getCreatedAt());
    $this->assertInstanceOf(DateTime::class, $this->folder->getUpdatedAt());
    $this->assertEquals($this->folder->getCreatedAt(), $this->folder->getUpdatedAt());

    sleep(1);
    $this->folder->updatedTimestamps();
    $this->assertGreaterThan($this->folder->getCreatedAt(), $this->folder->getUpdatedAt());
  }

  public function testToString(): void
  {
    $this->assertEquals((string) $this->folder->getId(), $this->folder->__toString());
  }
}
