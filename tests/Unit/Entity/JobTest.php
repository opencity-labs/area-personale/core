<?php

declare(strict_types=1);

namespace Tests\Unit\Entity;

use App\Entity\Ente;
use App\Entity\Job;
use DateTime;
use DomainException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class JobTest extends TestCase
{
  private Job $job;

  protected function setUp(): void
  {
    $this->job = new Job();
  }

  public function testConstructor(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->job->getId());
  }

  public function testNamedConstructorNew(): void
  {
    $tenant = $this->createMock(Ente::class);
    $jobType = 'simpleJobType';

    $job = Job::new($tenant, $jobType);

    $this->assertEquals(Job::STATUS_PENDING, $job->getStatus());
    $this->assertSame($tenant, $job->getTenant());
    $this->assertEquals($jobType, $job->getType());
  }

  public function testCanBeRun(): void
  {
    // Caso 1: STATUS_PENDING, startedAt = null
    $this->job->setStatus(Job::STATUS_PENDING);
    $this->job->setStartedAt(null);
    $this->assertTrue($this->job->canBeRun());

    // Caso 2: STATUS_PENDING, startedAt = DateTime
    $this->job->setStatus(Job::STATUS_PENDING);
    $this->job->setStartedAt(new DateTime());
    $this->assertFalse($this->job->canBeRun());

    // Caso 4: STATUS_SUSPENDED, startedAt = DateTime
    $this->job->setStatus(Job::STAUS_SUSPENDED);
    $this->job->setStartedAt(new DateTime());
    $this->assertTrue($this->job->canBeRun());

    // Caso 5: STATUS_RUNNING
    $this->job->setStatus(Job::STATUS_RUNNING);
    $this->job->setStartedAt(null);
    $this->assertFalse($this->job->canBeRun());
  }

  public function testRun(): void
  {
    $this->job->setStatus(Job::STATUS_PENDING);
    $this->job->setStartedAt(null);

    $this->job->run();

    $this->assertEquals(Job::STATUS_RUNNING, $this->job->getStatus());
    $this->assertInstanceOf(DateTime::class, $this->job->getStartedAt());
  }

  public function testRunWithInvalidStateShouldThrowException(): void
  {
    $this->job->setStatus(Job::STATUS_RUNNING);
    $this->job->setStartedAt(new DateTime());

    $this->expectException(DomainException::class);
    $this->expectExceptionMessage('Job cannot be started');

    $this->job->run();
  }

  public function testSuspend(): void
  {
    $this->job->setStatus(Job::STATUS_PENDING);
    $this->job->run();

    $this->job->suspend();

    $this->assertEquals(Job::STAUS_SUSPENDED, $this->job->getStatus());
  }

  public function testSuspendNotThrowExceptionIfAlreadySuspended(): void
  {
    $this->job->setStatus(Job::STAUS_SUSPENDED);
    $this->job->setStartedAt(new DateTime());

    $this->job->suspend();

    $this->assertEquals(Job::STAUS_SUSPENDED, $this->job->getStatus());
  }

  public function testJobCannotBeSuspendIfNotRun(): void
  {
    // Assicuriamoci che lo stato iniziale non sia SUSPENDED
    $this->job->setStatus(Job::STATUS_PENDING);
    $this->assertNotEquals(Job::STAUS_SUSPENDED, $this->job->getStatus());

    $this->expectException(DomainException::class);
    $this->expectExceptionMessage('Job cannot be suspended');

    $this->job->suspend();
  }

  public function testFailed(): void
  {
    $failedAt = new DateTime();
    $outputMessage = 'output message';
    $this->job->failed($outputMessage, $failedAt);

    $this->assertEquals(Job::STATUS_FAILED, $this->job->getStatus());
    $this->assertEquals($outputMessage, $this->job->getOutput());
    $this->assertEquals($failedAt, $this->job->getTerminatedAt());
  }

  public function testFinished(): void
  {
    $finishedAt = new DateTime();
    $outputMessage = 'output message';
    $this->job->finished($outputMessage, $finishedAt);

    $this->assertEquals(Job::STATUS_FINISHED, $this->job->getStatus());
    $this->assertEquals($outputMessage, $this->job->getOutput());
    $this->assertEquals($finishedAt, $this->job->getTerminatedAt());
  }

  public function testFinishedWithError(): void
  {
    $finishedAt = new DateTime();
    $outputMessage = 'output message';
    $this->job->finishedWithError($outputMessage, $finishedAt);

    $this->assertEquals(Job::STATUS_FAILED, $this->job->getStatus());
    $this->assertEquals($outputMessage, $this->job->getOutput());
    $this->assertEquals($finishedAt, $this->job->getTerminatedAt());
  }

  public function testGetId(): void
  {
    $this->assertInstanceOf(UuidInterface::class, $this->job->getId());
  }

  public function testSetAndGetType(): void
  {
    $type = 'test_job';
    $this->job->setType($type);
    $this->assertEquals($type, $this->job->getType());
  }

  public function testSetAndGetStatus(): void
  {
    $status = Job::STATUS_PENDING;
    $this->job->setStatus($status);
    $this->assertEquals($status, $this->job->getStatus());
  }

  public function testSetAndGetArgs(): void
  {
    $args = ['key' => 'value'];
    $this->job->setArgs(json_encode($args));
    $this->assertEquals(json_encode($args), $this->job->getArgs());
    $this->assertEquals($args, $this->job->getArgsAsArray());
  }

  public function testSetAndGetTenant(): void
  {
    $tenant = $this->createMock(Ente::class);
    $this->job->setTenant($tenant);
    $this->assertSame($tenant, $this->job->getTenant());
  }

  public function testSetAndGetFile(): void
  {
    $file = $this->createMock(UploadedFile::class);
    $file->method('getClientOriginalName')->willReturn('test.txt');

    $this->job->setFile($file);
    $this->assertSame($file, $this->job->getFile());
    $this->assertEquals('test.txt', $this->job->getOriginalFileName());
  }

  public function testSetAndGetAttachment(): void
  {
    $this->job->setOriginalFileName('test.txt');
    $this->job->setMimeType('text/plain');

    $expected = [
      'name' => 'test.txt',
      'mime_type' => 'text/plain',
    ];

    $this->assertEquals($expected, $this->job->getAttachment());
  }

  public function testSetAndGetFileName(): void
  {
    $fileName = 'test_file.txt';
    $this->job->setFileName($fileName);
    $this->assertEquals($fileName, $this->job->getFileName());
  }

  public function testSetAndGetFileSize(): void
  {
    $fileSize = 1024;
    $this->job->setFileSize($fileSize);
    $this->assertEquals($fileSize, $this->job->getFileSize());
  }

  public function testSetAndGetMimeType(): void
  {
    $mimeType = 'text/plain';
    $this->job->setMimeType($mimeType);
    $this->assertEquals($mimeType, $this->job->getMimeType());
  }

  public function testSetAndGetMetadata(): void
  {
    $metadata = ['key' => 'value'];
    $this->job->setMetadata($metadata);
    $this->assertEquals($metadata, $this->job->getMetadata());
  }

  public function testSetAndGetOutput(): void
  {
    $output = 'Job output';
    $this->job->setOutput($output);
    $this->assertEquals($output, $this->job->getOutput());
  }

  public function testSetAndGetRunningOutput(): void
  {
    $runningOutput = 'Running job output';
    $this->job->setRunningOutput($runningOutput);
    $this->assertEquals($runningOutput, $this->job->getRunningOutput());
  }

  public function testSetAndGetStartedAt(): void
  {
    $startedAt = new DateTime();
    $this->job->setStartedAt($startedAt);
    $this->assertEquals($startedAt, $this->job->getStartedAt());
  }

  public function testSetAndGetTerminatedAt(): void
  {
    $terminatedAt = new DateTime();
    $this->job->setTerminatedAt($terminatedAt);
    $this->assertEquals($terminatedAt, $this->job->getTerminatedAt());
  }

  public function testSetAndGetErrors(): void
  {
    $errors = ['error1', 'error2'];
    $this->job->setErrors($errors);
    $this->assertEquals($errors, $this->job->getErrors());
  }

  public function testIsNonSuccessfulFinalState(): void
  {
    $this->assertTrue(Job::isNonSuccessfulFinalState(Job::STATUS_CANCELLED));
    $this->assertTrue(Job::isNonSuccessfulFinalState(Job::STATUS_FAILED));
    $this->assertFalse(Job::isNonSuccessfulFinalState(Job::STATUS_FINISHED));
    $this->assertFalse(Job::isNonSuccessfulFinalState(Job::STATUS_PENDING));
    $this->assertFalse(Job::isNonSuccessfulFinalState(Job::STATUS_RUNNING));
  }

  public function testGetStates(): void
  {
    $expectedStates = [
      Job::STATUS_PENDING,
      Job::STATUS_CANCELLED,
      Job::STATUS_RUNNING,
      Job::STATUS_FINISHED,
      Job::STATUS_FAILED,
    ];
    $this->assertEquals($expectedStates, Job::getStates());
  }

  public function testTimestampableTraitMethods(): void
  {
    $now = new DateTime();

    $this->job->setCreatedAt($now);
    $this->assertEquals($now, $this->job->getCreatedAt());

    $this->job->setUpdatedAt($now);
    $this->assertEquals($now, $this->job->getUpdatedAt());
  }

  public function testFluentInterfaces(): void
  {
    $this->assertSame($this->job, $this->job->setType('test'));
    $this->assertSame($this->job, $this->job->setStatus(Job::STATUS_PENDING));
    $this->assertSame($this->job, $this->job->setArgs([]));
    $this->assertSame($this->job, $this->job->setTenant($this->createMock(Ente::class)));
    $this->assertSame($this->job, $this->job->setFile(null));
    $this->assertSame($this->job, $this->job->setMetadata([]));
    $this->assertSame($this->job, $this->job->setOutput(''));
    $this->assertSame($this->job, $this->job->setRunningOutput(''));
    $this->assertSame($this->job, $this->job->setStartedAt(new DateTime()));
    $this->assertSame($this->job, $this->job->setTerminatedAt(new DateTime()));
  }
}
