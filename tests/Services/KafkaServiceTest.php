<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Services\KafkaService;
use Tests\Base\AbstractAppTestCase;
use Tests\Helpers\EntitiesHelper;

class KafkaServiceTest extends AbstractAppTestCase
{
  public function testServiceExists(): void
  {
    $kafkaService = $this->container->get('ocsdc.kafka_service');
    $this->assertNotNull($kafkaService);
    $this->assertEquals(KafkaService::class, get_class($kafkaService));
  }

  public function testGenerateServiceMessage(): void
  {
    $kafkaService = $this->container->get('ocsdc.kafka_service');
    $topics = $this->container->getParameter('kafka_topics');

    $ente = EntitiesHelper::createEnte();
    $category = EntitiesHelper::createCategoria();
    $servizio = EntitiesHelper::createFormIOService($ente, $category);

    $message = $kafkaService->generateMessage($servizio);

    $this->assertEquals($topics['services'], $message['topic']);
    $this->assertTrue(isset($message['data']) && !empty($message['data']));

    $data = $message['data'];

    $this->assertEquals($servizio->getId(), $data['id']);

  }
}
