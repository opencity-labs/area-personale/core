<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Entity\Allegato;
use App\Services\DirectoryNamerService;
use Tests\Base\AbstractAppTestCase;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

/**
 * Class AllegatiDirectoryNamerTest.
 */
class DirectoryNamerServiceTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();
  }

  public function testClassExists(): void
  {
    $this->assertNotNull(new DirectoryNamerService());
  }

  public function testServiceExists(): void
  {
    $directoryNamer = $this->container->get('ocsdc.allegati.directory_namer');
    $this->assertInstanceOf(DirectoryNamerService::class, $directoryNamer);
    $this->assertInstanceOf(DirectoryNamerInterface::class, $directoryNamer);
  }

  public function testDirectoryNamerReturnsCPSUserIdIfObjectIsAllegatoClass(): void
  {
    $user = $this->createCPSUser();
    $allegato = new Allegato();
    $allegato->setOwner($user);

    $mockedMappings = $this->getMockBuilder(PropertyMapping::class)->disableOriginalConstructor()->getMock();

    $directoryNamer = $this->container->get('ocsdc.allegati.directory_namer');
    $directoryName = $directoryNamer->directoryName($allegato, $mockedMappings);
    $this->assertEquals($user->getId(), $directoryName);
  }
}
