<?php

declare(strict_types=1);

namespace Tests\Services;

use App\Entity\Allegato;
use App\Entity\Pratica;
use App\Entity\ScheduledAction;
use App\Entity\User;
use App\Protocollo\PiTreProtocolloHandler;
use App\Services\DelayedProtocolloService;
use App\Services\ProtocolloService;
use App\Services\ScheduleActionService;
use Tests\Base\AbstractAppTestCase;

class DelayedProtocolloServiceTest extends AbstractAppTestCase
{
  public function setUp(): void
  {
    parent::setUp();
    $this->cleanDb(ScheduledAction::class);
    $this->cleanDb(Allegato::class);
    $this->cleanDb(Pratica::class);
    $this->cleanDb(User::class);
  }

  /**
   * @test
   */
  public function testDelayedProtocolloService(): void
  {
    $user = $this->createCPSUser();
    $pratica = $this->createSubmittedPraticaForUser($user);

    $allegati = $this->setupNeededAllegatiForAllInvolvedUsers(3, $user);
    foreach ($allegati as $allegato) {
      $pratica->addAllegato($allegato);
    }

    $this->getMockDelayedProtocollo()->protocollaPratica($pratica);

    $repo = $this->em->getRepository('App:ScheduledAction');
    $this->assertCount(1, $repo->findAll());
  }

  /**
   * @test
   */
  public function testDelayedProtocolloServiceCannotResend(): void
  {
    $this->expectException(\App\ScheduledAction\Exception\AlreadyScheduledException::class);

    $user = $this->createCPSUser();
    $pratica = $this->createSubmittedPraticaForUser($user);

    $this->getMockDelayedProtocollo()->protocollaPratica($pratica);

    $this->getMockDelayedProtocollo()->protocollaPratica($pratica);

  }

  /**
   * @test
   */
  public function testDelayedProtocolloServiceSendAllegati(): void
  {
    $user = $this->createCPSUser();
    $pratica = $this->createSubmittedPraticaForUser($user);

    $allegati = $this->setupNeededAllegatiForAllInvolvedUsers(3, $user);
    foreach ($allegati as $allegato) {
      $pratica->addAllegato($allegato);
    }

    $this->getMockDelayedProtocollo()->protocollaPratica($pratica);

    $repo = $this->em->getRepository('App:ScheduledAction');
    $this->assertCount(1, $repo->findAll());

    $this->executeCron(5); // pratica + 3 allegati

    $repo = $this->em->getRepository('App:ScheduledAction');
    $this->assertCount(0, $repo->findAll());
    $this->assertEquals(Pratica::STATUS_REGISTERED, $pratica->getStatus());
    $this->assertCount(4, $pratica->getNumeriProtocollo());


    $allegati = $this->setupNeededAllegatiForAllInvolvedUsers(3, $user);
    foreach ($allegati as $allegato) {
      $pratica->addAllegato($allegato);
      $this->getMockDelayedProtocollo()->protocollaAllegato($pratica, $allegato);
    }

    $repo = $this->em->getRepository('App:ScheduledAction');
    $this->assertCount(3, $repo->findAll());

    $this->executeCron(3); // tre allegati
    $this->assertCount(7, $pratica->getNumeriProtocollo());
  }

  private function executeCron($expectedRemoteCalls): void
  {
    $responses = [];
    for ($i = 1; $i <= $expectedRemoteCalls; ++$i) {
      $responses[] = $this->getPiTreSuccessResponse();
    }

    $service = $this->getMockDelayedProtocollo($responses);

    $scheduleService = $this->container->get('ocsdc.schedule_action_service');
    /** @var ScheduledAction[] $actions */
    $actions = $scheduleService->getActions();
    foreach ($actions as $action) {
      if ('ocsdc.protocollo' == $action->getService()) {
        $service->executeScheduledAction($action);
        $scheduleService->markAsDone($action);
      }
    }
    $scheduleService->done();
  }

  private function getMockDelayedProtocollo($responses = [])
  {
    return
      new DelayedProtocolloService(
        $this->getMockProtocollo($responses),
        $this->em,
        $this->getMockLogger(),
        $this->getMockScheduleActionService()
      );

  }

  private function getMockScheduleActionService()
  {
    return new ScheduleActionService(
      $this->em,
      $this->getMockLogger()
    );
  }

  private function getMockProtocollo($responses = [], $dispatcher = null)
  {
    if (!$dispatcher) {
      $dispatcher = $this->container->get('event_dispatcher');
    }

    return
      new ProtocolloService(
        new PiTreProtocolloHandler($this->getMockGuzzleClient($responses), 'comune-di-tre-ville'),
        $this->em,
        $this->getMockLogger(),
        $dispatcher
      );
  }
}
