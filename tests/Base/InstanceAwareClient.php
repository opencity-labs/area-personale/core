<?php

declare(strict_types=1);

namespace Tests\Base;

use Symfony\Bundle\FrameworkBundle\Client;
use AppTestKernel;

class InstanceAwareClient extends Client
{
  /**
   * @var AppTestKernel
   */
  protected $kernel;

  public function request($method, $uri, array $parameters = [], array $files = [], array $server = [], $content = null, $changeHistory = true)
  {
    $prefix = '/' . $this->kernel->getIdentifier();
    if (false === strpos($uri, $prefix)) {
      $uri = $prefix . $uri;
    }

    return parent::request($method, $uri, $parameters, $files, $server, $content, $changeHistory);
  }
}
