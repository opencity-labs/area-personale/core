<?php

namespace App\DataTable\Traits;

use Symfony\Component\HttpFoundation\Request;

trait FiltersTrait
{
  private static function getFiltersFromRequest(Request $request): array
  {

    $filters = [];
    foreach ($request->request->all()['columns'] ?? [] as $key => $search) {
      if (!empty(trim($search['search']['value']))) {
        $filters['filters'][$key] = $search['search']['value'];
      }
    }

    return $filters;
  }
}
