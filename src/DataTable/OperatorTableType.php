<?php

namespace App\DataTable;


use App\Entity\OperatoreUser;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class OperatorTableType implements DataTableTypeInterface
{


  private AuthorizationCheckerInterface $authorizationChecker;

  public function __construct(AuthorizationCheckerInterface $authorizationChecker)
  {
    $this->authorizationChecker = $authorizationChecker;
  }

  public function configure(DataTable $dataTable, array $options): void
  {
    $dataTable
      ->add('nome', TwigColumn::class, [
        'label' => 'general.nome',
        'orderable' => true,
        'searchable' => true,
        'operator' => 'LIKE',
        'rightExpr' => function ($value) {
          return '%' . $value . '%';
        },
        'template' => 'Admin/table/operators/_fullname.html.twig',
      ])
      ->add('username', TextColumn::class, [
        'label' => 'general.username',
        'orderable' => true,
        'searchable' => true
      ])
      ->add('email', TextColumn::class, [
        'label' => 'general.email',
        'orderable' => true,
        'searchable' => true
      ])
      ->add('roles', TwigColumn::class, [
        'label' => 'general.role',
        'className' => 'text-center',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/operators/_role.html.twig',
      ])
      ->add('enabled', TwigColumn::class, [
        'label' => 'enabled',
        'className' => 'text-center',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/operators/_boolean.html.twig',
      ]);
    if ($this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
      $dataTable
        ->add('systemUser', TwigColumn::class, [
          'label' => 'general.api',
          'className' => 'text-center',
          'orderable' => false,
          'searchable' => false,
          'template' => 'Admin/table/operators/_boolean.html.twig',
        ])
        ->add('console', TwigColumn::class, [
          'label' => 'general.console',
          'className' => 'text-center',
          'orderable' => false,
          'searchable' => false,
          'template' => 'Admin/table/operators/_console.html.twig',
        ]);
    }
    $dataTable
      ->add('actions', TwigColumn::class, [
        'label' => '',
        'className' => 'text-right',
        'orderable' => false,
        'searchable' => false,
        'template' => 'Admin/table/operators/_actions.html.twig',
      ])
      ->createAdapter(ORMAdapter::class, [
        'entity' => OperatoreUser::class,
        'query' => function (QueryBuilder $builder) use ($options): void {

          $qb = $builder
            ->select('operator')
            ->from(OperatoreUser::class, 'operator');

          if (!$this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
            $qb->andWhere('operator.systemUser = false');
          }
          $qb->getQuery()->getSQL();
        },

      ])
      ->addOrderBy('nome', DataTable::SORT_ASCENDING);
  }
}
