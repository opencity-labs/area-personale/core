<?php


namespace App\BackOffice;


use App\Entity\Calendar;
use App\Entity\CPSUser;
use App\Entity\Meeting;
use App\Entity\Pratica;
use App\Services\MeetingService;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\AbstractList;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\OpeningHour;

class CalendarsBackOffice implements BackOfficeInterface
{
  public const IDENTIFIER = 'calendars';

  public const NAME = 'nav.backoffices.prenotazione_appuntamenti';

  public const PATH = 'operatori_calendars_index';

  private EntityManagerInterface $em;

  private MeetingService $meetingService;

  private TranslatorInterface $translator;

  private LoggerInterface $logger;

  private array $errors = [];

  private array $required_fields = [
    'applicant_meeting' => array(
      "applicant.data.completename.data.name",
      "applicant.data.completename.data.surname",
      "calendar"
    )
  ];

  private array $allowedActivationPoints = [
    Pratica::STATUS_PRE_SUBMIT,
    Pratica::STATUS_SUBMITTED,
    Pratica::STATUS_REGISTERED,
    Pratica::STATUS_COMPLETE
  ];

  public function __construct(EntityManagerInterface $em, MeetingService $meetingService, TranslatorInterface $translator, LoggerInterface $logger)
  {
    $this->translator = $translator;
    $this->meetingService = $meetingService;
    $this->em = $em;
    $this->logger = $logger;
  }


  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return $this->translator->trans(self::NAME);
  }

  public function getPath(): string
  {
    return self::PATH;
  }

  public function getRequiredFields(): array
  {
    return $this->required_fields;
  }

  public function execute($data): void
  {
    if ($data instanceof Pratica && is_callable([$data, 'getDematerializedForms'])) {
      $status = $data->getStatus();
      $integrations = $data->getServizio()->getIntegrations();

      // Create meetings on activation point
      if (isset($integrations[$status]) && $integrations[$status] == get_class($this)) {
        $this->createMeetingsFromPratica($data);
      }

      $this->handleMeetingsStatusBasedOnApplication($data);

    }
  }

  public function checkRequiredFields($schema): ?array
  {
    $errors = [];
    foreach ($this->getRequiredFields() as $key => $requiredFields) {
      foreach ($requiredFields as $field) {
        if (!array_key_exists($field . '.label', $schema)) {
          $errors[$key][] = $this->translator->trans('backoffice.integration.missing_field', ['field' => $field]);
        }
      }
      if (!array_key_exists($key, $errors)) {
        return null;
      }
    }
    return $errors;
  }

  public function getAllowedActivationPoints(): array
  {
    return $this->allowedActivationPoints;
  }

  public function isAllowedActivationPoint($activationPoint): bool
  {
    return in_array($activationPoint, $this->allowedActivationPoints);
  }

  private function createMeetingsFromPratica(Pratica $pratica): void
  {
    $submission = $pratica->getDematerializedForms();
    $integrationType = $this->getIntegrationType($submission);

    if (!$integrationType) {
      $this->logger->error($this->translator->trans('backoffice.integration.fields_error'));
      return;
    }

    $calendarsData = $this->parseCalendarsData($submission['data']['calendar']);

    if (!empty($calendarsData)) {
      $meetingGeneralData = $this->getMeetingGeneralData($pratica);
      foreach ($calendarsData as $calendarData) {
        $this->createMeeting(array_merge($meetingGeneralData, $calendarData), $pratica);
      }
    }
  }

  private function parseCalendarsData($data): array
  {
    /*
       Il payload di calendar è un array --> stiamo utilizzando il nuovo metodo di invio
        [{
        "date": "YYYY-MM-DD",
          "slot": "HH:ii-HH:ii",
          "calendar_id": "uuid",
          "meeting_id": "uuid",
          "opening_hour_id": "uuid",
       }]
    */
    if (is_array($data)) {
      if (isset($data['calendar_id'])) {
        return [$this->getMeetingData($data)];
      } else {
        return $this->getMeetingDataFromArray($data);
      }
    }

    // Il payload di calendar è una stringa --> stiamo utilizzando il vecchio metodo
    // 2024-09-10 @ 10:30-11:00 (calendar_id#meeting_id#opening_hour_id)
    if (is_string($data)) {
      return $this->getMeetingDataFromString($data);
    }
    return [];
  }

  // Recupera in base alla submission l'integrazione da applicare (da rivedere)
  private function getIntegrationType($submission): ?string
  {
    $data = $submission['flattened'];
    $integrationType = null;
    if (isset($data['applicant.data.completename.data.name']) && isset($data['applicant.data.completename.data.surname']) && isset($submission['data']['calendar']))
    {
      $integrationType = 'applicant_meeting';
    }
    return $integrationType;
  }

  private function getMeetingGeneralData(Pratica $pratica): array
  {
    $submission = $pratica->getDematerializedForms();
    $flattenedSubmission = $submission['flattened'];
    $user = $pratica->getUser();

    $data = [
      "email" => $flattenedSubmission['applicant.data.email_address'],
      "name" => $user->getFullName(),
      "phone_number" => null,
      "fiscal_code" => $flattenedSubmission['applicant.data.fiscal_code.data.fiscal_code'],
      "user" => $user,
      "user_message" => $flattenedSubmission['user_message'] ?? null,
      "location" => $flattenedSubmission['location'] ?? null,
      "reason" => $flattenedSubmission['service'] ?? $pratica->getServizio()->getFullName(),
      "locale" => $pratica->getLocale()
    ];

    if (isset($flattenedSubmission['applicant.data.phone_number']) && $flattenedSubmission['applicant.data.phone_number']) {
      $data["phone_number"] = $flattenedSubmission['applicant.data.phone_number'];
    } else if (isset($flattenedSubmission['applicant.data.cell_number'])) {
      $data["phone_number"] = $flattenedSubmission['applicant.data.cell_number'];
    }

    return $data;
  }

  private function getMeetingDataFromString(string $submission): array
  {
    $result = [];
    // Get start-end datetime
    $tmp = explode('@', $submission);
    $date = trim($tmp[0]);
    $time = $tmp[1];
    $time = trim(explode('(', $time)[0]);
    $tmp = explode('-', $time);
    $startTime = trim($tmp[0]);
    $endTime = trim($tmp[1]);
    $start = \DateTime::createFromFormat('d/m/Y:H:i', $date . ':' . $startTime);
    if (!$start) {
      $start = \DateTime::createFromFormat('Y-m-d:H:i', $date . ':' . $startTime);
    }
    $end = \DateTime::createFromFormat('d/m/Y:H:i', $date . ':' . $endTime);
    if (!$end) {
      $end = \DateTime::createFromFormat('Y-m-d:H:i', $date . ':' . $endTime);
    }

    // Extract meeting id from calendar string
    preg_match_all("/\(([^)]*)\)/", $submission, $matches);
    $meetingData = explode("#", $matches[1][0]);

    $result []= [
      "from_time" => $start,
      "to_time" => $end,
      "calendar" => trim($meetingData[0]),
      "meeting_id" => trim($meetingData[1]),
      "opening_hour" => isset($meetingData[2]) ? trim($meetingData[2]) : null,
    ];
    return $result;
  }

  private function getMeetingDataFromArray(array $submission): array
  {

    $meetingsData = [];
    if (empty($submission)) {
      return $meetingsData;
    }

    foreach ($submission as $value) {
      $meetingsData []= $this->getMeetingData($value);
    }
    return $meetingsData;
  }

  private function getMeetingData(array $data): array
  {

    $date = $data['date'];
    $time = explode('-', $data['slot']);
    $startTime = trim($time[0]);
    $endTime = trim($time[1]);

    $start = \DateTime::createFromFormat('d-m-Y:H:i', $date . ':' . $startTime);
    if (!$start) {
      $start = \DateTime::createFromFormat('Y-m-d:H:i', $date . ':' . $startTime);
    }
    $end = \DateTime::createFromFormat('d-m-Y:H:i', $date . ':' . $endTime);
    if (!$end) {
      $end = \DateTime::createFromFormat('Y-m-d:H:i', $date . ':' . $endTime);
    }

    return [
      "from_time" => $start,
      "to_time" => $end,
      "calendar" => $data['calendar_id'],
      "meeting_id" => $data['meeting_id'],
      "opening_hour" => $data['opening_hour_id'],
    ];
  }

  private function createMeeting(array $meetingData, Pratica $pratica): void
  {

    $calendar = null;
    if ($meetingData['calendar'] && Uuid::isValid($meetingData['calendar'])) {
      $calendar = $this->em->getRepository(Calendar::class)->find($meetingData['calendar']);
    }

    if (!$calendar instanceof Calendar) {
        $this->logger->error('Calendar not found during meeting creation in CalendarBackoffice for application: ' . $pratica->getId(), ['meeting_data' => $meetingData]);
        $this->errors []= $this->translator->trans('backoffice.integration.calendars.calendar_error', ['calendar_id' => $meetingData['calendar']]);
        return;
    }

    // Check contacts. At least one among tel and email is required
    if (!($meetingData['phone_number'] || $meetingData['email'])) {
      $this->logger->error('Contacts not found during meeting creation in CalendarBackoffice for application: ' . $pratica->getId(), ['meeting_data' => $meetingData]);
      $this->errors []= $this->translator->trans('backoffice.integration.calendars.missing_contacts');
      return;
    }

    try {
      $meeting = null;
      if ($meetingData['meeting_id'] && Uuid::isValid($meetingData['meeting_id'])) {
        $meeting = $this->em->getRepository(Meeting::class)->find($meetingData['meeting_id']);
      }

      if (!$meeting instanceof Meeting) {
        $meeting = new Meeting();
      }

      $meeting
        ->setEmail($meetingData['email'])
        ->setName($meetingData['name'])
        ->setPhoneNumber($meetingData['phone_number'])
        ->setFiscalCode($meetingData['fiscal_code'])
        ->setUser($meetingData['user'])
        ->setUserMessage($meetingData['user_message'] ?? null)
        ->setReason($meetingData['reason'])
        ->setFromTime($meetingData['from_time'])
        ->setToTime($meetingData['to_time'])
        ->setCalendar($calendar)
        ->setLocation($meetingData['location'] ?? $calendar->getLocation())
        ->setLocale($meetingData['locale']);


      $openingHour = $meetingData['opening_hour'] ? $this->em->getRepository(OpeningHour::class)->find($meetingData['opening_hour']) : null;
      if ($openingHour) {
        $meeting->setOpeningHour($openingHour);
      }

      if (!empty($this->meetingService->getMeetingErrors($meeting))) {
        // Send email
        $this->meetingService->sendEmailUnavailableMeeting($meeting);
        $this->logger->error('Invalid slot during meeting creation in CalendarBackoffice for application: ' . $pratica->getId(), ['meeting_data' => $meetingData]);
        $this->errors []= $this->translator->trans('backoffice.integration.calendars.invalid_slot');
        return;
      }

      if ($meeting->getOpeningHour()->getIsModerated() || $meeting->getCalendar()->getIsModerated()) {
        $meeting->setStatus(Meeting::STATUS_PENDING);
      } else {
        $meeting->setStatus(Meeting::STATUS_APPROVED);
      }

      $meeting->addApplication($pratica);
      $pratica->addMeeting($meeting);
      $this->meetingService->save($meeting);


    } catch (\Exception $exception) {
      $this->logger->error($this->translator->trans('backoffice.integration.calendars.save_meeting_error') . ' - ' . $exception->getMessage());
      ['error' => $this->translator->trans('backoffice.integration.calendars.save_meeting_error')];
      return;
    }
  }

  private function handleMeetingsStatusBasedOnApplication(Pratica $pratica): void
  {
    $meetings = $pratica->getMeetings()->toArray();
    if (empty($meetings)) {
      return;
    }
    $status = $pratica->getStatus();

    try {
      foreach ($meetings as $meeting) {
        if ($meeting->getFromTime() < new \DateTime()) {
          // Non effettuo modifiche sullo stato di appuntamenti passati
          continue;
        }
        switch ($status) {
          case Pratica::STATUS_WITHDRAW:
          case Pratica::STATUS_REVOKED:
            if (in_array($meeting->getStatus(), [Meeting::STATUS_APPROVED, Meeting::STATUS_PENDING])) {
              $meeting->setStatus(Meeting::STATUS_CANCELLED);
            }
            break;
          case Pratica::STATUS_PAYMENT_PENDING:
            if ($meeting->getStatus() == Meeting::STATUS_DRAFT) {
              $currentExpiration = clone $meeting->getDraftExpiration() ?? new \DateTime();
              $meeting->setDraftExpiration($currentExpiration->modify('+' . ($meeting->getCalendar()->getDraftsDurationIncrement() ?? Calendar::DEFAULT_DRAFT_INCREMENT) . 'seconds'));
            }
            break;
          case Pratica::STATUS_PAYMENT_ERROR:
          case Pratica::STATUS_CANCELLED:
            if (in_array($meeting->getStatus(), [Meeting::STATUS_APPROVED, Meeting::STATUS_PENDING])) {
              $meeting->setStatus(Meeting::STATUS_REFUSED);
            }
            break;
          case Pratica::STATUS_COMPLETE:
            if ($meeting->getStatus() == Meeting::STATUS_PENDING) {
              $meeting->setStatus(Meeting::STATUS_APPROVED);
            }
            break;
        }
        $this->meetingService->save($meeting, false);
      }
      $this->em->flush();
    } catch (\Exception $e) {
      $this->logger->error($this->translator->trans('backoffice.integration.calendars.save_meeting_error') . ' - ' . $e->getMessage());
    }
  }

  public function isSubmissionValid($data): bool
  {
    $submission = $data['flattened'];
    // La verifica viene fatta sulla submission flattned
    // L'ultima condizione tiene conto sia che il calendario passi un singolo valore che multipli
    if (!isset($submission['applicant.data.completename.data.name']) || !isset($submission['applicant.data.completename.data.surname']) || (!isset($data['data']['calendar']))) {
      return false;
    }

    // Verifico che sia presente almeno un recapito
    if (!(isset($submission['applicant.data.phone_number']) && $submission['applicant.data.phone_number']) &&
      !(isset($submission['applicant.data.cell_number']) && $submission['applicant.data.cell_number']) &&
      !(isset($submission['applicant.data.email_address']) && $submission['applicant.data.email_address'])) {
      $this->logger->error("Missing contacts for integration with calendar backoffice");
      return false;
    }

    if (!empty($submission["applicant.data.fiscal_code.data.fiscal_code"])
      && strlen($submission["applicant.data.fiscal_code.data.fiscal_code"]) > 16
    ) {
      $this->logger->error("The fiscal code is greeter then 16 chars");
      return false;
    }

    $meetingsData = $this->parseCalendarsData($data['data']['calendar']);
    if (!empty($meetingsData)) {
      foreach ($meetingsData as $meetingData) {
        if ($meetingData['meeting_id'] && Uuid::isValid($meetingData['meeting_id'])) {
          $meeting = $this->em->getRepository(Meeting::class)->find($meetingData['meeting_id']);
          if ($meeting instanceof Meeting) {

            if ($meeting->getApplicationId()) {
              $this->logger->error("This meeting is already linked to a previous application", [
                'meeting_data' => $meetingData,
                'previous_application_id' => $meeting->getApplicationId(),
              ]);
              return false;
            }

            $meeting
              ->setEmail($submission['applicant.data.email_address'])
              ->setFiscalCode($submission["applicant.data.fiscal_code.data.fiscal_code"]);
            if (!$this->meetingService->isWithinReservationLimits($meeting)) {
              $this->logger->error("Meeting: {$meetingData['meeting_id']} does not respect the reservation limits set on the calendar: {$meeting->getCalendarId()}", [
                'meeting_data' => $meetingData,
              ]);
              return false;
            }
          }
        }
      }
    }

    return true;
  }
}
