<?php

declare(strict_types=1);

namespace App\Applications\Domain;

use App\Entity\ApplicationLite;
use App\Model\Application;
use App\Model\Transition;
use App\Services\Manager\PraticaManager;

class ApplicationLiteDataTransformer
{
  private PraticaManager $praticaManager;

  public function __construct(PraticaManager $praticaManager)
  {
    $this->praticaManager = $praticaManager;
  }

  public function toApplicationEvent(
    ApplicationLite $pratica
  ): Application {
    $application = new Application();

    $application->setId($pratica->getId());
    $application->setUser($pratica->getUser()->getId());
    $application->setUserName($pratica->getUser()->getFullName());
    $application->setTenant($pratica->getEnte()->getId());
    $application->setService($pratica->getServizio()->getSlug());
    $application->setServiceId($pratica->getServizio()->getId());
    $application->setServiceName($pratica->getServizio()->getName());
    $application->setSubject($pratica->getOggetto() ?? $pratica->generateSubject());

    $application->setCreationTime($pratica->getCreationTime());
    $application->setCreatedAt($pratica->getCreatedAt());

    $application->setSubmissionTime($pratica->getSubmissionTime());
    if ($pratica->getSubmissionTime()) {
      try {
        $date = (new \DateTime())->setTimestamp($pratica->getSubmissionTime());
        $application->setSubmittedAt($date);
      } catch (\Exception $e) {
        $application->setSubmittedAt($pratica->getSubmissionTime());
      }
    } else {
      $application->setSubmittedAt(null);
    }

    $application->setLatestStatusChangeTime($pratica->getLatestStatusChangeTimestamp());
    if ($pratica->getLatestStatusChangeTimestamp()) {
      try {
        $date = (new \DateTime())->setTimestamp($pratica->getLatestStatusChangeTimestamp());
        $application->setLatestStatusChangeAt($date);
      } catch (\Exception $e) {
        $application->setLatestStatusChangeAt($pratica->getLatestStatusChangeTimestamp());
      }
    } else {
      $application->setLatestStatusChangeAt(null);
    }

    $application->setStatus($pratica->getStatus());
    $application->setStatusName(strtolower($pratica->getStatusName()));

    $application->setLinks($this->getAvailableTransitions($pratica));
    if ($pratica->isExternal()) {
      $application->setAsExternal();
      $application->setExternalId($pratica->getExternalId());
    } else {
      $application->setAsInternal();
    }

    $history = $this->praticaManager->getApplicationHistory($pratica);

    $serializedHistory = [];
    /** @var Transition $item */
    foreach ($history as $item) {
      $serializedHistory[] = [
        'status' => $item->getStatusCode(),
        'status_changed_at' => $item->getDate()
      ];
    }
    $application->setProcessHistory($serializedHistory);

    return $application;
  }

  private function getAvailableTransitions(ApplicationLite $applicationLite): array
  {
    $availableTransitions = [];

    foreach ($applicationLite->externalActions() as $externalAction) {
      $k = $externalAction->action();
      $availableTransitions[$k]['action'] = $k;
      $availableTransitions[$k]['url'] = $externalAction->url();
    }

    return array_values($availableTransitions);
  }
}
