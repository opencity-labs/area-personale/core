<?php

declare(strict_types=1);

namespace App\Applications\Domain;

use App\Entity\ApplicationLite;

interface ApplicationLiteRepositoryInterface
{
  public function save(ApplicationLite $applicationLite);
}
