<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

class PersonData
{
  /** @Groups({"read", "kafka"}) */
  public string $id;

  /** @Groups({"read", "write", "kafka"}) */
  public string $givenName;
  /** @Groups({"read", "write", "kafka"}) */
  public string $familyName;
  /**
   * @Serializer\Type("DateTime<'Y-m-d'>")
   * @Groups({"read", "write", "kafka"})
   */
  public ?DateTime $dateOfBirth = null;
  /** @Groups({"read", "write", "kafka"}) */
  public ?string $placeOfBirth = null;
  /**
   * @Serializer\Type("App\Applications\Application\DTOs\IdentifiersData")
   * @Groups({"read", "write", "kafka"})
   */
  public IdentifiersData $identifiers;
  /**
   * @Serializer\Type("App\Applications\Application\DTOs\ContactsData")
   * @Groups({"read", "write", "kafka"})
   */
  public ContactsData $contacts;

}
