<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class ServiceData
{
  /**
   * @OA\Property(description="Service's uuid", type="string", format="uuid")
   * @Groups({"read", "write"})
   */
  public string $id;
  /** @Groups({"read"}) */
  public string $name;
}
