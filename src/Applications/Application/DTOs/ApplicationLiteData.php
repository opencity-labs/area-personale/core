<?php

declare(strict_types=1);

namespace App\Applications\Application\DTOs;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

class ApplicationLiteData
{
  /** @Groups({"read"}) */
  public ?string $id;
  /** @Groups({"read", "write"}) */
  public string $externalId;
  /** @Groups({"read", "write"}) */
  public string $subject;
  /** @Groups({"read", "write"}) */
  public int $status;
  /** @Groups({"read", "write"}) */
  public ServiceData $service;
  /** @Groups({"read", "write"}) */
  public ApplicantData $applicant;
  /** @Groups({"read", "write"}) */
  public ?BeneficiaryData $beneficiary = null;
  /**
   * @var ActionData[]
   * @Serializer\Type("array<App\Applications\Application\DTOs\ActionData>")
   * @Groups({"read", "write"})
   */
  public array $actions = [];

  /**
   * @var ProcessHistory[]
   * @Serializer\Type("array<App\Applications\Application\DTOs\ProcessHistory>")
   * @Groups({"read", "write"})
   */
  public array $processHistory = [];
}
