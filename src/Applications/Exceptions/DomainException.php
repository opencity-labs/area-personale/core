<?php

declare(strict_types=1);

namespace App\Applications\Exceptions;

use Exception;

class DomainException extends Exception
{
}
