<?php

declare(strict_types=1);

namespace App\Applications\Exceptions;

use App\Entity\Servizio;

class ApplicationLiteException extends DomainException
{
  public static function becauseStatusIsInvalid(int $invalidSate): self
  {
    return new self(sprintf('ApplicationLite status is invalid: %s', $invalidSate));
  }

  public static function becauseServiceIsNotCompatible(Servizio $servizio): self
  {
    return new self(sprintf('The service provided is not compatible with ApplicationLite: %s', $servizio->getName()));
  }

  public static function becauseArgumentInvalid(string $expectedArgument): self
  {
    return new self(
      sprintf(
        'Argument is not of the expected type. Expected argument  should be %s',
        $expectedArgument,
      ),
    );
  }
}
