<?php

declare(strict_types=1);

namespace App\Handlers\Servizio;

class ServizioHandlerRegistry
{
  private array $handlers = [];

  public function registerHandler(ServizioHandlerInterface $flow, $alias)
  {
    $this->handlers[$alias] = $flow;
  }

  /**
   * @return ServizioHandlerInterface
   */
  public function getByName(string $alias = null)
  {
    if ($alias && isset($this->handlers[$alias])) {
      return $this->handlers[$alias];
    }

    return $this->handlers['default'];
  }
}
