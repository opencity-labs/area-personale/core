<?php

declare(strict_types=1);

namespace App\Handlers\Servizio;

use Exception;
use App\Entity\Servizio;
use Symfony\Component\HttpFoundation\Response;

interface ServizioHandlerInterface
{
  /**
   * @throws ForbiddenAccessException
   */
  public function canAccess(Servizio $servizio);

  /**
   * @return Response
   * @throws Exception
   */
  public function execute(Servizio $servizio);

  public function getCallToActionText();

  public function getErrorMessage();
}
