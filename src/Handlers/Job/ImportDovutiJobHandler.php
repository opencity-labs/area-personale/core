<?php

namespace App\Handlers\Job;

use App\Entity\CPSUser;
use App\Entity\Job;
use App\Entity\Servizio;
use App\Model\Payment;
use App\Model\Payment\PaymentTransaction;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Payment\GatewayCollection;
use App\Services\CPSUserProvider;
use App\Services\FileService\CommonFileService;
use App\Services\InstanceService;
use App\Services\KafkaService;
use App\Services\MailerService;
use App\Utils\LocaleUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Ramsey\Uuid\Uuid;
use \Exception;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class ImportDovutiJobHandler implements JobHandlerInterface
{

  private EntityManagerInterface $entityManager;
  private CommonFileService $fileService;
  private KafkaService $kafkaService;
  private CPSUserProvider $cpsUserProvider;
  private RouterInterface $router;
  private GatewayCollection $gatewayCollection;

  private array $gateways = [];

  private Servizio $service;
  private MailerService $mailerService;
  private string $defaultSender;
  private InstanceService $instanceService;
  private TranslatorInterface $translator;

  private string $separator = ',';

  private Environment $templating;


  /**
   * @param EntityManagerInterface $entityManager
   * @param CommonFileService $fileService
   * @param KafkaService $kafkaService
   * @param CPSUserProvider $cpsUserProvider
   * @param RouterInterface $router
   * @param GatewayCollection $gatewayCollection
   * @param MailerService $mailerService
   * @param string $defaultSender
   * @param InstanceService $instanceService
   * @param TranslatorInterface $translator
   * @param Environment $templating
   */
  public function __construct(EntityManagerInterface $entityManager, CommonFileService $fileService, KafkaService $kafkaService, CPSUserProvider $cpsUserProvider, RouterInterface $router, GatewayCollection $gatewayCollection, MailerService $mailerService, string $defaultSender, InstanceService $instanceService, TranslatorInterface $translator, Environment $templating)
  {
    $this->entityManager = $entityManager;
    $this->fileService = $fileService;
    $this->kafkaService = $kafkaService;
    $this->cpsUserProvider = $cpsUserProvider;
    $this->router = $router;
    $this->gatewayCollection = $gatewayCollection;
    $this->mailerService = $mailerService;
    $this->defaultSender = $defaultSender;
    $this->instanceService = $instanceService;
    $this->translator = $translator;
    $this->templating = $templating;

    // Recupero la lista dei gateway payment proxy disponibili
    $availablePaymentGateways = $this->gatewayCollection->getAvailablePaymentGateways();
    foreach ($availablePaymentGateways as $k => $g) {
      if (!in_array($k, [Bollo::IDENTIFIER, MyPay::IDENTIFIER])) {
        $this->gateways[$k] = $g;
      }
    }
  }

  public function processJob(Job $job): void
  {

    // Verifica delle configurazioni del job
    try {
      $this->validateJob($job);
    } catch (Exception $e) {
      $job->failed($e->getMessage(), new DateTime());
      $this->entityManager->persist($job);
      $this->entityManager->flush();
      return;
    }

    $tenant = $this->instanceService->getCurrentInstance();
    $serviceId = $this->service->getId();

    $header = [];
    $fileStream = $this->fileService->getFileStream($job->getFile());

    $processedRow = $this->getProcessedRow($job);
    $errors = $job->getErrors();
    $rowNumber = 0;

    // Leggo il file riga per riga
    while (($row = fgetcsv($fileStream, null, $this->separator)) !== false) {
      if (empty($header)) {
        $header = $row;
      } else {
        $rowNumber++;

        // Se il dato della riga corrente nel metadato del job è maggiore della riga attuale salto esecuzione
        if ($processedRow > $rowNumber) {
          continue;
        }

        $data = array_combine($header, $row);
        $this->saveJobCurrentState($job, $data, $rowNumber);

        try {

          $createdAt = \DateTime::createFromFormat('d-m-Y',$data['created_at']);
          if (!$createdAt instanceof \DateTimeInterface) {
            throw new ValidatorException("Invalid date format for field created_at (dd-mm-yyyy)");
          }

          $debtor = $this->provideUser($data);
          // Creo il Payment
          $payment = new Payment();
          $id = Uuid::uuid4()->toString();
          $payment->setId($id);
          $payment->setUserId($debtor->getId());
          $payment->setTenantId($tenant->getId());
          $payment->setServiceId($serviceId);
          $payment->setCreatedAt($createdAt);
          $payment->setUpdatedAt($createdAt);
          $payment->setStatus(Payment::STATUS_PAYMENT_PENDING);
          $payment->setReason($data['reason']);
          $payment->setRemoteId($id);

          $expireAt = \DateTime::createFromFormat('d-m-Y',$data['expire_at']);
          if (!$expireAt instanceof \DateTimeInterface) {
            throw new ValidatorException("Invalid date format for field expire_at (dd-mm-yyyy)");
          }

          $paymentTransaction = new PaymentTransaction();
          $paymentTransaction->setTransactionId($id);
          $paymentTransaction->setExpireAt($expireAt);
          $amount = str_replace(',', '.', $data['amount']);
          $paymentTransaction->setAmount(number_format((float)$amount, 2, '.', ''));
          $paymentTransaction->setNoticeCode($data['notice_number']);
          $paymentTransaction->setIud($id);
          $paymentTransaction->setIuv($data['iuv']);
          $payment->setPayment($paymentTransaction);

          $paymentLinks = new Payment\PaymentLinks();
          $paymentLinks->setOnlinePaymentBegin(
            Payment\PaymentLink::init($this->generateOnlinePaymentBeginUrl($id))
          );
          $paymentLinks->setOnlinePaymentLanding(
            Payment\PaymentLink::init($this->generateOnlinePaymentLandingUrl($id))
          );
          $paymentLinks->setOfflinePayment(
            Payment\PaymentLink::init()
          );
          $paymentLinks->setReceipt(
            Payment\PaymentLink::init($this->generateReceiptUrl($id))
          );
          $paymentLinks->setUpdate(
            Payment\PaymentLink::init()
          );
          $paymentLinks->setNotify(
            [Payment\PaymentLink::init('#', 'POST')]
          );
          $payment->setLinks($paymentLinks);

          $payer = new Payment\Payer();
          $payer->setTaxIdentificationNumber($debtor->getCodiceFiscale());
          $payer->setName($debtor->getNome());
          $payer->setFamilyName($debtor->getCognome());
          $payer->setStreetName($debtor->getIndirizzoResidenza() ?? '');
          $payer->setBuildingNumber('');
          $payer->setPostalCode($debtor->getCapResidenza() ?? '');
          $payer->setTownName($debtor->getCittaResidenza() ?? '');
          $payer->setCountrySubdivision($debtor->getProvinciaResidenza() ?? '');
          $payer->setCountry('IT');
          $payer->setEmail($debtor->getEmail());
          $payment->setPayer($payer);

          $paymentGateway = $this->gateways[$this->service->getActivePaymentGatewayIdentifier()];
          // Invio il pagamento a kafka
          $this->kafkaService->produceMessage($payment, $paymentGateway['event_version']);
          if ($this->service->getExternalCardUrl()) {
            $paymentLabel = 'servizio.vai';
            $paymentLink = $this->service->getExternalCardUrl();
          } else {
            $paymentLabel = 'payment.emails.import.call_to_action_label';
            $paymentLink = $this->router->generate('payments_details_cpsuser', ['paymentId' =>  $payment->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
          }

          // Invio la notifica al cittadino
          $subject = $this->translator->trans('payment.emails.import.subject', ['%notice_number%' => $data['notice_number'], '%service_name%'=>$this->service->getName()]);

          $message = $this->templating->render('Emails/Payment/_new_payment.html.twig', [
            'payment' => $payment,
            'debtor' => $debtor,
            'service' => $this->service,
            'locale' => LocaleUtils::DEFAULT_LOCALE
          ]);
          $callToActions = [
            [
              'label' => $paymentLabel,
              'link' => $paymentLink
            ]
          ];
          $this->mailerService->dispatchMail($this->defaultSender, $tenant->getName(), $debtor->getEmail(),  $debtor->getFullName(), $message, $subject, $tenant, $callToActions);


        } catch (\Exception $e) {
          $errors [$rowNumber] = "Errore d'importazione a riga {$rowNumber} (" . implode(', ', $data) . '): ' . $e->getMessage();
          $this->saveJobErrors($job, $errors);
        }
      }
    }

    if (empty($errors)) {
      $job->finished(
        'Job process finished successfully',
        new DateTime(),
      );
    } else {
      $job->failed(
        'Job process finished with errors',
        new DateTime(),
      );
    }
    $this->entityManager->persist($job);
    $this->entityManager->flush();

  }

  /**
   * @throws Exception
   */
  public function validateJob(Job $job)
  {
    $args = $job->getArgsAsArray();

    if (empty($args) || empty($args['service_id'])) {
      throw new ValidatorException('service_id argument is mandatory');
    }

    if (!Uuid::isValid($args['service_id'])) {
      throw new ValidatorException('service_id argument must be a valid Uuid');
    }

    $repository = $this->entityManager->getRepository('App\Entity\Servizio');
    $service = $repository->find($args['service_id']);

    if (!$service instanceof Servizio) {
      throw new ValidatorException("Service with id {$args['service_id']} not found");
    }

    $this->service = $service;

    $paymentParameters = $service->getPaymentParameters();
    if (empty($paymentParameters['gateways'])) {
      throw new ValidatorException("Service must have at least one payment gateway");
    }

    if (!$this->isValidFile($job)) {
      throw new ValidatorException("Non UTF-8 characters in uploaded file");
    }
    $this->detectSeparator($job);
  }

  private function detectSeparator($job): void
  {
    $delimiters = [
      ';' => 0,
      ',' => 0
    ];

    $fileStream = $this->fileService->getFileStream($job->getFile());
    $firstLine = fgets($fileStream);
    foreach ($delimiters as $delimiter => $count) {
      $delimiters[$delimiter] = count(str_getcsv($firstLine, $delimiter));
    }

    $this->separator =  array_search(max($delimiters), $delimiters, true);
  }


  private function provideUser($data): CPSUser
  {

    $fiscalCode = $data['debtor_fiscal_code'];
    $email = $data['debtor_fiscal_code'];

    $qb = $this->entityManager->createQueryBuilder()
      ->select('user')
      ->from('App:CPSUser', 'user')
      ->andWhere('lower(user.codiceFiscale) = :cf')
      ->setParameter('cf', strtolower($fiscalCode))
      ->setMaxResults(1);

    $result = $qb->getQuery()->getResult();

    if (count($result) > 0) {
      return reset($result);
    }

    $user = $this->cpsUserProvider->createAnonymousUser(false);
    $user
      ->setNome(substr($fiscalCode, 0, 3))
      ->setCognome(substr($fiscalCode, 2, 3))
      ->setUsername($fiscalCode)
      ->setCodiceFiscale($fiscalCode);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $user
        ->setEmail($email)
        ->setEmailContatto($email);
    }

    $this->entityManager->persist($user);
    $this->entityManager->flush();
    return $user;
  }

  /**
   * @param $paymentId
   * @return string
   */
  private function generateOnlinePaymentLandingUrl($paymentId): string
  {
    return $this->router->generate('payments_details_cpsuser', [
      'paymentId' => $paymentId
    ], UrlGeneratorInterface::ABSOLUTE_URL);
  }

  /**
   * @param $paymentId
   * @return string
   */
  private function generateOnlinePaymentBeginUrl($paymentId): string
  {
    $paymentGateway = $this->gateways[$this->service->getActivePaymentGatewayIdentifier()];

    $url = $paymentGateway['url'];
    if (substr($url, -1) !== '/') {
      $url .= '/';
    }

    return $url . 'online-payment/' . $paymentId;
  }

  private function generateReceiptUrl($paymentId): string
  {
    $paymentGateway = $this->gateways[$this->service->getActivePaymentGatewayIdentifier()];

    $url = $paymentGateway['url'];
    if (substr($url, -1) !== '/') {
      $url .= '/';
    }

    return $url . 'receipt/' . $paymentId;
  }

  private function saveJobCurrentState(Job $job, $row, $rowNumber)
  {
    $job->setMetadata(['current_row' => $rowNumber]);
    $job->setRunningOutput("Processing row {$rowNumber} (" . implode(', ', $row) . ")");
    $this->entityManager->persist($job);
    $this->entityManager->flush();
  }

  private function saveJobErrors(Job $job, $errors)
  {
    $job->setErrors($errors);
    $this->entityManager->persist($job);
    $this->entityManager->flush();
  }

  private function getProcessedRow(Job $job)
  {
    $currentRow = 0;
    if (!empty($job->getMetadata()['current_row'])) {
      $currentRow = $job->getMetadata()['current_row'];
    }
    return $currentRow;
  }

  private function isValidFile(Job $job): bool
  {
    try {
      $fileStream = $this->fileService->getFileStream($job->getFile());
    } catch (FileNotFoundException $e) {
      return false;
    }

    while (($line = fgets($fileStream)) !== false) {
      if (!mb_check_encoding($line, 'UTF-8')) {
        return false;
      }
    }
    return true;
  }
}
