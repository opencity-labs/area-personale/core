<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

class ApplicationException extends Exception
{
  public static function becauseOnlyDraftApplicationCanBeDeleted(): self
  {
    return new self(
      "Application can't be deleted, not in draft status",
    );
  }
}
