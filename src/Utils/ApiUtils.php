<?php

namespace App\Utils;

use DateTime;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class ApiUtils
{

  public static function validateDateTimeRangeParameter($parameterData, $parameterName = 'created_at', $asString = false): array
  {
    $result = [];
    if ($parameterData) {
      $dateFormat = 'Y-m-d';
      foreach ($parameterData as $k => $v) {
        $date = DateTime::createFromFormat($dateFormat, $v);
        // Se non viene passato l'orario viene calcolato automaticamente in base alla chiave di confronto
        // La chiave può assumere i seguenti valori: after|before|strictly_after|strictly_before
        if ($date instanceof DateTime) {
          if (in_array($k, ['after', 'strictly_after'])) {
            $date->setTime(0, 0, 0);
          } elseif (in_array($k, ['before', 'strictly_before'])) {
            $date->setTime(23, 59, 59);
          } else {
            throw new \RuntimeException("{$k} in parameter {$parameterName} is not a valid value, accepted values are: after, before, strictly_after, strictly_before");
          }
        } else {
          $date = DateTime::createFromFormat(DATE_W3C, $v);
        }

        if (!$date instanceof DateTime || ($date->format($dateFormat) !== $v && $date->format(DATE_W3C) !== $v)) {
          throw new \RuntimeException("Parameter {$parameterName} must be in on of these formats: yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP");
        }
        if ($asString) {
          $result[$parameterName][$k] = $date->format(DATE_W3C);
        } else {
          $result[$parameterName][$k] = $date;
        }
      }
    }
    return $result;
  }


  public static function validateDateTimeParameter($value, $parameterName = 'created_at'): array
  {
    $result = [];
    if ($value) {
      $date = DateTime::createFromFormat(DATE_W3C, $value);

      if (!$date instanceof DateTime) {
        throw new \RuntimeException("Parameter {$parameterName} is not valid format 'yyyy-mm-ddTHH:ii:ssP'. A valid example format is: " . (new DateTime())->format(DATE_W3C));
      }

      $result[$parameterName] = $date;
    }
    return $result;
  }

  /**
   * @throws ApiProblemException
   */
  public static function isValidUuid(string $uuid): void
  {
    if (!Uuid::isValid($uuid)) {
      throw new ApiProblemException(
        new BadRequestProblem("{$uuid} is not a valid uuid")
      );
    }
  }

}
