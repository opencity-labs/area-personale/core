<?php

namespace App\Utils;

class PdndSubmissionFilterUtils
{

  private array $filterMap;

  public function __construct($filterMap) {
    $this->filterMap = $filterMap;
  }

  // Method to get the subset of the array based on the filter map
  public function getSubset($array): array
  {
    $subset = [];
    foreach ($this->filterMap as $dotPath) {
      $value = $this->getByDotNotation($array, $dotPath);
      if ($value !== null) {
        $this->setByDotNotation($subset, $dotPath, $value);
      }
    }
    return $subset;
  }

  // Helper function to retrieve a value from the array using dot notation
  private function getByDotNotation($array, $path) {
    $keys = explode('.', $path);
    foreach ($keys as $key) {
      if (isset($array[$key])) {
        $array = $array[$key];
      } elseif (isset($array['data'][$key])) { // Check if data key exists in between
        $array = $array['data'][$key];
      } else {
        return null; // If key doesn't exist, return null
      }
    }
    return $array;
  }

  // Helper function to set a value in the array using dot notation
  private function setByDotNotation(&$array, $path, $value): void
  {
    $keys = explode('.', $path);
    foreach ($keys as $key) {
      $array = &$array[$key];
    }
    $array = $value;
  }
}
