<?php

namespace App\Utils;

use App\Validator\FiscalCodeValidator;

class FiscalCodeUtils
{
  public static function isValid(string $fiscalCode): bool
  {
    $fiscalCodevalidator = new FiscalCodeValidator($fiscalCode, ['omocodiaAllowed' => true]);
    return $fiscalCodevalidator->isFormallyValid();
  }
}
