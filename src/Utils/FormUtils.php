<?php

namespace App\Utils;

use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Form\FormInterface;
use GuzzleHttp\Client;
use Symfony\Component\Validator\ConstraintViolation;

class FormUtils
{
  /**
   * @param FormInterface $form
   * @return array
   */
  public static function getErrorsFromForm(FormInterface $form): array
  {
    $errors = [];

    foreach ($form->getErrors() as $error) {
      $cause = $error->getCause();

      if ($cause instanceof ConstraintViolation) {
        if ($cause->getCause()) {
          $message = $form->getName() . ': ' . $cause->getCause()->getMessage();
        } else {
          $invalidValue = $cause->getInvalidValue();
          $message = $error->getMessageTemplate();
          if ($invalidValue) {
            if (is_array($invalidValue)) {
              $invalidValue = json_encode($invalidValue);
            } elseif ($invalidValue instanceof \DateTime) {
              $invalidValue = $invalidValue->format('c');
            }
            $message .= ' ('.$invalidValue.')';
          }
        }
        $errors[] = $message;
      } elseif ($error->getMessageTemplate()) {
        $errors[] = $error->getMessageTemplate();
      }
    }
    foreach ($form->all() as $childForm) {
      if ($childForm instanceof FormInterface && $childErrors = self::getErrorsFromForm($childForm)) {
        $errors[] = $childErrors;
      }

    }
    return $errors;
  }

  /**
   * @param string|null $url
   * @return boolean
   * @throws GuzzleException
   */
  public static function isUrlValid(?string $url): bool
  {
    try {
      $client = new Client();
      $response = $client->request('GET', $url);
      return $response->getStatusCode() === 200;
    } catch (\Exception $e) {
      return false;
    }
  }

  /**
   * Checks if an array has at least one key with an empty value
   *
   * @param array|null $array $array
   * @return boolean
   */
  public static function isArrayValueEmpty(?array $array): bool
  {
    $isEmpty = false;

    array_walk_recursive($array, function ($leaf) use (&$isEmpty) {
      if ($leaf !== "") {
        return;
      }
      $isEmpty = true;
    });

    return $isEmpty;
  }

  public static function isArrayEmpty($array): bool
  {
    if (!is_array($array)) {
      return empty($array);
    }

    foreach ($array as $value) {
      if (!self::isArrayEmpty($value)) {
        return false;
      }
    }

    return true;
  }


}
