<?php

namespace App\Utils;

class FormIOUtils
{
  public static function simplifyFlattenedKey(string $key): string
  {
    $parts = explode('.', $key);
    $path = [];
    foreach ($parts as $p) {
      if ($p !== 'data' && !ctype_digit($p)) {
        $path [] = $p;
      }
    }
    return implode('.', $path);
  }

  // Cerca in un array passando la chiave in dot notation
  /*public static function getValueFromDotNotation(array $array, string $key) {
    // Split the dot notation into an array of keys
    $keys = explode('.', $key);

    // Traverse the array using the keys
    foreach ($keys as $k) {
      // Check if the current key exists in the array
      if (isset($array[$k])) {
        $array = $array[$k];
      } else {
        // Return null if the key doesn't exist
        return null;
      }
    }

    // Return the found value
    return $array;
  }*/


  // Cerca in un array passando la chiave in dot notation, verifica anche se ci sia 'data' tra un componente e l'altro
  public static function getValueFromDotNotation(array $array, string $key) {
    // Split the dot notation into an array of keys
    $keys = explode('.', $key);

    // Traverse the array using the keys
    foreach ($keys as $k) {
      // Check if the current key exists in the array
      if (isset($array[$k])) {
        $array = $array[$k];
      }
      // If the key does not exist, automatically check for 'data' at the current level
      elseif (isset($array['data']) && isset($array['data'][$k])) {
        $array = $array['data'][$k];
      } else {
        // Return null if the key doesn't exist and no 'data' is available
        return null;
      }
    }

    // Return the found value
    return $array;
  }

  public static function processSubmissionData($data, $path, $pdndFieldFilterMapFlipped) {
    $subset = [];

    foreach ($data as $k => $v) {
      $index = $path . '.' . $k;
      // Check if the value is an array and process it recursively
      if (isset($pdndFieldFilterMapFlipped[$index])) {
        $subset[$k] = $v; // Add to subset if it passes the filter
      }
    }
    return $subset;
  }


  public static function setIsValidFromDotNotation(array &$array, string $key, $isValid) {

    // Split the dot notation into an array of keys
    $keys = explode('.', $key);

    // Get reference to the root of the array
    $temp = &$array;

    // Traverse the array using the keys
    foreach ($keys as $k) {
      if (isset($temp[$k]) && is_array($temp[$k])) {
        $temp = &$temp[$k];
      } elseif (isset($temp['data'][$k]) && is_array($temp['data'][$k])) {
        // Traverse deeper by setting temp to the current key
        $temp = &$temp['data'][$k];
      } else {
        // Return null if the key doesn't exist and no 'data' is available
        return null;
      }
    }

    // Set the value at the specified key location
    $temp['meta']['is_valid'] = $isValid;
  }
}
