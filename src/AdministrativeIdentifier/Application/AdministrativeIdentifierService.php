<?php

namespace App\AdministrativeIdentifier\Application;

use App\AdministrativeIdentifier\Application\Command\CreateAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Command\DeleteAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Command\GetAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Command\UpdateAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Validator\AdministrativeIdentifierDtoValidator;
use App\AdministrativeIdentifier\Domain\Exception\AdministrativeIdentifierException;
use App\AdministrativeIdentifier\Domain\Repository\AdministrativeIdentifierRepositoryInterface;
use App\Entity\AdministrativeIdentifier;
use App\Entity\CPSUser;
use App\Handlers\Servizio\ForbiddenAccessException;
use App\Security\Voters\AdministrativeIdentifierVoter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdministrativeIdentifierService
{
  private AdministrativeIdentifierDtoValidator $validator;
  private AdministrativeIdentifierRepositoryInterface $repository;
  private EntityManagerInterface $entityManager;
  private AuthorizationCheckerInterface $authorizationChecker;

  public function __construct(AdministrativeIdentifierDtoValidator $validator, AdministrativeIdentifierRepositoryInterface $repository, EntityManagerInterface $entityManager, AuthorizationCheckerInterface $authorizationChecker)
  {
    $this->validator = $validator;
    $this->repository = $repository;
    // Todo: creare un cps user provider per sostituire l'import dell'entity manager
    $this->entityManager = $entityManager;
    $this->authorizationChecker = $authorizationChecker;
  }

  public function validate($dto): array
  {
    return $this->validator->validate($dto);
  }

  /**
   * @throws ForbiddenAccessException
   */
  public function get(GetAdministrativeIdentifier $command): AdministrativeIdentifier
  {
    $administrativeIdentifier =  $this->repository->findById(Uuid::fromString($command->getId()));
    if (!$this->authorizationChecker->isGranted(AdministrativeIdentifierVoter::VIEW, $administrativeIdentifier)) {
      throw new ForbiddenAccessException('You are not allowed to edit this administrative identifier');
    }
    return $administrativeIdentifier;
  }

  /**
   * @throws AdministrativeIdentifierException
   */
  public function create(CreateAdministrativeIdentifier $command): AdministrativeIdentifier
  {
    $dto = $command->getDto();
    $existing = $this->repository->findByIdentifiesTypeAndIdentifier($dto->identifies, $dto->type, $dto->identifier);

    if ($existing) {
      throw new AdministrativeIdentifierException('Administrative identifier already exists');
    }

    $userRepo = $this->entityManager->getRepository(CPSUser::class);
    $user = $userRepo->find($dto->identifies);

    if (!$user instanceof CPSUser) {
      throw new AdministrativeIdentifierException('User not found');
    }

    $administrativeIdentifier = new AdministrativeIdentifier($user, $dto->type, $dto->identifier);
    $this->repository->save($administrativeIdentifier);

    return $administrativeIdentifier;
  }

  /**
   * @throws ForbiddenAccessException|AdministrativeIdentifierException
   */
  public function update(UpdateAdministrativeIdentifier $command): AdministrativeIdentifier
  {
    $dto = $command->getDto();
    $administrativeIdentifier = $this->repository->findById($dto->id);

    if (!$this->authorizationChecker->isGranted(AdministrativeIdentifierVoter::EDIT, $administrativeIdentifier)) {
      throw new ForbiddenAccessException('You are not allowed to edit this administrative identifier');
    }

    // Check for existing identifier but exclude current one
    $existing = $this->repository->findByIdentifiesTypeAndIdentifierExcludingId(
      Uuid::fromString($dto->identifies),
      $dto->type,
      $dto->identifier,
      Uuid::fromString($administrativeIdentifier->getId())
    );


    if ($existing) {
      throw new AdministrativeIdentifierException('Administrative identifier already exists');
    }

    $administrativeIdentifier->update($dto->type, $dto->identifier);
    $this->repository->save($administrativeIdentifier);
    return $administrativeIdentifier;

  }

  public function delete(DeleteAdministrativeIdentifier $command): void
  {
    $administrativeIdentifier = $this->repository->findById(Uuid::fromString($command->getId()));
    $this->repository->remove($administrativeIdentifier);
  }

}
