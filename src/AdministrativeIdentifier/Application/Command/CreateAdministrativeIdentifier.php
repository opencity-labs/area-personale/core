<?php

namespace App\AdministrativeIdentifier\Application\Command;

use App\AdministrativeIdentifier\Application\Dto\CreateAdministrativeIdentifierDto;

class CreateAdministrativeIdentifier
{
  private CreateAdministrativeIdentifierDto $dto;

  public function __construct(
      CreateAdministrativeIdentifierDto $dto
  ) {
    $this->dto = $dto;
  }

  public function getDto(): CreateAdministrativeIdentifierDto
  {
    return $this->dto;
  }
}
