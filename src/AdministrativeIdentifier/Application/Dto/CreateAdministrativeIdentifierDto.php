<?php

namespace App\AdministrativeIdentifier\Application\Dto;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CreateAdministrativeIdentifierDto
{
  /**
   * @Assert\NotBlank(message="Identifies ID is required")
   * @Assert\Uuid(message="Identifies ID must be a valid UUID")
   */
  public UuidInterface $identifies;

  /**
   * @Assert\NotBlank(message="Type is required")
   * @Assert\Length(max=255, maxMessage="Type cannot be longer than 255 characters")
   */
  public string $type;

  /**
   * @Assert\NotBlank(message="Identifier is required")
   * @Assert\Length(max=255, maxMessage="Identifier cannot be longer than 255 characters")
   */
  public string $identifier;

  public function __construct(UuidInterface $identifies, string $type, string $identifier)
  {
    $this->identifies = $identifies;
    $this->type = $type;
    $this->identifier = $identifier;
  }
}
