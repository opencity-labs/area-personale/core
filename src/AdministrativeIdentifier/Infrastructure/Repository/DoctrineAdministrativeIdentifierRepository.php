<?php

namespace App\AdministrativeIdentifier\Infrastructure\Repository;

use App\AdministrativeIdentifier\Domain\Repository\AdministrativeIdentifierRepositoryInterface;
use App\Entity\AdministrativeIdentifier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @extends ServiceEntityRepository<AdministrativeIdentifier>
 *
 * @method AdministrativeIdentifier|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdministrativeIdentifier|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdministrativeIdentifier[]    findAll()
 * @method AdministrativeIdentifier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctrineAdministrativeIdentifierRepository extends ServiceEntityRepository implements AdministrativeIdentifierRepositoryInterface
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, AdministrativeIdentifier::class);
  }

  public function save(AdministrativeIdentifier $entity, bool $flush = true): void
  {
    $this->_em->persist($entity);
    if ($flush) {
      $this->_em->flush();
    }
  }

  public function remove(AdministrativeIdentifier $entity, bool $flush = true): void
  {
    $this->_em->remove($entity);
    if ($flush) {
      $this->_em->flush();
    }
  }

  /**
   * @throws NonUniqueResultException
   */
  public function findByIdentifiesTypeAndIdentifier(UuidInterface $identifies, string $type, string $identifier): ?AdministrativeIdentifier
  {
    $qb = $this->createQueryBuilder('ai');
    $qb
      ->where('ai.identifies = :identifies')
      ->andWhere('ai.type = :type')
      ->andWhere('ai.identifier = :identifier')
      ->setParameters([
        'identifies' => $identifies,
        'type' => $type,
        'identifier' => $identifier
      ]);

    return $qb
      ->getQuery()
      ->getOneOrNullResult();
  }

  /**
   * @throws NonUniqueResultException
   */
  public function findByIdentifiesTypeAndIdentifierExcludingId(UuidInterface $identifies, string $type, string $identifier, UuidInterface $excludeId): ?AdministrativeIdentifier
  {
    $qb = $this->createQueryBuilder('ai');
    $qb
      ->where('ai.identifies = :identifies')
      ->andWhere('ai.type = :type')
      ->andWhere('ai.identifier = :identifier')
      ->andWhere('ai.id != :excludeId')
      ->setParameters([
        'identifies' => $identifies,
        'type' => $type,
        'identifier' => $identifier,
        'excludeId' => $excludeId
      ]);

    return $qb
      ->getQuery()
      ->getOneOrNullResult();
  }

  public function findById(UuidInterface $id): AdministrativeIdentifier
  {
    $administrativeIdentifier = $this->find($id);
    if (!$administrativeIdentifier) {
      throw new NotFoundHttpException(sprintf('Administrative identifier %s not found', $id->toString()));
    }

    return $administrativeIdentifier;
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  public function getList($parameters = [], $onlyCount = false, $order = 'createdAt', $sort = 'ASC', $offset = 0, $limit = 10)
  {

    $qb = $this->createQueryBuilder('ai');

    if ($onlyCount) {
      $qb->select('COUNT(ai.id)');
      return $qb->getQuery()->getSingleScalarResult();
    }

    $qb
      ->orderBy('ai.' . $order, $sort)
      ->setFirstResult($offset)
      ->setMaxResults($limit);

    return $qb->getQuery()->execute();
  }
}
