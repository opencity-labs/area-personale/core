<?php

namespace App\Protocollo\Exception;

class InvalidStatusException extends BaseException
{
    protected $message = "Application isn't in correct state: invalid status change";
}
