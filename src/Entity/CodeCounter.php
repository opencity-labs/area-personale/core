<?php

namespace App\Entity;

use App\Repository\CodeCounterRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="code_counter",
 *     uniqueConstraints={
 *          @UniqueConstraint(name="code_counter_unique", columns={"code_generation_strategy_id", "key"})
 *     }
 * )
 * @ORM\Entity(repositoryClass=CodeCounterRepository::class)
 */
class CodeCounter
{

  use TimestampableEntity;

  /**
   * @ORM\Id
   * @ORM\Column(type="guid")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity=CodeGenerationStrategy::class, inversedBy="codeCounters")
   * @ORM\JoinColumn(nullable=false)
   */
  private ?CodeGenerationStrategy $codeGenerationStrategy;


  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private ?string $key;

  /**
   * @ORM\Column(type="integer")
   */
  private int $counter = 0;

  public function __construct()
  {
    if (!$this->id) {
      $this->id = Uuid::uuid4();
    }
  }

  public function getId()
  {
    return $this->id;
  }

  public function getCodeGenerationStrategy(): ?CodeGenerationStrategy
  {
    return $this->codeGenerationStrategy;
  }

  public function setCodeGenerationStrategy(?CodeGenerationStrategy $codeGenerationStrategy): self
  {
    $this->codeGenerationStrategy = $codeGenerationStrategy;

    return $this;
  }

  public function getKey(): ?string
  {
    return $this->key;
  }

  public function setKey(?string $key): self
  {
    $this->key = $key;

    return $this;
  }

  public function getCounter(): int
  {
    return $this->counter;
  }

  public function setCounter(int $counter): self
  {
    $this->counter = $counter;

    return $this;
  }

  public function incrementCounter(): int
  {
    $currentNumber = $this->counter;
    if ($currentNumber >= $this->codeGenerationStrategy->getMaxCounterValue()) {
      $this->counter = 0;
    }
    $this->counter++;
    return $this->counter;
  }
}
