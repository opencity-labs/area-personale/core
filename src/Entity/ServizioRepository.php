<?php

namespace App\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;

class ServizioRepository extends EntityRepository
{
  public function findByCriteria($criteria)
  {
    // grouped
    if (isset($criteria['grouped']) && !$criteria['grouped']) {
      return $this->findNotSharedByCriteria($criteria);
    } else {
      $results = [];

      $notShared = $this->findNotSharedByCriteria($criteria);
      foreach ($notShared as $item) {
        $results[$item->getSlug()]= $item;
      }

      if (isset($criteria['user_groups'])) {
        return $results;
      }

      $shared = $this->findSharedByCriteria($criteria);
      foreach ($shared as $item) {
        $results[$item->getSlug()]= $item;
      }

      if ($criteria['ascending']) {
        ksort($results);
      }

      return $results;
    }
  }

  private function findNotSharedByCriteria($criteria)
  {
    $qb = $this->createQueryBuilder('s')
    ->where('s.sharedWithGroup = :sharedWithGroup')
    ->setParameter('sharedWithGroup', false)
    ->orWhere('s.sharedWithGroup IS NULL');

    // Search text
    if (isset($criteria['q'])) {
      $qb
        ->andWhere($qb->expr()->like('LOWER(s.name)', ':q'))
        ->setParameter('q', '%' . strtolower($criteria['q']) . '%');
    }

    // Status
    if (isset($criteria['status'])) {
      $qb
        ->andWhere('s.status IN (:status)')
        ->setParameter('status', $criteria['status']);
    }

    // identifier
    if (isset($criteria['identifier'])) {
      $qb
        ->andWhere('s.identifier = :identifier')
        ->setParameter('identifier', $criteria['identifier']);
    }

    // grouped
    if (isset($criteria['grouped']) && !$criteria['grouped']) {
      $qb->andWhere('s.serviceGroup IS NULL');
    } else {
      // serviceGroup
      if (isset($criteria['serviceGroup'])) {
        $qb
          ->andWhere('s.serviceGroup = :serviceGroup')
          ->setParameter('serviceGroup', $criteria['serviceGroup']);
      }
    }

      // topics
    if (isset($criteria['topics'])) {
      $qb
        ->andWhere('s.topics IN (:topics)')
        ->setParameter('topics', $criteria['topics']);
    }

    // Recipients
    if (isset($criteria['recipients'])) {
      $qb
        ->leftJoin('s.recipients', 'recipients')
        ->andWhere('recipients.id IN (:recipients)')
        ->setParameter('recipients', $criteria['recipients']);
    }

    // GeographicAreas
    if (isset($criteria['geographic_areas'])) {
      $qb
        ->leftJoin('s.geographicAreas', 'geographicAreas')
        ->andWhere('geographicAreas.id IN (:geographic_areas)')
        ->setParameter('geographic_areas', $criteria['geographic_areas']);
    }

    // User groups
    if (isset($criteria['user_groups'])) {
      $qb
        ->leftJoin('s.userGroups', 'userGroups')
        ->andWhere('userGroups.id IN (:user_groups)')
        ->setParameter('user_groups', $criteria['user_groups']);
    }

    // sticky
    if (isset($criteria['sticky'])) {
      $qb->andWhere($criteria['sticky'] ? 's.sticky = true' : 's.sticky = false OR s.sticky IS NULL');
    }

    if (isset($criteria['order_by'])) {
      $sort = $criteria['ascending'] ? 'ASC' : 'DESC';
      $qb->orderBy('s.' . $criteria['order_by'], $sort);
    }

    if (isset($criteria['limit'])) {
      $qb->setMaxResults($criteria['limit']);
    }

    if (isset($criteria['with_payments']) && is_bool($criteria['with_payments'])) {
      if ($criteria['with_payments']){
        $qb
          ->andWhere('s.paymentRequired IN (:service_payments)')
          ->setParameter('service_payments', [Servizio::PAYMENT_DEFERRED, Servizio::PAYMENT_REQUIRED]);
      }else{
        $qb
          ->andWhere('(s.paymentRequired = :null_payment or s.paymentRequired is null)')
          ->setParameter('null_payment', Servizio::PAYMENT_NOT_REQUIRED);
      }
    }

    return $qb->getQuery()
      ->setHint(
        Query::HINT_CUSTOM_OUTPUT_WALKER,
        'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
      )
      ->setHint(
        \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
        $criteria['locale']
      )
      ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
      ->getResult();
  }

  private function findSharedByCriteria($criteria)
  {
    $qb = $this->createQueryBuilder('s')
      ->leftJoin('s.serviceGroup', 'serviceGroup')
      ->where('s.serviceGroup IS NOT NULL')
      ->andWhere('s.sharedWithGroup = :sharedWithGroup')
      ->setParameter('sharedWithGroup', true);

    // Search text
    if (isset($criteria['q'])) {
      $qb
        ->andWhere($qb->expr()->like('LOWER(s.name)', ':q'))
        ->setParameter('q', '%' . strtolower($criteria['q']) . '%');
    }

    // Status
    if (isset($criteria['status'])) {
      $qb
        ->andWhere('s.status IN (:status)')
        ->setParameter('status', $criteria['status']);
    }

    // identifier
    if (isset($criteria['identifier'])) {
      $qb
        ->andWhere('s.identifier = :identifier')
        ->setParameter('identifier', $criteria['identifier']);
    }

    // serviceGroup
    if (isset($criteria['serviceGroup'])) {
      $qb
        ->andWhere('s.serviceGroup = :serviceGroup')
        ->setParameter('serviceGroup', $criteria['serviceGroup']);
    }

    // topics
    if (isset($criteria['topics'])) {
      $qb
        ->andWhere('serviceGroup.topics IN (:topics)')
        ->setParameter('topics', $criteria['topics']);
    }

    // Recipients
    if (isset($criteria['recipients'])) {
      $qb
        ->leftJoin('serviceGroup.recipients', 'recipients')
        ->andWhere('recipients.id IN (:recipients)')
        ->setParameter('recipients', $criteria['recipients']);
    }

    // GeographicAreas
    if (isset($criteria['geographic_areas'])) {
      $qb
        ->leftJoin('serviceGroup.geographicAreas', 'geographicAreas')
        ->andWhere('geographicAreas.id IN (:geographic_areas)')
        ->setParameter('geographic_areas', $criteria['geographic_areas']);
    }

    // sticky
    if (isset($criteria['sticky'])) {
      $qb->andWhere($criteria['sticky'] ? 's.sticky = true' : 's.sticky = false OR s.sticky IS NULL');
    }

    if (isset($criteria['with_payments']) && is_bool($criteria['with_payments'])) {
      if ($criteria['with_payments']){
        $qb
          ->andWhere('s.paymentRequired IN (:service_payments)')
          ->setParameter('service_payments', [Servizio::PAYMENT_DEFERRED, Servizio::PAYMENT_REQUIRED]);
      }else{
        $qb
          ->andWhere('(s.paymentRequired = :null_payment or s.paymentRequired is null)')
          ->setParameter('null_payment', Servizio::PAYMENT_NOT_REQUIRED);
      }
    }

    if (isset($criteria['order_by'])) {
      $sort = $criteria['ascending'] ? 'ASC' : 'DESC';
      $qb->orderBy('s.' . $criteria['order_by'], $sort);
    }

    if (isset($criteria['limit'])) {
      $qb->setMaxResults($criteria['limit']);
    }

    return $qb->getQuery()
      ->setHint(
        Query::HINT_CUSTOM_OUTPUT_WALKER,
        'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
      )
      ->setHint(
        \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
        $criteria['locale']
      )
      ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_FALLBACK, 1)
      ->getResult();
  }

  public function findAvailable($criteria = [])
  {
    $criteria['grouped'] = false;
    $criteria['status'] = Servizio::PUBLIC_STATUSES;
    return $this->findByCriteria($criteria);
  }

  public function findStickyAvailable(string $orderBy = 'name', bool $ascending = true, int $limit = null)
  {
    $qb = $this->createQueryBuilder('s')
      ->where('s.status NOT IN (:notAvailableStatues)')
      ->setParameter('notAvailableStatues', [Servizio::STATUS_DRAFT, Servizio::STATUS_PRIVATE, Servizio::STATUS_SUSPENDED])
      ->andWhere('s.sticky = true')
      ->orderBy('s.' . $orderBy, $ascending ? 'ASC' : 'DESC');

    if ($limit){
      $qb->setMaxResults($limit);
    }

    return $qb->getQuery()->getResult();
  }

  public function findNotStickyAvailable(string $orderBy = 'name', bool $ascending = true, int $limit = null)
  {
    $qb = $this->createQueryBuilder('s')
      ->where('s.status NOT IN (:notAvailableStatues)')
      ->setParameter('notAvailableStatues', [Servizio::STATUS_DRAFT, Servizio::STATUS_PRIVATE, Servizio::STATUS_SUSPENDED])
      ->andWhere('s.sticky = false OR s.sticky IS NULL')
      ->orderBy('s.' . $orderBy, $ascending ? 'ASC' : 'DESC');

    if ($limit){
      $qb->setMaxResults($limit);
    }

    return $qb->getQuery()->getResult();
  }

  public function findAvailableForSubscriptionPaymentSettings()
  {
    $qb = $this->createQueryBuilder('s')
      ->where('s.paymentRequired IS NOT NULL')
      ->andWhere('s.integrations IS NOT NULL')
      ->andWhere('s.status NOT IN (:notAvailableStatues)')
      ->setParameter('notAvailableStatues', [Servizio::STATUS_DRAFT, Servizio::STATUS_SUSPENDED])
      ->orderBy('s.name', 'ASC');

    return $qb->getQuery()->getResult();
  }

  public function getPublicServicesCount()
  {
    $qb = $this->createQueryBuilder('s')
      ->select('count(s.id)')
      ->where('s.status NOT IN (:notAvailableStatues)')
      ->setParameter('notAvailableStatues', [Servizio::STATUS_DRAFT, Servizio::STATUS_PRIVATE, Servizio::STATUS_SUSPENDED]);

    return $qb->getQuery()->getSingleScalarResult();
  }

  /**
   * @throws NonUniqueResultException
   */
  public function findOneBySlugOrIdentifier($search)
  {
    $qb = $this->createQueryBuilder('s')
      ->where('s.identifier = :identifier OR s.slug = :slug')
      ->setParameter('identifier', $search)
      ->setParameter('slug', $search);

    return $qb->getQuery()->getOneOrNullResult();
  }

  public function findByRequiredPayment()
  {
    $qb = $this->createQueryBuilder('s')
      ->where('s.paymentRequired IN (:paymentRequiredValues)')
      ->setParameter('paymentRequiredValues',  [Servizio::PAYMENT_REQUIRED, Servizio::PAYMENT_DEFERRED]);

    return $qb->getQuery()->getResult();
  }

  public function countPaymentServicesByIdentifier()
  {
    // Conto i servizi collegati per ogni intermediario di pagamento
    $servicesCountByIdentifier = [];
    $services = $this->findByRequiredPayment();
    foreach ($services as $service) {
      $identifier = $service->getActivePaymentGatewayIdentifier();
      $servicesCountByIdentifier[$identifier] = isset($servicesCountByIdentifier[$identifier]) ? ++$servicesCountByIdentifier[$identifier] : 1;
    }
    return $servicesCountByIdentifier;
  }

}
