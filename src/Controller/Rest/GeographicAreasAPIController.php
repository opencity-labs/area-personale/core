<?php

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Form\Api\GeographicAreaApiType;
use App\Entity\GeographicArea;
use App\Entity\User;
use App\Services\GeoService;
use App\Utils\FormUtils;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use \Exception;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Throwable;

/**
 * Class GeographicAreasAPIController
 * @package App\Controller\Rest
 * @Route("/geographic-areas")
 */
class GeographicAreasAPIController extends AbstractFOSRestController
{
  /**
   * @var string
   */
  public const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;

  private LoggerInterface $logger;

  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  /**
   * List all geographic area
   * @Rest\Get("", name="geographic_areas_api_list")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of geographic areas",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=GeographicArea::class, groups={"read"}))
   *     )
   * )
   *
   * @OA\Tag(name="geographic-areas")
   * @return View
   */
  public function getGeographicAreasAction(): View
  {
      $result = $this->entityManager->getRepository(GeographicArea::class)->findBy([], ['name' => 'asc']);
      return $this->view($result, Response::HTTP_OK);
  }

  /**
   * Check if a list of geographic areas contains point
   * @Rest\Get("/contains", name="geographic_area_api_contains")
   *
   * @OA\Parameter(
   *   name="lon",
   *   in="query",
   *   @OA\Schema(type="number", format="double"),
   *   required=true,
   *   description="Longitude of the point to check"
   * )
   *
   * @OA\Parameter(
   *   name="lat",
   *   in="query",
   *   @OA\Schema(type="number", format="double"),
   *   required=true,
   *   description="Latitude of the point to check"
   * )
   *
   *
   * @OA\Parameter(
   *   in="query",
   *   name="geographic_areas_ids",
   *   style="form",
   *   explode=false,
   *   required=true,
   *   description="List of uuids of the geographic areas to check",
   *   example="90bfa40e-c930-4e48-adc3-8c9235e1c137,aa1a43b0-ea0c-471f-a8c0-47d62862b0cb",
   *   @OA\Schema(
   *     type="array",
   *     @OA\Items(type="string")
   *   )
   * )
   *
   * @OA\Response(
   *   response=200,
   *   description="Retrieve a GeographicArea",
   *   @OA\JsonContent(
   *     type="object",
   *     @OA\Property(property="result", type="bool"),
   *   ),
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="GeographicArea not found"
   * )
   * @OA\Tag(name="geographic-areas")
   *
   */
  public function geographicAreasContainsPointAction(Request $request, GeoService $geoService): View
  {

    $result = [];

    $lonParameter = $request->get('lon', false);
    $latParameter = $request->get('lat', false);
    $geographicAreasIdsParameter = $request->get('geographic_areas_ids', []);

    if (empty($lonParameter)) {
      return $this->view(["lon parameter is mandatory"], Response::HTTP_BAD_REQUEST);
    }

    if (empty($latParameter)) {
      return $this->view(["lat parameter is mandatory"], Response::HTTP_BAD_REQUEST);
    }

    if (empty($geographicAreasIdsParameter)) {
      return $this->view(["geographic_areas_ids parameter is mandatory"], Response::HTTP_BAD_REQUEST);
    }

    $lon = (float)$lonParameter;
    $lat = (float)$latParameter;

    $geographicAreas = [];
    try {
      $repository = $this->entityManager->getRepository(GeographicArea::class);
      $geographicAreasIds = explode(',', $geographicAreasIdsParameter);
      foreach ($geographicAreasIds as $id) {

        if (!Uuid::isValid($id)) {
          throw new Exception($id .' is not a valid uuid');
        }

        $geographicArea = $repository->find($id);

        if (!$geographicArea instanceof GeographicArea) {
          throw new Exception('There is no geographic area with id:' . $id);
        }

        $geographicAreas []= $geographicArea;
      }

      foreach ($geographicAreas as $area) {
        if ($area->getGeofence() && $geoService->geoFenceContainsPoint($lon, $lat, $area->getGeofence())) {
          $result []= $area;
        }
      }

    } catch (Throwable $throwable) {
      return $this->view([$throwable->getMessage()], Response::HTTP_NOT_FOUND);
    }

    return $this->view(['result' => $result !== []], Response::HTTP_OK);
  }

  /**
   * Retrieve a geographic area by id
   * @Rest\Get("/{id}", name="geographic_area_api_get")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a GeographicArea",
   *     @Model(type=GeographicArea::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="GeographicArea not found"
   * )
   * @OA\Tag(name="geographic-areas")
   * @param string $id
   * @return View
   */
  public function getGeographicAreaAction(string $id): View
  {
    try {
      $repository = $this->entityManager->getRepository(GeographicArea::class);
      $result = $repository->find($id);
    } catch (Exception $exception) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    if (!$result instanceof GeographicArea) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    return $this->view($result, Response::HTTP_OK);
  }

  /**
   * Create a geographic area
   * @Rest\Post(name="geographic_areas_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\RequestBody(
   *     description="The geographic area to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=GeographicArea::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a GeographicArea"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   * @OA\Tag(name="geographic-areas")
   *
   * @param Request $request
   * @return View
   */
  public function postGeographicAreaAction(Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    $item = new GeographicArea();
    $form = $this->createForm(GeographicAreaApiType::class, $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {

      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (Exception $exception) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $exception->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view($item, Response::HTTP_CREATED);
  }

  /**
   * Edit full geographic area
   * @Rest\Put("/{id}", name="geographic_areas_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\RequestBody(
   *     description="The recipient to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=GeographicArea::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full GeographicArea"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="geographic-areas")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function putGeographicAreaAction($id, Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    $repository = $this->entityManager->getRepository(GeographicArea::class);
    $item = $repository->find($id);

    if (!$item instanceof GeographicArea) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm(GeographicAreaApiType::class, $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'put_validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (Exception $exception) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $exception->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Modified Successfully"], Response::HTTP_OK);
  }

  /**
   * Patch a geographic area
   * @Rest\Patch("/{id}", name="geographic_areas_api_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\RequestBody(
   *     description="The recipient to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=GeographicArea::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch a GeographicArea"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="geographic-areas")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function patchGeographicAreaAction($id, Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    $repository = $this->entityManager->getRepository(GeographicArea::class);
    $item = $repository->find($id);

    if (!$item instanceof GeographicArea) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm(GeographicAreaApiType::class, $item);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->entityManager->persist($item);
      $this->entityManager->flush();
    } catch (Exception $exception) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error(
        $exception->getMessage(),
        ['request' => $request]
      );
      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_OK);
  }

  /**
   * Delete a geographic area
   * @Rest\Delete("/{id}", name="geographic_area_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="geographic-areas")
   *
   * @param $id
   * @return View
   */
  public function deleteGeographicAreaAction($id): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    $item = $this->entityManager->getRepository(GeographicArea::class)->find($id);
    if ($item instanceof GeographicArea) {
      $this->entityManager->remove($item);
      $this->entityManager->flush();
    }

    return $this->view(null, Response::HTTP_NO_CONTENT);
  }

  private function processForm(Request $request, FormInterface $form): void
  {
    $data = json_decode($request->getContent(), true);

    $clearMissing = $request->getMethod() !== 'PATCH';
    $form->submit($data, $clearMissing);
  }
}
