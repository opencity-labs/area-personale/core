<?php

namespace App\Controller\Rest;

use App\Entity\OpeningHour;
use App\Model\Api\PagedList;
use App\Services\Api\PaginationLinksFactory;
use App\Services\MeetingService;
use DateInterval;
use DatePeriod;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\NotFoundProblem;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use App\Entity\Calendar;
use App\Model\LinksPagedList;
use App\Model\MetaPagedList;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class AvailabilitiesAPIController
 * @property EntityManagerInterface $entityManager
 * @property LoggerInterface $logger
 * @package App\Controller
 * @Route("/availabilities")
 */
class AvailabilitiesAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';
  public const DEFAULT_CACHE_LIFETIME = 0;

  public const RANGE_DAYS_LIMIT = 10;

  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;
  private MeetingService $meetingService;

  // Nb: Il nome della variabile deve essere il nome del pool senza punto in camel case altrimenti non riesce a fare l'injection
  // Es. availability.cache --> $availabilityCache
  private CacheItemPoolInterface $availabilityCache;

  public function __construct(EntityManagerInterface $entityManager, MeetingService $meetingService, LoggerInterface $logger, CacheItemPoolInterface $availabilityCache)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->meetingService = $meetingService;
    $this->availabilityCache = $availabilityCache;
  }


  /**
   * Retrieve calendar's availabilities
   * @Rest\Get("", name="calendars_availabilities_api_get")
   *
   *
   * @OA\Parameter(
   *      name="calendar_ids",
   *      in="query",
   *      @OA\Schema(
   *          type="array",
   *          @OA\Items(type="string", format="uuid"),
   *          example="71cb742a-c088-4b09-961f-61c53647eff7,b3477bc2-a160-4d0d-a8f1-0444d0f81849"
   *      ),
   *      required=true,
   *      description="List of calendars to request availability for."
   * )
   *
   * @OA\Parameter(
   *      name="available",
   *      in="query",
   *      @OA\Schema(
   *          type="boolean", default="true"
   *      ),
   *      required=false,
   *      description="Get only available dates: available dates includes at least one available slot"
   *  )
   *
   * @OA\Parameter(
   *      name="from_date",
   *      in="query",
   *      @OA\Schema(
   *          type="string", format="Y-m-d"
   *      ),
   *      required=false,
   *      description="Get availabilities from given date with format <code>yyyy-mm-dd</code>, default value is current date"
   *  )
   *
   * @OA\Parameter(
   *      name="to_date",
   *      in="query",
   *      @OA\Schema(
   *          type="string", format="Y-m-d"
   *      ),
   *      required=false,
   *      description="Get availabilities to given datewith format <code>yyyy-mm-dd</code>, default value is last day of current month"
   *  )
   *
   * @OA\Response(
   *    response=200,
   *    description="Retrieve list of availabilities by calendars",
   *    @OA\JsonContent(
   *       type="object",
   *       @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *       @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *       @OA\Property(property="data", type="array",
   *          @OA\Items(
   *              type="object",
   *              @OA\Property(property="date", type="string", format="Y-m-d"),
   *              @OA\Property(property="calendar_ids", type="array", @OA\Items(type="string"), format="uuid"),
   *              @OA\Property(property="available", type="boolean")
   *          )
   *       )
   *    )
   * )
   *
   *
   * @OA\Response(
   *     response=404,
   *     description="Calendars not found"
   * )
   *
   * @OA\Tag(name="availabilities")
   *
   * @param Request $request
   * @return View
   * @throws ApiProblemException|InvalidArgumentException
   */
  public function getCalendarsAvailabilitiesAction(Request $request): View
  {
    $calendarsParameter = $request->get('calendar_ids', false);
    if (!$calendarsParameter) {
      throw new ApiProblemException(
        new BadRequestProblem('Parameter calendar_ids is mandatory')
      );
    }

    $fromDateParameter = $request->query->get('from_date');
    $fromDate = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
    if ($fromDateParameter) {
      $fromDate = DateTime::createFromFormat('Y-m-d', $fromDateParameter);
      if (!$fromDate || ($fromDate->format('Y-m-d') !== $fromDateParameter)) {
        throw new ApiProblemException(
          new BadRequestProblem('Parameter from_date must be in on of these formats: yyyy-mm-dd')
        );
      }
    }

    $endDateParameter = $request->query->get('to_date');
    $toDate = clone $fromDate;
    $toDate->modify('last day of this month');
    if ($endDateParameter) {
      $toDate = DateTime::createFromFormat('Y-m-d', $endDateParameter);
      if (!$toDate || ($toDate->format('Y-m-d') !== $endDateParameter)) {
        throw new ApiProblemException(
          new BadRequestProblem('Parameter to_date must be in on of these formats: yyyy-mm-dd')
        );
      }
    }

    $interval = $toDate->diff($fromDate);
    if ($interval->days > 365) {
      throw new ApiProblemException(
        new BadRequestProblem('Maximum allowed range is 365 days')
      );
    }

    $available = $request->query->getBoolean('available', true);
    $calendarIds = explode(',', $calendarsParameter);
    $availableAvailabilities = [];
    try {

      $cache = $this->availabilityCache->getItem(md5($request->getRequestUri()));
      if ($cache->isHit()) {
        return $this->view($cache->get(), Response::HTTP_OK);
      }

      foreach ($calendarIds as $id) {
        $calendar = $this->entityManager->getRepository(Calendar::class)->findOneBy(['id' => $id]);
        if (!$calendar instanceof Calendar) {
          throw new ApiProblemException(
            new NotFoundProblem("Calendar {$id} not found")
          );
        }

        // Compute availabilities
        $availabilities = [];
        $openingHours = $this->entityManager->getRepository(OpeningHour::class)->findBy(['calendar' => $id]);

        foreach ($openingHours as $openingHour) {
          $slots = $this->meetingService->explodeDays($openingHour, false, $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));
          foreach ($slots as $slot) {
            if (!isset($availabilities[$slot])) {
              $availabilities[$slot] = $slot;
            }
          }
        }

        // Dall'intero gruppo delle disponibilità elimina quelle che coincidono con i periodi di chiusura
        foreach ($availabilities as $date) {
          $slots = $this->meetingService->getAvailabilitiesByDate(
            $calendar,
            new DateTime($date),
            false,
            $available,
          );
          $isAvailable = !empty($slots);

          // Se voglio i giorni disponibili e il giorno non ha delle disponibilità
          if (!$isAvailable && $available) {
            continue;
          }

          if (!isset($availableAvailabilities[$date])) {
            $availableAvailabilities[$date] = [
              'date' => $date,
              'calendar_ids' => [$id],
              'available' => $isAvailable
            ];
          } else {

            $availableAvailabilities[$date]['calendar_ids'][] = $id;
          }
        }
      }
      ksort($availableAvailabilities);

      $data = array_values($availableAvailabilities);
      $meta = new MetaPagedList();
      $meta->setParameter([
        'calendar_ids' => $calendarsParameter,
        'from_date' => $fromDate->format('Y-m-d'),
        'to_date' => $toDate->format('Y-m-d'),
        'available' => $available
      ]);
      $meta->setCount(count($data));
      $links = new LinksPagedList();
      $self = $this->generateUrl('calendars_availabilities_api_get', $meta->getParameter(), UrlGeneratorInterface::ABSOLUTE_URL);
      $links->setSelf($self);
      $result = new PagedList($meta, $links, $data);
      $this->saveResultInCache($cache, $result);

      return $this->view($result, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('calendars_availabilities_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('calendars_availabilities_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  /**
   * Retrieve calendars availabilities by Date
   * @Rest\Get("/{date}", name="calendars_day_availabilities_api_get")
   *
   * @OA\Parameter(
   *       name="calendar_ids",
   *       in="query",
   *       @OA\Schema(
   *           type="array",
   *           @OA\Items(type="string", format="uuid"),
   *           example="71cb742a-c088-4b09-961f-61c53647eff7,b3477bc2-a160-4d0d-a8f1-0444d0f81849"
   *       ),
   *       required=true,
   *       description="List of calendars to request availability for."
   *  )
   *
   * @OA\Parameter(
   *      name="available",
   *      in="query",
   *      @OA\Schema(
   *          type="boolean", default="true"
   *      ),
   *      required=false,
   *      description="Get only available slots"
   *  )
   *
   *
   * @OA\Response(
   *    response=200,
   *    description="Retrieve list of availabilities per date by calendars",
   *    @OA\JsonContent(
   *       type="object",
   *       @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *       @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *       @OA\Property(property="data", type="array",
   *          @OA\Items(
   *              type="object",
   *              @OA\Property(property="date", type="string", format="Y-m-d", example="2024-02-20"),
   *              @OA\Property(property="start_time", type="string", format="H:i", example="09:00"),
   *              @OA\Property(property="end_time", type="string", format="H:i", example="09:30"),
   *              @OA\Property(property="slots_available", type="int"),
   *              @OA\Property(property="availability", type="boolean"),
   *              @OA\Property(property="opening_hour_id", type="string", format="uuid"),
   *              @OA\Property(property="calendar_id", type="string", format="uuid")
   *          )
   *       )
   *    )
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Calendars not found"
   * )
   * @OA\Tag(name="availabilities")
   *
   * @param $date
   * @param Request $request
   * @return View
   * @throws ApiProblemException|InvalidArgumentException
   */
  public function getCalendarsAvailabilitiesByDateAction($date, Request $request): View
  {

    $calendarsParameter = $request->get('calendar_ids', false);
    if (!$calendarsParameter) {
      throw new ApiProblemException(
        new BadRequestProblem('Parameter calendar_ids is mandatory')
      );
    }

    try {
      $inputDate = new DateTime($date);
    } catch (\Exception $e) {
      throw new ApiProblemException(
        new BadRequestProblem('Invalid parameter. ' . $date . ' is not a valid date')
      );
    }

    $available = $request->query->getBoolean('available', true);
    $selectedOpeningHours = $request->get('opening_hours', false) ? explode(",",$request->get('opening_hours', false)) : null;
    $calendarIds = explode(',', $calendarsParameter);

    $availabilities = [];
    try {

      $cache = $this->availabilityCache->getItem(md5($request->getRequestUri()));
      if ($cache->isHit()) {
        return $this->view($cache->get(), Response::HTTP_OK);
      }


      foreach ($calendarIds as $id) {
        $calendar = $this->entityManager->getRepository(Calendar::class)->findOneBy(['id' => $id]);
        if (!$calendar instanceof Calendar) {
          throw new ApiProblemException(
            new NotFoundProblem("Calendar {$id} not found")
          );
        }

        $slots = $this->meetingService->getAvailabilitiesByDate($calendar, $inputDate, false, $available,null, $selectedOpeningHours);

        foreach ($slots as $k => $v) {
          if (isset($availabilities[$k])) {
            continue;
          }
          $v['opening_hour_id'] = $v['opening_hour'];
          unset($v['opening_hour']);
          $v['calendar_id'] = $id;
          $availabilities[$k] = $v;
        }
      }

      ksort($availabilities);
      $data = array_values($availabilities);
      $meta = new MetaPagedList();
      $meta->setParameter([
        'calendar_ids' => $calendarsParameter,
        'date' => $date,
        'available' => $available

      ]);
      $meta->setCount(count($data));
      $links = new LinksPagedList();
      $self = $this->generateUrl('calendars_day_availabilities_api_get', $meta->getParameter(), UrlGeneratorInterface::ABSOLUTE_URL);
      $links->setSelf($self);
      $result = new PagedList($meta, $links, $data);

      $this->saveResultInCache($cache, $result);

      return $this->view($result, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('calendars_day_availabilities_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('calendars_day_availabilities_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  /**
   * Retrieve calendars availabilities by range
   * @Rest\Get("/{start_date}/{end_date}", name="calendars_range_availabilities_api_get")
   *
   * @OA\Parameter(
   *       name="calendar_ids",
   *       in="query",
   *       @OA\Schema(
   *           type="array",
   *           @OA\Items(type="string", format="uuid"),
   *           example="71cb742a-c088-4b09-961f-61c53647eff7,b3477bc2-a160-4d0d-a8f1-0444d0f81849"
   *       ),
   *       required=true,
   *       description="List of calendars to request availability for."
   *  )
   *
   * @OA\Parameter(
   *      name="available",
   *      in="query",
   *      @OA\Schema(
   *          type="boolean", default="true"
   *      ),
   *      required=false,
   *      description="Get only available slots"
   *  )
   *
   * @OA\Parameter(
   *       name="offset",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Offset of the result"
   *  )
   *
   * @OA\Parameter(
   *       name="limit",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Limit of the result"
   *  )
   *
   *
   * @OA\Response(
   *    response=200,
   *    description="Retrieve list of availabilities per date by calendars",
   *    @OA\JsonContent(
   *       type="object",
   *       @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *       @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *       @OA\Property(property="data", type="array",
   *          @OA\Items(
   *              type="object",
   *              @OA\Property(property="date", type="string", format="Y-m-d", example="2024-02-20"),
   *              @OA\Property(property="start_time", type="string", format="H:i", example="09:00"),
   *              @OA\Property(property="end_time", type="string", format="H:i", example="09:30"),
   *              @OA\Property(property="slots_available", type="int"),
   *              @OA\Property(property="availability", type="boolean"),
   *              @OA\Property(property="opening_hour_id", type="string", format="uuid"),
   *              @OA\Property(property="calendar_id", type="string", format="uuid")
   *          )
   *       )
   *    )
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Calendars not found"
   * )
   * @OA\Tag(name="availabilities")
   *
   * @param $start_date
   * @param $end_date
   * @param Request $request
   * @param PaginationLinksFactory $paginationLinksFactory
   * @return View
   * @throws ApiProblemException
   * @throws InvalidArgumentException
   */
  public function getCalendarsAvailabilitiesByRangeAction($start_date, $end_date, Request $request, PaginationLinksFactory $paginationLinksFactory): View
  {

    $offset = $request->query->getInt('offset');
    $limit = $request->query->getInt('limit', 20);

    $calendarsParameter = $request->get('calendar_ids', false);
    if (!$calendarsParameter) {
      throw new ApiProblemException(
        new BadRequestProblem('Parameter calendar_ids is mandatory')
      );
    }

    // Validate and parse start and end dates
    try {
      $startDate = new DateTime($start_date);
      $endDate = new DateTime($end_date);
      if ($endDate < $startDate) {
        throw new \Exception('end_date must be greater than start_date');
      }
      if ($endDate->diff($startDate)->days > self::RANGE_DAYS_LIMIT) {
        throw new \Exception('end_date cannot be more than '. self::RANGE_DAYS_LIMIT .' days later than the start_date');
      }
    } catch (\Exception $e) {
      throw new ApiProblemException(
        new BadRequestProblem('Invalid parameter: ' . $e->getMessage())
      );
    }

    $available = $request->query->getBoolean('available', true);
    $selectedOpeningHours = $request->get('opening_hours', false) ? explode(",",$request->get('opening_hours', false)) : null;
    $calendarIds = explode(',', $calendarsParameter);

    $availabilities = [];
    try {

      $cache = $this->availabilityCache->getItem(md5($request->getRequestUri()));
      if ($cache->isHit()) {
        return $this->view($cache->get(), Response::HTTP_OK);
      }

      foreach ($calendarIds as $id) {
        $calendar = $this->entityManager->getRepository(Calendar::class)->findOneBy(['id' => $id]);
        if (!$calendar instanceof Calendar) {
          throw new ApiProblemException(
            new NotFoundProblem("Calendar {$id} not found")
          );
        }

        $datePeriod = new DatePeriod(
          $startDate->setTime(0, 0),
          new DateInterval('P1D'), // Daily interval
          (clone $endDate)->setTime(23, 59, 59)
        );

        foreach ($datePeriod as $currentDate) {
          $currentDateKey = $currentDate->format('Y-m-d');
          $slots = $this->meetingService->getAvailabilitiesByDate($calendar, $currentDate, false, $available,null, $selectedOpeningHours);

          foreach ($slots as $k => $v) {
            $index = $currentDateKey . ' ' . $k;
            if (isset($availabilities[$index])) {
              continue;
            }
            $v['opening_hour_id'] = $v['opening_hour'];
            unset($v['opening_hour']);
            $v['calendar_id'] = $id;
            $availabilities[$index] = $v;
          }
        }
      }

      ksort($availabilities);
      $data = array_values($availabilities);
      $count = count($data);
      $paginatedData = array_slice($data, $offset, $limit);

      $meta = new MetaPagedList();
      $meta->setParameter([
        'calendar_ids' => $calendarsParameter,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'available' => $available,
        'offset' => $offset,
        'limit' => $limit,

      ]);
      $meta->setCount($count);

      $links = $paginationLinksFactory->createLinks(
        'calendars_range_availabilities_api_get',
        $offset,
        $limit,
        $count,
        $meta->getParameter()
      );
      $result = new PagedList($meta, $links, $paginatedData);

      $this->saveResultInCache($cache, $result);

      return $this->view($result, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('calendars_day_availabilities_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('calendars_range_availabilities_api_get', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  private function saveResultInCache(CacheItemInterface $cacheItem, $data): void
  {
    $cacheItem->set($data);
    $cacheItem->expiresAfter(self::DEFAULT_CACHE_LIFETIME);
    $this->availabilityCache->save($cacheItem);
  }

}
