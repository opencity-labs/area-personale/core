<?php

namespace App\Controller\Rest;


use App\Dto\ApplicationDto;
use App\Entity\Allegato;
use App\Entity\AllegatoOperatore;
use App\Entity\CPSUser;
use App\Entity\FormIO;
use App\Entity\GiscomPratica;
use App\Entity\Message as MessageEntity;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use App\Entity\RispostaOperatore;
use App\Entity\Servizio;
use App\Entity\StatusChange;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Event\GenerateApplicationReceiptEvent;
use App\Event\KafkaEvent;
use App\Exception\ApplicationException;
use App\Form\Rest\Transition\MessageOrFileFormType;
use App\Model\Application;
use App\Model\File as FileModel;
use App\Model\LinksPagedList;
use App\Model\MetaPagedList;
use App\Model\PaymentOutcome;
use App\Model\Transition;
use App\Payment\Gateway\MyPay;
use App\Protocollo\Exception\AlreadySentException;
use App\Protocollo\Exception\InvalidStatusException;
use App\Security\Voters\ApplicationVoter;
use App\Security\Voters\AttachmentVoter;
use App\Security\Voters\ServiceVoter;
use App\Services\FileService\AllegatoFileService;
use App\Services\FormServerApiAdapterService;
use App\Services\GiscomAPIAdapterServiceInterface;
use App\Services\InstanceService;
use App\Services\Manager\PraticaManager;
use App\Services\ModuloPdfBuilderService;
use App\Services\PaymentService;
use App\Services\PraticaStatusService;
use App\Utils\FormUtils;
use App\Utils\GeoUtils;
use App\Utils\StringUtils;
use App\Utils\UploadedBase64File;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Flagception\Manager\FeatureManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use GeoJson\Feature\Feature;
use GeoJson\Feature\FeatureCollection;
use GeoJson\Geometry\Point;
use Http\Discovery\Exception\NotFoundException;
use JMS\Serializer\SerializerBuilder;
use League\Csv\Exception;
use League\Flysystem\FileNotFoundException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Exception\ValidatorException;
use App\Form\Rest\ApplicationFormType;
use App\Form\Rest\Transition\OutcomeFormType;
use App\Form\Rest\Transition\RequestIntegrationFormType;
use App\Form\ApplicationType;

/**
 * Class ApplicationsAPIController
 * @Route("/applications")
 */
class ApplicationsAPIController extends AbstractFOSRestController
{

  public const TRANSITION_SUBMIT = [
    'action' => 'submit',
    'description' => 'Submit Application',
  ];

  public const TRANSITION_REGISTER = [
    'action' => 'register',
    'description' => 'Register Application',
  ];

  public const TRANSITION_ASSIGN = [
    'action' => 'assign',
    'description' => 'Assign Application',
  ];

  public const TRANSITION_REQUEST_INTEGRATION = [
    'action' => 'request-integration',
    'description' => 'Request integration',
  ];

  public const TRANSITION_REGISTER_INTEGRATION_REQUEST = [
    'action' => 'register-integration-request',
    'description' => 'Register integration request',
  ];

  public const TRANSITION_ACCEPT_INTEGRATION = [
    'action' => 'accept-integration',
    'description' => 'Accept integration',
  ];

  public const TRANSITION_REGISTER_INTEGRATION_ANSWER = [
    'action' => 'register-integration-answer',
    'description' => 'Register integration answer',
  ];

  public const TRANSITION_ACCEPT = [
    'action' => 'accept',
    'description' => 'Accept Application',
  ];

  public const TRANSITION_REJECT = [
    'action' => 'reject',
    'description' => 'Reject Application',
  ];

  public const TRANSITION_WITHDRAW = [
    'action' => 'withdraw',
    'description' => 'Withdraw Application',
  ];

  public const TRANSITION_REVOKE = [
    'action' => 'revoke',
    'description' => 'Revoke Application',
  ];

  private EntityManagerInterface $em;

  private InstanceService $is;

  private PraticaStatusService $statusService;

  protected ModuloPdfBuilderService $pdfBuilder;

  protected UrlGeneratorInterface $router;

  protected string $baseUrl = '';

  protected LoggerInterface $logger;

  private PraticaManager $praticaManager;

  private FormServerApiAdapterService $formServerService;

  private AllegatoFileService $fileService;

  private ApplicationDto $applicationDto;

  private PaymentService $paymentService;

  private GiscomAPIAdapterServiceInterface $giscomAPIAdapterService;

  private EventDispatcherInterface $dispatcher;
  private FeatureManagerInterface $featureManager;

  /**
   * ApplicationsAPIController constructor.
   * @param EntityManagerInterface $em
   * @param InstanceService $is
   * @param PraticaStatusService $statusService
   * @param ModuloPdfBuilderService $pdfBuilder
   * @param UrlGeneratorInterface $router
   * @param LoggerInterface $logger
   * @param PraticaManager $praticaManager
   * @param FormServerApiAdapterService $formServerService
   * @param AllegatoFileService $fileService
   * @param ApplicationDto $applicationDto
   * @param PaymentService $paymentService
   * @param GiscomAPIAdapterServiceInterface $giscomAPIAdapterService
   * @param EventDispatcherInterface $dispatcher
   * @param FeatureManagerInterface $featureManager
   */
  public function __construct(
    EntityManagerInterface           $em,
    InstanceService                  $is,
    PraticaStatusService             $statusService,
    ModuloPdfBuilderService          $pdfBuilder,
    UrlGeneratorInterface            $router,
    LoggerInterface                  $logger,
    PraticaManager                   $praticaManager,
    FormServerApiAdapterService      $formServerService,
    AllegatoFileService              $fileService,
    ApplicationDto                   $applicationDto,
    PaymentService                   $paymentService,
    GiscomAPIAdapterServiceInterface $giscomAPIAdapterService,
    EventDispatcherInterface         $dispatcher, FeatureManagerInterface $featureManager
  )
  {
    $this->em = $em;
    $this->is = $is;
    $this->statusService = $statusService;
    $this->pdfBuilder = $pdfBuilder;
    $this->router = $router;
    $this->baseUrl = $this->router->generate('applications_api_list', [], UrlGeneratorInterface::ABSOLUTE_URL);
    $this->logger = $logger;
    $this->praticaManager = $praticaManager;
    $this->formServerService = $formServerService;
    $this->fileService = $fileService;
    $this->applicationDto = $applicationDto;
    $this->paymentService = $paymentService;
    $this->giscomAPIAdapterService = $giscomAPIAdapterService;
    $this->dispatcher = $dispatcher;
    $this->featureManager = $featureManager;
  }

  /**
   * List all Applications
   *
   * @Rest\Get("", name="applications_api_list")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="version",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Version of Api, default 1. From version 2 data field keys are exploded in a json object instead of version 1.X where are flattened strings"
   * )
   *
   * @OA\Parameter(
   *      name="service",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Slug of the service",
   *      example="service-slug"
   * )
   *
   * @OA\Parameter(
   *      name="service_id",
   *      in="query",
   *      @OA\Schema(
   *        type="string",
   *        format="uuid"
   *      ),
   *      required=false,
   *      description="Service identifier (uuid)"
   * )
   *
   * @OA\Parameter(
   *      name="service_identifier",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Public service identifier"
   * )
   *
   * @OA\Parameter(
   *      name="order",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Order field. Default creationTime"
   * )
   *
   * @OA\Parameter(
   *      name="sort",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Sorting criteria of the order field. Default ASC"
   * )
   *
   * @OA\Parameter(
   *      name="createdAt[after|before|strictly_after|strictly_before]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Created at filter, format yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"
   * )
   *
   * @OA\Parameter(
   *      name="updatedAt[after|before|strictly_after|strictly_before]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Updated at filter, format yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"
   * )
   *
   * @OA\Parameter(
   *      name="submittedAt[after|before|strictly_after|strictly_before]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Submitted at filter, format yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"
   * )
   *
   * @OA\Parameter(
   *      name="status",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Status code of application"
   * )
   *
   * @OA\Parameter(
   *   name="bounding_box",
   *   in="query",
   *   @OA\Schema(
   *     type="array",
   *     @OA\Items(type="number"),
   *     example="6.294200,36.467214,18.796641,47.243420"
   *   ),
   *   required=false,
   *   description="Only applications that have a geometry that intersects the bounding box are selected, the bounding box is provided as four numbers.<br>The coordinate reference system is WGS 84 longitude/latitude <a href='http://www.opengis.net/def/crs/OGC/1.3/CRS84'>http://www.opengis.net/def/crs/OGC/1.3/CRS84</a>.<br>For WGS 84 longitude/latitude the values are: minimum longitude, minimum latitude, maximum longitude and maximum latitude.<br>Example: 6.294200,36.467214,18.796641,47.243420"
   * )
   *
   * @OA\Parameter(
   *      name="field[dot-path]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Filter for a form field: the value contained in the square brackets <b>dot-path</b> must be the key of a field present in the compiled form.<br>For example, it is possible to search for applications sent by the applicant with the tax code <b>cf</b> by specifying the filter <code>/applications?field[applicant.data.fiscal_code.data.fiscal_code]=cf</code> or or more simply <code>/applications?field[applicant.fiscal_code.fiscal_code]=cf</code><br><b>Warning:</b> search is not available for data-type components such as datagrid, editgrid, etc<br><br> <b> The property name must only contain alphanumeric characters, underscores, dots and dashes and should not be ended by dash or dot.</b>"
   * )
   *
   * @OA\Parameter(
   *      name="backoffice_field[dot-path]",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Filter for a backoffice field: the value contained in the square brackets <b>dot-path</b> must be the key of a field present in the backoffice form.<br>For example, it is possible to search for applications for which the backoffice <b>code</b> field has been filled in with the value <b>value</b> by specifying the filter <code>/applications?backoffice_field[code]=value</code>.<br>As for the <code>field</code> filter, in case of nested form it is possible to search both with the extended and with the reduced dotted notation.<br><b>Warning:</b> search is not available for data-type components such as datagrid, editgrid, etc<br><br> <b>The property name must only contain alphanumeric characters, underscores, dots and dashes and should not be ended by dash or dot.</b>"
   * )
   *
   * @OA\Parameter(
   *      name="offset",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Offset of the query"
   * )
   *
   * @OA\Parameter(
   *      name="limit",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Limit of the query"
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of applications",
   *     @OA\JsonContent(
   *       type="object",
   *       @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *       @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *       @OA\Property(property="data", type="array", @OA\Items(ref=@Model(type=Application::class, groups={"read"})))
   *     ),
   *     @OA\MediaType(
   *       mediaType="application/geo+json",
   *       @OA\Schema(
   *         type="object",
   *         @OA\Property(property="bounding_box", nullable="true", description="Bounding box parameter if passed", type="object",
   *           @OA\Property(property="bounds", type="array", @OA\Items(type="number"), minItems=4, maxItems=4, example="[6.294200,36.467214,18.796641,47.243420]")
   *         ),
   *         @OA\Property(property="type", type="string", default="FeatureCollection"),
   *         @OA\Property(property="features", type="array",
   *           @OA\Items(
   *             type="object",
   *             @OA\Property(property="type", type="string", default="Feature"),
   *             @OA\Property(property="geometry", type="object",
   *               @OA\Property(property="type", type="string", default="Point"),
   *               @OA\Property(property="coordinates", type="array", @OA\Items(type="number"), minItems=2, maxItems=2, example="[18.796641,47.243420]"),
   *             ),
   *             @OA\Property(property="id", format="uuid"),
   *             @OA\Property(property="properties", nullable="true", type="object", @OA\AdditionalProperties(type="string"))
   *             )
   *           )
   *         ),
   *       )
   *     ),
   * )
   *
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=400,
   *    description="Bad Request"
   * )
   *
   * @OA\Tag(name="applications")
   */

  public function getApplicationsAction(Request $request): View
  {
    if (!$this->isGranted(User::ROLE_CPS_USER) && !$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }

    $offset = $request->query->getInt('offset');
    $limit = $request->query->getInt('limit', 10);
    $version = $request->query->getInt('version', 1);

    $serviceParameter = $request->get('service', false);
    $serviceIdParameter = $request->get('service_id', false);
    $serviceIdentifierParameter = $request->get('service_identifier', false);
    $statusParameter = $request->get('status', false);
    $createdAtParameter = $request->get('createdAt', false);
    $updatedAtParameter = $request->get('updatedAt', false);
    $submittedAtParameter = $request->get('submittedAt', false);
    $boundinBoxParameter = $request->get('bounding_box', false);
    $fieldParameter = $request->get('field', false);
    $backofficeFieldParameter = $request->get('backoffice_field', false);
    $orderParameter = $request->get('order', false);
    $sortParameter = $request->get('sort', false);

    if ($limit > 100) {
      return $this->view(["Limit parameter is too high"], Response::HTTP_BAD_REQUEST);
    }

    $queryParameters = ['offset' => $offset, 'limit' => $limit, 'version' => $version];

    if ($serviceParameter) {
      $queryParameters['service'] = $serviceParameter;
    }
    if ($serviceIdParameter) {
      $queryParameters['service_id'] = $serviceIdParameter;
    }
    if ($serviceIdentifierParameter) {
      $queryParameters['service_identifier'] = $serviceIdentifierParameter;
    }
    if ($statusParameter) {
      $queryParameters['status'] = $statusParameter;
    }
    if ($orderParameter) {
      $queryParameters['order'] = $orderParameter;
    }
    if ($sortParameter) {
      $queryParameters['sort'] = $sortParameter;
    }

    $dateFormat = 'Y-m-d';

    if ($createdAtParameter) {
      foreach ($createdAtParameter as $v) {
        $date = DateTime::createFromFormat($dateFormat, $v) ?: DateTime::createFromFormat(DATE_ATOM, $v);
        if (!$date || ($date->format($dateFormat) !== $v && $date->format(DATE_ATOM) !== $v)) {
          return $this->view(
            ["Parameter createdAt must be in on of these formats: yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"],
            Response::HTTP_BAD_REQUEST
          );
        }
      }
      $queryParameters['createdAt'] = $createdAtParameter;
    }

    if ($updatedAtParameter) {
      foreach ($updatedAtParameter as $v) {
        $date = DateTime::createFromFormat($dateFormat, $v) ?: DateTime::createFromFormat(DATE_ATOM, $v);
        if (!$date || ($date->format($dateFormat) !== $v && $date->format(DATE_ATOM) !== $v)) {
          return $this->view(
            ["Parameter updatedAt must be in on of these formats: yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"],
            Response::HTTP_BAD_REQUEST
          );
        }
      }
      $queryParameters['updatedAt'] = $updatedAtParameter;
    }

    if ($submittedAtParameter) {
      foreach ($submittedAtParameter as $v) {
        $date = DateTime::createFromFormat($dateFormat, $v) ?: DateTime::createFromFormat(DATE_ATOM, $v);
        if (!$date || ($date->format($dateFormat) !== $v && $date->format(DATE_ATOM) !== $v)) {
          return $this->view(
            ["Parameter submittedAt must be in on of these formats: yyyy-mm-dd or yyyy-mm-ddTHH:ii:ssP"],
            Response::HTTP_BAD_REQUEST
          );
        }
      }
      $queryParameters['submittedAt'] = $submittedAtParameter;
    }

    $boundingBox = null;
    if ($boundinBoxParameter) {
      try {
        if (!is_array($boundinBoxParameter)) {
          $boundinBoxParameter = json_decode('[' . $boundinBoxParameter . ']');
        }
        $boundingBox = GeoUtils::createBoundingBox($boundinBoxParameter);
      } catch (\Exception $e) {
        return $this->view([$e->getMessage()], Response::HTTP_BAD_REQUEST);
      }
      $queryParameters['bounding_box'] = $boundinBoxParameter;
    }

    if ($statusParameter) {
      $applicationStatuses = array_keys(Pratica::getStatuses());
      if (!in_array($statusParameter, $applicationStatuses)) {
        return $this->view(
          ["Status code not present, chose one between: " . implode(',', $applicationStatuses)],
          Response::HTTP_BAD_REQUEST
        );
      }
    }

    if ($fieldParameter) {
      $keyParameter = array_keys($fieldParameter);
      if (!preg_match("/^[A-Za-z0-9_.-]*([A-Za-z0-9])$/", current($keyParameter))) {
        return $this->view(
          ["Character not allowed in: " . current($keyParameter) . " the property name must only contain alphanumeric characters, underscores, dots and dashes and should not be ended by dash or dot"],
          Response::HTTP_BAD_REQUEST
        );
      }
      $queryParameters['field'] = $fieldParameter;
    }

    if ($backofficeFieldParameter) {
      $keyParameter = array_keys($backofficeFieldParameter);
      if (!preg_match("/^[A-Za-z0-9_.-]*([A-Za-z0-9])$/", current($keyParameter))) {
        return $this->view(
          ["Character not allowed in: " . current($keyParameter) . " the property name must only contain alphanumeric characters, underscores, dots and dashes and should not be ended by dash or dot"],
          Response::HTTP_BAD_REQUEST
        );
      }
      $queryParameters['backoffice_field'] = $backofficeFieldParameter;
    }

    $user = $this->getUser();
    $repositoryService = $this->em->getRepository(Servizio::class);
    $allowedServices = $this->getAllowedServices();
    $servicesByOperatore = $this->getServicesByOperatore();

    if (empty($servicesByOperatore) && $user instanceof OperatoreUser) {
      return $this->view(["You are not allowed to view applications"], Response::HTTP_FORBIDDEN);
    }

    if ($serviceParameter || $serviceIdParameter) {
      if ($serviceParameter) {
        $service = $repositoryService->findOneBy(['slug' => $serviceParameter]);
        $queryParameters['service'] = $serviceParameter;
      } else {
        if (!Uuid::isValid($serviceIdParameter)) {
          return $this->view(["Parameter service_id must be a valid uuid"], Response::HTTP_BAD_REQUEST);
        }
        $service = $repositoryService->find($serviceIdParameter);
        $queryParameters['service_id'] = $serviceIdParameter;
      }
      $getOnlyAssignedApplications = false;
      if (!$service instanceof Servizio) {
        return $this->view(["Service not found"], Response::HTTP_NOT_FOUND);
      }
      if (!empty($servicesByOperatore) && !in_array($service->getId(), $servicesByOperatore)) {
        return $this->view(["You are not allowed to view applications of passed service"], Response::HTTP_FORBIDDEN);
      }
      /* Se il servizio passato non è uno a cui l'operatore (a esclusione dell'operatore api) è abilitato è uno nel quale ha almeno
         una pratica assegnata allora mi predispongo in modo tale da ottenere SOLO le pratiche a lui assegnate */
      if ($user instanceof OperatoreUser && !$user->isSystemUser() && !in_array($service->getId(), $allowedServices) && in_array($service->getId(), $servicesByOperatore)) {
        $getOnlyAssignedApplications = true;
      }
      $allowedServices = [$service->getId()];
    }

    $result = [];
    $result['meta']['parameter'] = $queryParameters;
    $repoApplications = $this->em->getRepository(Pratica::class);

    try {
      $parameters = $queryParameters;
      if (!empty($allowedServices)) {
        $parameters['service'] = $allowedServices;
      }

      if ($user instanceof CPSUser) {
        $parameters['user'] = $user->getId();
        if ($this->featureManager->isActive('feature_personal_area_v2')) {
          $parameters['beneficiary'] = $user->getId();
        }
      } elseif ($user instanceof OperatoreUser) {
        if (isset($getOnlyAssignedApplications)) {
          $parameters['get_only_assigned_applications'] = $getOnlyAssignedApplications;
        }
        $parameters['operatore'] = $user;
      }
      $count = $repoApplications->getApplications($parameters, true);

    } catch (NoResultException $e) {
      $count = 0;
    } catch (NonUniqueResultException $e) {
      return $this->view($e->getMessage(), Response::HTTP_I_AM_A_TEAPOT);
    }

    $result['meta']['count'] = $count;
    $result['links']['self'] = $this->generateUrl(
      'applications_api_list',
      $queryParameters,
      UrlGeneratorInterface::ABSOLUTE_URL
    );
    $result['links']['prev'] = null;
    $result['links']['next'] = null;
    $result ['data'] = [];

    if ($offset != 0) {
      $queryParameters['offset'] = $offset - $limit;
      $result['links']['prev'] = $this->generateUrl(
        'applications_api_list',
        $queryParameters,
        UrlGeneratorInterface::ABSOLUTE_URL
      );
    }

    if ($offset + $limit < $count) {
      $queryParameters['offset'] = $offset + $limit;
      $result['links']['next'] = $this->generateUrl(
        'applications_api_list',
        $queryParameters,
        UrlGeneratorInterface::ABSOLUTE_URL
      );
    }
    $order = $orderParameter ?: "creationTime";
    $sort = $sortParameter ?: "ASC";

    try {

      if ($request->headers->get('accept') === 'application/geo+json') {
        $parameters['geojson'] = true;

        $applications = $repoApplications->getClusteredApplications($parameters);
        $features = [];
        /** @var Pratica $a */
        foreach ($applications as $a) {
          $clusterData = json_decode($a['center'], true);
          $point = new Point($clusterData['coordinates']);
          $properties = $id = null;

          // Se è un cluster aggiungo le properties con il numero di punti e genero un uuid
          if ($a['point_number'] > 1) {
            $properties['point_number'] = $a['point_number'];
            $id = Uuid::fromInteger($a['dbscan_cid'])->toString();
          } else {
            $id = $a['application_ids'];
          }
          $features [] = new Feature($point, $properties, $id);
        }
        $featuresCollection = new FeatureCollection($features, $boundingBox);
        $view = new View($featuresCollection);
        $view->setContext($view->getContext()->setSerializeNull(false));
        return $view;
      }

      $applications = $repoApplications->getApplications($parameters, false, $order, $sort, $offset, $limit);
      foreach ($applications as $s) {
        $result ['data'][] = $this->applicationDto->fromEntity($s, true, $version);
      }
      return $this->view($result, Response::HTTP_OK);

    } catch (\Exception $exception) {
      return $this->view($exception->getMessage(), Response::HTTP_BAD_REQUEST);
    }
  }


  /**
   * Retrieve an Application
   * @Rest\Get("/{id}", name="application_api_get")
   *
   * @OA\Parameter(
   *      name="version",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Version of Api, default 1. From version 2 data field keys are exploded in a json object instead of version 1.X where are flattened strings"
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve an Application",
   *     @OA\JsonContent(
   *       ref=@Model(type=Application::class, groups={"read"})
   *     ),
   *     @OA\MediaType(
   *       mediaType="application/geo+json",
   *       @OA\Schema(
   *         @OA\Property(property="type", type="string", default="Feature"),
   *         @OA\Property(property="geometry", type="object",
   *           @OA\Property(property="type", type="string", default="Point"),
   *           @OA\Property(property="coordinates", type="array", @OA\Items(type="number"), minItems=2, maxItems=2, example="[18.796641,47.243420]"),
   *         ),
   *         @OA\Property(property="id", format="uuid"),
   *       )
   *     ),
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function getApplicationAction($id, Request $request): View
  {
    return $this->getApplicationById($id, $request);
  }

  /**
   * Retrieve an Application by External Id
   * @Rest\Get("/byexternal-id/{externalId}", name="application_api_get_by_external_id")
   *
   * @OA\Parameter(
   *      name="externalId",
   *      in="path",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=true,
   *      description="Application's id for external integrations. This field can be used as a reference to a mapped entity present in an external system."
   * )
   *
   * @OA\Parameter(
   *      name="version",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Version of Api, default 1. From version 2 data field keys are exploded in a json object instead of version 1.X where are flattened strings"
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve an Application",
   *     @OA\JsonContent(
   *       ref=@Model(type=Application::class, groups={"read"})
   *     ),
   *     @OA\MediaType(
   *       mediaType="application/geo+json",
   *       @OA\Schema(
   *         @OA\Property(property="type", type="string", default="Feature"),
   *         @OA\Property(property="geometry", type="object",
   *           @OA\Property(property="type", type="string", default="Point"),
   *           @OA\Property(property="coordinates", type="array", @OA\Items(type="number"), minItems=2, maxItems=2, example="[18.796641,47.243420]"),
   *         ),
   *         @OA\Property(property="id", format="uuid"),
   *       )
   *     ),
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $externalId
   * @param Request $request
   * @return View
   */
  public function getApplicationByExternalIdAction($externalId, Request $request): View
  {
    return $this->getApplicationById($externalId, $request, true);
  }

  /**
   * Create an Application
   * @Rest\Post(name="applications_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Application::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create an Application"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param Request $request
   * @return View
   * @throws \Doctrine\DBAL\Driver\Exception|\JsonException
   */
  public function postApplicationAction(Request $request): View
  {

    if (!$this->isGranted(User::ROLE_CPS_USER) && !$this->isGranted(User::ROLE_OPERATORE) && !$this->isGranted(User::ROLE_ADMIN)) {
      throw $this->createAccessDeniedException();
    }
    $applicationModel = new Application();
    $form = $this->createForm(ApplicationFormType::class, $applicationModel);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    $serviceRepo = $this->em->getRepository(Servizio::class);
    if (Uuid::isValid($applicationModel->getService())) {
      $service = $serviceRepo->find($applicationModel->getService());
    } else {
      $service = $serviceRepo->findOneBySlugOrIdentifier($applicationModel->getService());
    }

    if (!$service instanceof Servizio) {
      return $this->view(["Service not found"], Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->denyAccessUnlessGranted(ServiceVoter::ACCESS, $service);
    } catch (AccessDeniedException $e) {
      return $this->view(["Is not possible add an application, the service you are trying to apply for is not available"], Response::HTTP_FORBIDDEN);
    }

    $result = $this->formServerService->getSchema($service);
    if ($result['status'] !== 'success') {
      return $this->view(["There was an error on retrieve form schema"], Response::HTTP_BAD_REQUEST);
    }
    $schema = $result['schema'];
    $this->praticaManager->setSchema($schema);
    $flatSchema = $this->praticaManager->flatSchema($schema);
    $this->praticaManager->setFlatSchema($flatSchema);
    $cleanedData = StringUtils::cleanData($applicationModel->getData());
    $flatData = $this->praticaManager->arrayFlat($cleanedData);

    if (empty($cleanedData)) {
      return $this->view(["Empty application are not allowed"], Response::HTTP_BAD_REQUEST);
    }

    $data = [
      'data' => $cleanedData,
      'flattened' => $flatData,
      'schema' => $flatSchema,
    ];

    $user = $this->getUser();
    if ($user instanceof CPSUser) {
      try {
        // se l'utente non è anonimo verifico la coerenza dei dati
        if (!$user->isAnonymous()) {
          $this->praticaManager->validateUserData($flatData, $user);
        }
        $this->praticaManager->updateUserFromApplicantData($data, $user);
      } catch (\Exception $e) {
        $data = [
          'type' => 'error',
          'title' => 'There was an error during save process',
          'description' => $e->getMessage(),
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }
    } else {
      try {
        // Se è stato passato un user_id nel campo uuid provo a recuperare l'utente
        if ($applicationModel->getUser() && Uuid::isValid($applicationModel->getUser())) {
          $user = $this->em->getRepository(CPSUser::class)->find($applicationModel->getUser());
        }

        // Se non ho un utente lo creo
        if (!$user instanceof CPSUser) {
          // La funzione check user prova a riconciliare l'utente se il codice fiscale passato è uguale alla username recuperata da Spid
          // In questo caso va bene la riconciliazione perchè questa azione la sta svolgendo un admin o un operatore
          $user = $this->praticaManager->fetchOrCreateApplicant($data);
        }

      } catch (\Exception $e) {
        $this->logger->error($e->getMessage());
        return $this->view(["Something is wrong"], Response::HTTP_INTERNAL_SERVER_ERROR);
      }
    }

    try {
      $statusChange = null;
      $currentUser = $this->getUser();
      $praticaStatus = $applicationModel->getStatus();
      if ($user !== $currentUser) {
        $statusChange = new StatusChange();
        $statusChange->setEvento('Creazione pratica da altro soggetto.');
        $statusChange->setOperatore(($currentUser instanceof User ? $currentUser->getFullName() : $currentUser->getUsername()));
        if ($applicationModel->getCreatedAt() instanceof DateTime) {
          $statusChange->setTimestamp($applicationModel->getCreatedAt()->format('U'));
        }

        if (
          $service->isPaymentRequired()
          && $service->getActivePaymentGatewayIdentifier() !== MyPay::IDENTIFIER
          && $praticaStatus == Pratica::STATUS_PRE_SUBMIT
        ) {
          $praticaStatus = Pratica::STATUS_PAYMENT_PENDING;
        }
      }

      /** @var FormIO $pratica */
      $pratica = $this->applicationDto->toEntity($applicationModel, new FormIO());
      $pratica->setUser($user);
      $this->praticaManager->fetchOrCreateBeneficiary($pratica);
      $pratica->setEnte($this->is->getCurrentInstance());
      $pratica->setServizio($service);
      $pratica->setLocale($request->getLocale());
      $pratica->setStatus($praticaStatus, $statusChange);

      // Data di invio
      if ($pratica->getStatus() > Pratica::STATUS_DRAFT) {
        if ($applicationModel->getCreatedAt()) {
          $pratica->setSubmissionTime($applicationModel->getCreatedAt()->getTimestamp());
        } else {
          $pratica->setSubmissionTime(time());
        }
      }

      // Salto la validazione dei dati inseriti finché la pratica si trova in bozza
      if ($pratica->getStatus() !== Pratica::STATUS_DRAFT) {
        $this->praticaManager->validateDematerializedData($data, $pratica);
      }

      // Verifica la presenza di aree collegate al servizio, validata le coordinate dell'utente e assegna un'area
      // Solleva un errore nel caso in cui le coordinate dell'utente non sono collegate in nessuna area del servizio
      $this->praticaManager->addApplicationToGeographicAreas($pratica, $data);

      $pratica->setDematerializedForms($data);

      $this->praticaManager->addAttachmentsToApplication($pratica, $flatData);
      $pratica->setGeneratedSubject();

      $this->praticaManager->saveAndDispatchEvent($pratica);

      // Emette l'evento per la generazione del pdf
      $this->dispatcher->dispatch(new GenerateApplicationReceiptEvent($pratica), GenerateApplicationReceiptEvent::NAME);

    } catch (ValidatorException $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during data validation',
        'description' => $e->getMessage(),
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (UniqueConstraintViolationException $ucve) {
      $data = [
        'type' => 'duplicated_application',
        'title' => 'There was an error during save process',
        'description' => "An application with external id {$pratica->getExternalId()} already exists"
      ];
      $this->logger->error(
        $ucve->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view($this->applicationDto->fromEntity($pratica), Response::HTTP_CREATED);
  }

  /**
   * Edit full Application
   * @Rest\Put("/{id}", name="applications_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application to edit",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=Application::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full Application"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   * @throws \Doctrine\DBAL\Driver\Exception
   */
  public function putApplicationAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      /** @var Pratica $application */
      $application = $repository->find($id);

      if (!$application) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }

      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      if (in_array(
        $application->getStatus(),
        [Pratica::FINAL_STATES]
      )) {
        return $this->view(["Application is in a final state, can't be updated."], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $applicationModel = new Application();
      $form = $this->createForm(ApplicationFormType::class, $applicationModel);
      $this->processForm($request, $form);

      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $result = $this->formServerService->getSchema($application->getServizio());
      if ($result['status'] !== 'success') {
        return $this->view(["There was an error on retrieve form schema"], Response::HTTP_BAD_REQUEST);
      }
      $schema = $result['schema'];
      $this->praticaManager->setSchema($schema);
      $flatSchema = $this->praticaManager->flatSchema($schema);
      $this->praticaManager->setFlatSchema($flatSchema);
      $cleanedData = StringUtils::cleanData($applicationModel->getData());
      $flatData = $this->praticaManager->arrayFlat($cleanedData);

      if (empty($cleanedData)) {
        return $this->view(["Empty application are not allowed"], Response::HTTP_BAD_REQUEST);
      }

      $data = [
        'data' => $cleanedData,
        'flattened' => $flatData,
        'schema' => $flatSchema,
      ];

      // Verifica la presenza di aree collegate al servizio, validata le coordinate dell'utente e assegna un'area
      // Solleva un errore nel caso in cui le coordinate dell'utente non sono collegate in nessuna area del servizio
      $this->praticaManager->addApplicationToGeographicAreas($application, $data);

      // Salto la validazione dei dati inseriti finché la pratica si trova in bozza
      if ($application->getStatus() !== Pratica::STATUS_DRAFT) {
        $this->praticaManager->validateDematerializedData($data, $application);
      }

      if ($application->getStatus() == Pratica::STATUS_DRAFT && $applicationModel->getStatus() > Pratica::STATUS_DRAFT) {
        $application->setSubmissionTime(time());
      }

      $application->setDematerializedForms($data);
      $this->praticaManager->fetchOrCreateBeneficiary($application);

      $this->praticaManager->addAttachmentsToApplication($application, $flatData);

      $application->setGeneratedSubject();
      $application->setExternalId($applicationModel->getExternalId());

      if ($applicationModel->getUserCompilationNotes() !== null && $this->getUser()->getId() == $application->getUser()->getId()) {
        $application->setUserCompilationNotes($applicationModel->getUserCompilationNotes());
      }

      if ($application->getStamps() !== $applicationModel->getStamps()) {
        $application->setStamps($applicationModel->getStamps());
      }

      if ($application->getStatus() == Pratica::STATUS_DRAFT
        && ($applicationModel->getStatus() == Pratica::STATUS_PRE_SUBMIT
          || $applicationModel->getStatus() == Pratica::STATUS_DRAFT_FOR_INTEGRATION)
      ) {
        $this->praticaManager->finalizeSubmission($application);
      } else {
        $application->setStatus($applicationModel->getStatus());
        $this->em->persist($application);
        $this->em->flush();
      }

    } catch (ValidatorException $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during data validation',
        'description' => $e->getMessage(),
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (UniqueConstraintViolationException $ucve) {
      $data = [
        'type' => 'duplicated_application',
        'title' => 'There was an error during save process',
        'description' => "An application with external id {$application->getExternalId()} already exists"
      ];
      $this->logger->error(
        $ucve->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to edit this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Application Modified Successfully"], Response::HTTP_OK);

  }

  /**
   * Delete an Application
   * @Rest\Delete("/{id}", name="application_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *    response=400,
   *    description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   * response=403,
   * description="Forbidden"
   * )
   *
   * @OA\Response(
   * response=404,
   * description="Not found"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @return View
   */
  public function deleteApplicationAction(
    $id
  ): View {

    if (!Uuid::isValid($id)) {
      return $this->view(["Id $id is not a valid Uuid"], Response::HTTP_BAD_REQUEST);
    }

    $repository = $this->em->getRepository(Pratica::class);
    /** @var Pratica $application */
    $application = $repository->find($id);

    if (!$application) {
      return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(
      ApplicationVoter::DELETE,
      $application,
      "User can not delete application {$application->getId()}",
    );

    try {

      $this->praticaManager->delete($application);

    } catch (ApplicationException $e) {

      return $this->view([$e->getMessage()], Response::HTTP_FORBIDDEN);
    }

    return $this->view(null, Response::HTTP_NO_CONTENT);
  }


  /**
   * Retrieve backoffice data of an application
   * @Rest\Get("/{id}/backoffice", name="application_backoffice_api_get")
   *
   *
   * @OA\Parameter(
   *      name="version",
   *      in="query",
   *      @OA\Schema(
   *          type="string"
   *      ),
   *      required=false,
   *      description="Version of Api, default 1. From version 2 data field keys are exploded in a json objet instead of version 1.* the are flattened strings"
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve backoffice data of an application",
   *     @OA\Schema(
   *         type="object",
   *         @OA\Property(property="backoffice_data", type="object")
   *     )
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function getApplicationBackofficeDataAction($id, Request $request): View
  {
    $version = intval($request->get('version', 1));

    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      /** @var Pratica $result */
      $result = $repository->find($id);
      if ($result === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }

      $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $result);

      $allowedServices = $this->getAllowedServices();
      if (!in_array($result->getServizio()->getId(), $allowedServices)) {
        return $this->view(["You are not allowed to view this application"], Response::HTTP_FORBIDDEN);
      }

      $data = $this->applicationDto->fromEntity($result, true, $version);

      return $this->view([
        'backoffice_data' => $data->getBackofficeData(),
      ], Response::HTTP_OK);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed view this application's backoffice data"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      return $this->view(["Identifier conversion error"], Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * Add or update backoffice data an Application
   * @Rest\Put("/{id}/backoffice", name="applications_backoffice_api_put")
   * @Rest\Post("/{id}/backoffice", name="applications_backoffice_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="backoffice_data", type="object")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create or update backoffice data"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   * @throws Exception
   */
  public function updateApplicationBackofficeDataAction($id, Request $request): View
  {

    $repository = $this->em->getRepository('App\Entity\Pratica');
    /** @var Pratica $application */
    $application = $repository->find($id);
    if ($application === null) {
      throw new Exception('Application not found');
    }
    $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

    $form = $this->createForm('App\Form\Rest\ApplicationBackofficeFormType');
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    $service = $application->getServizio();

    $schema = null;
    $result = $this->formServerService->getFormSchema($service->getBackofficeFormId());

    if ($result['status'] === 'success') {
      $schema = $result['schema'];
    }

    $flatSchema = $this->praticaManager->flatSchema($schema);
    $flatData = $this->praticaManager->arrayFlat($request->request->get('backoffice_data'));

    foreach ($flatData as $k => $v) {
      if (!isset($flatSchema[$k . '.type']) && !isset($flatSchema[$k])) {
        return $this->view(["Service's schema does not match data sent"], Response::HTTP_BAD_REQUEST);
      }
    }

    $data = [
      'data' => $request->request->get('backoffice_data'),
      'flattened' => $flatData,
      'schema' => $flatSchema,
    ];

    try {
      $application->setBackofficeFormData($data);
      $this->em->persist($application);
      $this->em->flush();

    } catch (\Exception $e) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view($this->applicationDto->fromEntity($application), Response::HTTP_CREATED);
  }

  /**
   * Retrieve application history
   * @Rest\Get("/{id}/history", name="application_api_get_history")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve application history",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=Transition::class))
   *     )
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function getApplicationHistoryAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      $result = $repository->find($id);
      if ($result === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $result);

      $completeHistory = !($this->getUser() instanceof CPSUser);
      $data = $this->praticaManager->getApplicationHistory($result, $completeHistory);

      return $this->view($data, Response::HTTP_OK);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to view this application's history"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      return $this->view(["Identifier conversion error"], Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * Retrieve an Applications attachment
   * @Rest\Get("/{id}/attachments/{attachmentId}", name="application_api_attachment_get")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve attachment file",
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Attachment not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param $attachmentId
   * @return View|Response
   */
  public function attachmentAction($id, $attachmentId)
  {

    $repository = $this->em->getRepository(Allegato::class);
    $result = $repository->find($attachmentId);
    if ($result === null) {
      return $this->view(["Attachment not found"], Response::HTTP_NOT_FOUND);
    }
    $pratica = $this->em->getRepository(Pratica::class)->find($id);

    $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $pratica);

    try {
      // Attempt to retrieve the file content first
      $fileContent = $this->fileService->getAttachmentContent($result);
    } catch (FileNotFoundException $e) {
      // If file content is not found, generate it
      if ($result->getType() === RispostaOperatore::TYPE_DEFAULT) {
        try {
          $fileContent = $this->pdfBuilder->renderForResponse($pratica);
        } catch (Exception $exception) {
          return $this->view(["Unable to create outcome attachment"], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
      } else {
        return $this->view(["Attachment not found"], Response::HTTP_NOT_FOUND);
      }
    }

    // Sanitize the filename
    $filename = mb_convert_encoding(StringUtils::sanitizeFileName($result->getFilename()), "ASCII", "auto");

    // Create response
    $response = new Response($fileContent);
    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $filename
    );

    $response->headers->set('Content-Disposition', $disposition);

    return $response;
  }

  /**
   * Patch an Application Attachment
   * @Rest\Patch("/{id}/attachments/{attachmentId}", name="application_api_attachment_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application attachment to patch",
   *     required=false,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="external_id", description="External identifier of the message for external integrations", type="string")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch an Application Attachment"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param $attachmentId
   * @param Request $request
   * @return Response|View
   */
  public function patchApplicationAttachmentAction($id, $attachmentId, Request $request)
  {

    $attachmentRepository = $this->em->getRepository(Allegato::class);

    /** @var Allegato $attachment */
    $attachment = $attachmentRepository->find($attachmentId);

    if (!$attachment) {
      return $this->view(["Attachment not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(AttachmentVoter::EDIT, $attachment);

    try {
      // persist della patch
      $requestBody = json_decode($request->getContent(), true);
      if (isset($requestBody['external_id'])) {
        $attachment = $attachment->setExternalId($requestBody['external_id']);
      }
      $this->em->persist($attachment);
      $this->em->flush();
    } catch (UniqueConstraintViolationException $ucve) {
      $data = [
        'type' => 'duplicated_attachment',
        'title' => 'There was an error during save process',
        'description' => "An attachment with external id {$attachment->getExternalId()} already exists"
      ];
      $this->logger->error(
        $ucve->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_OK);
  }

  /**
   * Retrieve an Application payment's info
   * @Rest\Get("/{id}/payment", name="application_api_payment_get")
   *
   * @OA\Parameter(
   *      in="query",
   *      name="status",
   *      style="form",
   *      explode=false,
   *      required=false,
   *      description="Status of payment, accepts coma separated values. Valid values are creation_pending, payment_pending, creation_failed, payment_started, payment_confirmed, payment_failed, notification_pending, complete, expired",
   *      @OA\Schema(
   *        type="array",
   *        @OA\Items(type="string"),
   *        enum={"creation_pending", "payment_pending", "creation_failed", "payment_started", "payment_confirmed", "payment_failed", "notification_pending", "complete", "expired"}
   *      )
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve an Application"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function getApplicationPaymentAction($id, Request $request): View
  {
    $filters['status'] = $request->query->get('status', false);

    try {
      $repository = $this->em->getRepository(Pratica::class);
      /** @var Pratica $result */
      $result = $repository->find($id);
      if ($result === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $result);

      $data = $this->paymentService->getPaymentStatusByApplication($result, $filters);

      if (empty($data)) {
        return $this->view(["Payment data not found"], Response::HTTP_NOT_FOUND);
      }

      return $this->view($data, Response::HTTP_OK);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to view this application's payment data"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $this->logger->error('Error fetching payment of application: ' . $id . ' - ' . $e->getMessage());
      return $this->view(['Error fetching payment of application: ' . $id], Response::HTTP_BAD_REQUEST);
    }
  }


  /**
   * Update payment data of an application
   * @Route("/{id}/payment", name="applications_payment_api_post")
   * @Rest\Post("/{id}/payment", name="applications_payment_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="Update payment data of an application",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=PaymentOutcome::class)
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   *
   * @OA\Response(
   *     response=422,
   *     description="Unprocessable Entity"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function postApplicationPaymentAction($id, Request $request): View
  {
    $repository = $this->em->getRepository('App\Entity\Pratica');
    /** @var Pratica $application */
    $application = $repository->find($id);

    if (!$application) {
      return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $application);

    if (!in_array(
      $application->getStatus(),
      [Pratica::STATUS_PAYMENT_OUTCOME_PENDING, Pratica::STATUS_PAYMENT_PENDING]
    )) {
      return $this->view(["Application isn't in correct state"], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $paymentOutcome = new paymentOutcome();
    $form = $this->createForm('App\Form\PaymentOutcomeType', $paymentOutcome);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {

      $paymentData = $application->getPaymentData();
      $serializer = SerializerBuilder::create()->build();
      $paymentData['outcome'] = $serializer->toArray($paymentOutcome);
      $application->setPaymentData($paymentData);
      $this->em->persist($application);
      $this->em->flush();

      if ($paymentOutcome->getStatus() == 'OK') {
        $this->statusService->setNewStatus($application, Pratica::STATUS_PAYMENT_SUCCESS);
      } else {
        $this->statusService->setNewStatus($application, Pratica::STATUS_PAYMENT_ERROR);
      }

    } catch (\Exception $e) {

      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view(["Application Payment Modified Successfully"], Response::HTTP_OK);
  }

  /**
   * Patch an Application
   * @Rest\Patch("/{id}", name="applications_api_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The application to patch | Register integration request | Register integration answer",
   *     required=false,
   *     @OA\JsonContent(
   *         oneOf={
   *             @OA\Schema(
   *                 type="object",
   *                 ref=@Model(type=Application::class, groups={"write"})
   *             ),
   *             @OA\Schema(
   *                 type="object",
   *                 @OA\Property(property="integration_outbound_protocol_document_id", type="string", description="Integration request protocol number"),
   *                 @OA\Property(property="integration_outbound_protocol_number", type="string", description="Integration request protocol document id"),
   *                 @OA\Property(property="integration_outbound_protocolled_at", type="string", description="Integration request protocol date")
   *             ),
   *             @OA\Schema(
   *                 type="object",
   *                 @OA\Property(property="integration_inbound_protocol_document_id", type="string", description="Integration answer protocol number"),
   *                 @OA\Property(property="integration_inbound_protocol_number", type="string", description="Integration answer protocol document id"),
   *                 @OA\Property(property="integration_inbound_protocolled_at", type="string", description="Integration answer protocol date")
   *             )
   *         }
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch an Application"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return Response|View
   */
  public function patchApplicationAction($id, Request $request)
  {

    $repository = $this->em->getRepository(Pratica::class);
    /** @var Pratica $application */
    $application = $repository->find($id);

    if (!$application) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

    if (in_array(
      $application->getStatus(),
      [Pratica::STATUS_DRAFT, Pratica::STATUS_PAYMENT_OUTCOME_PENDING, Pratica::STATUS_PAYMENT_PENDING]
    )) {
      return $this->view(["Application isn't in correct state"], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    // Todo: Passare alle transition prima possibile
    if ($application->getStatus() == Pratica::STATUS_REQUEST_INTEGRATION) {
      return $this->forward(ApplicationsAPIController::class . '::applicationTransitionRegisterIntegrationRequestAction', [
        'id' => $application->getId(),
      ]);
    } elseif ($application->getStatus() == Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION) {
      return $this->forward(ApplicationsAPIController::class . '::applicationTransitionRegisterIntegrationAnswerAction', [
        'id' => $application->getId(),
      ]);
    }

    $applicationModel = $this->applicationDto->fromEntity($application);
    $form = $this->createForm(ApplicationType::class, $applicationModel);
    $this->processForm($request, $form);

    if (!$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {

      // calcolo degli eventuali cambi stato prima di persistere
      $needChangeStateToRegistered = !$application->getNumeroProtocollo()
        && $application->getStatus() == Pratica::STATUS_SUBMITTED
        && $application->getServizio()->isProtocolRequired();

      $rispostaOperatore = $application->getRispostaOperatore();
      $needChangeStateToComplete = $rispostaOperatore
        && !$rispostaOperatore->getNumeroProtocollo()
        && $application->getStatus() == Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE;

      $needChangeStateToCancelled = $rispostaOperatore
        && !$rispostaOperatore->getNumeroProtocollo()
        && $application->getStatus() == Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE;

      // persist della patch
      $application = $this->applicationDto->toEntity($applicationModel, $application);
      $this->em->persist($application);
      $this->em->flush();

      // esecuzione degli eventuali cambi stato
      if ($needChangeStateToRegistered) {
        $this->statusService->setNewStatus($application, Pratica::STATUS_REGISTERED);
      }
      if ($needChangeStateToComplete) {
        $this->statusService->setNewStatus($application, Pratica::STATUS_COMPLETE);
      }
      if ($needChangeStateToCancelled) {
        $this->statusService->setNewStatus($application, Pratica::STATUS_CANCELLED);
      }

      // Invio a giscom delle pratiche legacy, serve per registry esterno. Può essere eliminata dopo aver eliminato le pratiche legacy
      if ($application instanceof GiscomPratica) {
        $this->giscomAPIAdapterService->sendPraticaToGiscom($application);
      }
    } catch (UniqueConstraintViolationException $ucve) {
      $data = [
        'type' => 'duplicated_application',
        'title' => 'There was an error during save process',
        'description' => "An application with external id {$application->getExternalId()} already exists"
      ];
      $this->logger->error(
        $ucve->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error(
        $e->getMessage(),
        ['request' => $request]
      );

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Submit an application
   * @Rest\Post("/{id}/transition/submit", name="application_api_post_transition_submit")
   *
   * @Security(name="Bearer")
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionSubmitAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      $application = $repository->find($id);
      if ($application === null) {
        throw new Exception('Application not found');
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::SUBMIT, $application);
      $this->praticaManager->finalizeSubmission($application);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to submit this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Register an Application
   * @Rest\Post("/{id}/transition/register", name="application_api_post_transition_register")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="protocol_folder_number", type="string", description="Protocol folder number"),
   *             @OA\Property(property="protocol_folder_code", type="string", description="Protocol folder code"),
   *             @OA\Property(property="protocol_number", type="string", description="Protocol number"),
   *             @OA\Property(property="protocol_document_id", type="string", description="Protocol document id"),
   *             @OA\Property(property="protocolled_at", type="string", format="date-time", description="Protocol datetime")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRegisterAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      if (!empty($application->getNumeroProtocollo())) {
        return $this->view(["Application already has a protocol number"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      if (!$application->getServizio()->isProtocolRequired()) {
        return $this->view(["Application does not need to be protocolled, is the protocol enabled in service settings?"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $form = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add(
          'protocol_folder_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add('protocol_folder_code', TextType::class)
        ->add(
          'protocol_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add(
          'protocol_document_id',
          TextType::class
        )
        ->add(
          'protocolled_at',
          DateTimeType::class, [
            'widget' => 'single_text'
          ]
        )
        ->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $application->setNumeroFascicolo($data['protocol_folder_number']);
      $application->setCodiceFascicolo($data['protocol_folder_code']);
      $application->setNumeroProtocollo($data['protocol_number']);
      $application->setIdDocumentoProtocollo($data['protocol_document_id']);
      if (isset($data['protocolled_at']) && $data['protocolled_at']) {
        $application->setProtocolTime($data['protocolled_at']->getTimestamp());
      }

      $this->statusService->setNewStatus($application, Pratica::STATUS_REGISTERED);

      // Invio a giscom delle pratiche legacy, serve per registry esterno. Può essere eliminata dopo aver eliminato le pratiche legacy
      if ($application instanceof GiscomPratica) {
        $this->giscomAPIAdapterService->sendPraticaToGiscom($application);
      }
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to register this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Register application outcome
   * @Rest\Post("/{id}/transition/register-outcome", name="application_api_post_transition_register_outcome")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="protocol_number", type="string", description="Outcome protocol number"),
   *             @OA\Property(property="protocol_document_id", type="string", description="Outcome protocol document id")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRegisterOutcomeAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      $outcome = $application->getRispostaOperatore();
      if (empty($outcome)) {
        return $this->view(["Application doesn't has an outcome"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      if (!empty($outcome->getNumeroProtocollo())) {
        return $this->view(["Outcome already has a protocol number"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      if (!$application->getServizio()->isProtocolRequired()) {
        return $this->view(["Application does not need to be protocolled, is the protocol enabled in service settings?"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $form = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add(
          'protocol_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add(
          'protocol_document_id',
          TextType::class
        )
        ->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $outcome->setNumeroProtocollo($data['protocol_number']);
      $outcome->setIdDocumentoProtocollo($data['protocol_document_id']);

      $this->em->persist($outcome);
      $this->em->flush();

      $application->addNumeroDiProtocollo([
        'id' => $outcome->getId(),
        'protocollo' => $data['protocol_document_id'],
      ]);

      if ($application->getEsito()) {
        $this->statusService->setNewStatus($application, Pratica::STATUS_COMPLETE);
      } else {
        $this->statusService->setNewStatus($application, Pratica::STATUS_CANCELLED);
      }
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to register this application's outcome"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Assign an operator to an Application
   * @Rest\Post("/{id}/transition/assign", name="application_api_post_transition_assign")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="user_id", type="string", format="uuid", description="Identifier of the operator
  user to which the application is assigned through the transition. If not specified, the application will be assigned
  to the operator requesting the status change"),
   *             @OA\Property(property="user_group_id", type="string", format="uuid", description="Identifier of the
  user group to which the application is assigned through the transition. If not specified, the group to which the
  operator requesting the change of status belongs will be considered.</br><b>Warning</b>: the selected group must
  agree with what is specified in the user_id field or with the operator requesting its assignment"),
   *             @OA\Property(property="message", type="string", description="Message or customized note that can be
  associated with the assignment of the application. This message will only be visible by operators who have visibility
  of the application in the iter of the same<br>Warning: this field can only be used if the user requesting the
  transition is a system user"),
   *             @OA\Property(property="assigned_at", type="string", format="date-time", description="Date and time of
  assignment of the application, this field can be used to override the default value which is automatically set at the
  effective moment of the change of status of the application.<br>Warning: this field can only be used if the user
  requesting the transition is a system user")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionAssignAction($id, Request $request): View
  {
    /** @var User $user */
    $user = $this->getUser();

    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      $application = $repository->find($id);
      if ($application === null) {
        throw new Exception('Application not found');
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::ASSIGN, $application);

      $qb = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add('user_id', EntityType::class, [
          'class' => OperatoreUser::class,
          'required' => false,
          'choice_value' => 'id'
        ])
        ->add('user_group_id', EntityType::class, [
          'class' => UserGroup::class,
          'required' => false,
          'choice_value' => 'id'
        ]);

      if ($user instanceof OperatoreUser && $user->isSystemUser()) {
        // Solo l'operatore di sistema ha il permesso di aggiungere un messaggio e modificarne la data di assegnazione
        $qb
          ->add('message', TextType::class)
          ->add('assigned_at', DateTimeType::class, [
            'widget' => 'single_text',
            'required' => false,
            'empty_data' => '',
          ]);
      }

      $form = $qb->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $extraData = $form->getExtraData();

      $operatorAssigned = $data['user_id'] ?? null;
      $userGroupAssigned = $data['user_group_id'] ?? null;

      // Solo l'utente API puo specificare il messaggio e la data di assegnazione
      if (isset($extraData['message']) || isset($extraData['assigned_at'])) {
        throw new BadRequestHttpException("Only API users can specify a transition message or transition timestamp");
      }

      $message = $data['message'] ?? null;
      $assignedAt = $data['assigned_at'] ?? null;

      $this->praticaManager->assign($application, $user, $operatorAssigned, $userGroupAssigned, $message, $assignedAt);
    } catch (BadRequestHttpException $e) {
      $data = [
        'type' => 'assignment_error',
        'title' => 'Invalid assignment',
        'description' => $e->getMessage(),
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to assign this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Answer to an application
   * @Rest\Post("/{id}/transition/accept", name="application_api_post_transition_accept")
   * @Rest\Post("/{id}/transition/reject", name="application_api_post_transition_reject")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="message", type="string", description="Application outcome"),
   *             @OA\Property(property="main_attachment", type="object", ref=@Model(type=FileModel::class, groups={"write"}), description="Main attachment, if not provided one will be generated automatically from message's contennt"),
   *             @OA\Property(property="attachments", type="array", @OA\Items(ref=@Model(type=FileModel::class, groups={"write"})))
   *         )
   *     )
   * )
   *
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionOutcomeAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      $application = $repository->find($id);
      if ($application === null) {
        throw new Exception('Application not found');
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::ACCEPT_OR_REJECT, $application);

      $defaultData = [
        'message' => null,
        'main_attachment' => null,
        'attachments' => null,
      ];
      $form = $this->createForm(OutcomeFormType::class, $defaultData);
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $application->setEsito($request->get('_route') === 'application_api_post_transition_accept');
      if ($data['message']) {
        $application->setMotivazioneEsito($data['message']);
      }

      if (!empty($data['main_attachment'])) {
        $mainAttachment = $data['main_attachment'];
        $base64Content = $mainAttachment->getFile();
        $file = new UploadedBase64File($base64Content, $mainAttachment->getMimeType(), $mainAttachment->getName());
        $operatorResponse = new RispostaOperatore();
        $operatorResponse->setFile($file);
        $operatorResponse->setOwner($application->getUser());
        $operatorResponse->setDescription('Risposta operatore');
        $operatorResponse->setOriginalFilename($mainAttachment->getName());
        $this->em->persist($operatorResponse);
        $application->addRispostaOperatore($operatorResponse);
      }

      foreach ($data['attachments'] as $attachment) {
        $base64Content = $attachment->getFile();
        $file = new UploadedBase64File($base64Content, $attachment->getMimeType(), $attachment->getName());
        $allegato = new AllegatoOperatore();
        $allegato->setFile($file);
        $allegato->setOwner($application->getUser());
        $allegato->setDescription('Allegato risposta operatore');
        $allegato->setOriginalFilename($attachment->getName());
        $this->em->persist($allegato);
        $application->addAllegatoOperatore($allegato);
      }
      $this->praticaManager->finalize($application, $this->getUser(), null);

      // Invio a giscom delle pratiche legacy, serve per registry esterno. Può essere eliminata dopo aver eliminato le pratiche legacy
      if ($application instanceof GiscomPratica) {
        $this->giscomAPIAdapterService->sendPraticaToGiscom($application);
      }
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to accept or reject this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Request integration on an application
   * @Rest\Post("/{id}/transition/request-integration", name="application_api_post_transition_request_integration")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="message", type="string", description="Reason of the integration request"),
   *             @OA\Property(property="attachments", type="array", @OA\Items(ref=@Model(type=FileModel::class)))
   *         )
   *     )
   * )
   *
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRequestIntegrationAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      $application = $repository->find($id);
      if ($application === null) {
        throw new Exception('Application not found');
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::ACCEPT_OR_REJECT, $application);
      $defaultData = [
        'message' => null,
      ];

      if ($application->getStatus() == Pratica::STATUS_DRAFT_FOR_INTEGRATION) {
        $this->logger->error("New Integration Request when there is an active one");

        $data = [
          'type' => 'validation_error',
          'title' => 'Integration request already active on application',
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }
      $form = $this->createForm(RequestIntegrationFormType::class, $defaultData);


      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $this->praticaManager->requestIntegration($application, $this->getUser(), $data);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to request an integration for this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => $e->getMessage(),
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Accept integration on an application
   * @Rest\Post("/{id}/transition/accept-integration", name="application_api_post_transition_accept_integration")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="Message's uuid to include in integration request response, accepted values: <ul><li>null - All messages added to applicaton from integration request will be added to the intergration request response</li><li>an empty array - No message will be added to the intergration request response</li><li>an array of message's uuid - Only messages with uuuid in array  will be added to the intergration request response</li></ul>",
   *     required=false,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="messages", nullable=true, type="array", @OA\Items(type="string"))
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionAcceptIntegrationAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        throw new Exception('Application not found');
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::ACCEPT_OR_REJECT, $application);

      if ($application->getStatus() !== Pratica::STATUS_DRAFT_FOR_INTEGRATION) {
        throw new Exception('Application is not in the correct state');
      }

      $messages = null;
      $messagesID = $request->get('messages', []);
      if (!empty($messagesID)) {
        $messageRepository = $this->em->getRepository('App\Entity\Message');
        foreach ($messagesID as $messageId) {
          if (!Uuid::isValid($messageId)) {
            throw new Exception("$messageId not is a valid Uuid");
          }
          $message = $messageRepository->findOneBy([
            'id' => $messageId,
            'application' => $application->getId(),
          ]);
          if (!$message instanceof MessageEntity) {
            throw new Exception("Message $messageId not found");
          }
          $messages[] = $message;
        }
      }

      $this->praticaManager->acceptIntegration($application, $this->getUser(), $messages);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to accept an integration for this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => $e->getMessage(),
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Cancel integration request on an application
   * @Rest\Post("/{id}/transition/cancel-integration", name="application_api_post_transition_cancel_integration")
   *
   * @Security(name="Bearer")
   *
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionCancelIntegrationAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        throw new Exception('Application not found');
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::ACCEPT_OR_REJECT, $application);

      if ($application->getStatus() !== Pratica::STATUS_DRAFT_FOR_INTEGRATION) {
        throw new Exception('Application is not in the correct state');
      }

      $this->praticaManager->cancelIntegration($application, $this->getUser());
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to cancel an integration for this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => $e->getMessage(),
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Withdraw an application
   * @Rest\Post("/{id}/transition/withdraw", name="application_api_post_transition_withdraw")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *      description="Withdraw motivation, accept a string or a file. If string is passed a pdf file will be generated with the motivation and attached to the application. If a file is passed, it will be directly attached to the application.",
   *      required=false,
   *      @OA\MediaType(
   *          mediaType="application/json",
   *          @OA\Schema(
   *              oneOf={
   *                 @OA\Schema(type="object", @OA\Property(property="message", type="string", description="Reason of the withdraw, a pdf file will be generated with the message and attached to the application")),
   *                 @OA\Schema(type="object", @OA\Property(property="attachment", description="The passed file will be directly attached to the application", type="object", ref=@Model(type=FileModel::class, groups={"write"})))
   *             }
   *          )
   *      )
   *  )
   *
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionWithDrawAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }

      $this->denyAccessUnlessGranted(ApplicationVoter::WITHDRAW, $application);

      $form = $this->createForm(MessageOrFileFormType::class);
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $this->praticaManager->withdrawApplication($application, $data);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to withdraw this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Register an application withdraw
   * @Rest\Post("/{id}/transition/register-withdraw", name="application_api_post_transition_register_withdraw")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="protocol_number", type="string", description="Withdraw protocol number"),
   *             @OA\Property(property="protocol_document_id", type="string", description="Withdraw protocol document id"),
   *             @OA\Property(property="protocolled_at", type="string", format="date-time", description="Withdraw protocol date")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   *
   * @OA\Response(
   *     response=422,
   *     description="Unprocessable withdraw"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRegisterWithdrawAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      if (!$application->getServizio()->isProtocolRequired()) {
        return $this->view(["Application does not need to be protocolled, is the protocol enabled in service settings?"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $withdrawAttachment = $application->getWithdrawAttachment();
      if (!$withdrawAttachment || $application->getStatus() !== Pratica::STATUS_WITHDRAW) {
        return $this->view(["Application  not is in correct state or withdraw not found"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      if (!empty($withdrawAttachment->getNumeroProtocollo())) {
        return $this->view(["Withdraw already has a protocol number"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $form = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add(
          'protocol_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add(
          'protocol_document_id',
          TextType::class
        )
        ->add('protocolled_at', DateTimeType::class, [
          'widget' => 'single_text',
          'required' => false,
          'empty_data' => (new DateTime())->format(\DateTimeInterface::W3C),
        ])
        ->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $withdrawAttachment->setIdDocumentoProtocollo($data['protocol_document_id']);
      $withdrawAttachment->setNumeroProtocollo($data['protocol_number']);
      $withdrawAttachment->setProtocolTime($data['protocolled_at']->getTimeStamp());

      $this->em->persist($withdrawAttachment);
      $this->em->flush();
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to register this application's withdraw"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Revoke an application
   * @Rest\Post("/{id}/transition/revoke", name="application_api_post_transition_revoke")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *      description="Revoke motivation, accept a string or a file. If string is passed a pdf file will be generated with the motivation and attached to the application. If a file is passed, it will be directly attached to the application.",
   *      required=false,
   *      @OA\MediaType(
   *          mediaType="application/json",
   *          @OA\Schema(
   *              oneOf={
   *                 @OA\Schema(type="object", @OA\Property(property="message", type="string", description="Reason of revocation, a pdf file will be generated with the message and attached to the application")),
   *                 @OA\Schema(type="object", @OA\Property(property="attachment", description="The passed file will be directly attached to the application", type="object", ref=@Model(type=FileModel::class, groups={"write"})))
   *             }
   *          )
   *      )
   *  )
   *
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRevokeAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }

      $this->denyAccessUnlessGranted(ApplicationVoter::CHANGE_STATUS, $application);

      $form = $this->createForm(MessageOrFileFormType::class);
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $this->praticaManager->revokeApplication($application, $this->getUser(), $data);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to revoke this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Register an application revocation
   * @Rest\Post("/{id}/transition/register-revocation", name="application_api_post_transition_register_revocation")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="protocol_number", type="string", description="Revocation protocol number"),
   *             @OA\Property(property="protocol_document_id", type="string", description="Revocation protocol document id"),
   *             @OA\Property(property="protocolled_at", type="string", format="date-time", description="Revocation protocol date")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   *
   * @OA\Response(
   *     response=422,
   *     description="Unprocessable revocation"
   * )
   *
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRegisterRevocationAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      if (!$application->getServizio()->isProtocolRequired()) {
        return $this->view(["Application does not need to be registered, is the protocol enabled in service settings?"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      if ($application->getStatus() !== Pratica::STATUS_REVOKED) {
        return $this->view(["Application not is in correct state"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $attachment = $application->getRevocationAttachment();
      if (!$attachment) {
        return $this->view(["Revocation attachment not found"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      if (!empty($attachment->getNumeroProtocollo())) {
        return $this->view(["Revocation attachment already has a protocol number"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $form = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add(
          'protocol_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add(
          'protocol_document_id',
          TextType::class
        )
        ->add('protocolled_at', DateTimeType::class, [
          'widget' => 'single_text',
          'required' => false,
          'empty_data' => (new DateTime())->format(\DateTimeInterface::W3C),
        ])
        ->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $attachment->setIdDocumentoProtocollo($data['protocol_document_id']);
      $attachment->setNumeroProtocollo($data['protocol_number']);
      $attachment->setProtocolTime($data['protocolled_at']->getTimeStamp());

      $this->em->persist($attachment);
      $this->em->flush();
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to register this application's revocation"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Change status of an application
   * @Rest\Post("/{id}/transition/change-status", name="application_api_post_transition_change_status")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *      description="Change status of an application",
   *      required=true,
   *      @OA\MediaType(
   *          mediaType="application/json",
   *          @OA\Schema(type="object", @OA\Property(property="status", type="int", description="Change status of the current application", enum={2000, 3000, 4000, 50000})),
   *      )
   *  )
   *
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionChangeStatus($id, Request $request): View
  {

    if (!Uuid::isValid($id)) {
      return $this->view(["Id $id is not a valid Uuid"], Response::HTTP_BAD_REQUEST);
    }

    try {
      $repository = $this->em->getRepository(Pratica::class);
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }

      $this->denyAccessUnlessGranted(ApplicationVoter::CHANGE_STATUS, $application);
      $newStatus = $request->request->get('status');

      $this->praticaManager->changeStatus($application, $this->getUser(), $newStatus);

    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to change status on this application"], Response::HTTP_FORBIDDEN);
    } catch (PreconditionFailedHttpException $e) {
      return $this->view([$e->getMessage()], Response::HTTP_PRECONDITION_FAILED);
    } catch (BadRequestHttpException $e) {
      return $this->view([$e->getMessage()], Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * @param Request $request
   * @param FormInterface $form
   */
  private function processForm(Request $request, FormInterface $form): void
  {
    $data = json_decode($request->getContent(), true);

    // Todo: find better way
    if (isset($data['data']) && count($data['data']) > 0) {
      $data['data'] = \json_encode($data['data']);
    } else {
      $data['data'] = \json_encode([]);
    }

    if (isset($data['backoffice_data']) && count($data['backoffice_data']) > 0) {
      $data['backoffice_data'] = \json_encode($data['backoffice_data']);
    } else {
      $data['backoffice_data'] = \json_encode([]);
    }

    $clearMissing = $request->getMethod() != 'PATCH';
    $form->submit($data, $clearMissing);
  }

  /**
   * Register application integration request
   * @Rest\Post("/{id}/transition/register-integration-request", name="application_api_post_transition_register_integration_request")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to execute",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="integration_outbound_protocol_document_id", type="string", description="Integration request protocol number"),
   *             @OA\Property(property="integration_outbound_protocol_number", type="string", description="Integration request protocol document id"),
   *             @OA\Property(property="integration_outbound_protocolled_at", type="string", format="date-time", description="Integration request protocol date")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRegisterIntegrationRequestAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository(Pratica::class);
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      if (!$application->getServizio()->isProtocolRequired()) {
        return $this->view(["Application does not need to be protocolled, is the protocol enabled in service settings?"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $form = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add(
          'integration_outbound_protocol_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add(
          'integration_outbound_protocol_document_id',
          TextType::class
        )
        ->add('integration_outbound_protocolled_at', DateTimeType::class, [
          'widget' => 'single_text',
          'required' => false,
          'empty_data' => '',
        ])
        ->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $this->praticaManager->registerIntegrationRequest($application, $this->getUser(), $data);
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to register this application's integration request"], Response::HTTP_FORBIDDEN);
    } catch (NotFoundException $e) {
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view(["Integration request does not exist"], Response::HTTP_UNPROCESSABLE_ENTITY);
    } catch (InvalidStatusException $e) {
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view(["Application isn't in correct state"], Response::HTTP_UNPROCESSABLE_ENTITY);
    } catch (AlreadySentException $e) {
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view(["Integration request already has a protocol number"], Response::HTTP_UNPROCESSABLE_ENTITY);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Register application integration answer
   * @Rest\Post("/{id}/transition/register-integration-answer", name="application_api_post_transition_register_integration_answer")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The transition to execute",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="integration_inbound_protocol_document_id", type="string", description="Integration answer protocol number"),
   *             @OA\Property(property="integration_inbound_protocol_number", type="string", description="Integration answer protocol document id"),
   *             @OA\Property(property="integration_inbound_protocolled_at", type="string", format="date-time", description="Integration answer protocol date")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionRegisterIntegrationAnswerAction($id, Request $request): View
  {
    try {
      $repository = $this->em->getRepository('App\Entity\Pratica');
      /** @var Pratica $application */
      $application = $repository->find($id);
      if ($application === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::EDIT, $application);

      if (!$application->getServizio()->isProtocolRequired()) {
        return $this->view(["Application does not need to be protocolled, is the protocol enabled in service settings?"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $form = $this->createFormBuilder(null, ['allow_extra_fields' => true, 'csrf_protection' => false])
        ->add(
          'integration_inbound_protocol_document_id',
          TextType::class
        )
        ->add(
          'integration_inbound_protocol_number',
          TextType::class,
          [
            'constraints' => [
              new NotBlank(),
              new NotNull(),
            ],
          ]
        )
        ->add('integration_inbound_protocolled_at', DateTimeType::class, [
          'widget' => 'single_text',
          'required' => false,
          'empty_data' => '',
        ])
        ->getForm();
      $this->processForm($request, $form);
      if ($form->isSubmitted() && !$form->isValid()) {
        $errors = FormUtils::getErrorsFromForm($form);
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors,
        ];

        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $data = $form->getData();
      $this->praticaManager->registerIntegrationAnswer($application, $this->getUser(), $data);

      // Invio a giscom delle pratiche legacy, serve per registry esterno. Può essere eliminata dopo aver eliminato le pratiche legacy
      if ($application instanceof GiscomPratica) {
        $this->giscomAPIAdapterService->sendPraticaToGiscom($application);
      }
    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to register this application's integration answer"], Response::HTTP_FORBIDDEN);
    } catch (NotFoundException $e) {
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view(["Integration answer does not exist"], Response::HTTP_UNPROCESSABLE_ENTITY);
    } catch (InvalidStatusException $e) {
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view(["Application isn't in correct state"], Response::HTTP_UNPROCESSABLE_ENTITY);
    } catch (AlreadySentException $e) {
      $this->logger->error($e->getMessage(), ['request' => $request]);
      return $this->view(["Integration answer already has a protocol number"], Response::HTTP_UNPROCESSABLE_ENTITY);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view([], Response::HTTP_NO_CONTENT);
  }

  /**
   * Force change application status payment pending by to payment success
   * @Rest\Post("/{id}/transition/complete-payment", name="application_api_post_transition_complete_payment")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The message to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             @OA\Property(property="message", type="string", description="Text message"),
   *             @OA\Property(property="subject", type="string", description="Subject message text"),
   *             @OA\Property(property="visibility", type="boolean", description="Visibility of message")
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=204,
   *     description="Updated"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Application not found"
   * )
   * @OA\Tag(name="applications")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function applicationTransitionPaymentCompletedAction($id, Request $request): View
  {
    $user = $this->getUser();
    try {
      $repository = $this->em->getRepository(Pratica::class);
      $application = $repository->find($id);
      if (!$application) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }
      $this->denyAccessUnlessGranted(ApplicationVoter::ASSIGN, $application);
      if (!in_array($application->getStatus(), [Pratica::STATUS_PAYMENT_OUTCOME_PENDING, Pratica::STATUS_PAYMENT_PENDING])) {
        return $this->view(["Application isn't in correct state"], Response::HTTP_UNPROCESSABLE_ENTITY);
      }

      $message = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
      $this->praticaManager->finalizePaymentCompleteSubmission($application, $user, $message);

    } catch (AccessDeniedException $e) {
      return $this->view(["You are not allowed to complete this application's payment"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during transition process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    return $this->view(["Application Status Payment Modified Successfully"], Response::HTTP_OK);

  }

  /**
   * @return array
   */
  private function getAllowedServices(): array
  {
    $user = $this->getUser();
    $allowedServices = [];
    if ($user instanceof OperatoreUser) {
      $allowedServices = $user->getServiziAbilitati()->toArray();
    }

    return $allowedServices;
  }

  /**
   * Get the services to which the operatore is enabled
   * as well as the services where the operatore has at least
   * an application to which he is assigned
   *
   * @return array
   */
  private function getServicesByOperatore(): array
  {
    /** @var PraticaRepository $praticaRepository */
    $praticaRepository = $this->getDoctrine()->getRepository(Pratica::class);

    $user = $this->getUser();
    $servicesByOperatore = [];
    if ($user instanceof OperatoreUser) {
      $servicesByOperatore = $praticaRepository->getServizioIdListByOperatore($user, PraticaRepository::OPERATORI_LOWER_STATE);
    }
    return $servicesByOperatore;
  }

  /**
   * Get Application by Id
   *
   * @param string $id
   * @param Request $request
   * @param boolean $isExternal
   * @return View
   */
  private function getApplicationById(string $id, Request $request, bool $isExternal = false): View
  {
    $version = $request->query->getInt('version', 1);

    try {
      $repository = $this->em->getRepository(Pratica::class);
      if (!$isExternal && !Uuid::isValid($id)) {
        return $this->view(["The identifier passed as a parameter is not a valid uuid"], Response::HTTP_BAD_REQUEST);
      }
      $result = $isExternal ? $repository->findOneBy(['externalId' => $id]) : $repository->find($id);
      if ($result === null) {
        return $this->view(["Application not found"], Response::HTTP_NOT_FOUND);
      }

      $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $result);

      if ($request->headers->get('accept') === 'application/geo+json') {
        $formData = $result->getDematerializedForms();
        if (!isset($formData['data']['address'])) {
          return $this->view("Application required not has geo data ", Response::HTTP_OK);
        }
        $point = new Point([(float)$formData['data']['address']['lon'], (float)$formData['data']['address']['lat']]);
        $feature = new Feature($point, null, $result->getId());
        $view = new View($feature);
        $view->setContext($view->getContext()->setSerializeNull(false));
        return $view;
      }

      $data = $this->applicationDto->fromEntity($result, true, $version);


      return $this->view($data, Response::HTTP_OK);
    } catch (AccessDeniedException $ade) {
      return $this->view(["You are not allowed to view this application"], Response::HTTP_FORBIDDEN);
    } catch (\Exception $e) {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during get process',
        'description' => 'Contact technical support at support@opencontent.it',
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);

      return $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }
}
