<?php

namespace App\Controller\Rest;

use App\Entity\Calendar;
use App\Entity\Categoria;
use App\Entity\OperatoreUser;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Utils\FormUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Api\UserGroupApiType;

/**
 * Class AbstractFOSRestController
 * @Route("/user-groups")
 */
class UserGroupsAPIController extends AbstractFOSRestController
{

  private EntityManagerInterface $entityManager;

  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  /**
   * List all user groups
   * @Rest\Get("", name="user_group_api_list")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false  ,
   *      @OA\Schema(type="string")
   *  )
   *
   * @OA\Parameter(
   *      name="name",
   *      in="query",
   *      @OA\Schema(type="string"),
   *      required=false,
   *      description="Name of the user group"
   *  )
   *
   * @OA\Parameter(
   *      name="has_calendar",
   *      in="query",
   *      @OA\Schema(type="boolean"),
   *      required=false,
   *      description="If true retreive only user groups that have calendars, if false only user groups that not have calendars"
   *  )
   *
   * @OA\Parameter(
   *      name="service_id",
   *      in="query",
   *      @OA\Schema(type="string"),
   *      required=false,
   *      description="Uuid of the user group"
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of user groups",
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref=@Model(type=UserGroup::class, groups={"read"}))
   *     )
   * )
   *
   * @OA\Tag(name="user-groups")
   * @param Request $request
   * @return View
   */
  public function getUserGroupsAction(Request $request): View
  {
    $criteria = [];
    if ($request->query->has('has_calendar')) {
      $criteria['has_calendar'] = $request->query->getBoolean('has_calendar');
    }

    $name = $request->get('name', false);
    if ($name) {
      $criteria['name'] = $name;
    }

    $serviceId = $request->get('service_id', false);
    if ($serviceId) {
      if (!Uuid::isValid($serviceId)) {
        return $this->view('Parameter service_id is not a valid uuid');
      }
      $criteria['service_id'] = $serviceId;
    }

    $repo =  $this->entityManager->getRepository('App\Entity\UserGroup');
    $result = $repo->findByCriteria($criteria);

    return $this->view($result, Response::HTTP_OK);
  }


  /**
   * Retrieve a user group by id
   * @Rest\Get("/{id}", name="user_group_api_get")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve a UserGroup",
   *     @Model(type=UserGroup::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="UserGroup not found"
   * )
   * @OA\Tag(name="user-groups")
   *
   * @param Request $request
   * @param string $id
   * @return View
   */
  public function getUserGroupAction(Request $request, $id): View
  {
    try {
      $repository = $this->getDoctrine()->getRepository('App\Entity\UserGroup');
      $result = $repository->find($id);
    } catch (\Exception $e) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    if ($result === null) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    return $this->view($result, Response::HTTP_OK);
  }


  /**
   * Create a UserGroup
   * @Rest\Post(name="user_group_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false  ,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\RequestBody(
   *     description="The user group to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=UserGroup::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a User Group"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="user-groups")
   *
   * @param Request $request
   * @return View
   */
  public function postUserGroupAction(Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    $userGroup = new UserGroup();
    $form = $this->createForm('App\Form\Api\UserGroupApiType', $userGroup);

    $this->processForm($request, $form);
    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors,
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->checkRelations($userGroup, $request);
      $this->entityManager->persist($userGroup);
      $this->entityManager->flush();
    } catch (\Exception $e) {
      return $this->generateExceptionResponse($e, $request);
    }

    return $this->view($userGroup, Response::HTTP_CREATED);
  }




  /**
   * Edit full user group
   * @Rest\Put("/{id}", name="user_group_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\RequestBody(
   *     description="The recipient to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=UserGroup::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full User Group"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function putUserGroupAction($id, Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    $repository = $this->getDoctrine()->getRepository('App\Entity\UserGroup');
    $userGroup = $repository->find($id);

    if (!$userGroup) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm(UserGroupApiType::class, $userGroup);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'put_validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->checkRelations($userGroup, $request);
      $this->entityManager->persist($userGroup);
      $this->entityManager->flush();
    } catch (\Exception $e) {
      $this->logger->error('user_groups_api', ['exception' => $e->getMessage()]);
      return $this->generateExceptionResponse($e, $request);
    }

    return $this->view(["Object Modified Successfully"], Response::HTTP_OK);
  }


  /**
   * Patch a user group
   * @Rest\Patch("/{id}", name="user_group_api_patch")
   *
   * @Security(name="Bearer")
   *
   * @OA\Parameter(
   *      name="x-locale",
   *      in="header",
   *      description="Request locale",
   *      required=false,
   *      @OA\Schema(
   *           type="string"
   *      )
   *  )
   *
   * @OA\RequestBody(
   *     description="The recipient to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=UserGroup::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch a User Group"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function patchUserGroupAction($id, Request $request)
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);

    $repository = $this->getDoctrine()->getRepository('App\Entity\UserGroup');
    $userGroup = $repository->find($id);

    if (!$userGroup) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }

    $form = $this->createForm('App\Form\Api\UserGroupApiType', $userGroup);
    $this->processForm($request, $form);

    if ($form->isSubmitted() && !$form->isValid()) {
      $errors = FormUtils::getErrorsFromForm($form);
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $errors
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->checkRelations($userGroup, $request);
      $this->entityManager->persist($userGroup);
      $this->entityManager->flush();
    } catch (\Exception $e) {
      return $this->generateExceptionResponse($e, $request);
    }

    return $this->view(["Object Patched Successfully"], Response::HTTP_OK);
  }


  /**
   * Delete a user group
   * @Rest\Delete("/{id}", name="user_group_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @return View
   */
  public function deleteUserGroupAction($id): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    $item = $this->getDoctrine()->getRepository('App\Entity\UserGroup')->find($id);
    if ($item) {
      $this->entityManager->remove($item);
      $this->entityManager->flush();
    }
    return $this->view(null, Response::HTTP_NO_CONTENT);
  }

  /**
   * Retrieve user group members
   * @Rest\Get("/{id}/members", name="user_group_api_members_list")
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of users for selected group",
   *     @OA\JsonContent(
   *       type="array",
   *       @OA\Items(
   *         type="object",
   *         @OA\Property(property="id", type="string", format="uuid", description="Id of the member"),
   *         @OA\Property(property="username", type="string", description="Username of the member"),
   *         @OA\Property(property="name", type="string", description="Name of the member"),
   *         @OA\Property(property="lastname", type="string", description="Lastname of the member"),
   *         @OA\Property(property="email", type="string", description="Email of the member")
   *       )
   *     )
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="User group not found"
   * )
   *
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @return View
   */
  public function getMembersAction($id): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    $item = $this->entityManager->getRepository('App\Entity\UserGroup')->find($id);

    if ($item === null) {
      $data = [
        'title' => 'Not Found',
        'detail' => "The user group with id {$id} does not exist",
        'status' => Response::HTTP_NOT_FOUND,
      ];
      return $this->view($data, Response::HTTP_NOT_FOUND);
    }

    return $this->view($item->getMembers(), Response::HTTP_OK);
  }

  /**
   * Add users to selected user group, only user of type Operator could be added.
   * @Rest\Post("/{id}/members",name="user_group_api_members_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="List of the uuid of the user to add to user group",
   *     required=true,
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(type="string", format="uuid", description="Id of the user to add")
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of users for selected group",
   *     @OA\JsonContent(
   *       type="array",
   *       @OA\Items(
   *         type="object",
   *         @OA\Property(property="id", type="string", format="uuid", description="Id of the member"),
   *         @OA\Property(property="username", type="string", description="Username of the member"),
   *         @OA\Property(property="name", type="string", description="Name of the member"),
   *         @OA\Property(property="lastname", type="string", description="Lastname of the member"),
   *         @OA\Property(property="email", type="string", description="Email of the member")
   *       )
   *     )
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="User group or users not found"
   * )
   *
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function postMembersAction($id, Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    $item = $this->entityManager->getRepository('App\Entity\UserGroup')->find($id);
    if ($item === null) {
      $data = [
        'title' => 'Not Found',
        'detail' => "The user group with id {$id} does not exist",
        'status' => Response::HTTP_NOT_FOUND,
      ];
      return $this->view($data, Response::HTTP_NOT_FOUND);
    }

    $userIds = $request->request->all();

    if (empty($userIds)) {
      $data = [
        'title' => 'Empty parameters',
        'detail' => 'The uuid list of users cannot be empty',
        'status' => Response::HTTP_BAD_REQUEST,
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }

    $operatorRepo = $this->entityManager->getRepository('App\Entity\OperatoreUser');
    foreach ($userIds as $userId) {
      if (!Uuid::isValid($userId)) {
        $data = [
          'title' => 'Bad Request',
          'detail' => "{$userId} must be a valid uuid",
          'status' => Response::HTTP_BAD_REQUEST,
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $user = $operatorRepo->find($userId);
      if (!$user instanceof OperatoreUser) {
        $data = [
          'title' => 'User not found',
          'detail' => "The user with id {$userId} does not exist",
          'status' => Response::HTTP_NOT_FOUND,
        ];
        return $this->view($data, Response::HTTP_NOT_FOUND);
      }

      $item->addUser($user);
    }

    $this->entityManager->persist($item);
    $this->entityManager->flush();

    return $this->view($item->getMembers(), Response::HTTP_OK);
  }

  /**
   * Modifies the whole list of users for the selected group
   * @Rest\Put("/{id}/members",name="user_group_api_members_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="List of the uuid of the users for the group",
   *     required=true,
   *     @OA\JsonContent(
   *         type="array",
   *         @OA\Items(type="string", format="uuid", description="Id of the users")
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of users for selected group",
   *     @OA\JsonContent(
   *       type="array",
   *       @OA\Items(
   *         type="object",
   *         @OA\Property(property="id", type="string", format="uuid", description="Id of the member"),
   *         @OA\Property(property="username", type="string", description="Username of the member"),
   *         @OA\Property(property="name", type="string", description="Name of the member"),
   *         @OA\Property(property="lastname", type="string", description="Lastname of the member"),
   *         @OA\Property(property="email", type="string", description="Email of the member")
   *       )
   *     )
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="User group or users not found"
   * )
   *
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @param Request $request
   * @return View
   */
  public function putMembersAction($id, Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    $item = $this->entityManager->getRepository('App\Entity\UserGroup')->find($id);
    if ($item === null) {
      $data = [
        'title' => 'Not Found',
        'detail' => "The user group with id {$id} does not exist",
        'status' => Response::HTTP_NOT_FOUND,
      ];
      return $this->view($data, Response::HTTP_NOT_FOUND);
    }

    $userIds = $request->request->all();

    $operatorRepo = $this->entityManager->getRepository('App\Entity\OperatoreUser');
    $users = new ArrayCollection();
    foreach ($userIds as $userId) {
      if (!Uuid::isValid($userId)) {
        $data = [
          'title' => 'Bad Request',
          'detail' => "{$userId} must be a valid uuid",
          'status' => Response::HTTP_BAD_REQUEST,
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }

      $user = $operatorRepo->find($userId);
      if (!$user instanceof OperatoreUser) {
        $data = [
          'title' => 'User not found',
          'detail' => "The user with id {$userId} does not exist",
          'status' => Response::HTTP_NOT_FOUND,
        ];
        return $this->view($data, Response::HTTP_NOT_FOUND);
      }

      $users->add($user);
    }

    $item->setUsers($users);
    $this->entityManager->persist($item);
    $this->entityManager->flush();

    return $this->view($item->getMembers(), Response::HTTP_OK);
  }

  /**
   * Remove users from group
   * @Rest\Delete("/{id}/members/{user_id}", name="user_group_api_members_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="User group or users not found"
   * )
   *
   * @OA\Tag(name="user-groups")
   *
   * @param $id
   * @param $userId
   * @return View
   */
  public function deleteMembersAction($id, Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_ADMIN);
    $item = $this->entityManager->getRepository('App\Entity\UserGroup')->find($id);
    if ($item === null) {
      $data = [
        'title' => 'Not Found',
        'detail' => "The user group with id {$id} does not exist",
        'status' => Response::HTTP_NOT_FOUND,
      ];
      return $this->view($data, Response::HTTP_NOT_FOUND);
    }


    $userId = $request->get('user_id');
    if (!Uuid::isValid($userId)) {
      $data = [
        'title' => 'Bad Request',
        'detail' => "{$userId} must be a valid uuid",
        'status' => Response::HTTP_BAD_REQUEST,
      ];
      return $this->view($data, Response::HTTP_BAD_REQUEST);
    }


    $operatorRepo = $this->entityManager->getRepository('App\Entity\OperatoreUser');
    $user = $operatorRepo->find($userId);
    if (!$user instanceof OperatoreUser) {
      $data = [
        'title' => 'User not found',
        'detail' => "The user with id {$userId} does not exist",
        'status' => Response::HTTP_NOT_FOUND,
      ];
      return $this->view($data, Response::HTTP_NOT_FOUND);
    }

    $item->removeUser($user);

    $this->entityManager->persist($item);
    $this->entityManager->flush();

    return $this->view(null, Response::HTTP_NO_CONTENT);
  }

  /**
   * @param Request $request
   * @param FormInterface $form
   * @return void
   */
  private function processForm(Request $request, FormInterface $form): void
  {
    $data = json_decode($request->getContent(), true);

    $clearMissing = $request->getMethod() != 'PATCH';
    $form->submit($data, $clearMissing);
  }

  /**
   * @param UserGroup $userGroup
   * @param Request $request
   * @return void
   */
  private function checkRelations(UserGroup &$userGroup, Request $request): void
  {

    if ($request->request->has('topic_id')) {
      // Per eliminare l'associazione della categoria
      if ($request->request->get('topic_id') === null) {
        $userGroup->setTopic(null);
      } else {
        $category = $this->entityManager->getRepository('App\Entity\Categoria')->find($request->request->get('topic_id'));
        if (!$category instanceof Categoria) {
          throw new InvalidArgumentException("Category does not exist");
        }
        $userGroup->setTopic($category);
      }

    }

    if ($request->request->has('manager_id')) {
      // Per eliminare l'associazione del manager
      if ($request->request->get('manager_id') === null) {
        $userGroup->setManager(null);
      } else {
        $manager = $this->entityManager->getRepository('App\Entity\OperatoreUser')->find($request->request->get('manager_id'));
        if (!$manager instanceof OperatoreUser) {
          throw new InvalidArgumentException("Manager does not exist");
        }
        $userGroup->setManager($manager);
      }
    }

    if ($request->request->has('calendar_id')) {
      // Per eliminare l'associazione del calendario
      if ($request->request->get('calendar_id') === null) {
        $userGroup->setCalendar(null);
      } else {
        $calendar = $this->entityManager->getRepository('App\Entity\Calendar')->find($request->request->get('calendar_id'));
        if (!$calendar instanceof Calendar) {
          throw new InvalidArgumentException("Calendar does not exist");
        }
        $userGroup->setCalendar($calendar);
      }
    }
  }

  private function generateExceptionResponse(\Exception $e, Request $request): View
  {
    if  ( $e instanceof InvalidArgumentException ) {
      $data = [
        'type' => 'validation_error',
        'title' => 'There was a validation error',
        'errors' => $e->getMessage(),
      ];
    } else {
      $data = [
        'type' => 'error',
        'title' => 'There was an error during save process',
        'description' => 'Contact technical support at support@opencontent.it'
      ];
      $this->logger->error($e->getMessage(), ['request' => $request]);
    }
    return $this->view($data, Response::HTTP_BAD_REQUEST);
  }

}
