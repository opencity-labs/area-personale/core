<?php

declare(strict_types=1);

namespace App\Controller\Rest;

use App\Entity\Allegato;
use App\Entity\User;
use App\Security\Voters\AttachmentVoter;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AvailabilitiesAPIController
 * @package App\Controller
 * @Route("/attachments")
 */
class AttachmentsAPIController extends AbstractFOSRestController
{
  private EntityManagerInterface $entityManager;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->entityManager = $entityManager;
  }

  /**
   * Option on attachment
   * @Rest\Options("/{id}", name="attachment_api_option")
   *
   * @OA\Response(
   *     response=200,
   *     description="Empty response",
   * )
   *
   * @OA\Tag(name="attachments")
   *
   * @return View
   */
  public function optionsAttachmentAction(): View
  {
    return $this->view([]);
  }

  /**
   * Delete an attachment, an attachment can only be deleted by the owner, if the attachment is linked to an application it can only be deleted if the application is in status draft
   * @Rest\Delete("/{id}", name="attachment_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=403,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="attachments")
   *
   * @param $id
   * @return View
   */
  public function deleteAttachmentAction($id): View
  {

    $item = $this->entityManager->getRepository(Allegato::class)->find($id);
    if (!$item instanceof Allegato) {
      return $this->view(["Object not found"], Response::HTTP_NOT_FOUND);
    }
    $this->denyAccessUnlessGranted(AttachmentVoter::DELETE, $item);
    $this->entityManager->remove($item);
    $this->entityManager->flush();
    return $this->view(null, Response::HTTP_NO_CONTENT);
  }
}
