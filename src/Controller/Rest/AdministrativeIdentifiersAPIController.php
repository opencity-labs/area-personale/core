<?php

namespace App\Controller\Rest;


use App\AdministrativeIdentifier\Application\AdministrativeIdentifierService;
use App\AdministrativeIdentifier\Application\Command\CreateAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Command\DeleteAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Command\GetAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Command\UpdateAdministrativeIdentifier;
use App\AdministrativeIdentifier\Application\Dto\CreateAdministrativeIdentifierDto;
use App\AdministrativeIdentifier\Application\Dto\UpdateAdministrativeIdentifierDto;
use App\AdministrativeIdentifier\Domain\Exception\AdministrativeIdentifierException;
use App\Entity\User;
use App\Entity\AdministrativeIdentifier;
use App\Model\Api\PagedList;
use App\Utils\FormUtils;
use DateTime;
use App\Model\LinksPagedList;
use App\Model\MetaPagedList;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\BadRequestProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\NotFoundProblem;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class AdministrativeIdentifiersAPIController
 * @package App\Controller
 * @Route("/administrative-identifiers")
 */
class AdministrativeIdentifiersAPIController extends AbstractFOSRestController
{
  public const CURRENT_API_VERSION = '1.0';

  private EntityManagerInterface $entityManager;

  private LoggerInterface $logger;
  private AdministrativeIdentifierService $administrativeIdentifierService;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   * @param AdministrativeIdentifierService $administrativeIdentifierService
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger,  AdministrativeIdentifierService $administrativeIdentifierService)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
    $this->administrativeIdentifierService = $administrativeIdentifierService;
  }

  /**
   * List all administrative identifiers
   * @Rest\Get("", name="administrative_identifiers_api_list")
   *
   * @OA\Parameter(
   *       name="offset",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Offset of the query"
   *   )
   * @OA\Parameter(
   *       name="limit",
   *       in="query",
   *       @OA\Schema(
   *           type="string"
   *       ),
   *       required=false,
   *       description="Limit of the query"
   *   )
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve list of administrative identifiers",
   *     @OA\JsonContent(
   *        type="object",
   *        @OA\Property(property="meta", type="object", ref=@Model(type=MetaPagedList::class)),
   *        @OA\Property(property="links", type="object", ref=@Model(type=LinksPagedList::class)),
   *        @OA\Property(property="data", type="array",
   *           @OA\Items(ref=@Model(type=AdministrativeIdentifier::class, groups={"read"}))
   *        )
   *     )
   *  )
   *
   * @OA\Tag(name="administrative-identifiers")
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function getAdministrativeIdentifiersAction(Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_CPS_USER);

    $offset = $request->query->getInt('offset', 0);
    $limit = $request->query->getInt('limit', 10);

    try {

      if ($limit > 100) {
        return $this->view(["Limit parameter is too high"], Response::HTTP_BAD_REQUEST);
      }

      $repo = $this->entityManager->getRepository(AdministrativeIdentifier::class);
      $user = $this->getUser();

      $meta = new MetaPagedList();
      $meta->setParameter(['offset' => $offset, 'limit' => $limit]);
      $meta->setCount($repo->getList(['user' => $user], true));
      $links = new LinksPagedList();
      $self = $this->generateUrl('administrative_identifiers_api_list', $meta->getParameter(), UrlGeneratorInterface::ABSOLUTE_URL);
      $links->setSelf($self);
      $result = new PagedList($meta, $links, $repo->getList(['user' => $user]));

      return $this->view($result, Response::HTTP_OK);
    } catch (ApiProblemException $e) {
      $this->logger->error('administrative_identifiers_api_list', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('administrative_identifiers_api_list', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(500, ['detail' => 'General Error']));
    }
  }

  /**
   * Retrieve an administrative identifier by id
   * @Rest\Get("/{id}", name="administrative_identifiers_api_get")
   *
   *
   * @OA\Response(
   *     response=200,
   *     description="Retrieve an administrative identifier",
   *     @Model(type=AdministrativeIdentifier::class, groups={"read"})
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Administrative identifier not found"
   * )
   *
   * @OA\Response(
   *    response=401,
   *    description="Access denied"
   *  )
   *
   * @OA\Tag(name="administrative-identifiers")
   *
   * @param string $id
   * @return View
   * @throws ApiProblemException
   */
  public function getAdministrativeIdentifierAction(string $id): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_CPS_USER);

    try {
      $command = new GetAdministrativeIdentifier($id);
      $result = $this->administrativeIdentifierService->get($command);
    } catch (NotFoundHttpException $e) {
      $this->logger->error('administrative_identifiers_api_get', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_NOT_FOUND, ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('administrative_identifiers_api_get', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_INTERNAL_SERVER_ERROR, ['detail' => 'General Error']));
    }
    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($result, Response::HTTP_OK)->setContext($context);
  }

  /**
   * Create an administrative identifier
   * @Rest\Post("", name="administrative_identifiers_api_post")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The Administrative identifier to create",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=AdministrativeIdentifier::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=201,
   *     description="Create a Adminstrative identifier"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *      response=403,
   *      description="Forbidden"
   *  )
   *
   * @OA\Tag(name="administrative-identifiers")
   *
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function postAdministrativeIdentifierAction(Request $request): View
  {
    $this->denyAccessUnlessGranted(User::ROLE_CPS_USER);
    $user= $this->getUser();

    try {
      $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
      $type = $data['type'] ?? '';
      $identifier = $data['identifier'] ?? '';
      $dto = new CreateAdministrativeIdentifierDto(Uuid::fromString($user->getId()), $type, $identifier);
      $errors = $this->administrativeIdentifierService->validate($dto);

      if (!empty($errors)) {
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }
      $command = new CreateAdministrativeIdentifier($dto);
      $item = $this->administrativeIdentifierService->create($command);

    } catch (AdministrativeIdentifierException $e) {
      $this->logger->error('administrative_identifiers_api_post', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_BAD_REQUEST, ['detail' => $e->getMessage()]));
    } catch (ApiProblemException $e) {
      $this->logger->error('administrative_identifiers_api_post', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('administrative_identifiers_api_post', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_INTERNAL_SERVER_ERROR, ['detail' => 'General Error']));
    }

    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($item, Response::HTTP_CREATED)->setContext($context);
  }

  /**
   * Edit full Administrative Identifier
   * @Rest\Put("/{id}", name="administrative_identifiers_api_put")
   *
   * @Security(name="Bearer")
   *
   * @OA\RequestBody(
   *     description="The administrative identifier to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=AdministrativeIdentifier::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Edit full administrative identifier"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=403,
   *    description="Forbidden"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="administrative-identifiers")
   *
   * @param $id
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function putAdministrativeIdentifierAction($id, Request $request): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_CPS_USER);
    $user = $this->getUser();
    try {
      $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
      $type = $data['type'] ?? '';
      $identifier = $data['identifier'] ?? '';
      $dto = new UpdateAdministrativeIdentifierDto(Uuid::fromString($id), $user->getId(), $type, $identifier);
      $errors = $this->administrativeIdentifierService->validate($dto);

      if (!empty($errors)) {
        $data = [
          'type' => 'validation_error',
          'title' => 'There was a validation error',
          'errors' => $errors
        ];
        return $this->view($data, Response::HTTP_BAD_REQUEST);
      }
      $command = new UpdateAdministrativeIdentifier($dto);
      $item = $this->administrativeIdentifierService->update($command);

    } catch (NotFoundHttpException $e) {
      $this->logger->error('administrative_identifiers_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_NOT_FOUND, ['detail' => $e->getMessage()]));
    } catch (AdministrativeIdentifierException $e) {
      $this->logger->error('administrative_identifiers_api_put', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_BAD_REQUEST, ['detail' => $e->getMessage()]));
    } catch (ApiProblemException $e) {
      $this->logger->error('administrative_identifiers_api_put', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('administrative_identifiers_api_put', [
        'exception' => $e->getMessage(),
      ]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_INTERNAL_SERVER_ERROR, ['detail' => 'General Error']));
    }

    $context = new Context();
    $context->setGroups(['read']);
    return $this->view($item, Response::HTTP_OK)->setContext($context);
  }

  /**
   * Patch an administrative identifier
   * @Rest\Patch("/{id}", name="administrative_identifiers_api_patch")
   *
   * @Security(name="Bearer")
   *
   *
   * @OA\RequestBody(
   *     description="The administrative identifier to update",
   *     required=true,
   *     @OA\MediaType(
   *         mediaType="application/json",
   *         @OA\Schema(
   *             type="object",
   *             ref=@Model(type=AdministrativeIdentifier::class, groups={"write"})
   *         )
   *     )
   * )
   *
   * @OA\Response(
   *     response=200,
   *     description="Patch an administrative identifier"
   * )
   *
   * @OA\Response(
   *     response=400,
   *     description="Bad request"
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Response(
   *    response=403,
   *    description="Forbidden"
   * )
   *
   * @OA\Response(
   *     response=404,
   *     description="Not found"
   * )
   * @OA\Tag(name="administrative-identifiers")
   *
   * @param $id
   * @param Request $request
   * @return View
   * @throws ApiProblemException
   */
  public function patchAdministrativeIdentifierAction($id, Request $request): View
  {
    return $this->putAdministrativeIdentifierAction($id, $request);
  }

  /**
   * Delete an administrative identifier
   * @Rest\Delete("/{id}", name="administrative_identifiers_api_delete")
   *
   * @OA\Response(
   *     response=204,
   *     description="The resource was deleted successfully."
   * )
   *
   * @OA\Response(
   *     response=401,
   *     description="Access denied"
   * )
   *
   * @OA\Tag(name="administrative-identifiers")
   *
   * @param $id
   * @return View
   * @throws ApiProblemException
   */
  public function deleteAdministrativeIdentifierAction($id): View
  {

    $this->denyAccessUnlessGranted(User::ROLE_CPS_USER);
    try {
      $command = new DeleteAdministrativeIdentifier($id);
      $this->administrativeIdentifierService->delete($command);
      return $this->view(null, Response::HTTP_NO_CONTENT);

    } catch (NotFoundHttpException $e) {
      $this->logger->error('administrative_identifiers_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_NOT_FOUND, ['detail' => $e->getMessage()]));
    } catch (ApiProblemException $e) {
      $this->logger->error('administrative_identifiers_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem($e->getCode(), ['detail' => $e->getMessage()]));
    } catch (\Exception $e) {
      $this->logger->error('administrative_identifiers_api_delete', ['exception' => $e->getMessage(),]);
      throw new ApiProblemException(new HttpApiProblem(Response::HTTP_INTERNAL_SERVER_ERROR, ['detail' => AbstractApiController::MESSAGE_INTERNAL_SERVER_ERROR]));
    }
  }
}
