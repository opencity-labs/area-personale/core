<?php

namespace App\Controller\General;

use App\Security\CohesionAuthenticator;
use App\Security\OAuthAuthenticator;
use App\Utils\LocaleUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Security\AbstractAuthenticator;
use App\Security\CasAuthenticator;
use App\Security\SiagAuthenticator;
use App\Security\DedaLoginAuthenticator;
use App\Security\DedaLogin\DedaLoginClient;
use Exception;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class SecurityController extends AbstractController
{

  use TargetPathTrait;

  private DedaLoginClient $dedaLoginClient;

  private SessionInterface $session;

  private OAuthAuthenticator $oAuthAuthenticator;
  private SiagAuthenticator $siagAuthenticator;

  public function __construct(DedaLoginClient $dedaLoginClient, SessionInterface $session, OAuthAuthenticator $oAuthAuthenticator, SiagAuthenticator $siagAuthenticator)
  {
    $this->dedaLoginClient = $dedaLoginClient;
    $this->session = $session;
    $this->oAuthAuthenticator = $oAuthAuthenticator;
    $this->siagAuthenticator = $siagAuthenticator;
  }

  /**
   * @Route("/login", name="login")
   * @param Request $request
   * @return RedirectResponse
   */
  public function loginAction(Request $request): RedirectResponse
  {
    $parameters = [];
    $format = $request->query->get('format', false);
    if ($format) {
      $parameters['format'] = $format;
    }

    $returnUrl = $request->query->get('return-url', false);
    if ($returnUrl) {
      $parameters['return-url'] = $returnUrl;
    }

    $locale = $request->query->get('lang', $request->query->get('locale', LocaleUtils::DEFAULT_LOCALE));
    $target = parse_url($this->getTargetPath($this->session, 'open_login'));
    if (!empty($target['host']) && $target['host'] === $request->getHost()) {
      $locale = explode('/', $target['path'])[2];
    }
    $parameters['locale'] = $locale;

    // Redirect in base a configurazione di istanza
    return $this->redirectToRoute($this->getAuthRedirect(), $parameters);
  }

  /**
   * @Route("/auth/spid/metadata", name="metadata")
   * @param Request $request
   * @return Response
   * @throws Exception
   */
  public function metadataAction(Request $request): Response
  {
    if ($this->getParameter('login_route') === DedaLoginAuthenticator::LOGIN_ROUTE) {
      return new Response($this->dedaLoginClient->getMetadata(), 200, ['Content-Type' => 'text/xml']);
    }

    throw new NotFoundHttpException("Current authentication handler does not handle metadata");
  }


  /**
   * @Route("/auth/spid/acs", name="acs")
   * @param Request $request
   * @return Response
   * @throws Exception
   */
  public function acsAction(Request $request): Response
  {
    if ($this->getParameter('login_route') === DedaLoginAuthenticator::LOGIN_ROUTE) {
      if ($samlResponse = $request->get('SAMLResponse')) {
        $assertionResponseData = $this->dedaLoginClient->checkAssertion($samlResponse);
        $userData = $this->dedaLoginClient->createUserDataFromAssertion($assertionResponseData);
        $this->session->set('DedaLoginUserData', $userData);
        return $this->redirectToRoute("login_deda");
      }
    }

    throw new NotFoundHttpException("Current authentication handler does not handle attribute consumer service");
  }

  /**
   * @Route("/auth/login-pat", name="login_pat")
   */
  public function loginPatAction(): void
  {
    throw new UnauthorizedHttpException("Something went wrong in authenticator");
  }

  /**
   * @Route("/auth/login-open", name="login_open")
   * @param Request $request
   * @return RedirectResponse
   */
  public function loginOpenAction(Request $request): RedirectResponse
  {
    if ($request->query->has('_abort')){
      return $this->redirectToRoute('home');
    }
    throw new UnauthorizedHttpException("Something went wrong in authenticator");
  }

  /**
   * @Route("/auth/login-deda", name="login_deda")
   * @param Request $request
   * @return Response
   * @throws Exception
   */
  public function loginDedaAction(Request $request): Response
  {
    if ($this->getParameter('login_route') !== DedaLoginAuthenticator::LOGIN_ROUTE) {
      throw new UnauthorizedHttpException("User can not login with login_deda");
    }

    if ($request->query->has('idp')){
      return new RedirectResponse($this->dedaLoginClient->getAuthRequest($request->query->get('idp')));
    }
    return $this->render('Default/loginDeda.html.twig');
  }

  /**
   * @Route("/auth/login-cas", name="login_cas")
   */
  public function loginCasAction(Request $request): RedirectResponse
  {
    if ($request->get(CasAuthenticator::QUERY_TICKET_PARAMETER)) {
      throw new UnauthorizedHttpException("Something went wrong in authenticator");
    }
    return new RedirectResponse($this->getParameter('cas_login_url').'?service='.urlencode($request->getUri()));
  }

  /**
   * @Route("/auth/login-siag", name="login_siag")
   */
  public function loginSiagAction(Request $request): RedirectResponse
  {
    return $this->siagAuthenticator->start($request);
  }

  /**
   * @Route("/auth/login-marche", name="login_cohesion")
   * @throws Exception
   */
  public function loginMarcheAction(Request $request, CohesionAuthenticator $authenticator): RedirectResponse
  {
    return $authenticator->start($request);
  }

  /**
   * @Route("/auth/login-token", name="login_token")
   * @param Request $request
   * @return RedirectResponse
   */
  public function loginTokenAction(Request $request): RedirectResponse
  {
    throw new UnauthorizedHttpException("Something went wrong in authenticator");
  }

  /**
   * @Route("/auth/login-oauth/{path}", name="login_oauth")
   * @param Request $request
   * @param bool $path
   * @return RedirectResponse
   */
  public function loginOAuthAction(Request $request, $path = false): RedirectResponse
  {
    return $this->oAuthAuthenticator->start($request);
  }

  /**
   * @Route("/auth/login-success", name="login_success")
   * @param Request $request
   * @return Response
   */
  public function loginSuccess(Request $request): Response
  {
    return $this->render(
      'Default/loginSuccess.html.twig',
      ['user' => $this->getUser()]
    );
  }

  /**
   * @Route("/logout", name="user_logout")
   * @throws Exception
   * @see LogoutSuccessHandler
   */
  public function logout(): void
  {
    throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
  }

  private function getAuthRedirect()
  {
    if ($this->getParameter('login_route') === AbstractAuthenticator::LOGIN_TYPE_NONE) {
      return 'home';
    }

    return $this->getParameter('login_route');
  }
}
