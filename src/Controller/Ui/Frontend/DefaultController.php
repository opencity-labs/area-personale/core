<?php

namespace App\Controller\Ui\Frontend;


use App\Form\TermsAndPrivacyType;
use App\InstancesProvider;
use App\Services\InstanceService;
use App\Entity\CPSUser;
use App\Logging\LogConstants;
use App\Services\Manager\JwksManager;
use App\Services\Manager\UserManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{

  /** @var LoggerInterface */
  private LoggerInterface $logger;

  /** @var TranslatorInterface */
  private TranslatorInterface $translator;

  /** @var InstanceService */
  private InstanceService $instanceService;
  /**
   * DefaultController constructor.
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   * @param InstanceService $instanceService
   */
  public function __construct(TranslatorInterface $translator, LoggerInterface $logger, InstanceService $instanceService)
  {
    $this->logger = $logger;
    $this->translator = $translator;
    $this->instanceService = $instanceService;
  }


  /**
   * @param Request $request
   * @return Response|null
   */
  public function commonAction(Request $request): ?Response
  {
    if ($this->instanceService->hasInstance()) {
      return $this->forward(ServiziController::class . '::serviziAction');
    }

    $host = $request->server->get('HTTP_HOST');
    $enti = [];
    foreach (InstancesProvider::factory()->getInstances() as $identifier => $instance) {
      $indentifierParts = explode('/', $identifier);
      if ($indentifierParts[0] === $host) {
        $enti[] = [
          'name' => $instance['name'] ?? ucwords(str_replace('-', ' ', $instance['identifier'])),
          'slug' => $indentifierParts[1]
        ];
      }
    }

    if (count($enti) === 1) {
      return $this->redirect('/'. $enti[0]['slug']. '/'. $request->getDefaultLocale());
    }

    return $this->render(
      'Default/common.html.twig',
      ['enti' => $enti]
    );
  }

  /**
   * @Route("/", name="instance_home")
   * @param Request $request
   * @return Response
   */
  public function indexAction(Request $request): Response
  {
    $ente = $this->instanceService->getCurrentInstance();

    if ($ente->isSearchAndCatalogueEnabled()) {
      $homepage = $this->getParameter('home_page');
      $routes = $this->get('router')->getRouteCollection();
      if ($routes->get($homepage)) {
        return $this->forward($routes->get($homepage)->getDefaults()['_controller']);
      }
      return $this->forward(ServiziController::class . '::serviziAction');
    }
    $servicesI18n = $this->translator->trans('services', [], null, $request->getLocale());
    return $this->redirect($ente->getServicesUrl($servicesI18n));
  }

  /**
   * @Route("/terms", name="terms")
   * @param Request $request
   *
   * @return Response
   */
  public function termsAction(Request $request, UserManager $userManager): Response
  {

    $privacyUrl = $this->instanceService->getCurrentInstance()->getPrivacyInfo();
    if (null === $privacyUrl) {
      return $this->redirectToRoute('instance_home');
    }

    $user = $this->getUser();
    $form = $this->createForm(TermsAndPrivacyType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      try {
        $user->markTermsAndPrivacyAsRead();
        $userManager->save($user);
        $redirectUrl = $request->query->has('r') ? $request->query->get('r') : $this->generateUrl('home');
        $this->logger->info(LogConstants::USER_HAS_ACCEPTED_TERMS, ['userid' => $user->getId()]);
        return $this->redirect($redirectUrl);
      } catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    return $this->render( 'Default/terms.html.twig', [
      'form' => $form->createView(),
      'privacy_url' => $privacyUrl,
      'user' => $user,
    ]);
  }


  /**
   * @Route("/elenco-segnalazioni/", name="inefficiencies")
   * @return Response|null
   */
  public function listInefficienciesAction(): Response
  {
    $ente = $this->instanceService->getCurrentInstance();
    return $this->render('Servizi/inefficiency.html.twig', [
      'user' => $this->getUser(),
      'privacy_url' => $ente->getPrivacyInfo() ?? ''
    ]);
  }

  /**
   * @Route("/error", name="error")
   * @return Response|null
   */
  // Todo: Pensare ad un meccanismo generico per gestire tutti gli errori voluti (non eccezioni)
  public function errorAction(): Response
  {
    return $this->render('Errors/minor.html.twig', [
      'user' => $this->getUser()
    ]);
  }

  /**
   * @return JsonResponse
   */
  public function statusAction(): JsonResponse
  {
    return new JsonResponse(['status' => 'ok'], Response::HTTP_OK);
  }

  /**
   * @param JwksManager $jwksManager
   * @return JsonResponse
   */
  public function jwksAction(JwksManager $jwksManager): JsonResponse
  {
    return new JsonResponse($jwksManager->generateJwks(), 200);
  }

}
