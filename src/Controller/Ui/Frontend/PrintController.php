<?php

namespace App\Controller\Ui\Frontend;

use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Security\Voters\ApplicationVoter;
use App\Services\ModuloPdfBuilderService;
use Gotenberg\Exceptions\GotenbergApiErroed;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


/**
 * Class PraticheAnonimeController
 *
 * @package App\Controller
 * @Route("/print")
 */
class PrintController extends AbstractController
{

  private ModuloPdfBuilderService $moduloPdfBuilderService;
  private LoggerInterface $logger;
  private UrlGeneratorInterface $router;

  /**
   * PrintController constructor.
   * @param ModuloPdfBuilderService $moduloPdfBuilderService
   * @param LoggerInterface $logger
   * @param UrlGeneratorInterface $router
   */
  public function __construct(ModuloPdfBuilderService $moduloPdfBuilderService, LoggerInterface $logger, UrlGeneratorInterface $router)
  {
    $this->moduloPdfBuilderService = $moduloPdfBuilderService;
    $this->logger = $logger;
    $this->router = $router;
  }


  /**
   * @Route("/pratica/{pratica}", name="print_pratiche")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   * @param Request $request
   * @param Pratica $pratica
   *
   * @return Response
   * @throws LoaderError|RuntimeError|SyntaxError
   */
  public function printPraticaAction(Request $request, Pratica $pratica): Response
  {

    $this->denyAccessUnlessGranted(ApplicationVoter::VIEW, $pratica, "User can not read application {$pratica->getId()}");
    $showProtocolNumber = $request->get('protocol', false);

    try {

      if ($request->query->has('preview')) {
        return new Response($this->moduloPdfBuilderService->renderForPratica($pratica, $showProtocolNumber));
      }

      $fileContent = $this->moduloPdfBuilderService->generateApplicationPdf($pratica, $showProtocolNumber);
      return $this->prepareFileResponse($fileContent);
    } catch (GotenbergApiErroed $e) {
      $this->logger->error('print_pratiche_show', [
        'application_id' => $pratica->getId(),
        'exception' => $e->getMessage(),
      ]);
    }

    return new Response("Unable to print the requested application", Response::HTTP_INTERNAL_SERVER_ERROR);
  }

  /**
   * @Route("/application/{pratica}", name="print_application")
   * @ParamConverter("pratica", class="App\Entity\Pratica")
   * @param Request $request
   * @param Pratica $pratica
   *
   * @return Response
   * @throws LoaderError|RuntimeError|SyntaxError
   */
  public function printApplicationAction(Request $request, Pratica $pratica): Response
  {
    $showProtocolNumber = $request->get('protocol', false);
    return new Response($this->moduloPdfBuilderService->renderForPratica($pratica, $showProtocolNumber));
  }

  /**
   * @Route("/service/{service}", name="print_service")
   * @ParamConverter("service", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $service
   *
   * @return Response
   */
  public function printServiceAction(Request $request, Servizio $service): Response
  {

    $pratica = $this->createApplication($service);

    $form = $this->createForm('App\Form\FormIO\FormIORenderType', $pratica);

    return $this->render('Print/printService.html.twig', [
      'formserver_url' => $this->getParameter('formserver_admin_url'),
      'form' => $form->createView(),
      'pratica' => $pratica
    ]);
  }

  /**
   * @Route("/service/{service}/pdf", name="print_service_pdf")
   * @ParamConverter("service", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $service
   *
   * @return Response
   * @throws GotenbergApiErroed
   */
  public function printServicePdfAction(Request $request, Servizio $service): Response
  {
    $url = $this->router->generate('print_service', ['service' => $service->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
    $fileContent = $this->moduloPdfBuilderService->generatePdfFromUrlUsingGotemberg($url);
    return $this->prepareFileResponse($fileContent);
  }

  /**
   * @Route("/preview-pdf", name="print_preview_pdf")
   * @param Request $request
   *
   * @return Response
   * @throws GotenbergApiErroed
   */
  public function printPreviewPdfAction(Request $request): Response
  {
    $url = $this->router->generate('template_preview', [], UrlGeneratorInterface::ABSOLUTE_URL);
    $fileContent = $this->moduloPdfBuilderService->generatePdfFromUrlUsingGotemberg($url, '2ms');
    return $this->prepareFileResponse($fileContent);
  }

  /**
   * @param string $fileContent
   * @return Response
   */
  private function prepareFileResponse(string $fileContent): Response
  {
    // Provide a default name for your file with extension
    $filename = time() . '.pdf';
    // Return a response with a specific content
    $response = new Response($fileContent);
    // Create the disposition of the file
    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $filename
    );
    // Set the content disposition
    $response->headers->set('Content-Disposition', $disposition);
    // Dispatch request
    return $response;
  }


  /**
   * @Route("/service/{service}/preview", name="preview_service")
   * @ParamConverter("service", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $service
   *
   * @return Response
   */
  public function previewServiceAction(Request $request, Servizio $service): Response
  {

    $pratica = $this->createApplication($service);

    $form = $this->createForm('App\Form\FormIO\FormIORenderType', $pratica);

    return $this->render('Print/previewService.html.twig', [
      'formserver_url' => $this->getParameter('formserver_admin_url'),
      'form' => $form->createView(),
      'pratica' => $pratica
    ]);
  }

  /**
   * @Route("/template-preview", name="template_preview")
   * @param Request $request
   *
   * @return Response
   */
  public function previewPdfAction(Request $request): Response
  {
    return $this->render('Print/printTemplatePreview.html.twig');
  }

  /**
   * @param Servizio $service
   * @return Pratica
   */
  private function createApplication(Servizio $service): Pratica
  {
    $praticaClassName = $service->getPraticaFCQN();
    $pratica = new $praticaClassName();
    if (!$pratica instanceof Pratica) {
      throw new \RuntimeException("Wrong Pratica FCQN for servizio {$service->getName()}");
    }
    $pratica
      ->setServizio($service)
      ->setStatus(Pratica::STATUS_DRAFT);

    return $pratica;
  }


}
