<?php

namespace App\Controller\Ui\Backend\Admin;

use App\Dto\ServiceDto;
use App\Entity\Categoria;
use App\Entity\Pratica;
use App\Entity\Servizio;
use App\Entity\UserGroup;
use App\Form\Admin\Servizio\AdvancedSettingsType;
use App\Form\Admin\Servizio\AssignmentConfigType;
use App\Form\Admin\Servizio\CardDataType;
use App\Form\Admin\Servizio\CustomTemplateType;
use App\Form\Admin\Servizio\FeedbackMessagesDataType;
use App\Form\Admin\Servizio\FormIOBuilderRenderType;
use App\Form\Admin\Servizio\FormIOI18nType;
use App\Form\Admin\Servizio\FormIOTemplateType;
use App\Form\Admin\Servizio\GeneralDataType;
use App\Form\Admin\Servizio\IntegrationsDataType;
use App\Form\Admin\Servizio\IOIntegrationDataType;
use App\Form\Admin\Servizio\MultiplePaymentsDataType;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Form\Admin\Servizio\PdndType;
use App\Form\Admin\Servizio\ProtocolDataType;
use App\Form\Admin\Servizio\ReceiptType;
use App\Form\Admin\Servizio\StampsDataType;
use App\Form\ServizioFormType;
use App\FormIO\SchemaComponent;
use App\FormIO\SchemaFactoryInterface;
use App\Model\FlowStep;
use App\Model\PublicFile;
use App\Model\Service;
use App\Model\ServiceSource;
use App\Services\FileService\ServiceAttachmentsFileService;
use App\Services\FormServerApiAdapterService;
use App\Services\InstanceService;
use App\Services\IOService;
use App\Services\Manager\PdndManager;
use App\Services\Manager\ServiceManager;
use App\Services\Satisfy\SatisfyService;
use App\Utils\FormUtils;
use App\Utils\StringUtils;
use DateTime;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Flagception\Manager\FeatureManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use League\Flysystem\FileNotFoundException;
use Omines\DataTablesBundle\DataTableFactory;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\FormIO;

/**
 * Class ServiceController
 * @Route("/admin")
 */
class ServiceController extends AbstractController
{

  private FormServerApiAdapterService $formServer;
  private TranslatorInterface $translator;
  private SchemaFactoryInterface $schemaFactory;
  private IOService $ioService;
  private LoggerInterface $logger;
  private ServiceManager $serviceManager;
  private EntityManagerInterface $entityManager;
  private ServiceAttachmentsFileService $fileService;
  private $locales;
  private InstanceService $instanceService;

  public function __construct(
    FormServerApiAdapterService   $formServer,
    TranslatorInterface           $translator,
    SchemaFactoryInterface        $schemaFactory,
    IOService                     $ioService,
    LoggerInterface               $logger,
    ServiceManager                $serviceManager,
    EntityManagerInterface        $entityManager,
    ServiceAttachmentsFileService $fileService,
    InstanceService               $instanceService,
    string                        $locales
  )
  {
    $this->formServer = $formServer;
    $this->translator = $translator;
    $this->schemaFactory = $schemaFactory;
    $this->ioService = $ioService;
    $this->logger = $logger;
    $this->serviceManager = $serviceManager;
    $this->entityManager = $entityManager;
    $this->fileService = $fileService;
    $this->locales = explode('|', $locales);
    $this->instanceService = $instanceService;
  }

  /**
   * Lists all Services entities.
   * @Route("/servizio", name="admin_servizio_index", methods={"GET"})
   */
  public function indexServizioAction(): Response
  {
    $accessLevels = [
      Servizio::ACCESS_LEVEL_ANONYMOUS => $this->translator->trans('general.anonymous'),
      Servizio::ACCESS_LEVEL_SOCIAL => $this->translator->trans('general.social'),
      Servizio::ACCESS_LEVEL_SPID_L1 => $this->translator->trans('general.level_spid_1'),
      Servizio::ACCESS_LEVEL_SPID_L2 => $this->translator->trans('general.level_spid_2'),
      Servizio::ACCESS_LEVEL_CIE => $this->translator->trans('general.cie'),
    ];

    $items = $this->entityManager->getRepository(Servizio::class)->findBy([], ['name' => 'ASC']);

    return $this->render('Admin/indexServizio.html.twig', [
      'user' => $this->getUser(),
      'items' => $items,
      'access_levels' => $accessLevels,
    ]);
  }

  /**
   * @Route("/servizio/list", name="admin_servizio_list", methods={"GET"})
   * @deprecated Use API
   */
  public function listServizioAction(): JsonResponse
  {

    $items = $this->entityManager->getRepository(Servizio::class)->findBy(['praticaFCQN' => Servizio::FORMIO_SERVICE_CLASS],
      ['name' => 'ASC']);

    $data = [];
    foreach ($items as $s) {
      $descLimit = 150;
      $description = strip_tags($s->getDescription());
      if (strlen($description) > $descLimit) {
        $description = utf8_encode(substr($description, 0, $descLimit) . '...');
      }
      $data [] = [
        'id' => $s->getId(),
        'title' => $s->getName(),
        'description' => $description,
      ];
    }

    return new JsonResponse($data);
  }

  /**
   * @Route("/servizio/import", name="admin_servizio_import")
   * @param Request $request
   * @param ServiceDto $serviceDto
   * @return RedirectResponse
   * @throws GuzzleException
   */
  public function importServizioAction(Request $request, ServiceDto $serviceDto): RedirectResponse
  {
    $ente = $this->instanceService->getCurrentInstance();

    $remoteUrl = $request->get('url');
    $client = new Client();
    $serviceRequest = new \GuzzleHttp\Psr7\Request(
      'GET',
      $remoteUrl,
      [
        'Content-Type' => 'application/json',
        'x-locale' => $request->getLocale(),
      ]
    );

    try {
      $response = $client->send($serviceRequest);

      if ($response->getStatusCode() === 200) {
        $responseBody = json_decode($response->getBody(), true);
        $responseBody['tenant'] = $ente->getId();

        $dto = new Service();
        $dto->setTenant($this->instanceService->getCurrentInstance());
        $form = $this->createForm(ServizioFormType::class, $dto);
        $serviceId = $responseBody['id'];
        $md5Response = md5(json_encode($responseBody));
        $identifier = $responseBody['identifier'] ?? null;
        unset($responseBody['id'], $responseBody['slug']);

        //$responseBody['payment_parameters'], $responseBody['payment_required']

        if ($responseBody['payment_required']) {
          $originGateway = $responseBody['payment_parameters']['gateways'][0]['identifier'] ?? null;

          if (!$originGateway) {
            // Non è stato possibile recuperare il gateway selezionato sul servizio d'origine
            $this->addFlash('warning', $this->translator->trans('servizio.undefined_gateway_import', ['%service_name%' => $responseBody['name']]));
            unset($responseBody['payment_parameters'], $responseBody['payment_required']);
          } else {
            // Verifico se è possibile abilitare lo stesso intermediario di pagamento altrimenti seleziono il primo disponibile
            $enabledGateways = $this->instanceService->getCurrentInstance()->getGateways();
            if (empty($enabledGateways)) {
              // Non sono abilitati intermediari di pagamento
              unset($responseBody['payment_parameters'], $responseBody['payment_required']);
              $this->addFlash('warning', $this->translator->trans('servizio.missing_tenant_gateways_import', ['%service_name%' => $responseBody['name']]));
            } else if (!array_key_exists($originGateway, $enabledGateways)) {
              // Seleziono il primo intermediario disponibile tra quelli abilitati sull'ente
              $responseBody['payment_parameters']['gateways'][0]['identifier'] = array_key_first($enabledGateways);
              $this->addFlash('warning', $this->translator->trans('servizio.missing_gateway_import', ['%service_name%' => $responseBody['name'], '%origin_gateway%' => $originGateway]));
            } else {
              $this->addFlash('warning', $this->translator->trans('servizio.complete_payment_config_import', ['%service_name%' => $responseBody['name']]));
            }
          }
        }

        $data = ServiceDto::normalizeData($responseBody);

        // Populates default messages in the language provided in the request if not provided or incomplete
        // Messages may not be valued because they were entered only when the service was first saved
        $defaultFeedbackMessages = $this->serviceManager->getDefaultFeedbackMessages()[$request->getLocale()];

        // Todo: sistemare assolutamente
        foreach ($data['feedback_messages'] as $statusName => $feedbackMessage) {
          $status = Pratica::getStatusCodeByName($statusName);
          if (!isset($feedbackMessage['subject']) || !$feedbackMessage['subject']) {
            $feedbackMessage['subject'] = $defaultFeedbackMessages[$status]->getSubject();
          }
          if (!isset($feedbackMessage['message']) || !$feedbackMessage['message']) {
            $feedbackMessage['message'] = $defaultFeedbackMessages[$status]->getMessage();
          }
          if (!isset($feedbackMessage['is_active'])) {
            $feedbackMessage['is_active'] = $defaultFeedbackMessages[$status]->isActive();
          }
          $data['feedback_messages'][$statusName] = $feedbackMessage;
        }


        if (empty($data['short_description'])) {
          $data['short_description'] = '-';
        }

        $form->submit($data, true);

        if ($form->isSubmitted() && !$form->isValid()) {
          $errors = FormUtils::getErrorsFromForm($form);
          $this->addFlash('error', $this->translator->trans('servizio.error_import'));
          foreach ($errors as $e) {
            if (is_array($e)) {
              $this->addFlash('error', implode(', ', $e));
            } else {
              $this->addFlash('error', $e);
            }
          }
          $this->logger->error("Import validation error: ", $errors);
          return $this->redirectToRoute('admin_servizio_index');
        }

        try {
          $updatedAt = $responseBody['updated_at'] ? new DateTime($data['updated_at']) : null;
        } catch (\Exception $e) {
          $updatedAt = null;
        }
        $serviceSource = new ServiceSource();
        $serviceSource
          ->setId($serviceId)
          ->setUrl($remoteUrl)
          ->setUpdatedAt($updatedAt)
          ->setMd5($md5Response)
          ->setVersion('1')
          ->setIdentifier($identifier);
        $dto = $dto->setSource($serviceSource);
        $dto->setBookingCallToAction(null);
        $dto->setCoverage([]);
        $dto->setSatisfyEntrypointId(null);

        $this->serviceManager->checkServiceRelations($dto);
        $service = $serviceDto->toEntity($dto);

        $service->setIdentifier($serviceSource->getIdentifier());
        $importedAt = ' (' . $this->translator->trans('imported') . ' ' . date('d/m/Y H:i:s') . ')';
        $shortenedImportedServiceName = StringUtils::shortenString($service->getName(), 255 - strlen($importedAt));
        $service->setName($shortenedImportedServiceName . $importedAt);
        $service->setEnte($ente);
        $this->serviceManager->save($service);

        if (!empty($service->getFormIoId())) {
          $response = $this->formServer->cloneFormFromRemote($service, $remoteUrl . '/form');
          if ($response['status'] === 'success') {
            $formId = $response['form_id'];
            $flowStep = new FlowStep();
            $flowStep
              ->setIdentifier($formId)
              ->setType('formio')
              ->addParameter('formio_id', $formId);
            $service->setFlowSteps([$flowStep]);
            // Backup
            $additionalData = $service->getAdditionalData();
            $additionalData['formio_id'] = $formId;
            $service->setAdditionalData($additionalData);
          } else {
            $this->entityManager->remove($service);
            $this->entityManager->flush();
            $this->addFlash('error', $this->translator->trans('servizio.error_create_form'));

            return $this->redirectToRoute('admin_servizio_index');
          }
        }

        $this->serviceManager->save($service);

        $this->addFlash('success', $this->translator->trans('servizio.success_import_service'));

        return $this->redirectToRoute('admin_servizio_index');

      }
    } catch (UniqueConstraintViolationException $e) {
      $this->logger->error("Import error: duplicated identifier $identifier");
      $this->addFlash(
        'error',
        $this->translator->trans(
          'servizio.error_duplicated_identifier',
          ['%identifier%' => $identifier]
        )
      );
    } catch (\Exception $e) {
      $this->logger->error("Import error: " . $e->getMessage());
      $this->addFlash('error', $this->translator->trans('servizio.error_create_service'));
    }

    return $this->redirectToRoute('admin_servizio_index');
  }

  /**
   * @Route("/servizio/{id}/edit", name="admin_servizio_edit")
   * @ParamConverter("id", class="App\Entity\Servizio")
   * @param Servizio $servizio
   * @param Request $request
   * @param SatisfyService $satisfyService
   * @param FeatureManagerInterface $featureManager
   * @param PdndManager $pdndManager
   * @return Response
   */
  public function editServizioAction(Servizio $servizio, Request $request, SatisfyService $satisfyService, FeatureManagerInterface $featureManager, PdndManager $pdndManager): Response
  {
    $user = $this->getUser();
    $steps = [
      'template' => [
        'label' => $this->translator->trans('general.form_template'),
        'class' => FormIOTemplateType::class,
        'icon' => 'fa-clone',
      ],
      'general' => [
        'label' => $this->translator->trans('operatori.dati_generali'),
        'class' => GeneralDataType::class,
        'template' => 'Admin/servizio/_generalStep.html.twig',
        'icon' => 'fa-file-o',
      ],
      'card' => [
        'label' => $this->translator->trans('operatori.scheda'),
        'class' => CardDataType::class,
        'template' => 'Admin/servizio/_cardStep.html.twig',
        'icon' => 'fa-file-text-o',
      ],
      'formio' => [
        'label' => $this->translator->trans('operatori.modulo'),
        'class' => FormIOBuilderRenderType::class,
        'template' => 'Admin/servizio/_formIOBuilderStep.html.twig',
        'icon' => 'fa-server',
      ],
      'formioI18n' => [
        'label' => $this->translator->trans('servizio.i18n.translations_module'),
        'class' => FormIOI18nType::class,
        'template' => 'Admin/servizio/_formIOI18nStep.html.twig',
        'icon' => 'fa-language',
      ],
      'pdnd' => [
        'label' => $this->translator->trans('nav.admin.pdnd'),
        'class' => PdndType::class,
        'template' => 'Admin/servizio/_pdnd.html.twig',
        'icon' => 'fa-handshake-o',
      ],
      'messages' => [
        'label' => $this->translator->trans('operatori.messaggi.titolo'),
        'class' => FeedbackMessagesDataType::class,
        'template' => 'Admin/servizio/_feedbackMessagesStep.html.twig',
        'icon' => 'fa-envelope-o',
      ],
      'app-io' => [
        'label' => $this->translator->trans('app_io.title'),
        'class' => IOIntegrationDataType::class,
        'template' => 'Admin/servizio/_ioIntegrationStep.html.twig',
        'icon' => 'fa-bullhorn',
      ],
      'stamps' => [
        'label' => $this->translator->trans('general.stamps'),
        'class' => StampsDataType::class,
        'template' => 'Admin/servizio/_stampsStep.html.twig',
        'icon' => 'fa-barcode',
      ],
      'payments' => [
        'label' => $this->translator->trans('general.payment_data'),
        'class' => PaymentDataType::class,
        'template' => 'Admin/servizio/_paymentsStep.html.twig',
        'icon' => 'fa-credit-card',
      ],
      'protocol' => [
        'label' => $this->translator->trans('servizio.protocollo'),
        'class' => ProtocolDataType::class,
        'template' => 'Admin/servizio/_protocolStep.html.twig',
        'icon' => 'fa-folder-open-o',
      ],
      'receipt' => [
        'label' => $this->translator->trans('general.receipt'),
        'class' => ReceiptType::class,
        'template' => 'Admin/servizio/_receipt.html.twig',
        'icon' => 'fa-file-pdf-o',
      ],
      'custom_template' => [
        'label' => $this->translator->trans('ente.custom_template.title'),
        'class' => CustomTemplateType::class,
        'template' => 'Admin/servizio/_customTemplate.html.twig',
        'icon' => 'fa-file-code-o',
      ],
      'backoffices' => [
        'label' => $this->translator->trans('integrations'),
        'class' => IntegrationsDataType::class,
        'template' => 'Admin/servizio/_backofficesStep.html.twig',
        'icon' => 'fa-cogs',
      ],
      'assignment' => [
        'label' => $this->translator->trans('servizio.assignment.title'),
        'class' => AssignmentConfigType::class,
        'template' => 'Admin/servizio/_assignmentConfig.html.twig',
        'icon' => 'fa-android',
      ],
      'advanced' => [
        'label' => $this->translator->trans('operatori.advanced_settings'),
        'class' => AdvancedSettingsType::class,
        'template' => 'Admin/servizio/_advancedSettings.html.twig',
        'icon' => 'fa-sliders',
      ],
    ];

    if (!$featureManager->isActive('feature_custom_templates')) {
      unset($steps['custom_template']);
    }

    if (!$featureManager->isActive('feature_pdnd')) {
      unset($steps['pdnd']);
    }

    if (!$featureManager->isActive('feature_automatic_applications_assignment')) {
      unset($steps['assignment']);
    }

    if ($this->serviceManager->hasGatewayWithDigitalStamps($servizio)) {
      unset($steps['stamps']);
    }

    if ($servizio->isLegacy()) {
      unset($steps['template'], $steps['formio'], $steps['formioI18n'], $steps['backoffices'], $steps['assignment']);
    }

    if ($servizio->isBuiltIn()) {
      unset($steps['template'], $steps['formio'], $steps['formioI18n'], $steps['backoffices'], $steps['stamps'], $steps['payments'], $steps['pdnd']);
    }

    if ($servizio->isDuePayment()) {
      unset($steps['template'], $steps['formio'], $steps['formioI18n'], $steps['messages'], $steps['app-io'], $steps['stamps'], $steps['protocol'], $steps['receipt'], $steps['custom_template'], $steps['backoffices'], $steps['advanced'], $steps['pdnd'], $steps['assignment']);
    }

    if (!$servizio->isLegacy() && !empty($servizio->getFormIoId())) {
      unset($steps['template']);
    }

    if (count($this->locales) <= 1) {
      unset($steps['formioI18n']);
    }

    if (isset($steps['payments']) && $featureManager->isActive('feature_multiple_payments')) {
      $steps['payments'] = [
        'label' => $this->translator->trans('general.payment_data'),
        'class' => MultiplePaymentsDataType::class,
        'template' => 'Admin/servizio/_multiplePaymentsStep.html.twig',
        'icon' => 'fa-credit-card',
      ];
    }

    $currentStep = $request->query->get('step');
    $nexStep = false;
    $keys = array_keys($steps);
    if (!in_array($currentStep, $keys)) {
      $currentStep = $keys[0];
    }
    $currentKey = array_search($currentStep, $keys, true);
    if (isset($keys[$currentKey + 1])) {
      $nexStep = $keys[$currentKey + 1];
    }


    $schema = false;
    if ($servizio->isFormio() || $servizio->isBuiltIn()) {
      $schema = $this->schemaFactory->createFromService($servizio, false);
    }
    $backofficeSchema = false;
    if ($servizio->getBackofficeFormId()) {
      $backofficeSchema = $this->schemaFactory->createFromFormId($servizio->getBackofficeFormId(), false);
    }

    $form = null;
    if (isset($steps[$currentStep]['class'])) {
      $form = $this->createForm($steps[$currentStep]['class'], $servizio);
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {

        // Satisfy
        if (empty($servizio->getSatisfyEntrypointId())) {
          try {
            $satisfyService->syncEntryPoint($servizio, false);
          } catch (\Exception $e) {
            $this->logger->error('Error on configure satisfy entrypoint for service ' . $servizio->getName() . ' - ' . $e->getMessage());
          }
        }

        try {
          $this->serviceManager->save($servizio);
        }catch (UniqueConstraintViolationException $e){
          $this->addFlash('error', $this->translator->trans('servizio.save.duplicate_identifier_error'));
          $this->logger->error('Error on save service due to duplicated service identifier' . $servizio->getName() . ' - ' . $e->getMessage());
        } catch (\Exception $e) {
          $this->addFlash('error', $this->translator->trans('servizio.save.error'));
          $this->logger->error('Error on save service ' . $servizio->getName() . ' - ' . $e->getMessage());
        }

        if ($request->request->get('save') === 'next') {
          return $this->redirectToRoute('admin_servizio_edit', ['id' => $servizio->getId(), 'step' => $nexStep]);
        }
        return $this->redirectToRoute('admin_servizio_edit', ['id' => $servizio->getId(), 'step' => $currentStep]);
      }
    }

    $pdndServiceConfigs = [];
    if (isset($steps['pdnd'])) {
      $pdndServiceConfigs = $pdndManager->getServiceConfigs($servizio);
    }

    $assignementOptions = [];
    $userGroups = [];
    if (isset($steps['assignment']) && $currentStep === 'assignment') {
      foreach ($this->entityManager->getRepository(UserGroup::class)->findAll() as $userGroup) {
        $userGroups[$userGroup->getId()] = $userGroup->getName();
      }

      $choicesComponents = $schema->getChoiceComponents();
      if (!empty($choicesComponents)) {
        /** @var SchemaComponent $choiceComponent */
        foreach ($choicesComponents as $choiceComponent) {
          if (!empty($choiceComponent->getFormOptions()['choices'])) {
            $assignementOptions[] = [
              'key' => $choiceComponent->getName(),
              'name' => $choiceComponent->getFormOptions()['label'],
              'choices' => $choiceComponent->getFormOptions()['choices'],
            ];
          }
        }
      }
    }


    return $this->render('Admin/editServizio.html.twig', [
      'form' => $form ? $form->createView() : null,
      'steps' => $steps,
      'current_step' => $currentStep,
      'next_step' => $nexStep,
      'servizio' => $servizio,
      'schema' => $schema,
      'backoffice_schema' => $backofficeSchema,
      'formserver_url' => $this->getParameter('formserver_admin_url'),
      'user' => $user,
      'pdnd_configs' => $pdndServiceConfigs,
      'assignemnt_options' => $assignementOptions,
      'user_groups' => $userGroups,
    ]);
  }


  /**
   * Creates a new Service entity.
   * @Route("/servizio/new", name="admin_service_new", methods={"GET", "POST"})
   * @param Request $request
   * @return RedirectResponse|Response|null
   */
  public function newServiceAction(Request $request)
  {
    $ente = $this->instanceService->getCurrentInstance();
    // Il default è formio
    $type = $request->query->get('type', Servizio::TYPES[Servizio::FORMIO_SERVICE_CLASS]);
    $servizio = Servizio::createService($type);


    // Todo: ok metodo statico, creare una factory
    $servizio->setEnte($ente);
    $category = $this->entityManager->getRepository(Categoria::class)->findOneBy([], ['name' => 'ASC']);
    if ($category instanceof Categoria) {
      $servizio->setTopics($category);
    }

    $defaultFeedbackMessages = $this->serviceManager->getDefaultFeedbackMessages();
    $translationsRepo = $this->entityManager->getRepository('Gedmo\Translatable\Entity\Translation');
    foreach ($this->locales as $locale) {
      $translationsRepo->translate($servizio, "feedbackMessages", $locale, $defaultFeedbackMessages[$locale]);
    }

    $this->serviceManager->save($servizio);

    return $this->redirectToRoute('admin_servizio_edit', ['id' => $servizio->getId()]);
  }

  /**
   * @Route("/servizio/{servizio}/custom-validation", name="admin_servizio_custom_validation")
   * @ParamConverter("servizio", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $servizio
   *
   * @return Response
   */
  public function editCustomValidationServizioAction(Request $request, Servizio $servizio): Response
  {
    $user = $this->getUser();

    $schema = $this->schemaFactory->createFromService($servizio);

    $form = $this->createFormBuilder(null)->add(
      "post_submit_validation_expression",
      TextareaType::class,
      [
        "label" => $this->translator->trans('servizio.validate_submit'),
        'required' => false,
        'data' => $servizio->getPostSubmitValidationExpression(),
      ]
    )->add(
      "post_submit_validation_message",
      TextType::class,
      [
        "label" => $this->translator->trans('servizio.validate_error_service'),
        'required' => false,
        'data' => $servizio->getPostSubmitValidationMessage(),
      ]
    )->add(
      'Save',
      SubmitType::class
    )->getForm()->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $data = $form->getData();
      $servizio->setPostSubmitValidationExpression($data['post_submit_validation_expression']);
      $servizio->setPostSubmitValidationMessage($data['post_submit_validation_message']);
      $this->serviceManager->save($servizio);
      $this->addFlash('feedback', $this->translator->trans('servizio.validate_service'));

      return $this->redirectToRoute('admin_servizio_custom_validation', ['servizio' => $servizio->getId()]);
    }

    return $this->render('Admin/editCustomValidationServizio.html.twig', [
      'form' => $form->createView(),
      'servizio' => $servizio,
      'user' => $user,
      'schema' => $schema,
      'statuses' => Pratica::getStatuses(),
    ]);
  }

  /**
   * Deletes a service entity.
   * @Route("/servizio/{id}/delete", name="admin_servizio_delete", methods={"GET"})
   */
  public function deleteServiceAction(Request $request, Servizio $servizio): RedirectResponse
  {

    try {

      /*$applicationsRepo = $this->entityManager->getRepository(Pratica::class);
      $applications = $applicationsRepo->findBy(['servizio' => $servizio]);

      foreach ($applications as $a) {
        $this->entityManager->remove($a);
      }*/

      $this->serviceManager->delete($servizio);
      $this->addFlash('feedback', $this->translator->trans('servizio.service_successfully_deleted'));
    } catch (ForeignKeyConstraintViolationException $exception) {
      $this->addFlash('warning', $this->translator->trans('servizio.impossible_delete_service'));
    } catch (\Exception $exception) {
      $this->addFlash('warning', $this->translator->trans('generic_error'));
    }
    return $this->redirectToRoute('admin_servizio_index');
  }

  /**
   * @Route("/servizio/{servizio}/schema", name="admin_servizio_schema_edit")
   * @ParamConverter("servizio", class="App\Entity\Servizio")
   * @param Request $request
   * @return JsonResponse
   */
  public function formioValidateAction(Request $request): JsonResponse
  {
    $data = $request->get('schema');
    if (empty($data)) {
      return JsonResponse::create("Parameter schema is mandatory", Response::HTTP_BAD_REQUEST);
    }

    $schema = \json_decode($data, true);
    try {
      $response = $this->formServer->editForm($schema);
      return JsonResponse::create($response, Response::HTTP_OK);
    } catch (\Exception $exception) {
      return JsonResponse::create($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * @Route("/io-test", name="test_io", methods={"POST"})
   * @param Request $request
   *
   * @return JsonResponse
   */
  public function testIo(Request $request): JsonResponse
  {
    $serviceId = $request->get('service_id');
    $primaryKey = $request->get('primary_key');
    $secondaryKey = $request->get('secondary_key');
    $fiscalCode = $request->get('fiscal_code');

    if (!($serviceId && $primaryKey && $fiscalCode)) {
      return new JsonResponse(
        ["error" => $this->translator->trans('app_io.errore.parametro_mancante')],
        Response::HTTP_BAD_REQUEST
      );
    }

    $response = $this->ioService->test($serviceId, $primaryKey, $secondaryKey, $fiscalCode);
    if (array_key_exists('error', $response)) {
      return new JsonResponse($response, Response::HTTP_BAD_REQUEST);
    } else {
      return new JsonResponse($response, Response::HTTP_OK);
    }
  }

  /**
   * @Route("/servizio/{servizio}/attachments/{attachmentType}/{filename}", name="admin_delete_service_attachment", methods={"DELETE"})
   * @ParamConverter("servizio", class="App\Entity\Servizio")
   * @param Request $request
   * @param Servizio $servizio
   * @param string $attachmentType
   * @param string $filename
   * @return JsonResponse
   */
  public function deletePublicAttachmentAction(
    Request  $request,
    Servizio $servizio,
    string   $attachmentType,
    string   $filename
  ): JsonResponse
  {
    if (!in_array($attachmentType, [PublicFile::CONDITIONS_TYPE, PublicFile::COSTS_TYPE])) {
      $this->logger->error("Invalid type $attachmentType");

      return new JsonResponse(["Invalid type: $attachmentType is not supported"], Response::HTTP_BAD_REQUEST);
    }

    if ($attachmentType === PublicFile::CONDITIONS_TYPE) {
      $attachment = $servizio->getConditionAttachmentByName($filename);
    } elseif ($attachmentType === PublicFile::COSTS_TYPE) {
      $attachment = $servizio->getCostAttachmentByName($filename);
    } else {
      $attachment = null;
    }

    if (!$attachment) {
      return new JsonResponse(["Attachment $filename does not exists"], Response::HTTP_NOT_FOUND);
    }

    try {
      $this->fileService->deleteFilename($attachment->getName(), $servizio, $attachment->getType());
    } catch (FileNotFoundException $e) {
      $this->logger->error("Unable to delete $filename: file not found");
    }

    if ($attachmentType === PublicFile::CONDITIONS_TYPE) {
      $servizio->removeConditionsAttachment($attachment);
    } elseif ($attachmentType === PublicFile::COSTS_TYPE) {
      $servizio->removeCostsAttachment($attachment);
    }

    $this->serviceManager->save($servizio);

    return new JsonResponse(["$filename deleted successfully"], Response::HTTP_OK);
  }
}
