<?php

namespace App\Controller\Ui\Backend;

use App\Entity\Calendar;
use App\Entity\NotificationSetting;
use App\Entity\OperatoreUser;
use App\Services\Manager\NotificationSettingsManager;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserNotificationSettingController
 */
class NotificationSettingController extends AbstractController
{

  private EntityManagerInterface $entityManager;
  private NotificationSettingsManager $notificationManager;


  public function __construct(EntityManagerInterface $entityManager, NotificationSettingsManager $notificationManager)
  {
    $this->entityManager = $entityManager;
    $this->notificationManager = $notificationManager;
  }


  /**
   * @Route("/operatori/notification/settings", name="operator_notification_settings", methods={"GET", "POST"})
   */
  public function notificationSettings(Request $request): Response
  {
    // Per adesso questa funzionalità è limitata al solo operatore
    /** @var OperatoreUser $user */
    $user = $this->getUser();
    $calendars = $this->entityManager->getRepository(Calendar::class)->findByModerator($user);

    if ($request->request->has('submit')) {

      // Rimuovo tutti le preferenze sulle notifiche dell'operatore
      if ($user->getNotificationSettings()->count() > 0) {
        foreach ($user->getNotificationSettings() as $notificationSetting) {
          $user->removeNotificationSetting($notificationSetting);
        }
        $this->entityManager->persist($user);
      }

      $subscribedCalendars = $request->get('calendars', []);
      foreach ($calendars as $calendar) {
        $tempNotificationSetting = new NotificationSetting();
        $tempNotificationSetting
          ->setOwner($user)
          ->setObjectType(NotificationSetting::NOTIFICATION_SETTING_CALENDAR_TYPE)
          ->setObjectId($calendar->getId());

        // Per ogni calendario a cui può accedere l'operatore salvo la preferenza sulle notifiche
        // Per retrocompatibilità e per gestire nuove associazioni l'assenza di preferenza viene considerata come spedizione Abolitata
        if (in_array($calendar->getId(), $subscribedCalendars)) {
          $tempNotificationSetting->setEmailNotification(true);
        } else {
          $tempNotificationSetting->setEmailNotification(false);
        }
        $user->addNotificationSetting($tempNotificationSetting);
      }

      // Todo: da verficare, il comportamento con gli * non convince molto
      /*if (empty($subscribedCalendars)) {
        $tempNotificationSetting = new NotificationSetting();
        $tempNotificationSetting
          ->setOwner($user)
          ->setObjectType(NotificationSetting::NOTIFICATION_SETTING_ALL_TYPE)
          ->setObjectId('*')
          ->setEmailNotification(false);
        $user->addNotificationSetting($tempNotificationSetting);
      }*/

      $this->entityManager->persist($user);
      $this->entityManager->flush();

      return $this->redirectToRoute('operator_notification_settings');
    }

    $notificationSubscriptions = $this->notificationManager->getNotificationSettingsObjectIds($user);
    return $this->render('Notification/notification_settings.twig', [
      'user'  => $user,
      'calendars' => $calendars,
      'notification_subscriptions' => $notificationSubscriptions,
    ]);
  }

}
