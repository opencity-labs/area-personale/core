<?php


namespace App\Controller\Ui\Backend;


use App\Entity\AdminUser;
use App\Entity\OperatoreUser;
use App\Entity\User;
use App\Form\Security\NewPasswordType;
use App\Form\Security\PasswordRequestType;
use App\Security\BackendOAuthAuthenticator;
use App\Security\OAuth\BackendConfigurationProviderFactory;
use App\Services\InstanceService;
use App\Services\MailerService;
use App\Services\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\ChangePasswordType;

/**
 * Class SecurityController
 * @Route("/operatori")
 */
class SecurityController extends AbstractController
{
  use TargetPathTrait;

  private MailerService $mailer;
  private ParameterBagInterface $params;
  private InstanceService $instanceService;
  private RouterInterface $router;
  private UserManager $userManager;
  private TranslatorInterface $translator;
  private LoggerInterface $logger;

  /**
   * SecurityController constructor.
   * @param MailerService $mailer
   * @param ParameterBagInterface $params
   * @param InstanceService $instanceService
   * @param RouterInterface $router
   * @param UserManager $userManager
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   */
  public function __construct(
    MailerService         $mailer,
    ParameterBagInterface $params,
    InstanceService       $instanceService,
    RouterInterface       $router,
    UserManager           $userManager,
    TranslatorInterface   $translator,
    LoggerInterface       $logger
  )
  {
    $this->mailer = $mailer;
    $this->params = $params;
    $this->instanceService = $instanceService;
    $this->router = $router;
    $this->userManager = $userManager;
    $this->translator = $translator;
    $this->logger = $logger;
  }

  /**
   * @Route("/login", name="security_login")
   * @param Request $request
   * @param Security $security
   * @param AuthenticationUtils $helper
   * @param BackendOAuthAuthenticator $backendOAuthAuthenticator
   * @param BackendConfigurationProviderFactory $backendConfigurationProviderFactory
   * @return Response
   */
  public function login(Request $request, Security $security, AuthenticationUtils $helper, BackendOAuthAuthenticator $backendOAuthAuthenticator, BackendConfigurationProviderFactory $backendConfigurationProviderFactory): Response
  {
    // if user is already logged in, don't display the login page again
    if ($security->isGranted('ROLE_ADMIN')) {

      $path = $this->getTargetPath($request->getSession(), "backend");

      $checkRedirectOperatore = null;
      preg_match_all("/\/operatori\/$/m", $path, $checkRedirectOperatore, PREG_SET_ORDER);


      if (!empty($path) && $checkRedirectOperatore === null) {
        $this->removeTargetPath($request->getSession(), "backend");
        return $this->redirect($path);
      }

      return $this->redirectToRoute('admin_index');
    }

    if ($security->isGranted('ROLE_OPERATORE')) {

      $path = $this->getTargetPath($request->getSession(), "backend");
      if (!empty($path)) {
        $this->removeTargetPath($request->getSession(), "backend");
        return $this->redirect($path);
      }

      return $this->redirectToRoute('operatori_index');
    }

    if ($request->query->get('oauth', false)) {
      return $backendOAuthAuthenticator->start($request);
    }

    $isEnabledBackendOauth = $this->getParameter('enable_backend_oauth');

    return $this->render(
      'Security/login.html.twig',
      [
        'last_username' => $helper->getLastUsername(),
        'is_ebabled_backend_oauth' => !empty($this->getParameter('backend_oauth')),
        'provider' => $isEnabledBackendOauth ? $backendConfigurationProviderFactory->instanceProvider() : null,
        'error' => $helper->getLastAuthenticationError(),
      ]
    );
  }

  /**
   * @Route("/reset-password", name="reset_password", methods={"GET", "POST"})
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @return RedirectResponse|Response
   * @throws Exception
   */
  public function resetPassword(Request $request, EntityManagerInterface $entityManager)
  {

    $form = $this->createForm(PasswordRequestType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $email = $form->get('email')->getData();

      // Limito la ricerca a operatori e admin
      $sql = "SELECT id FROM utente WHERE utente.type <> 'cps' AND utente.system_user = false AND utente.enabled = true AND (utente.email = :email OR utente.username = :username )";
      $stmt = $this->getDoctrine()->getConnection()->prepare($sql);
      $stmt->bindValue('email', $email);
      $stmt->bindValue('username', $email);
      $userIds = $stmt->executeQuery()->fetchAllAssociative();

      $users = [];
      foreach ($userIds as $userId) {
        $users []= $entityManager->getRepository(User::class)->find($userId);
      }

      // e-mail not found
      if (empty($users)) {
        $this->logger->error("Reset password failed: email {$email} not found");
      } else if (count($users) === 1) {
        $this->userManager->resetPassword($users[0]);
      } else {
        $this->userManager->resetPasswordRequest($users, $email, $request->getLocale());
      }

      $this->addFlash('info', $this->translator->trans('security.reset_message'));
      return $this->redirectToRoute('security_login');
    }

    return $this->render('Security/reset-password.html.twig', ['form' => $form->createView()]);
  }

  /**
   * @Route("/change-password", name="security_change_password", methods={"GET", "POST"})
   *
   * @param Request $request
   * @param TokenStorageInterface $tokenStorage
   * @param SessionInterface $session
   *
   * @return RedirectResponse|Response
   */
  public function changePassword(Request $request, TokenStorageInterface $tokenStorage, SessionInterface $session)
  {
    /** @var User $user */
    $user = $this->getUser();
    $form = $this->createForm(ChangePasswordType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $plainPassword = $form->get('plainPassword')->get('plainPassword')->getData();
      $this->userManager->changePassword($user, $plainPassword);

      $token = new UsernamePasswordToken($user, $user->getPassword(), 'main');
      $tokenStorage->setToken($token);
      $session->set('_security_main', serialize($token));

      $this->addFlash('success', $this->translator->trans('security.changed_password'));
      return $this->redirectToRoute('security_profile');
    }

    return $this->render(
      'Security/change_password.twig',
      array(
        'user' => $this->getUser(),
        'form' => $form->createView(),
      )
    );
  }

  /**
   * @Route("/reset-operator-password/{user}", name="security_reset_operator_password", methods={"GET", "POST"})
   *
   * @param Request $request
   * @param User $user
   * @return RedirectResponse|Response
   */
  public function resetOperatorPassword(Request $request, User $user)
  {
    $currentUser = $this->getUser();
    $this->userManager->resetPassword($user, $currentUser);
    $this->logger->info("Password resetted for user {$user->getUsername()} from user {$currentUser->getUsername()}");

    if ($user instanceof OperatoreUser) {
      if ($user->isSystemUser()) {
        $this->addFlash('success', $this->translator->trans('security.check_email_reset_password'));
      } else {
        $this->addFlash('success', $this->translator->trans('security.reset_password_requested', ['%fullname%' => $user->getFullName()]));
      }
      return $this->redirectToRoute('admin_operatore_index');
    } else {
      $this->addFlash('success', $this->translator->trans('security.reset_password_requested', ['%fullname%' => $user->getFullName()]));
      return $this->redirectToRoute('admin_administrator_users_index');
    }
  }

  /**
   * @Route("/reset-password/confirm/{token}", name="reset_password_confirm", methods={"GET", "POST"})
   *
   * @param Request $request
   * @param string $token
   * @param EntityManagerInterface $entityManager
   * @param UserPasswordHasherInterface $passwordHasher
   * @param TokenStorageInterface $tokenStorage
   * @param SessionInterface $session
   *
   * @return RedirectResponse|Response
   */
  public function resetPasswordCheck(Request $request, string $token, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher, TokenStorageInterface $tokenStorage, SessionInterface $session)
  {
    $user = $this->getUser();
    $resetPasswordUser = $entityManager->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);

    if (!$token || !$resetPasswordUser instanceof User) {
      $this->addFlash('danger', $this->translator->trans('security.invalid_reset_link'));

      return $this->redirectToRoute('reset_password');
    }

    $form = $this->createForm(NewPasswordType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $plainPassword = $form->get('plainPassword')->getData();
      $password = $passwordHasher->hashPassword($resetPasswordUser, $plainPassword);
      $resetPasswordUser->setPassword($password);
      $resetPasswordUser->setConfirmationToken(null);
      $resetPasswordUser->setLastChangePassword(new \DateTime());
      $entityManager->flush();

      if ($user instanceof AdminUser && ($resetPasswordUser instanceof OperatoreUser && $resetPasswordUser->isSystemUser())) {
        $this->addFlash('success', $this->translator->trans('security.changed_password'));
        return $this->redirectToRoute('admin_operatore_index');
      } else {
        $token = new UsernamePasswordToken($resetPasswordUser, $password, 'main');
        $tokenStorage->setToken($token);
        $session->set('_security_main', serialize($token));

        $this->addFlash('success', $this->translator->trans('security.changed_password_login'));
        return $this->redirectToRoute('security_login');
      }
    }

    return $this->render(
      'Security/reset-password-confirm.html.twig',
      ['form' => $form->createView()]
    );

  }

  /**
   * @Route("/profile", name="security_profile")
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @return Response
   */
  public function profile(Request $request, EntityManagerInterface $entityManager): Response
  {

    $user = $this->getUser();
    $form = $this->createForm('App\Form\ProfileUserType', $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      $this->userManager->save($user);
      return $this->redirectToRoute('security_profile');
    }

    return $this->render(
      'Security/profile.twig',
      array(
        'user' => $this->getUser(),
        'form' => $form->createView(),
      )
    );
  }

  /**
   * @Route("/profile/edit", name="security_edit_profile")
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @return Response
   */
  public function editProfile(Request $request, EntityManagerInterface $entityManager): Response
  {

    $user = $this->getUser();
    $form = $this->createForm('App\Form\ProfileUserType', $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->userManager->save($user);
      return $this->redirectToRoute('security_profile');
    }

    return $this->render(
      'Security/edit.twig',
      array(
        'user' => $this->getUser(),
        'form' => $form->createView(),
      )
    );
  }

  /**
   * @Route("/feedback", name="security_feedback")
   * @param Request $request
   * @return Response
   */
  public function feedback(Request $request): Response
  {
    $status = $request->query->get('status', null);
    $msg = $request->query->get('msg', null);

    return $this->render(
      'Security/feedback.html.twig',
      [
        'status' => $status,
        'msg' => $msg,
      ]
    );
  }

  /**
   * This is the route the user can use to logout.
   *
   * But, this will never be executed. Symfony will intercept this first
   * and handle the logout automatically. See logout in config/packages/security.yaml
   *
   * @Route("/logout", name="security_logout")
   * @throws Exception
   */
  public function logout(): void
  {
    throw new Exception('This should never be reached!');
  }
}
