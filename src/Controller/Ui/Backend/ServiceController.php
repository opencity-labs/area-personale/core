<?php

namespace App\Controller\Ui\Backend;

use App\DataTable\ServiceTableType;
use App\Entity\User;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ServiceController extends AbstractController
{
  private DataTableFactory $dataTableFactory;

  /**
   * @param DataTableFactory $dataTableFactory
   */
  public function __construct(DataTableFactory $dataTableFactory)
  {
    $this->dataTableFactory = $dataTableFactory;
  }

  /**
   * Lists all services entities.
   * @Route("/operatori/services", name="backend_services_index", methods={"GET", "POST"})
   *
   */
  public function indexServicesAjxAction(Request $request): Response
  {
    /** @var User $user */
    $user = $this->getUser();

    $table = $this->dataTableFactory->createFromType(ServiceTableType::class, [
      'user' => $user
    ])
      ->handleRequest($request);

    if ($table->isCallback()) {
      return $table->getResponse();
    }

    return $this->render('Operatori/indexServices.html.twig', [
      'user' => $this->getUser(),
      'datatable' => $table
    ]);

  }



}
