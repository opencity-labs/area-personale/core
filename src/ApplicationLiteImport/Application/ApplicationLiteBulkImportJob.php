<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application;

use App\ApplicationLiteImport\Application\ImportCommand\ImportBulkCommand;
use App\ApplicationLiteImport\Application\ImportCommand\ImportCommand;
use App\ApplicationLiteImport\Application\ImportCommand\ImportData;
use App\Entity\CPSUser;
use App\Entity\Job;
use App\Handlers\Job\JobHandlerInterface;
use App\Services\FileService\CommonFileService;
use App\Services\InstanceService;
use App\Services\JobService;
use App\Utils\UploadedFileFactory;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use League\Flysystem\FileNotFoundException;
use Throwable;

class ApplicationLiteBulkImportJob implements JobHandlerInterface
{
  private const DEFAULT_STEP_SIZE = 1000;

  private ApplicationLiteImportService $applicationLiteImportService;
  private CommonFileService $fileService;
  private EntityManagerInterface $entityManager;
  private InstanceService $instanceService;
  private JobService $jobService;
  private SerializerInterface $serializer;

  public function __construct(
    ApplicationLiteImportService $applicationLiteImportService,
    CommonFileService $fileService,
    EntityManagerInterface $entityManager,
    InstanceService $instanceService,
    JobService $jobService,
    SerializerInterface $serializer
  ) {
    $this->applicationLiteImportService = $applicationLiteImportService;
    $this->fileService = $fileService;
    $this->instanceService = $instanceService;
    $this->serializer = $serializer;
    $this->jobService = $jobService;
    $this->entityManager = $entityManager;
  }

  public function createJob(ImportBulkCommand $command): Job
  {
    $job = Job::new(
      $command->ente,
      'applicationlite.bulk_import_job',
    );

    $this->setStepSize($job);

    $serializedData = $this->serializer->serialize(
      $command->importData,
      'json',
      null,
      'array<App\ApplicationLiteImport\Application\ImportCommand\ImportData>',
    );

    $file = UploadedFileFactory::fromJsonString($serializedData);

    $job->setFile($file);

    $this->entityManager->persist($job);
    $this->entityManager->flush();

    $this->jobService->processJobAsync($job);

    return $job;
  }

  public function processJob(Job $job): void
  {
    try {

      $dataToImport = $this->getDataToImport($job);

    } catch (FileNotFoundException $e) {
      $job->failed($e->getMessage(), new DateTime());

      return;
    }

    $ente = $this->instanceService->getCurrentInstance();
    $currentRow = $this->getProcessedRow($job);
    $stepSize = $this->getStepSize($job);

    $elementForThisJob = min(count($dataToImport), $currentRow + $stepSize);

    try {

      while ($elementForThisJob > $currentRow) {

        $importData = $dataToImport[$currentRow];
        $importCommand = new ImportCommand();
        $importCommand->importData = $importData;
        $importCommand->ente = $ente;
        $importCommand->requester = new CPSUser();

        $importResult = $this->applicationLiteImportService->import($importCommand);
        if (!$importResult->isCompletedWithoutError()) {
          $this->addError($job, $currentRow, $importResult->getError());
        }

        ++$currentRow;
        $this->updateCurrentRow($job, $currentRow);
      }

    } catch (Throwable $e) {
      $job->finishedWithError('Job process terminated with errors', new DateTime());
      $this->saveJobCurrentState($job);

      return;
    }

    if (count($dataToImport) > $currentRow) {
      $job->suspend();
    } elseif (empty($job->getErrors())) {
      $job->finished(
        'Job process finished successfully',
        new DateTime(),
      );
    } else {
      $job->finishedWithError(
        'Job process terminated with errors',
        new DateTime(),
      );
    }

    $this->saveJobCurrentState($job);
  }

  /**
   * @param Job $job
   * @return ImportData[]
   * @throws FileNotFoundException
   */
  private function getDataToImport(Job $job): array
  {
    $fileContent = $this->fileService->getFileContent($job->getFile());

    /** @var $dataToImport ImportData[] */
    $dataToImport = $this->serializer->deserialize(
      $fileContent,
      'array<App\ApplicationLiteImport\Application\ImportCommand\ImportData>',
      'json',
    );

    return $dataToImport;
  }

  public function validateJob(Job $job): void {}

  private function getProcessedRow(Job $job)
  {
    $currentRow = 0;
    if (!empty($job->getMetadata()['current_row'])) {
      $currentRow = $job->getMetadata()['current_row'];
    }

    return $currentRow;
  }

  private function updateCurrentRow(Job $job, $rowNumber): void
  {
    $metadata = $job->getMetadata();
    if ($metadata === null) {
      $metadata = [];
    }
    $metadata['current_row'] = $rowNumber;
    $job->setMetadata($metadata);
  }

  private function getStepSize(Job $job)
  {
    $metadata = $job->getMetadata();
    if (empty($metadata['step_size'])) {
      return self::DEFAULT_STEP_SIZE;
    }

    return $metadata['step_size'];
  }

  private function setStepSize(Job $job)
  {
    $metadata = $job->getMetadata();
    if ($metadata === null) {
      $metadata = [];
    }
    $metadata['step_size'] = self::DEFAULT_STEP_SIZE;

    $job->setMetadata($metadata);
  }

  private function addError(Job $job, $currentRow, $error): void
  {
    $errorBag = $job->getErrors();
    $errorBag[] =
      [
        'index' => $currentRow,
        'error' => $error,
      ];
    $job->setErrors($errorBag);

    $this->saveJobCurrentState($job);
  }

  private function saveJobCurrentState(Job $job): void
  {
    $this->entityManager->persist($job);
    $this->entityManager->flush();
  }
}
