<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application;

use App\ApplicationLiteImport\Application\ImportCommand\ImportCommand;
use App\ApplicationLiteImport\Domain\ApplicationImportResult;
use App\Applications\Application\ApplicationLiteService;
use App\Applications\Application\DTOs\ServiceData;
use App\Applications\Exceptions\ApplicationLiteException;
use App\Applications\Exceptions\DomainException;
use App\Entity\ApplicationLite;
use App\Entity\Servizio;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

// TODO come gestisco la provenienza della pratica? mi baso sull'utente che ha inoltrato la richiesta?

class ApplicationLiteImportService
{
  private ApplicationLiteService $applicationLite;
  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;

  public function __construct(
    ApplicationLiteService $applicationLite,
    EntityManagerInterface $entityManager,
    LoggerInterface $logger
  ) {
    $this->applicationLite = $applicationLite;
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  // TODO Saverio: aggiungere test funzionale
  public function import(ImportCommand $command): ApplicationImportResult
  {
    $result = new ApplicationImportResult();
    $applicationData = $command->importData->application;

    try {
      $service = $this->processService($applicationData->service);
      $application = $this->processApplication($command, $service);

      $result->applicationLiteImportedWithSuccess($application);

      // TODO aggiorna o crea documento
      // TODO aggiorna o crea pagamento
    } catch (DomainException $e) {
      $this->logger->error($e->getMessage());
      $result->reportDomainException($e);
    }

    return $result;
  }

  /** @throws DomainException */
  private function processService(ServiceData $serviceData): Servizio
  {
    $serviceRepo = $this->entityManager->getRepository(Servizio::class);

    $service = $serviceRepo->find($serviceData->id);

    if (null === $service) {
      throw new DomainException(sprintf("Unable to find service with id '%s'", $serviceData->id));
    }

    return $service;
  }

  /** @throws ApplicationLiteException */
  private function processApplication(ImportCommand $command, Servizio $service): ApplicationLite
  {
    $applicationData = $command->importData->application;
    $ente = $command->ente;

    return $this->applicationLite->createOrUpdate($applicationData, $service, $ente);
  }
}
