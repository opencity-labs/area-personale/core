<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Application\ImportCommand;

class PaymentData
{
  public string $iud;
  public string $iuv;
  public float $amount;
}
