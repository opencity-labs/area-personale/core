<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Infrastructure;

use JsonSchema\Validator;
use RuntimeException;

class InvalidJsonException extends RuntimeException
{
  public static function becauseThereAreUnmetConstraints(Validator $unmetConstrains): InvalidJsonException
  {
    $errors = $unmetConstrains->getErrors();

    $message = 'The provided JSON is not valid:';
    foreach ($errors as $error) {
      $message .= sprintf(' %s (%s)', $error['message'], $error['property']);
    }

    return new self($message);
  }
}
