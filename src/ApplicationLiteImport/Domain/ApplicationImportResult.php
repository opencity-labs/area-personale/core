<?php

declare(strict_types=1);

namespace App\ApplicationLiteImport\Domain;

use App\Applications\Exceptions\DomainException;
use App\Entity\ApplicationLite;

class ApplicationImportResult
{
  private ?ApplicationLite $applicationLite = null;
  private ?string $error = null;

  public function applicationLiteImportedWithSuccess(ApplicationLite $applicationLite): void
  {
    $this->applicationLite = $applicationLite;
  }

  public function reportDomainException(DomainException $e): void
  {
    $this->error = $e->getMessage();
  }

  public function isCompletedWithoutError(): bool
  {
    return null === $this->error;
  }

  public function getError(): string
  {
    return $this->error ?? '';
  }

  public function applicationLiteImported(): ?ApplicationLite
  {
    return $this->applicationLite;
  }
}
