<?php

namespace App\Form;


use App\Services\InstanceService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IOServiceParametersType extends AbstractType
{
  private InstanceService $instanceService;

  public function __construct(InstanceService $instanceService)
  {
    $this->instanceService = $instanceService;
  }
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $readOnly = !$this->instanceService->getCurrentInstance()->isIOEnabled();

    $builder
      ->add('IOserviceId', TextType::class, [
        'label' => 'app_io.service_id',
        'required' => false,
        'disabled' => $readOnly
      ])
      ->add('primaryKey', TextType::class, [
        'label' => 'app_io.primary_key',
        'required' => false,
        'disabled' => $readOnly
      ])
      ->add('secondaryKey', TextType::class, [
        'label' => 'app_io.secondary_key',
        'required' => false,
        'disabled' => $readOnly
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array());
  }
}
