<?php

namespace App\Form\Operatore\Base;

use App\Dto\ApplicationOutcome;
use App\Entity\Pratica;
use App\Form\Admin\Servizio\PaymentDataType;
use App\Form\Base\StampType;
use App\Services\Manager\ServiceManager;
use Doctrine\ORM\EntityManagerInterface;
use Flagception\Manager\FeatureManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ApplicationOutcomeType extends AbstractType
{

  private EntityManagerInterface $entityManager;

  private ServiceManager $serviceManager;
  private FeatureManagerInterface $featureManager;

  /**
   * ApplicationOutcomeType constructor.
   * @param EntityManagerInterface $entityManager
   * @param ServiceManager $serviceManager
   * @param FeatureManagerInterface $featureManager
   */
  public function __construct(EntityManagerInterface $entityManager, ServiceManager $serviceManager, FeatureManagerInterface $featureManager)
  {
    $this->entityManager = $entityManager;
    $this->serviceManager = $serviceManager;
    $this->featureManager = $featureManager;
  }

  public function buildForm(FormBuilderInterface $builder, array $options): void
  {

    $builder->add("outcome", ChoiceType::class,
      [
        "label" => false,
        "required" => true,
        "expanded" => true,
        "multiple" => false,
        "choices" => [
          "Approva" => true,
          "Rigetta" => false,
        ],
      ]
    )->add("message", TextareaType::class,
      [
        "label" => 'operatori.flow.approva_o_rigetta.motivazione.esito_label',
        "required" => false,
        'purify_html' => true,
        'attr' => ['rows' => 5, 'class' => 'simple-editor'],
        'help' => 'operatori.flow.approva_o_rigetta.motivazione.esito_help'
      ]
    )->add("mainAttachment", HiddenType::class,
      [
        "label" => false,
        "required" => false,
      ]
    )->add("attachments", HiddenType::class,
      [
        "label" => false,
        "required" => false,
      ]
    );

    /** @var ApplicationOutcome $outcome */
    $outcome = $builder->getData();
    $repo = $this->entityManager->getRepository(Pratica::class);
    /** @var Pratica $application */
    $application = $repo->find($outcome->getApplicationId());

    if ($this->featureManager->isActive('feature_multiple_payments')) {
      $builder->add('paymentConfigs', HiddenType::class, [
        "label" => false,
        "data" => json_encode($application->getServizio()->getReleasePaymentConfigsAsArray()),
        "required" => false,
      ]);
    } else {

      if ($application->getServizio()->needsPayments()) {
        $builder->add(PaymentDataType::PAYMENT_AMOUNT, MoneyType::class, [
          'required' => false,
          'data' => $application->getServizio()->isPaymentDeferred() ? $application->getPaymentAmount() : 0,
          'empty_data' => 0,
          'attr' => ['class' => 'form-control-sm'],
          'label' => 'operatori.flow.approva_o_rigetta.importo_da_pagare'
        ]);
      }

      if (!$this->serviceManager->hasGatewayWithDigitalStamps($application->getServizio())) {
        $builder->add('stamps', CollectionType::class, [
          'label' => false,
          'data' => $application->getServizio()->getReleaseStamps(),
          'entry_type' => StampType::class,
          'entry_options' => ['view' => StampType::VIEW_BACKEND],
          'allow_add' => true,
          'allow_delete' => true
        ]);
      }
    }

  }

  public function getBlockPrefix(): string
  {
    return 'outcome';
  }
}
