<?php


namespace App\Form\Admin\Ente;

use App\Entity\Pratica;
use App\Entity\Webhook;
use App\Services\InstanceService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebhookType extends AbstractType
{

  private InstanceService $instanceService;

  public function __construct(InstanceService $instanceService)
  {
    $this->instanceService = $instanceService;
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {

    $methods = [
      'POST' => 'POST'
    ];

    $servizi = $this->instanceService->getServices();
    $serviceChoices = [];
    $serviceChoices['Tutti'] = 'all';
    foreach ($servizi as $s) {
      $serviceChoices[$s->getName()] = $s->getId();
    }

    /** @var Webhook $webHook */
    $webHook = $builder->getData();
    if ($webHook->getVersion() === null) {
      $versions = Webhook::VERSIONS;
      $webHook->setVersion(end($versions));
    }

    $builder
      ->add('title', TextType::class, [
        'label' => 'webhook.nome',
        'required' => true
      ])
      ->add('endpoint', UrlType::class, [
        'label' => 'webhook.endpoint',
        'required' => true
      ])
      ->add('method', ChoiceType::class, [
        'label' => 'webhook.method',
        'choices' => $methods,
        'mapped' => false
      ])
      ->add('trigger', ChoiceType::class, [
        'label' => 'webhook.trigger',
        'choices' => array_flip(Webhook::TRIGGERS),
      ])
      ->add('filters', ChoiceType::class, [
        'choices' => $serviceChoices,
        'expanded' => true,
        'multiple' => true,
        'required' => true,
        'label' => 'webhook.filters_label'
      ])
      ->add('headers', TextareaType::class, [
        'label' => 'webhook.headers',
        'required' => false
      ])
      ->add('version', ChoiceType::class, [
        'label' => 'webhook.version',
        'choices' => Webhook::VERSIONS,
      ])
      ->add('active', CheckboxType::class, [
        'label' =>  'webhook.active' ,
        'required' => false
      ])
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => Webhook::class
    ));
  }
}
