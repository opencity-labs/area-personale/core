<?php


namespace App\Form\Admin\Servizio;


use App\Model\StampSettings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class StampSettingsType extends AbstractType
{
  private TranslatorInterface $translator;

  public function __construct(TranslatorInterface $translator)
  {
    $this->translator = $translator;
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('reason', TextType::class, [
        'label' => 'servizio.stamps.reason',
        'required' => true,
        'attr' => ['class' => 'reason'],
      ])
      ->add('identifier', TextType::class, [
        'label' => 'servizio.stamps.identifier',
        'required' => true,
        'attr' => ['class' => 'identifier'],
      ])
      ->add('amount', MoneyType::class, [
        'label' => 'servizio.stamps.amount',
        'required' => true,
      ])
      ->add('phase', ChoiceType::class, [
        'label' => 'servizio.stamps.phase.label',
        'choices' => [
          $this->translator->trans('servizio.stamps.phase.data.request') => StampSettings::PHASE_REQUEST,
          $this->translator->trans('servizio.stamps.phase.data.release') => StampSettings::PHASE_RELEASE
        ],
        'required' => true,
      ])
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => StampSettings::class,
      'csrf_protection' => false
    ));
  }
}
