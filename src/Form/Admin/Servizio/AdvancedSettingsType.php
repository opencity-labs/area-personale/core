<?php


namespace App\Form\Admin\Servizio;

use App\Form\I18n\AbstractI18nType;
use App\Form\I18n\I18nDataMapperInterface;
use App\Form\I18n\I18nTextareaType;
use App\Form\I18n\I18nTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Servizio;

class AdvancedSettingsType extends AbstractI18nType
{
  private TranslatorInterface $translator;

  /**
   * @param I18nDataMapperInterface $dataMapper
   * @param $locale
   * @param $locales
   * @param TranslatorInterface $translator
   */
  public function __construct(I18nDataMapperInterface $dataMapper, $locale, $locales, TranslatorInterface $translator)
  {
    parent::__construct($dataMapper, $locale, $locales);
    $this->translator = $translator;
  }

  public function buildForm(FormBuilderInterface $builder, array $options): void
  {

    $this->createTranslatableMapper($builder, $options)
      ->add("externalApplicationUrl", I18nTextType::class, [
        "label" => 'servizio.external_application_url.label',
        'help' => 'servizio.external_application_url.helptext',
        'help_html' => true,
        'attr' => [
          'maxlength' => 255
        ],
        'required' => false,
      ]);

    $builder
      ->add('disable_message_notifications', CheckboxType::class, [
        'label' => "messages.disable_message_notifications",
        'help' => $this->translator->trans("messages.disable_message_notifications_help") . "<p>",
        'help_html' => true,
        'required' => false
      ])
      ->add('allow_reopening', CheckboxType::class,[
        'label' => 'servizio.consenti_riapertura',
        'required' => false
      ])
    ;
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults(array(
      'data_class' => Servizio::class
    ));

    $this->configureTranslationOptions($resolver);
  }

  public function getBlockPrefix(): string
  {
    return 'advanced_settings';
  }
}
