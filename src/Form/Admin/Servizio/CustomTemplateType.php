<?php

namespace App\Form\Admin\Servizio;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomTemplateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('custom_template', \App\Form\Admin\Ente\CustomTemplateType::class, [
        'label' => false,
      ]);
  }

  public function getBlockPrefix()
  {
    return 'custom_template';
  }
}
