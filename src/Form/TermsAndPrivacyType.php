<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TermsAndPrivacyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
      $builder
        ->add('confirm_read', CheckboxType::class, [
          'label' => 'terms.confirm_read_field_label',
          'required' => true
        ])
      ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
