<?php

namespace App\Form;

use App\Entity\Meeting;
use App\Entity\OpeningHour;
use App\Form\I18n\AbstractI18nType;
use App\Form\I18n\I18nDataMapperInterface;
use App\Form\I18n\I18nTextType;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Contracts\Translation\TranslatorInterface;

class OpeningHourType extends AbstractI18nType
{
  /**
   * @param I18nDataMapperInterface $dataMapper
   * @param $locale
   * @param $locales
   * @var TranslatorInterface $translator
   */
  private TranslatorInterface $translator;
  private RequestStack $requestStack;

  public function __construct(
    I18nDataMapperInterface $dataMapper,
                            $locale,
                            $locales,
    TranslatorInterface     $translator,
    RequestStack            $requestStack
  ) {
    parent::__construct($dataMapper, $locale, $locales);
    $this->translator = $translator;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $maxDate = (new DateTime())->setTimestamp(mktime(0, 0, 0, 1, 0, date('Y') + 2));

    $this
      ->createTranslatableMapper($builder, $options)
      ->add("name", I18nTextType::class, [
        'required' => true,
        'label' => 'general.nome',
      ]);

    $builder
      ->add('start_date', DateType::class, [
        'widget' => 'single_text',
        'required' => true,
        'label' => 'calendars.opening_hours.start_date',
        'label_attr' => ['class' => 'active'],
        'constraints' => [
          new LessThan([
            'propertyPath' => 'parent.all[end_date].data',
            'message' => $this->translator->trans('calendars.opening_hours.errors.less_than_end_date'),
          ]),
        ],
      ])
      ->add('end_date', DateType::class, [
        'widget' => 'single_text',
        'required' => true,
        'label' => 'calendars.opening_hours.end_date',
        'label_attr' => ['class' => 'active'],
        'constraints' => [
          new GreaterThan([
            'propertyPath' => 'parent.all[start_date].data',
            'message' => $this->translator->trans('calendars.opening_hours.errors.greater_than_start_date'),
          ]),
          new LessThanOrEqual([
            'value' => $maxDate->format('c'),
            'message' => $this->translator->trans(
              'calendars.opening_hours.errors.end_date_limit',
              ['%max_date%' => $maxDate->format('d/m/Y')],
            ),
          ]),
        ],
      ])
      ->add('days_of_week', ChoiceType::class, [
        'label' => 'calendars.opening_hours.days_of_week',
        'required' => false,
        'choices' => OpeningHour::WEEKDAYS,
        'multiple' => true,
        'expanded' => true,
      ])
      ->add('begin_hour', TimeType::class, [
        'widget' => 'single_text',
        'required' => true,
        'label' => 'calendars.opening_hours.begin_hour',
        'constraints' => [new LessThan([
          'propertyPath' => 'parent.all[end_hour].data',
          'message' => $this->translator->trans('calendars.opening_hours.errors.less_than_end_hour')
        ]),]
      ])
      ->add('end_hour', TimeType::class, [
        'widget' => 'single_text',
        'required' => true,
        'label' => 'calendars.opening_hours.end_hour',
        'constraints' => [new GreaterThan([
          'propertyPath' => 'parent.all[begin_hour].data',
          'message' => $this->translator->trans('calendars.opening_hours.errors.greater_than_begin_hour')
        ]),]
      ])
      ->add('is_moderated', CheckboxType::class, [
        'required' => false,
        'label' => 'calendars.opening_hours.is_moderated',
      ])
      ->add('meeting_minutes', IntegerType::class, [
        'required' => true,
        'label' => 'calendars.opening_hours.meeting_minutes',
        'attr' => ['min' => '1'],
        'constraints' => [
          new Positive([],
            $this->translator->trans('calendars.opening_hours.errors.meeting_minutes_strictly_positive'),
          ),
        ],
      ])
      ->add('interval_minutes', IntegerType::class, [
        'required' => true,
        'label' => 'calendars.opening_hours.interval_minutes',
      ])
      ->add('meeting_queue', IntegerType::class, [
        'required' => true,
        'label' => 'calendars.opening_hours.meeting_queue',
        'attr' => ['min' => '1'],
        'constraints' => [
          new Positive([],
            $this->translator->trans('calendars.opening_hours.errors.meeting_queue_strictly_positive'),
          ),
        ],
      ])
      ->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
  }

  public function onPreSubmit(FormEvent $event): void
  {
    /**
     * @var OpeningHour $openingHour
     */
    $openingHour = $event->getForm()->getData();
    $data = $event->getData();

    if ($openingHour && !empty($data['interval_minutes']) && !empty($data['meeting_minutes'])) {
      // Check if duration can be changed, i.e there are no scheduled meetings (past meetings are excluded)
      $intervalChanged = $openingHour->getIntervalMinutes() != $data['interval_minutes'];
      $durationChanged = $openingHour->getMeetingMinutes() != $data['meeting_minutes'];
      if ($durationChanged || $intervalChanged) {
        $canChange = true;
        $availableOn = new DateTime();
        foreach ($openingHour->getMeetings() as $meeting) {
          if ($meeting->getFromTime() >= $availableOn &&
            !in_array($meeting->getStatus(), [Meeting::STATUS_REFUSED, Meeting::STATUS_CANCELLED])) {
            $availableOn = $meeting->getToTime();
            $canChange = false;
          }
        }
        if (!$canChange) {
          $event->getForm()->addError(
            new FormError(
              $this->translator->trans(
                'calendars.opening_hours.error.cannot_change',
                ['next_availability' => $availableOn->modify('+1days')->format('d/m/Y')],
              ),
            ),
          );
        }
      }
    }

    // Serve far convivere il payload dell'interfaccia che accetta un array con tutte le lingue e quello via API
    if (!empty($data['name']) && !is_array($data['name'])) {
      $temp [$this->requestStack->getCurrentRequest()->getLocale()] = $data['name'];
      $data['name'] = $temp;
      $event->setData($data);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => 'App\Entity\OpeningHour',
      'csrf_protection' => false,
    ]);
    $this->configureTranslationOptions($resolver);
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix()
  {
    return 'App_openinghour';
  }

}
