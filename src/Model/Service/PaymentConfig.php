<?php


namespace App\Model\Service;


use App\Model\Payment\PaymentPhase;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class PaymentConfig implements \JsonSerializable
{

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Stamp uuid", example="83ea22d2-ca5e-4b29-8efc-af23361350c4")
   * @Groups({"read", "write"})
   */
  private string $id;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Identifies the phase at which the user pays.<ul><li>'request' => the payment is executed when user send an application</li><li>'release' => the payment is executed when application is accepted</li></ul>", enum={"request", "release"})
   * @Groups({"read", "write"})
   */
  private string $phase;

  public function getId(): string
  {
    return $this->id;
  }

  public function setId(string $id): void
  {
    $this->id = $id;
  }

  public function getPhase(): string
  {
    return $this->phase;
  }

  public function setPhase(string $phase): void
  {
    $this->phase = $phase;
  }


  /**
   * @return array
   */
  public function jsonSerialize(): ?array
  {
    return [
      'id' => $this->id,
      'phase' => $this->phase,
    ];
  }


  public static function fromArray($data = []): ?PaymentConfig
  {
    $paymentConfig = new self();
    $paymentConfig->setId($data['id'] ?? null);
    $paymentConfig->setPhase($data['phase'] ?? PaymentPhase::PHASE_REQUEST);

    return $paymentConfig;
  }

}
