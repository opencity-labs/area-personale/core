<?php

namespace App\Model\Api;

use App\Model\LinksPagedList;
use App\Model\MetaPagedList;

class PagedList
{
  private MetaPagedList $meta;

  private LinksPagedList $links;

  private array $data = [];

  /**
   * @param MetaPagedList $meta
   * @param LinksPagedList $links
   * @param array $data
   */
  public function __construct(MetaPagedList $meta, LinksPagedList $links, array $data)
  {
    $this->meta = $meta;
    $this->links = $links;
    $this->data = $data;
  }

  /**
   * @return MetaPagedList
   */
  public function getMeta(): MetaPagedList
  {
    return $this->meta;
  }

  /**
   * @param MetaPagedList $meta
   */
  public function setMeta(MetaPagedList $meta): void
  {
    $this->meta = $meta;
  }

  /**
   * @return LinksPagedList
   */
  public function getLinks(): LinksPagedList
  {
    return $this->links;
  }

  /**
   * @param LinksPagedList $links
   */
  public function setLinks(LinksPagedList $links): void
  {
    $this->links = $links;
  }

  /**
   * @return array
   */
  public function getData(): array
  {
    return $this->data;
  }

  /**
   * @param array $data
   */
  public function setData(array $data): void
  {
    $this->data = $data;
  }

}
