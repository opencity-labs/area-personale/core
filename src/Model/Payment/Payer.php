<?php

namespace App\Model\Payment;

use App\Entity\CPSUser;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class Payer
{
  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Type")
   * @Groups({"write", "kafka"})
   */
  private string $type = 'human';

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Tax identification number")
   * @Groups({"write", "kafka"})
   */
  private string $taxIdentificationNumber;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Name")
   * @Groups({"write", "kafka"})
   */
  private string $name;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Family name")
   * @Groups({"write", "kafka"})
   */
  private string $familyName;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Street name")
   * @Groups({"write", "kafka"})
   */
  private string $streetName;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Building number")
   * @Groups({"write", "kafka"})
   */
  private string $buildingNumber;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Postal code")
   * @Groups({"write", "kafka"})
   */
  private string $postalCode;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Town name")
   * @Groups({"write", "kafka"})
   */
  private string $townName;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Country subdivision")
   * @Groups({"write", "kafka"})
   */
  private ?string $countrySubdivision;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Country")
   * @Groups({"write", "kafka"})
   */
  private ?string $country;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Email")
   * @Groups({"write", "kafka"})
   */
  private string $email;

  /**
   * @return string
   */
  public function getType(): string
  {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType(string $type): void
  {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getTaxIdentificationNumber(): string
  {
    return $this->taxIdentificationNumber;
  }

  /**
   * @param string $taxIdentificationNumber
   */
  public function setTaxIdentificationNumber(string $taxIdentificationNumber): void
  {
    $this->taxIdentificationNumber = $taxIdentificationNumber;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getFamilyName(): string
  {
    return $this->familyName;
  }

  /**
   * @param string $familyName
   */
  public function setFamilyName(string $familyName): void
  {
    $this->familyName = $familyName;
  }

  /**
   * @return string
   */
  public function getStreetName(): string
  {
    return $this->streetName;
  }

  /**
   * @param string $streetName
   */
  public function setStreetName(string $streetName): void
  {
    $this->streetName = $streetName;
  }

  /**
   * @return string
   */
  public function getBuildingNumber(): string
  {
    return $this->buildingNumber;
  }

  /**
   * @param string $buildingNumber
   */
  public function setBuildingNumber(string $buildingNumber): void
  {
    $this->buildingNumber = $buildingNumber;
  }

  /**
   * @return string
   */
  public function getPostalCode(): string
  {
    return $this->postalCode;
  }

  /**
   * @param string $postalCode
   */
  public function setPostalCode(string $postalCode): void
  {
    $this->postalCode = $postalCode;
  }

  /**
   * @return string
   */
  public function getTownName(): string
  {
    return $this->townName;
  }

  /**
   * @param string $townName
   */
  public function setTownName(string $townName): void
  {
    $this->townName = $townName;
  }

  /**
   * @return string
   */
  public function getCountrySubdivision(): ?string
  {
    return $this->countrySubdivision;
  }

  /**
   * @param string $countrySubdivision
   */
  public function setCountrySubdivision(?string $countrySubdivision): void
  {
    $this->countrySubdivision = $countrySubdivision;
  }

  /**
   * @return string
   */
  public function getCountry(): ?string
  {
    return $this->country;
  }

  /**
   * @param string $country
   */
  public function setCountry(?string $country): void
  {
    $this->country = $country;
  }

  /**
   * @return string
   */
  public function getEmail(): string
  {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail(string $email): void
  {
    $this->email = $email;
  }

  public static function fromUser(CPSUser $user): self
  {
    $payer = new self();
    $payer->setTaxIdentificationNumber($user->getCodiceFiscale());
    $payer->setName($user->getNome());
    $payer->setFamilyName($user->getCognome());
    $payer->setStreetName($user->getIndirizzoResidenza() ?? '');
    $payer->setBuildingNumber($user->getCivicoResidenza() ?? '');
    $payer->setPostalCode($user->getCapResidenza() ?? '');
    $payer->setTownName($user->getCittaResidenza() ?? '');
    $payer->setCountrySubdivision($user->getProvinciaResidenza() ?? '');
    $payer->setCountry($user->getStatoResidenza() ?? 'IT');
    $payer->setEmail($user->getEmail());
    return $payer;
  }

}
