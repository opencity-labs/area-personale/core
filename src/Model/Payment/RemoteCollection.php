<?php

namespace App\Model\Payment;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

class RemoteCollection
{
  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Remote collection id")
   * @Groups({"read", "write"})
   */
  private string $id;

  /**
   * @Serializer\Type("string")
   * @OA\Property(description="Remote collection type")
   * @Groups({"read", "write"})
   */
  private string $type;

  public function getId(): string
  {
    return $this->id;
  }

  public function setId(string $id): void
  {
    $this->id = $id;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): void
  {
    $this->type = $type;
  }

  public static function init($id, $type): self
  {
    $remoteCollection = new self();
    $remoteCollection->setId($id);
    $remoteCollection->setType($type);
    return $remoteCollection;
  }

}
