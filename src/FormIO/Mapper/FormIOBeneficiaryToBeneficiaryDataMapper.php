<?php

namespace App\FormIO\Mapper;

use App\ApplicationLiteImport\Infrastructure\InvalidJsonException;
use App\Applications\Application\DTOs\BeneficiaryData;
use App\Applications\Application\DTOs\ContactsData;
use App\Applications\Application\DTOs\IdentifiersData;
use DateTime;
use JMS\Serializer\SerializerBuilder;
use JsonSchema\Constraints\Factory;
use JsonSchema\SchemaStorage;
use JsonSchema\Validator;

class FormIOBeneficiaryToBeneficiaryDataMapper
{
  /**
   * @throws \JsonException
   */
  public static function map(array $data): BeneficiaryData
  {
    $serializer = SerializerBuilder::create()->build();

    $beneficiaryData = $data['data']['beneficiary']['data'];
    $content = json_encode($beneficiaryData, JSON_THROW_ON_ERROR);
    self::validate($content);

    /** @var BeneficiaryData $beneficiary */
    $beneficiary = $serializer->deserialize($content, BeneficiaryData::class, 'json');

    $dateOfBirth = !empty($beneficiaryData['birth_date']) ? DateTime::createFromFormat('d/m/Y', $beneficiaryData['birth_date']) : null;
    $beneficiary->dateOfBirth = $dateOfBirth;
    $beneficiary->placeOfBirth = !empty($beneficiaryData['birth_place']) ? $beneficiaryData['birth_place'] : null;
    $identifiers = new IdentifiersData();
    $identifiers->taxCode = $data['data']['beneficiary']['data']['tax_id'];
    $beneficiary->identifiers = $identifiers;
    $beneficiary->contacts = (new ContactsData());

    return $beneficiary;

  }

  public static function validate(string $json): bool
  {
    $jsonSchema = self::getSchema();
    $jsonSchemaObject = json_decode($jsonSchema);

    $schemaStorage = new SchemaStorage();
    $schemaStorage->addSchema('file://mySchema', $jsonSchemaObject);

    $jsonValidator = new Validator(new Factory($schemaStorage));

    $jsonToValidateObject = json_decode($json);
    $jsonValidator->validate($jsonToValidateObject, $jsonSchemaObject);

    $jsonValidator->getErrors();

    if (!$jsonValidator->isValid()) {

      throw InvalidJsonException::becauseThereAreUnmetConstraints($jsonValidator);
    }

    return true;
  }

  public static function getSchema(): string
  {
    return <<<'JSON'
      {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "given_name": {
            "type": "string"
          },
          "family_name": {
            "type": "string"
          },
          "birth_place": {
            "type": "string"
          },
          "birth_date": {
            "type": "string"
          },
          "gender": {
            "type": "string"
          },
          "tax_id": {
            "type": "string"
          },
          "nationality": {
            "type": "string"
          },
          "address": {
            "type": "object",
            "properties": {
              "data": {
                "type": "object",
                "properties": {
                  "street_address": {
                    "type": "string"
                  },
                  "address_number": {
                    "type": "string"
                  },
                  "address_locality": {
                    "type": "string"
                  },
                  "postal_code": {
                    "type": "string"
                  },
                  "address_country": {
                    "type": "string"
                  }
                },
                "required": [
                  "street_address",
                  "address_locality",
                  "postal_code",
                  "address_country"
                ]
              }
            },
            "required": [
              "data"
            ]
          }
        },
        "required": [
          "given_name",
          "family_name",
          "birth_place",
          "birth_date",
          "gender",
          "tax_id",
          "nationality",
          "address"
        ]
      }
   JSON;
  }
}
