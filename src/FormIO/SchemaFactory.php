<?php

namespace App\FormIO;

use App\Entity\Servizio;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SchemaFactory implements SchemaFactoryInterface
{
  /**
   * @var array
   * @todo usare un FormIOSchemaProviderInterface decorator
   */
  private static array $cacheRemoteForms = [];

  /**
   * @var Schema[]
   * @todo usare uno SchemaFactoryInterface decorator
   */
  private static array $cacheSchemas = [];

  private FormIOSchemaProviderInterface $provider;

  private HttpClientInterface $httpClient;

  private SessionInterface $session;

  private bool $useCache = true;

  public function __construct(
    FormIOSchemaProviderInterface $provider,
    HttpClientInterface $httpClient,
    SessionInterface $session
  ) {
    $this->provider = $provider;
    $this->httpClient = $httpClient;
    $this->session = $session;
  }

  public function createFromService(Servizio $servizio, bool $forceCache = true): Schema
  {
    if($servizio->isBuiltIn()){
      return $this->createFromFormIdentifier($servizio->getIdentifier());
    }
    return $this->createFromFormId($servizio->getFormIoId(), $forceCache);
  }


  /**
   * @param $formIOId
   * @param bool $forceCache
   * @return Schema
   */
  public function createFromFormId($formIOId, bool $forceCache = true): Schema
  {
    if (empty(trim($formIOId))) {
      return new Schema();
    }

    $cacheId = $this->provider->getFormServerUrl().$formIOId;

    if ($this->useCache && $forceCache && $this->hasCache($cacheId)) {
      return $this->getCache($cacheId);
    }

    $schema = new Schema();
    $schema->setId($formIOId);
    $schema->setServer($this->provider->getFormServerUrl());
    $schema->setServerPublicUrl($this->provider->getFormServerPublicUrl());
    $this->parseForm($schema, $formIOId);

    if ($this->useCache && $forceCache && $schema->countComponents() > 0) {
      $this->setCache($cacheId, $schema);
    }

    return $schema;
  }

  /**
   * @param String $identifier
   * @return Schema
   */
  public function createFromFormIdentifier(string $identifier): Schema
  {
    $form = file_get_contents('../assets/app/schemas/'. $identifier .'.json');
    return $this->createFromFormJson($form);
  }


  public function createFromFormJson(string $json): Schema
  {
    $schema = new Schema();
    $schema->setServer($this->provider->getFormServerUrl());
    $this->parseComponent($schema, json_decode($json, true));

    return $schema;
  }

  private function hasCache($id): bool
  {
    if ($this->session->isStarted()) {
      return $this->session->has('form.factory.'.$id);
    }

    return isset(self::$cacheSchemas[$id]);
  }

  private function getCache($id)
  {
    if ($this->session->isStarted()) {
      return $this->session->get('form.factory.'.$id);
    }

    return self::$cacheSchemas[$id];
  }

  private function parseForm(Schema $schema, $formIOId, $prefixKey = null, $prefixLabel = null): void
  {
    $form = $this->getRemoteSchema($formIOId);
    if ($form) {
      $schema->addSource($formIOId, $form);
      $this->parseComponent($schema, $form, $prefixKey, $prefixLabel);
    }
  }

  private function getRemoteSchema($formIOId)
  {
    if (!isset(self::$cacheRemoteForms[$formIOId])) {
      $adapterResult = $this->provider->getForm($formIOId);
      if ($adapterResult['status'] == 'success') {
        self::$cacheRemoteForms[$formIOId] = $adapterResult['form'];
      } else {
        return false;
      }
    }

    return self::$cacheRemoteForms[$formIOId];
  }

  // Questa modifica è necessaria perché abbiamo spostato le properties eservice e format sul primo
  // component del nested form di tipo pdnd per avere l'attivazione automatica
  private function isPdndBlock($formIOId): bool
  {
    return !empty($this->getPdndBlockProperties($formIOId));
  }

  private function getPdndBlockProperties($formIOId): array
  {
    $form = $this->getRemoteSchema($formIOId);
    if (empty($form['components'])) {
      return [];
    }
    $firstChild = $form['components'][0];

    if ($firstChild && $this->isValidPdndBlock($firstChild)) {
      return $firstChild['properties'];
    }

    return [];
  }

  private function isValidPdndBlock(array $component): bool
  {
    return $component['type'] === 'columns'
      && !empty($component['properties']['eservice'])
      && !empty($component['properties']['format']);
  }

  private function parseComponent(Schema $schema, $component, $prefixKey = null, $prefixLabel = null): void
  {
    if (!isset($component['type']) && isset($component['components'])) {
      foreach ($component['components'] as $item) {
        $this->parseComponent($schema, $item, $prefixKey, $prefixLabel);
      }
    } else {
      switch ($component['type']) {
        case 'form':
        case 'panel':
        case 'column':
        case 'fieldset':
        case 'well':
        case 'datagrid':

          if ($component['type'] === 'form' && isset($component['form'])) {
            if (isset($component['key'])) {
              // I nested form all'interno dell'applicant vanno gestiti differentemente perchè non possono essere attivati con la proprietà profile_block
              if ($prefixKey == 'applicant.') {
                $prefixKey .= $component['key'].'.';
                $component['path'] = substr($prefixKey, 0, -1);
                if ($this->isPdndBlock($component['form'])) {
                  $component['properties'] = array_merge(['profile_block' => 'true'], $this->getPdndBlockProperties($component['form']));
                  $schema->addComponentToProfileBlocks($component);
                }

              } else {
                $prefixKey .= $component['key'].'.';
                if (isset($component['properties']['profile_block']) && $component['properties']['profile_block']) {
                  $component['path'] = substr($prefixKey, 0, -1);
                  if ($this->isPdndBlock($component['form'])) {
                    $component['properties'] = array_merge($component['properties'], $this->getPdndBlockProperties($component['form']));
                  }
                  $schema->addComponentToProfileBlocks($component);
                }
              }
            }
            $this->parseForm($schema, $component['form'], $prefixKey, $prefixLabel);
          }

          if ($component['type'] === 'datagrid') {
            if (isset($component['key'])) {
              $prefixKey .= $component['key'].'.';
            }
            $prefixLabel .= $prefixLabel === null ? $component['label'] : '/'. $component['label'];
          }

          if ($component['type'] === 'panel' && isset($component['title'])) {
            if ( $prefixLabel === null ) {
              $prefixLabel .= $component['title'];
            } else {
              $prefixLabel .= '/'.$component['title'];
            }
          }

          if (isset($component['components'])) {
            foreach ($component['components'] as $item) {
              $this->parseComponent($schema, $item, $prefixKey, $prefixLabel);
            }
          }
          break;

        case 'columns':
          if (isset($component['columns'])) {
            foreach ($component['columns'] as $item) {
              $this->parseComponent($schema, $item, $prefixKey, $prefixLabel);
            }
          }
          break;

        case 'email':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\EmailType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'financial_report':
          foreach ($component['data']['values'] as $index => $value) {
            foreach ($value as $key => $item) {
              $schema->addComponent(
                $prefixKey.$component['key'].'.'.$index.'.'.$key,
                Type\TextareaType::class,
                $this->parseComponentOptions($component, $prefixLabel)
              );
            }
          }
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\EmailType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'checkbox':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\CheckboxType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'radio':
          $choices = [];
          if (isset($component['values'])) {
            $choices = array_column($component['values'], 'value');
          }
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\ChoiceType::class,
            $this->parseComponentOptions(
              $component,
              $prefixLabel,
              ['choices' => $choices,]
            )
          );
          break;

        case 'number':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\NumberType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'file': //@todo
        case 'sdcfile': //@todo
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\FileType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'hidden':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\HiddenType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'textarea':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\TextareaType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'url':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\UrlType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'currency':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\CurrencyType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'time':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\TimeType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'day':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\DateType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'phoneNumber': //@todo
        case 'textfield':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\TextType::class,
            $this->parseComponentOptions($component, $prefixLabel)
          );
          break;

        case 'date':
        case 'datetime':
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\DateTimeType::class,
            $this->parseComponentOptions(
              $component,
              $prefixLabel,
              [
                'format' => Type\DateTimeType::HTML5_FORMAT,
                'widget' => 'single_text',
              ]
            )
          );
          break;

        case 'select':
          $choices = [];
          $data = $component['data'];
          if (isset($component['dataSrc'])) {
            try {
              $url = $data[$component['dataSrc']];
              $remoteResponse = $this->httpClient->request('GET', $url);
              $remoteData = (array)json_decode($remoteResponse->getContent(), true, 512, JSON_THROW_ON_ERROR);
              $property = $component['valueProperty'] ?? null;

              foreach ($remoteData as $item) {
                if ($property && isset($item[$property])) {
                  $choices[] = $item[$property];
                } else {
                  $choices[] = $item;
                }
              }
            } catch (\Throwable $e) {
              //@todo log error
            }
          } elseif (isset($data['values'])) {
            $choices = array_column($data['values'], 'value');
          }
          $schema->addComponent(
            $prefixKey.$component['key'],
            Type\ChoiceType::class,
            $this->parseComponentOptions(
              $component,
              $prefixLabel,
              [
                'choices' => $choices,
              ]
            )
          );
          break;
      }
    }
  }

  private function parseComponentOptions($component, $prefixLabel = null, $options = [])
  {

    if (!isset($options['required'])) {
      $required = isset($component['validate']['required']) && $component['validate']['required'];
      $options['required'] = $required;
    }

    if (!isset($options['invalid_message'])) {
      $message = false;
      if (!empty($component['validate']['customMessage'])) {
        $message = $component['validate']['customMessage'];
      } elseif (!empty($component['errorLabel'])) {
        $message = $component['errorLabel'];
      }
      if ($message) {
        $options['invalid_message'] = $message;
      }
    }

    if (isset($component['label']) && !isset($options['label'])) {
      $options['label'] = $component['label'];
    }
    if (isset($options['label']) && $prefixLabel) {
      $options['label'] = $prefixLabel.'/'.$options['label'];
    }

    $constraints = [];
    if ($options['required']) {
      $constraints[] = new NotBlank();
    }
    if (!empty($component['validate']['pattern'])) {
      $constraints[] = new Regex(['pattern' => '/'.$component['validate']['pattern'].'/']);
    }
    if (count($constraints) > 0) {
      $options['constraints'] = $constraints;
    }

    $options['mapped'] = false;

    if (isset($component['properties']['pdnd_field'])) {
      $options['pdnd_field'] = true;
    }

    return $options;
  }

  private function setCache($id, $data): void
  {
    if ($this->session->isStarted()) {
      $this->session->set('form.factory.'.$id, $data);
    }
    self::$cacheSchemas[$id] = $data;
  }

}
