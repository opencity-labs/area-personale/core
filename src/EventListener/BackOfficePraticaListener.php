<?php

namespace App\EventListener;

use App\BackOffice\BackOfficeInterface;
use App\Entity\DematerializedFormPratica;
use App\Event\PraticaOnChangeStatusEvent;
use App\Services\BackOfficeCollection;
use Psr\Log\LoggerInterface;

class BackOfficePraticaListener
{
  private BackOfficeCollection $backOfficeCollection;
  private LoggerInterface $logger;

  public function __construct(
    BackOfficeCollection $backOfficeCollection,
    LoggerInterface      $logger
  ) {
    $this->backOfficeCollection = $backOfficeCollection;
    $this->logger = $logger;
  }

  public function onStatusChange(PraticaOnChangeStatusEvent $event)
  {
    $pratica = $event->getPratica();
    $service = $pratica->getServizio();
    $integrations = $service->getIntegrations();

    if (!empty($integrations) && $pratica instanceof DematerializedFormPratica) {

      /** @var BackOfficeInterface $backOfficeHandler */
      $backOfficeHandler = $this->backOfficeCollection->getBackOffice(reset($integrations));

      $integrationDesc = $integrations[$event->getNewStateIdentifier()] ?? '';

      if ($backOfficeHandler instanceof BackOfficeInterface) {

        $this->logger->info(
          'Backoffice: executing ' . $integrationDesc
          . ' on application ' . $pratica->getId()
          . ' with status ' . $pratica->getStatus(),
        );
        $backOfficeHandler->execute($pratica);

      } else {

        $this->logger->critical('Backoffice ' . $integrationDesc . ' not loaded');
      }
    }
  }
}
