<?php

namespace App\EventListener;

use App\Entity\Pratica;
use App\Event\GenerateApplicationReceiptEvent;
use App\Event\ProtocollaAllegatiIntegrazioneSuccessEvent;
use App\Event\ProtocollaAllegatiOperatoreSuccessEvent;
use App\Event\ProtocollaPraticaSuccessEvent;
use App\Event\ProtocollaRichiesteIntegrazioneSuccessEvent;
use App\Services\PraticaStatusService;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProtocolloSuccessSubscriber implements EventSubscriberInterface
{
  private PraticaStatusService $praticaStatusService;

  private LoggerInterface $logger;
  private EventDispatcherInterface $dispatcher;

  public function __construct(PraticaStatusService $praticaStatusService, LoggerInterface $logger, EventDispatcherInterface $dispatcher)
  {
    $this->praticaStatusService = $praticaStatusService;
    $this->logger = $logger;
    $this->dispatcher = $dispatcher;
  }

  public static function getSubscribedEvents()
  {
    return [
      ProtocollaPraticaSuccessEvent::class => ['onProtocollaPratica'],
      ProtocollaRichiesteIntegrazioneSuccessEvent::class => ['onProtocollaRichiesteIntegrazione'],
      ProtocollaAllegatiIntegrazioneSuccessEvent::class => ['onProtocollaAllegatiIntegrazione'],
      ProtocollaAllegatiOperatoreSuccessEvent::class => ['onProtocollaAllegatiOperatore'],
    ];
  }

  public function onProtocollaPratica(ProtocollaPraticaSuccessEvent $event)
  {
    $this->praticaStatusService->setNewStatus($event->getPratica(), Pratica::STATUS_REGISTERED);

    // Emette l'evento per la rigenerazione del pdf
    $this->dispatcher->dispatch(new GenerateApplicationReceiptEvent($event->getPratica()), GenerateApplicationReceiptEvent::NAME);
  }

  public function onProtocollaRichiesteIntegrazione(ProtocollaPraticaSuccessEvent $event)
  {
    $this->praticaStatusService->setNewStatus($event->getPratica(), Pratica::STATUS_DRAFT_FOR_INTEGRATION);
  }

  public function onProtocollaAllegatiIntegrazione(ProtocollaPraticaSuccessEvent $event)
  {
    $this->praticaStatusService->setNewStatus($event->getPratica(), Pratica::STATUS_REGISTERED_AFTER_INTEGRATION);
  }

  public function onProtocollaAllegatiOperatore(ProtocollaAllegatiOperatoreSuccessEvent $event)
  {
    $pratica = $event->getPratica();
    if ($pratica->getEsito()) {
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_COMPLETE);
    } else {
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_CANCELLED);
    }

  }
}
