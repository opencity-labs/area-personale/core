<?php


namespace App\EventListener;


use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MaintenanceListener
{
  private bool $enable;
  private string $statusPage;
  private array $ips;

  private Environment $twigEnvironment;


  /**
   * MaintenanceListener constructor.
   *
   * @param bool $enable
   * @param string $statusPage
   * @param string $ips
   * @param Environment $environment
   */
  public function __construct(bool $enable, string $statusPage, string $ips, Environment $environment)
  {
    $this->enable = $enable ?: false;
    $this->statusPage = $statusPage ?: '';
    $this->ips = $ips ? explode(',', $ips) : [];
    $this->twigEnvironment = $environment;

  }

  /**
   * @throws SyntaxError
   * @throws RuntimeError
   * @throws LoaderError
   */
  public function onKernelRequest(RequestEvent $event): void
  {
    $request = $event->getRequest();
    if ($this->enable && !IpUtils::checkIp($request->getClientIp(), $this->ips)) {
      if (strpos($request->getRequestUri(), '/api') !== false) {
        $event->setResponse(new JsonResponse([
          "title" => "We'll be back soon!",
          "status" =>  Response::HTTP_SERVICE_UNAVAILABLE,
          "detail" => "Sorry for the inconvenience but we are performing some maintenance at the moment.",
        ], Response::HTTP_SERVICE_UNAVAILABLE));
      } else {
        $content = $this->twigEnvironment->render('Maintenance/maintenance.html.twig', ['status_page' => $this->statusPage]);
        $event->setResponse(new Response($content, Response::HTTP_SERVICE_UNAVAILABLE));
      }
    }
  }
}
