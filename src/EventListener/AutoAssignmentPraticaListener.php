<?php

namespace App\EventListener;

use App\Entity\Pratica;
use App\Event\PraticaOnChangeStatusEvent;
use App\Services\Manager\PraticaManager;
use Flagception\Manager\FeatureManagerInterface;

class AutoAssignmentPraticaListener
{

  private PraticaManager $praticaManager;
  private FeatureManagerInterface $featureManager;

  public function __construct(FeatureManagerInterface $featureManager, PraticaManager $praticaManager)
  {
    $this->praticaManager = $praticaManager;
    $this->featureManager = $featureManager;
  }

  public function onStatusChange(PraticaOnChangeStatusEvent $event)
  {
    $pratica = $event->getPratica();

    if ($this->featureManager->isActive('feature_automatic_applications_assignment') &&
        (($pratica->getServizio()->isProtocolRequired() && $pratica->getStatus() == Pratica::STATUS_REGISTERED) || (!$pratica->getServizio()->isProtocolRequired() && $pratica->getStatus() == Pratica::STATUS_SUBMITTED))) {
      $this->praticaManager->autoAssign($pratica);
    }
  }

}
