<?php

namespace App\EventListener;

use App\Event\NotificationEvent;
use App\Services\MeetingService;
use Psr\Log\LoggerInterface;

class NotificationListener
{

  private MeetingService $meetingService;
  private LoggerInterface $logger;

  public function __construct(MeetingService $meetingService, LoggerInterface $logger)
  {
    $this->meetingService = $meetingService;
    $this->logger = $logger;
  }

  public function onNotificationEvent(NotificationEvent $event): void
  {

  }
}
