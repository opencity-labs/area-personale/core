<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class BackendAccessDeniedHandler implements AccessDeniedHandlerInterface
{
  private Security $security;
  private RouterInterface $router;
  private SessionInterface $session;
  private TranslatorInterface $translator;

  public function __construct(
    RouterInterface     $router,
    SessionInterface    $session,
    TranslatorInterface $translator,
    Security            $security
  )
  {
    $this->security = $security;
    $this->router = $router;
    $this->session = $session;
    $this->translator = $translator;
  }

  public function handle(
    Request               $request,
    AccessDeniedException $accessDeniedException
  ): ?Response
  {

    if ($this->security->isGranted('ROLE_ADMIN')) {

      $this->session->getFlashBag()
        ->add('error', $this->translator->trans("security.access_denied_address"));

      return $this->redirectToRoute('admin_index');
    }

    if ($this->security->isGranted('ROLE_OPERATORE')) {

      $this->session->getFlashBag()
        ->add('error', $this->translator->trans("security.access_denied_address"));

      return $this->redirectToRoute('operatori_index');
    }

    // Restituisce la classica pagina d'errore
    return null;
  }

  protected function redirectToRoute(string $route, array $parameters = [], int $status = 302): RedirectResponse
  {
    return $this->redirect($this->generateUrl($route, $parameters), $status);
  }

  protected function generateUrl(string $route, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
  {
    return $this->router->generate($route, $parameters, $referenceType);
  }

  protected function redirect(string $url, int $status = 302): RedirectResponse
  {
    return new RedirectResponse($url, $status);
  }
}
