<?php

namespace App\Security\OAuth;

interface ConfigurableProviderInterface
{
  public function getName(): string;

  public function getIdentifier(): string;

  public function getConfiguration(): ConfigurationInterface;

  public function setConfiguration(ConfigurationInterface $configuration): void;
}
