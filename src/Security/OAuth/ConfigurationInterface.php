<?php

namespace App\Security\OAuth;

interface ConfigurationInterface
{
  public function getRedirectUrl(): string;

  public function setRedirectUrl(string $redirectUrl): self;

  public function getUrlAuthorize(): string;

  public function setUrlAuthorize(string $urlAuthorize): self;

  public function getUrlAccessToken(): string;

  public function setUrlAccessToken(string $urlAccessToken): self;

  public function getUrlResourceOwnerDetails(): string;

  public function setUrlResourceOwnerDetails(string $urlResourceOwnerDetails): self;

  public function getClientId(): string;

  public function setClientId(string $clientId): self;

  public function getClientSecret(): ?string;

  public function setClientSecret(string $clientSecret): self;

  public function getUrlLogout(): string;

  public function setUrlLogout(string $urlLogout): self;
}
