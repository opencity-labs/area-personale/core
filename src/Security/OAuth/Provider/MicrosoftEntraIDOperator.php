<?php

namespace App\Security\OAuth\Provider;

use App\Entity\CPSUser;
use App\Entity\UserSession;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ConfigurationAwareTrait;
use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\LogoutUrlTrait;
use App\Security\OAuth\ResourceOwner;
use DateTime;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;

class MicrosoftEntraIDOperator extends GenericProvider implements LogoutProviderInterface,
                                                   ConfigurableProviderInterface
{
  use LogoutUrlTrait;

  use ConfigurationAwareTrait;

  public const IDENTIFIER = 'microsoft-entra-id-operator';

  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return "Microsoft Entra ID";
  }

  public function getDefaultScopes(): array
  {
    return ['openid profile email https://graph.microsoft.com/User.Read'];
  }

  protected function createResourceOwner(array $response, AccessToken $token)
  {

    $tokenParts = explode('.', $token);
    $tokenPayload = json_decode(base64_decode($tokenParts[1]), true);

    $sessionInstant = new DateTime();
    if (!empty($tokenPayload['iat'])) {
      $sessionInstant->setTimestamp($tokenPayload['iat']);
    }

    return (new ResourceOwner($response))
      ->setCodiceFiscale($response['mail'])
      ->setNome($response['givenName'])
      ->setCognome($response['surname'])
      ->setEmailAddress($response['mail'])
      ->setEmailAddressPersonale($response['mail'])
      ->setCellulare($response['mobilePhone'] ?? null)
      ->setInstant($sessionInstant->format(DATE_ATOM))
      ->setSessionId($tokenPayload['uti'] ?? null)
      ->setSessionIndex($sessionInstant->format(DATE_ATOM))
      ;
  }

}
