<?php

namespace App\Security\OAuth\Provider;

use App\Entity\CPSUser;
use App\Entity\UserSession;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ConfigurationAwareTrait;
use App\Security\OAuth\ResourceOwner;
use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\LogoutUrlTrait;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class TnDigit extends GenericProvider implements LogoutProviderInterface,
  ConfigurableProviderInterface,
  LoggerAwareInterface
{
  use LogoutUrlTrait;

  use ConfigurationAwareTrait;

  use LoggerAwareTrait;

  public const IDENTIFIER = 'tndigit';

  public function __construct(array $options = [], array $collaborators = [])
  {
    $options['pkceMethod'] = AbstractProvider::PKCE_METHOD_S256;
    parent::__construct($options, $collaborators);
  }

  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return "Trentino Digitale";
  }

  public function getDefaultScopes(): array
  {
    return [
      'openid',
      'profile',
      'email',
      'offline_access',
      'profile.cdc-codicefiscale.me',
      'profile.cdc-issuersource.me',
      'profile.cdc-acr.me',
      'profile.cdc-spid.me',
      'profile.cdc-cns.me',
    ];
  }

  protected function getScopeSeparator(): string
  {
    return ' ';
  }

  private function getAccessTokenPayload($encodedToken): ?array
  {
    [$header, $payload, $signature] = explode('.', $encodedToken);
    $jsonToken = base64_decode($payload);

    return json_decode($jsonToken, true);
  }

  protected function createResourceOwner(array $response, AccessToken $token)
  {

    $token = $this->getAccessTokenPayload($token->getToken());

    $dataNascita = null;
    $dateOfBirth = $response['birthdate'] ?? null;
    if ($dateOfBirth) {
      $dateTime = \DateTime::createFromFormat('Y-m-d', $dateOfBirth);
      if ($dateTime instanceof \DateTime) {
        $dataNascita = $dateTime->format('d/m/Y');
      }
    }

    $fiscalCode = $response['cdc-codicefiscale']['fiscalCode'];

    $email = $response['email'] ?? null;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email = null;
    }

    $authType = $this->getAuthType(!empty($response['cdc-issuersource']) ? $response['cdc-issuersource']['issuerSource'] : '');

    return (new ResourceOwner($response))
      ->setCodiceFiscale($fiscalCode)
      ->setNome($response['given_name'])
      ->setCognome($response['family_name'])
      ->setDataNascita($dataNascita)
      ->setSesso(!empty($response['gender']) ? $response['gender'] : null)
      ->setEmailAddress($email)
      ->setEmailAddressPersonale($email)
      ->setProvider($token['iss'])
      ->setAuthenticationMethod($authType)
      ->setSpidCode(!empty($response['cdc-spid']['spidCode']) ? $response['cdc-spid']['spidCode'] : null)
      ->setSpidLevel($this->getSpidLevel(!empty($response['cdc-acr']) ? $response['cdc-acr']['acr'] : ''))
      ->setInstant(date('c', $token['iat'] ?? null))
      ->setSessionId($response['sub'])
      ->setSessionIndex($token['jti']);
  }

  public function getUrlLogout(?UserSession $userSession): string
  {
    $sessionData = $userSession ? $userSession->getSessionData() : [];
    $token = $sessionData['token'] ?? false;

    return $this->logoutUrl . '&id_token_hint=' . $token;
  }

  private function getAuthType(string $acr): string
  {
    if ($acr === 'CIE') {
      return CPSUser::IDP_CIE;
    }

    if ($acr === 'SPID') {
      return CPSUser::IDP_SPID;
    }

    if ($acr === 'CPS_CNS') {
      return CPSUser::IDP_CPS_OR_CNS;
    }

    return CPSUser::IDP_OTHER;
  }

  private function getSpidLevel(string $acr): int
  {
    if ($acr === 'https://www.spid.gov.it/SpidL3') {
      return 3;
    }

    if ($acr === 'https://www.spid.gov.it/SpidL2') {
      return 2;
    }

    return 1;
  }

}
