<?php

namespace App\Security\OAuth\Provider;

use App\Entity\CPSUser;
use App\Entity\UserSession;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ConfigurationAwareTrait;
use App\Security\OAuth\ResourceOwner;
use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\LogoutUrlTrait;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

class Arpa extends GenericProvider implements LogoutProviderInterface,
  ConfigurableProviderInterface,
  LoggerAwareInterface
{
  use LogoutUrlTrait;

  use ConfigurationAwareTrait;

  use LoggerAwareTrait;

  public const IDENTIFIER = 'arpa';

  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return "OpenToscana";
  }

  public function getDefaultScopes(): array
  {
    return [
      'openid',
      'profile',
      'email',
      'phone',
    ];
  }

  protected function getScopeSeparator(): string
  {
    return ' ';
  }

  private function getAccessTokenPayload($encodedToken): ?array
  {
    [$header, $payload, $signature] = explode('.', $encodedToken);
    $jsonToken = base64_decode($payload);

    return json_decode($jsonToken, true);
  }

  protected function createResourceOwner(array $response, AccessToken $token)
  {

    $extras = $response['extras'];
    $token = $this->getAccessTokenPayload($token->getToken());

    $dataNascita = null;
    $dateOfBirth = $extras['dateOfBirth'] ?? null;
    if ($dateOfBirth) {
      $dateTime = \DateTime::createFromFormat('Y-m-d', $dateOfBirth);
      if ($dateTime instanceof \DateTime) {
        $dataNascita = $dateTime->format('d/m/Y');
      }
    }

    $digitalAddress = $response['digitalAddress'] ?? null;
    if (!filter_var($digitalAddress, FILTER_VALIDATE_EMAIL)) {
      $digitalAddress = null;
    }

    $email = $response['email'] ?? null;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email = null;
    }

    return (new ResourceOwner($response))
      ->setCodiceFiscale(str_replace('TINIT-', '', $extras['fiscalNumber']))
      ->setNome($extras['name'])
      ->setCognome($extras['familyName'])
      ->setLuogoNascita($extras['placeOfBirth'] ?? null)
      ->setProvinciaNascita($extras['countyOfBirth'] ?? null)
      ->setDataNascita($dataNascita)
      ->setSesso($response['gender'] ?? null)
      ->setIdCard($extras['idCard'] ?? null)
      ->setCellulare($response['phone_number'] ?? null)
      ->setEmailAddress($digitalAddress ?? $email)
      ->setEmailAddressPersonale($email)
      ->setProvider($token['auth_type'] ?? null)
      ->setAuthenticationMethod(CPSUser::IDP_SPID)
      ->setSpidCode($extras['spidCode'])
      ->setSpidLevel((int)($token['auth_level'] ?? 1))
      ->setInstant(date('c', $token['auth_time'] ?? null))
      ->setSessionId($token['sid'] ?? $response['sub'] ?? null)
      ->setSessionIndex($token['session_state'] ?? null);
  }

  public function getUrlLogout(?UserSession $userSession): string
  {
    try {
      $clientId = $this->getConfiguration()->getClientId();
      $clientSecret = $this->getConfiguration()->getClientSecret();
      $sessionData = $userSession ? $userSession->getSessionData() : [];
      $token = $sessionData['token'] ?? false;
      $refreshToken = $sessionData['refresh_token'] ?? false;
      if ($token && $refreshToken) {
        $request = $this->getAuthenticatedRequest(
          self::METHOD_POST, $this->logoutUrl, $token, [
            'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'body' => \http_build_query([
              'client_id' => $clientId,
              'client_secret' => $clientSecret,
              'refresh_token' => $refreshToken
              ], '', '&'),
          ]
        );
        $this->getResponse($request);
      }
    } catch (\Exception $e) {
      if ($this->logger) {
        $this->logger->error('arpa_logout_request_error', [
          'message' => $e->getMessage(),
          'request' => $request
        ]);
      }
    }

    return '';
  }

}
