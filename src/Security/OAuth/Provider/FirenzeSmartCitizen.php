<?php

namespace App\Security\OAuth\Provider;

use App\Entity\CPSUser;
use App\Entity\UserSession;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ConfigurationAwareTrait;
use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\LogoutUrlTrait;
use App\Security\OAuth\ResourceOwner;
use DateTime;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;

class FirenzeSmartCitizen extends GenericProvider implements LogoutProviderInterface,
                                                   ConfigurableProviderInterface
{
  use LogoutUrlTrait;

  use ConfigurationAwareTrait;

  public const IDENTIFIER = 'firenzesmart-citizen';

  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return "Firenze Smart";
  }

  public function getDefaultScopes(): array
  {
    return ['openid'];
  }

  protected function createResourceOwner(array $response, AccessToken $token)
  {

    $dateOfBirth = null;
    if (!empty($response['birthdate'])) {
      $dateTime = DateTime::createFromFormat('d/m/Y', $response['birthdate']);
      if ($dateTime instanceof DateTime) {
        $dateOfBirth = $dateTime->format('d/m/Y');
      }
    }

    $email = $response['email'] ?? null;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email = null;
    }

    $tokenParts = explode('.', $token);
    $tokenPayload = json_decode(base64_decode($tokenParts[1]), true);

    $code = $tokenPayload['jti'];
    $authMetod = CPSUser::IDP_CIE;
    if (!empty($response['userid'])) {
      $code = $response['userid'];
      $authMetod = CPSUser::IDP_SPID;
    }

    $sessionInstant = new DateTime();
    if (!empty($tokenPayload['iat'])) {
      $sessionInstant->setTimestamp($tokenPayload['iat']);
    }

    return (new ResourceOwner($response))
      ->setCodiceFiscale($response['cn'])
      ->setNome($response['given_name'])
      ->setCognome($response['family_name'])
      ->setLuogoNascita($response['place_of_birth'] ?? null)
      ->setDataNascita($dateOfBirth)
      ->setEmailAddress($email)
      ->setEmailAddressPersonale($email)
      ->setCellulare($response['mobile'] ?? null)
      ->setAuthenticationMethod($authMetod)
      ->setSpidCode($code)
      ->setSpidLevel($response['spid-level'] ?? 2)
      ->setInstant($sessionInstant->format(DATE_ATOM))
      ->setSessionId($tokenPayload['jti'] ?? null)
      ->setSessionIndex($response['session'] ?? null)
      ;
  }

  public function getUrlLogout(?UserSession $userSession): string
  {
    $sessionData = $userSession ? $userSession->getSessionData() : [];
    $token = $sessionData['token'] ?? false;

    return $this->logoutUrl . '&id_token_hint=' . $token;
  }

}
