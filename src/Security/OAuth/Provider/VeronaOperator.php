<?php

namespace App\Security\OAuth\Provider;

use App\Entity\UserSession;
use App\Security\OAuth\ConfigurableProviderInterface;
use App\Security\OAuth\ConfigurationAwareTrait;
use App\Security\OAuth\ResourceOwner;
use App\Security\OAuth\LogoutProviderInterface;
use App\Security\OAuth\LogoutUrlTrait;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;

class VeronaOperator extends GenericProvider implements LogoutProviderInterface,
  ConfigurableProviderInterface,
  LoggerAwareInterface
{
  use LogoutUrlTrait;

  use ConfigurationAwareTrait;

  use LoggerAwareTrait;

  use BearerAuthorizationTrait;

  public const IDENTIFIER = 'verona-operator';

  public function getIdentifier(): string
  {
    return self::IDENTIFIER;
  }

  public function getName(): string
  {
    return "Ldap Verona";
  }

  public function getDefaultScopes(): array
  {
    return [
      'openid',
      'profile_portale',
      'email',
    ];
  }

  protected function getScopeSeparator(): string
  {
    return ' ';
  }

  protected function createResourceOwner(array $response, AccessToken $token)
  {

    $email = $response['email'] ?? null;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email = null;
    }

    return (new ResourceOwner($response))
      ->setNome($response['name'])
      ->setCognome($response['surname'])
      ->setEmailAddress($email)
      ->setProvider($this->getIdentifier())
      ;
  }

  public function getUrlLogout(?UserSession $userSession): string
  {
    if (!$userSession instanceof UserSession) {
      return '';
    }

    $sessionData = $userSession->getSessionData();
    return (isset($sessionData['provider']) && $sessionData['provider'] === self::IDENTIFIER) ? $this->getConfiguration()->getUrlLogout() : '';

  }

}
