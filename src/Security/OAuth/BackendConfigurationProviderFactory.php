<?php

namespace App\Security\OAuth;

use Psr\Log\LoggerInterface;

class BackendConfigurationProviderFactory extends AbstractProviderFactory implements ProviderFactoryInterface
{
  private array $parameters;

  private ?ProviderRegistry $providerRegistry;

  public function __construct(ProviderRegistry $providerRegistry, LoggerInterface $logger, $parameters)
  {
    $this->logger = $logger;

    $this->parameters = $parameters;
    $this->providerRegistry = $providerRegistry;
    $this->providerIdentifier = $this->getParameterOrDie('provider');

    $this->configuration = (new Configuration())
      ->setRedirectUrl($this->getParameterOrDie('redirect_url'))
      ->setUrlAuthorize($this->getParameterOrDie('url_authorize'))
      ->setUrlAccessToken($this->getParameterOrDie('url_access_token'))
      ->setUrlResourceOwnerDetails($this->getParameterOrDie('url_resource_owner_details'))
      ->setUrlLogout($this->getParameterOrDie('url_logout'))
      ->setClientId($this->getParameterOrDie('client_id'))
      ->setClientSecret($this->getParameterOrDie('client_secret'));
  }

  protected function supports(): string
  {
    return $this->providerRegistry->getProviderByIdentifier($this->getProviderIdentifier());
  }

  private function getParameterOrDie($name)
  {
    return $this->parameters[$name] ?? self::throwError($name);
  }

  protected static function throwError($missing)
  {
    throw new \InvalidArgumentException('Missing oauth config parameter ' . $missing);
  }

}
