<?php

namespace App\Security\OAuth;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericProvider;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

class AbstractProviderFactory implements LoggerAwareInterface
{
  use LoggerAwareTrait;

  protected ?Configuration $configuration = null;

  protected ?AbstractProvider $provider = null;

  protected ?string $providerIdentifier = null;

  public function getConfiguration(): Configuration
  {
    return $this->configuration;
  }

  public function getProviderIdentifier(): string
  {
    return $this->providerIdentifier;
  }

  protected function supports(): string
  {
    return GenericProvider::class;
  }

  public function instanceProvider(): AbstractProvider
  {
    if (!$this->provider instanceof AbstractProvider) {
      $configuration = $this->getConfiguration();
      $providerClass = $this->supports();
      $this->provider = new $providerClass([
        'clientId' => $configuration->getClientId(),
        'clientSecret' => $configuration->getClientSecret(),
        'redirectUri' => $configuration->getRedirectUrl(),
        'urlAuthorize' => $configuration->getUrlAuthorize(),
        'urlAccessToken' => $configuration->getUrlAccessToken(),
        'urlResourceOwnerDetails' => $configuration->getUrlResourceOwnerDetails(),
      ]);
      if ($this->provider instanceof LogoutProviderInterface) {
        $this->provider->setUrlLogout($configuration->getUrlLogout());
      }
      if ($this->provider instanceof ConfigurableProviderInterface) {
        $this->provider->setConfiguration($configuration);
      }
      if ($this->provider instanceof LoggerAwareInterface && $this->logger instanceof LoggerInterface){
        $this->provider->setLogger($this->logger);
      }
    }

    return $this->provider;
  }
}
