<?php

namespace App\Security\Voters;

use App\Entity\AdminUser;
use App\Entity\OperatoreUser;
use App\Entity\Servizio;
use App\Entity\User;
use App\Handlers\Servizio\ForbiddenAccessException;
use App\Handlers\Servizio\ServizioHandlerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ServiceVoter extends Voter
{
  public const ACCESS = 'access';
  public const IMPORT_PAYMENTS = 'import_payments';

  public const CREATE_APPLICATION = 'create_application';

  /**
   * @var Security
   */
  private Security $security;
  private ServizioHandlerRegistry $servizioHandlerRegistry;

  public function __construct(Security $security, ServizioHandlerRegistry $servizioHandlerRegistry)
  {
    $this->security = $security;
    $this->servizioHandlerRegistry = $servizioHandlerRegistry;
  }

  /**
   * @inheritDoc
   */
  protected function supports($attribute, $subject): bool
  {
    return in_array($attribute, [self::IMPORT_PAYMENTS, self::CREATE_APPLICATION, self::ACCESS])
      && $subject instanceof Servizio;
  }

  /**
   * @param string $attribute
   * @param Servizio $subject
   * @param TokenInterface $token
   * @return bool
   */
  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    switch ($attribute) {
      case self::ACCESS:
        return $this->canAccess($subject);
      case self::IMPORT_PAYMENTS:
        return $this->canImportPayments($subject, $user);
      case self::CREATE_APPLICATION:
        return $this->canCreateApplication($subject, $user);
    }

    throw new \LogicException('This code should not be reached!');
  }

  private function canImportPayments(Servizio $subject, $user): bool
  {

    $userIsAdminOrOperator = $this->security->isGranted('ROLE_ADMIN')
      || $this->security->isGranted('ROLE_OPERATORE');

    $userCanAccessToService = (
        $user instanceof OperatoreUser
        && $this->isOperatorEnabled($subject, $user)
      ) || $user instanceof AdminUser;

    return $userIsAdminOrOperator
      && $userCanAccessToService
      && ($subject->isPaymentRequired() || $subject->isPaymentDeferred());
  }

  private function canCreateApplication(Servizio $subject, $user): bool
  {

    $userIsAdminOrOperator = $this->security->isGranted('ROLE_ADMIN')
      || $this->security->isGranted('ROLE_OPERATORE');

    $userCanAccessToService = (
        $user instanceof OperatoreUser
        && $this->isOperatorEnabled($subject, $user)
      ) || $user instanceof AdminUser;

    return $userIsAdminOrOperator
      && $userCanAccessToService
      && $subject->isActive()
      && $subject->isFormio();
  }

  private function canAccess(Servizio $subject): bool
  {
    $handler = $this->servizioHandlerRegistry->getByName($subject->getHandler());

    try {
      $handler->canAccess($subject);
      return true;
    } catch (ForbiddenAccessException $e) {
      return false;
    }
  }

  /**
   * @param Servizio $servizio
   * @param OperatoreUser $operatoreUser
   * @return bool
   */
  private function isOperatorEnabled(Servizio $servizio, OperatoreUser $operatoreUser): bool
  {
    // Verifico che l'operatore sia di sistema oppure che sia abilitato al servizio
    return $operatoreUser->isSystemUser() || in_array($servizio->getId(), $operatoreUser->getAllEnabledServicesIds());
  }
}
