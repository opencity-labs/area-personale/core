<?php

namespace App\Security\Voters;


use App\Entity\AdministrativeIdentifier;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AdministrativeIdentifierVoter extends Voter
{
  public const EDIT = 'edit';
  public const VIEW = 'view';


  private Security $security;

  public function __construct(Security $security)
  {
    $this->security = $security;
  }

  protected function supports($attribute, $subject): bool
  {
    // if the attribute isn't one we support, return false
    if (!in_array($attribute, [self::EDIT, self::VIEW])) {
      return false;
    }

    // only vote on `Folder` objects
    if ($subject && !$subject instanceof AdministrativeIdentifier) {
      return false;
    }

    return true;
  }

  protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if (!$user instanceof UserInterface) {
      // the user must be logged in; if not, deny access
      return false;
    }

    // you know $subject is a Folder object, thanks to `supports()`
    /** @var AdministrativeIdentifier $administrativeIdentifier */
    $administrativeIdentifier = $subject;

    switch ($attribute) {
      case self::EDIT:
        return $this->canEdit($administrativeIdentifier, $user);
      case self::VIEW:
        return $this->canView($administrativeIdentifier, $user);

    }

    throw new \LogicException('This code should not be reached!');
  }

  private function canView(AdministrativeIdentifier $administrativeIdentifier, UserInterface $user): bool
  {
    // if they can edit, they can view
    if ($this->canEdit($administrativeIdentifier, $user)) {
      return true;
    }

    return false;
  }

  private function canEdit(AdministrativeIdentifier $administrativeIdentifier, UserInterface $user): bool
  {
    return $administrativeIdentifier->identifiesUser($user);
  }
}
