<?php

namespace App\Security;

use App\Dto\UserAuthenticationData;
use App\Entity\CPSUser;
use App\Security\AbstractAuthenticator;
use App\Services\InstanceService;
use App\Services\UserSessionService;
use GuzzleHttp\Exception\GuzzleException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use GuzzleHttp\Client;

class SiagAuthenticator extends AbstractAuthenticator
{
  public const LOGIN_ROUTE = 'login_siag';

  public const QUERY_TOKEN_PARAMETER = 'token';

  public const ATTRIBUTE_MAPPER = [
    'spidCode' => 'Code',
    'nome' => 'Firstname',
    'cognome' => 'Lastname',
    //'dataNascita' => 'Birthdate',
    self::KEY_PARAMETER_NAME => 'Fiscalcode',
    'cellulare' => 'Phone',
    'emailAddress' => 'Email'
  ];

  /**
   * @var InstanceService
   */

  private InstanceService $instanceService;

  private string $siagBaseUrl;

  private array $userdata = [];

  private LoggerInterface $logger;
  private SessionInterface $session;


  /**
   * OpenLoginAuthenticator constructor.
   * @param UrlGeneratorInterface $urlGenerator
   * @param string $loginRoute
   * @param UserSessionService $userSessionService
   * @param InstanceService $instanceService
   * @param JWTTokenManagerInterface $JWTTokenManager
   * @param string $siagBaseUrl
   * @param LoggerInterface $logger
   * @param SessionInterface $session
   */
  public function __construct(
    UrlGeneratorInterface    $urlGenerator,
    string                   $loginRoute,
    UserSessionService       $userSessionService,
    InstanceService          $instanceService,
    JWTTokenManagerInterface $JWTTokenManager,
    string                   $siagBaseUrl,
    LoggerInterface          $logger,
    SessionInterface         $session
  )
  {
    $this->urlGenerator = $urlGenerator;
    $this->loginRoute = $loginRoute;
    $this->userSessionService = $userSessionService;
    $this->instanceService = $instanceService;
    $this->JWTTokenManager = $JWTTokenManager;
    $this->siagBaseUrl = rtrim($siagBaseUrl, '/');
    $this->logger = $logger;
    $this->session = $session;
  }

  /**
   * @inheritDoc
   */
  protected function getLoginRouteSupported(): array
  {
    return [self::LOGIN_ROUTE];
  }

  public function supports(Request $request): bool
  {
    try {
      $this->checkLoginRoute();
    } catch (\Exception $e) {
      return false;
    }

    if ($this->session->has('request.return-url')) {
      $request->query->set('return-url', $this->session->get('request.return-url'));
      $this->session->remove('request.return-url');
    }

    return $request->attributes->get('_route') === self::LOGIN_ROUTE && $request->get(self::QUERY_TOKEN_PARAMETER);
  }

  /**
   * @param Request $request
   * @return array
   * @throws GuzzleException
   */
  protected function createUserDataFromRequest(Request $request): ?array
  {

    if ($request->get('test')) {
      $string = '{
        "Fiscalcode": "RSSMRA77T05H501Z",
        "Firstname": "Mario",
        "Lastname": "Rossi",
        "Birthdate": "1977-12-05T00:00:00",
        "Code": "1234567890",
        "Phone": "+393331234567",
        "Email": "indirizzo@email.com",
        "Username": null
      }';

    } else {

      $token = $request->query->get(self::QUERY_TOKEN_PARAMETER);
      $url = $this->siagBaseUrl . '/api/Auth/Profile/' . $token;

      $client = new Client();
      $response = $client->request('GET', $url, [
        'query' => [
          'onlyAuth' => 'true',
        ],
      ]);
      $string = $response->getBody()->getContents();
    }

    $userData = json_decode($string, true);
    $cfKey = self::ATTRIBUTE_MAPPER[self::KEY_PARAMETER_NAME];

    if (isset($userData[$cfKey]) && !empty($userData[$cfKey])) {
      $data = [];

      // Fixme: aggiungo un fix veloce per l'autenticazione tramite cie, da risolvere
      if (empty($userData['Code'])) {
        $userData['Code'] = 'cie_' . $userData[$cfKey];
        $this->logger->info('siag_cie_auth', ['siag_response' => $string]);
      }

      foreach (self::ATTRIBUTE_MAPPER as $k => $v) {
        $data[$k] = $userData[$v];
      }
      $dateTime = \DateTime::createFromFormat('Y-m-dTH:i:s', $userData['Birthdate']);
      if ($dateTime instanceof \DateTime) {
        $data['dataNascita'] = $dateTime->format('d/m/Y');
      }
      return $data;
    }

    $this->logger->error('SiagAuthenticator - authentication failed', ['validation_response' => $string]);

    return null;
  }

  protected function getRequestDataToStoreInUserSession(Request $request): array
  {
    return $request->headers->all();
  }

  protected function getUserAuthenticationData(Request $request, UserInterface $user)
  {
    $dateTimeObject = new \DateTime();

    $data = [
      'authenticationMethod' => CPSUser::IDP_SPID,
      'sessionId' => '',
      'spidCode' => $user->getSpidCode(),
      'spidLevel' => '',
      'instant' => $dateTimeObject->format(\DateTimeInterface::W3C),
      'sessionIndex' => '',
    ];

    return UserAuthenticationData::fromArray($data);
  }

  public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
  {
    if ($this->loginRoute !== self::LOGIN_ROUTE) {
      throw new UnauthorizedHttpException("Something went wrong in authenticator");
    }

    if ($request->get(self::QUERY_TOKEN_PARAMETER)) {
      throw new UnauthorizedHttpException("Something went wrong in authenticator");
    }

    $locale = $request->query->get('locale', $request->getLocale());

    $returnUrl = $request->query->get('return-url', false);
    if ($returnUrl) {
      $this->session->set('request.return-url', $returnUrl);
    }

    return new RedirectResponse($this->siagBaseUrl . '/api/Auth/Login?targetUrl=' . $request->getUri() . '&acceptedAuthTypes=SPID%20CNS%20CIE&onlyAuth=true&lang=' . $locale);
  }
}
