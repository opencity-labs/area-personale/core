<?php

namespace App\Security\Cohesion;

use DOMDocument;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;


/**
 * Classe per la gestione del SSO di Cohesion2
 * @link http://cohesion.regione.marche.it/cohesioninformativo/
 */
class Cohesion2Client
{
  public const COHESION2_CHECK = 'https://cohesion2.regione.marche.it/sso/Check.aspx?auth=';
  public const COHESION2_LOGIN = 'https://cohesion2.regione.marche.it/SA/AccediCohesion.aspx?auth=';
  public const COHESION2_WS = 'https://cohesion2.regione.marche.it/sso/WsCheckSessionSSO.asmx';
  public const COHESION2_WEB = 'https://cohesion2.regione.marche.it/SSO/webCheckSessionSSO.aspx';
  public const COHESION2_SAML20_CHECK = 'https://cohesion2.regione.marche.it/SPManager/WAYF.aspx?auth=';
  public const COHESION2_SAML20_WS = 'https://cohesion2.regione.marche.it/SPManager/wsCheckSessionSPM.asmx';
  public const COHESION2_SAML20_WEB = 'https://cohesion2.regione.marche.it/SPManager/webCheckSessionSSO.aspx';
  public const SESSION_NAME = 'cohesion2';
  public const EIDAS_FLAG = 'eidas=1';
  public const PURPOSE_FLAG = 'purpose=';


  private string $authRestriction = '0,1,2,3';
  private $cert_file = null;
  private $key_file = null;
  private bool $sso = true;
  private bool $saml20 = false;
  private bool $eIDAS = false;
  private bool $SPIDProPurpose = false;


  /**
   * ID sessione SSO
   * @var string
   */
  public $id_sso;

  /**
   * ID sessione ASPNET
   * @var string
   */
  public $id_aspnet;

  /**
   * Username utente autenticato in Cohesion
   * @var string
   */
  public $username;

  /**
   * Profilo dell'utente come fornito da Cohesion. Valori forniti (alcuni non
   * sempre valorizzati):
   * titolo, nome, cognome, sesso, login, codice_fiscale, telefono,
   * localita_nascita, provincia_nascita, cap_nascita, regione_nascita,
   * data_nascita, nazione_nascita, gruppo, ruolo, email, email_certificata,
   * telefono_ufficio, fax_ufficio, numero_cellulare, indirizzo_residenza,
   * localita_residenza, provincia_residenza, cap_residenza, regione_residenza,
   * nazione_residenza, professione, settore_azienda, profilo_familiare,
   * tipo_autenticazione (PW,CF,PIN,DRM)
   * @var array
   */
  public $profile;
  private string $siteId;
  private SessionInterface $session;
  private RouterInterface $router;
  private LoggerInterface $logger;

  public function __construct(
    RouterInterface  $router,
    SessionInterface $session,
    LoggerInterface  $logger,
    string           $siteId = 'TEST'
  )
  {
    $this->router = $router;
    $this->session = $session;
    $this->siteId = $siteId;

    //controllo se la sessione è stata avviata
    if ($this->isAuth()) {
      //Utente già autenticato, ripristino sessione
      $this->retrieveFromSession();
    }
    $this->logger = $logger;
  }

  private function storeToSession(): void
  {
    $data = [
      'id_sso' => $this->id_sso,
      'id_aspnet' => $this->id_aspnet,
      'username' => $this->username,
      'profile' => $this->profile,
    ];
    $this->session->set(self::SESSION_NAME, $data);
  }

  private function retrieveFromSession(): void
  {
    $data = $this->session->get(self::SESSION_NAME, []);
    try {
      $this->id_sso = $data['id_sso'];
      $this->id_aspnet = $data['id_aspnet'];
      $this->username = $data['username'];
      $this->profile = $data['profile'];
    } catch (\Exception $e) {
    }
  }

  /**
   * Imposta i metodi di autenticazione permessi
   * @param string $authRestriction (separare le varie scelte con una virgola)
   * Valore di default: 0,1,2,3
   * 0 indica di mostrare l’autenticazione con Utente e Password
   * 1 indica di mostrare l’autenticazione con Utente, Password e PIN
   * 2 indica di mostrare l’autenticazione con Smart Card
   * 3 indica di mostrare l’autenticazione di Dominio (valida solo per utenti
   * interni alla rete regionale)
   * NON TUTTE LE COMBINAZIONI VENGONO ACCETTATE (es. 0,1 vengono comunque
   * mostrati tutti i metodi)
   * @return Cohesion2Client
   */
  public function setAuthRestriction($authRestriction): Cohesion2Client
  {
    if ($authRestriction) {
      $this->authRestriction = $authRestriction;
    }
    return $this;
  }

  /**
   * Controlla se l'utente è già stato autenticato
   * @return boolean
   */
  public function isAuth(): bool
  {
    return $this->session->has(self::SESSION_NAME);
  }

  /**
   * Imposta il ceritificato per invocare il WS del SSO. (Opzionale)
   * @param string $certFilePath File .pem contenente il certificato
   * @param string $keyFilePath File .pem contenente la chiave privata
   * @return Cohesion2Client
   */
  public function setCertificate($certFilePath, $keyFilePath): Cohesion2Client
  {
    $this->cert_file = $certFilePath;
    $this->key_file = $keyFilePath;
    return $this;
  }

  /**
   * Attiva o meno l'uso del SingleSignOn. Se disabilitato, l'utente verrà
   * sempre reindirizzato alla pagina di login senza controllare se esso
   * risulta autenticato o meno tramite SSO
   * @param boolean $on
   * @return Cohesion2Client
   */
  public function useSSO(bool $on = true): Cohesion2Client
  {
    $this->sso = $on;
    return $this;
  }

  /**
   * Abilita o meno il funzionamento del SSO in modalità SAML2.0. Questa modalità
   * attiva, se non attivato, il SSO.
   * @param boolean $on
   * @return Cohesion2Client
   */
  public function useSAML20(bool $on = true): Cohesion2Client
  {
    $this->useSSO(true);
    $this->saml20 = $on;

    return $this;
  }

  /**
   * Abilita il login eIDAS (e automaticamente la modalità SAML2.0).
   * @return Cohesion2Client
   */
  public function enableEIDASLogin(): Cohesion2Client
  {

    $this->useSAML20(true);
    $this->eIDAS = true;

    return $this;
  }

  /**
   * Abilita il login con SPID Professionale (PF,PG,LP) e automaticamente la modalità SAML2.0).
   * @param string[] $SPIDProPurposes inserire un array con i purpose richiesti. Default: PF - SPID per Persone Fisiche ad Uso Professionale.
   * I possibili valori sono: LP, PG, PF, PX Così come indicato nell'avviso SPID 18 v2: https://www.agid.gov.it/sites/default/files/repository_files/spid-avviso-n18_v.2-_autenticazione_persona_giuridica_o_uso_professionale_per_la_persona_giuridica.pdf
   * @return Cohesion2Client
   */
  public function enableSPIDProLogin(array $SPIDProPurposes = array("PF")): Cohesion2Client
  {

    $this->useSAML20(true);
    $this->SPIDProPurpose = implode("|", $SPIDProPurposes);

    return $this;
  }

  /**
   * Chiude la sessione locale e quella del SSO
   * @return void
   */
  public function logout(): void
  {
    if ($this->isAuth()) {

      $this->session->remove(self::SESSION_NAME);

      $data = array('Operation' => 'LogoutSito', 'IdSessioneSSO' => $this->id_sso, 'IdSessioneASPNET' => $this->id_aspnet);
      $context = stream_context_create(array(
        'http' => array(
          'header' => "Content-type: application/x-www-form-urlencoded\r\n",
          'method' => 'POST',
          'content' => http_build_query($data),
        ),
      ));
      file_get_contents($this->saml20 ? self::COHESION2_SAML20_WEB : self::COHESION2_WEB,
        false,
        $context);

    }
  }

  /**
   * Chiude la sessione locale lasciando aperta quella del SSO
   * @return void
   */
  public function logoutLocal(): void
  {
    if ($this->isAuth()) {
      $this->session->remove(self::SESSION_NAME);
    }
  }

  public function generateAuthRequest(string $loginRoute): string
  {
    $urlPagina = $this->router->generate(
      $loginRoute,
      ['cohesionCheck' => '1',],
      UrlGeneratorInterface::ABSOLUTE_URL,
    );

    $xmlAuth = '<dsAuth xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://tempuri.org/Auth.xsd">
            <auth>
                <user />
                <id_sa />
                <id_sito>' . $this->siteId . '</id_sito>
                <esito_auth_sa />
                <id_sessione_sa />
                <id_sessione_aspnet_sa />
                <url_validate><![CDATA[' . $urlPagina . ']]></url_validate>
                <url_richiesta><![CDATA[' . $urlPagina . ']]></url_richiesta>
                <esito_auth_sso />
                <id_sessione_sso />
                <id_sessione_aspnet_sso />
                <stilesheet>AuthRestriction=' . $this->authRestriction . ($this->eIDAS ? ";" . self::EIDAS_FLAG : "") . ($this->SPIDProPurpose ? ";" . self::PURPOSE_FLAG . $this->SPIDProPurpose : "") . '</stilesheet>
                <AuthRestriction xmlns="">' . $this->authRestriction . '</AuthRestriction>
            </auth>
        </dsAuth>';
    //file_put_contents('log.txt',$xmlAuth."\n",FILE_APPEND);
    $auth = urlencode(base64_encode($xmlAuth));
    if ($this->saml20) {
      $urlLogin = self::COHESION2_SAML20_CHECK . $auth;
    } else {
      $urlLogin = ($this->sso) ? self::COHESION2_CHECK . $auth : self::COHESION2_LOGIN . $auth;
    }
    //file_put_contents('log.txt',$urlLogin."\n",FILE_APPEND);
    return $urlLogin;
  }


  /** @throws Exception */
  public function verify($auth): bool
  {
    //file_put_contents('log.txt',__METHOD__."\n",FILE_APPEND);
    $xml = trim(base64_decode($auth));
    //file_put_contents('log.txt',$xml."\n",FILE_APPEND);
    $domXML = new DOMDocument;
    $domXML->loadXML($xml);

    $this->id_sso = $domXML->getElementsByTagName('id_sessione_sso')->item(0)->nodeValue;
    $this->id_aspnet = $domXML->getElementsByTagName('id_sessione_aspnet_sso')->item(0)->nodeValue;
    $this->username = $domXML->getElementsByTagName('user')->item(0)->nodeValue;
    $esito = $domXML->getElementsByTagName('esito_auth_sso')->item(0)->nodeValue;

    if ($esito !== 'OK') {
      $this->logger->error('CohesionAuthenticator - authentication failed', [
        'id_sso' => $this->id_sso,
        'id_aspnet' => $this->id_aspnet,
        'username' => $this->username,
        'auth_result' => $esito,
      ]);
      return false;
    }

    $data = array('Operation' => 'GetCredential', 'IdSessioneSSO' => $this->id_sso, 'IdSessioneASPNET' => $this->id_aspnet);
    $context = stream_context_create(array(
      'http' => array(
        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
        'method' => 'POST',
        'content' => http_build_query($data),
      ),
    ));
    $result = file_get_contents($this->saml20 ? self::COHESION2_SAML20_WEB : self::COHESION2_WEB, false, $context);
    $domXML = new DOMDocument;
    $domXML->loadXML($result);

    $profilo = simplexml_import_dom($domXML);
    $base = current($profilo->xpath('//base'));
    if ($base->login) {
      $resp = array();
      foreach ($base->children() as $node) {
        $resp[$node->getName()] = (string)$node;
      }
      $this->profile = $resp;

      $this->storeToSession();

      return true;

    } else {
      throw new Exception('Impossibile recuperare il profilo utente da Cohesion2');
    }
  }
}

