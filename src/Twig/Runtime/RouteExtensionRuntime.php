<?php

declare(strict_types=1);

namespace App\Twig\Runtime;

use App\Entity\Pratica;
use App\Services\Manager\PraticaManager;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\RuntimeExtensionInterface;

class RouteExtensionRuntime implements RuntimeExtensionInterface
{

  private PraticaManager $praticaManager;

  public function __construct(PraticaManager $praticaManager)
  {

    $this->praticaManager = $praticaManager;
  }

  public function getApplicationDetailUrl(Pratica $pratica): string
  {
    return $this->praticaManager->getApplicationDetailUrl($pratica);
  }
}
