<?php

namespace App\Services\Manager;

use App\Entity\Calendar;
use App\Entity\NotificationSetting;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class NotificationSettingsManager
{

  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;

  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  public function getNotificationSettingsObjectIds(User $user): array
  {
    $calendars = $this->entityManager->getRepository(Calendar::class)->findByModerator($user);
    $enabledNotifications = [];
    $disabledNotifications = [];
    if ($user->getNotificationSettings() !== null) {
      /** @var NotificationSetting $setting */
      foreach ($user->getNotificationSettings() as $setting) {
        if ($setting->getEmailNotification()) {
          $enabledNotifications []= $setting->getObjectId();
        } else {
          $disabledNotifications []= $setting->getObjectId();
        }
      }
    }

    // Per retrocompatibilità e per gestire nuove associazioni l'assenza di preferenza viene considerata come spedizione Abolitata
    foreach ($calendars as $calendar) {
      $calendarId = $calendar->getId();
      if (!in_array($calendarId, $enabledNotifications) && !in_array($calendarId, $disabledNotifications)) {
        $enabledNotifications []= $calendar->getId();
      }
    }

    return $enabledNotifications;

  }


}
