<?php


namespace App\Services\Manager;


use App\Entity\Message;
use App\Event\KafkaEvent;
use App\Event\MessageEvent;
use App\Services\InstanceService;
use App\Services\IOService;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MessageManager
{
  /** @var EntityManagerInterface */
  private EntityManagerInterface $entityManager;

  /** @var TranslatorInterface */
  private TranslatorInterface $translator;

  /** @var InstanceService */
  private InstanceService $instanceService;

  /** @var RouterInterface */
  private RouterInterface $router;

  /** @var MailerService */
  private MailerService $mailerService;

  /** @var FlashBagInterface */
  private FlashBagInterface $flashBag;

  /** @var EventDispatcherInterface */
  private EventDispatcherInterface $dispatcher;

  /** @var LoggerInterface  */
  private LoggerInterface $logger;

  private string $defaultSender;
  private IOService $ioService;


  /**
   * MessageManager constructor.
   * @param EntityManagerInterface $entityManager
   * @param TranslatorInterface $translator
   * @param InstanceService $instanceService
   * @param RouterInterface $router
   * @param MailerService $mailerService
   * @param IOService $ioService
   * @param FlashBagInterface $flashBag
   * @param string $defaultSender
   * @param EventDispatcherInterface $dispatcher
   * @param LoggerInterface $logger
   */
  public function __construct(
    EntityManagerInterface   $entityManager,
    TranslatorInterface      $translator,
    InstanceService          $instanceService,
    RouterInterface          $router,
    MailerService            $mailerService,
    IOService                $ioService,
    FlashBagInterface        $flashBag,
    string                   $defaultSender,
    EventDispatcherInterface $dispatcher,
    LoggerInterface          $logger
  )
  {
    $this->entityManager = $entityManager;
    $this->translator = $translator;
    $this->instanceService = $instanceService;
    $this->router = $router;
    $this->mailerService = $mailerService;
    $this->ioService = $ioService;
    $this->flashBag = $flashBag;
    $this->defaultSender = $defaultSender;
    $this->dispatcher = $dispatcher;
    $this->logger = $logger;
  }


  /**
   * @param Message $message
   * @param bool|null $flush
   */
  public function save(Message $message, ?bool $flush = true)
  {
    $this->entityManager->persist($message);
    if ($flush) {
      $this->entityManager->flush();
    }

    $message = $this->dispatchMailForMessage($message);

    $this->dispatcher->dispatch(new KafkaEvent($message), KafkaEvent::NAME);
    $this->dispatcher->dispatch(new MessageEvent($message), MessageEvent::NAME);
  }

  /**
   * @param Message $message
   * @return Message
   * @throws BadRequestHttpException
   */
  public function dispatchMailForMessage(Message $message): Message
  {
    $application = $message->getApplication();

    // Invio il messaggio solo se è pubblico, se non è stato ancora inviato e ci sono dei destinatari validi
    if ($message->getVisibility() !== Message::VISIBILITY_APPLICANT || $message->getSentAt() || $application->getServizio()->getDisableMessageNotifications()) {
      $this->logger->info("Message {$message->getId()} should not be sent", [
        'message_id' => $message->getId(),
        'application_id' => $application->getId(),
        'visibility' => $message->getVisibility(),
        'sent_at' => $message->getSentAt(),
        'disable_message_notifications_service' => $application->getServizio()->getDisableMessageNotifications(),
      ]);
      return $message;
    }

    $receivers = $message->getReceivers();
    // Non ci sono destinatari validi per ricevere la notifica del messaggio
    if (empty($receivers)) {
      $this->logger->info("Unable to dispatch message {$message->getId()}: missing receivers for application {$application->getId()}");
      return $message;
    }


    $subject = $this->translator->trans('pratica.messaggi.oggetto', ['%pratica%' => $application]);
    $mess = $this->translator->trans('pratica.messaggi.messaggio', [
      '%message%' => $message->getMessage(),
      '%link%' => $this->router->generate('track_message', ['id' => $message->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
    ]);
    $defaultSender = $this->defaultSender;
    $instance = $this->instanceService->getCurrentInstance();

    $userReceiver = array_shift($receivers);
    $bccReceivers = [];

    $message->setEmail($userReceiver->getEmail());

    foreach ($receivers as $bccReceiver) {
      $bccReceivers[] = $bccReceiver->getEmail();
    }

    $this->mailerService->dispatchMail(
      $defaultSender,
      $message->isInbound() ? $message->getAuthor()->getFullName() : $instance->getName(),
      $message->isInbound() ? $userReceiver->getEmail() : $userReceiver->getEmailContatto(),
      $userReceiver->getFullName(),
      $mess,
      $subject,
      $instance,
      $message->getCallToAction(),
      $bccReceivers
    );

    $message->setSentAt(time());
    $this->entityManager->persist($message);
    $this->entityManager->flush();

    // Todo: viene inviato solo nel caso dell'operatore, è veramente  necessario?
    if (!$message->isInbound()) {
      // Invio messaggio IO e feedback solo se il messaggio è in uscita ovvero è rivolto al cittadino
      if ($message->getApplication()->getServizio()->isIOEnabled()) {
        $this->ioService->sendMessageForPratica(
          $message->getApplication(),
          $mess,
          $subject
        );
      }
      $this->flashBag->add('info', $this->translator->trans('operatori.messaggi.feedback_inviato', ['%email%' => $message->getApplication()->getUser()->getEmailContatto()]));
    }

    // Restituisco il messaggio aggiornato con le informazioni relative all'email
    return $message;
  }
}
