<?php


namespace App\Services\Manager;


use App\Entity\AdminUser;
use App\Entity\OperatoreUser;
use App\Entity\Servizio;
use App\Entity\User;
use App\Event\KafkaEvent;
use App\Event\SecurityEvent;
use App\Model\Security\SecurityLogInterface;
use App\Services\InstanceService;
use App\Services\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class UserManager
{
  private EntityManagerInterface $entityManager;
  private TranslatorInterface $translator;
  private InstanceService $instanceService;
  private RouterInterface $router;
  private MailerService $mailerService;
  private TokenGeneratorInterface $tokenGenerator;
  private UserPasswordHasherInterface $passwordHasher;
  private string $defaultSender;
  private EventDispatcherInterface $dispatcher;
  private Environment $templating;


  /**
   * UserManager constructor.
   * @param EntityManagerInterface $entityManager
   * @param TranslatorInterface $translator
   * @param InstanceService $instanceService
   * @param RouterInterface $router
   * @param MailerService $mailerService
   * @param TokenGeneratorInterface $tokenGenerator
   * @param UserPasswordHasherInterface $passwordHasher
   * @param EventDispatcherInterface $dispatcher
   * @param Environment $templating
   * @param string $defaultSender
   */
  public function __construct(
    EntityManagerInterface      $entityManager,
    TranslatorInterface         $translator,
    InstanceService             $instanceService,
    RouterInterface             $router,
    MailerService               $mailerService,
    TokenGeneratorInterface     $tokenGenerator,
    UserPasswordHasherInterface $passwordHasher,
    EventDispatcherInterface    $dispatcher,
    Environment                 $templating,
    string                      $defaultSender
  )
  {
    $this->entityManager = $entityManager;
    $this->translator = $translator;
    $this->instanceService = $instanceService;
    $this->router = $router;
    $this->mailerService = $mailerService;
    $this->tokenGenerator = $tokenGenerator;
    $this->passwordHasher = $passwordHasher;
    $this->defaultSender = $defaultSender;
    $this->dispatcher = $dispatcher;
    $this->templating = $templating;
  }

  /**
   * @param UserInterface $user
   * @param bool $flush
   * @return void
   */
  public function save(UserInterface $user, ?bool $flush = true): void
  {
    $this->entityManager->persist($user);
    if ($flush) {
      $this->entityManager->flush();
    }

    if ($user instanceof AdminUser) {
      $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_ADMIN_CREATED, $user));
    } elseif ($user instanceof OperatoreUser) {
      $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_OPERATOR_CREATED, $user));
    }

    $this->dispatcher->dispatch(new KafkaEvent($user), KafkaEvent::NAME);
  }

  /**
   * @param UserInterface $user
   * @return void
   */
  public function remove(UserInterface $user): void
  {
    $this->entityManager->remove($user);
    $this->entityManager->flush();

    if ($user instanceof AdminUser) {
      $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_ADMIN_REMOVED, $user));
    } elseif ($user instanceof OperatoreUser) {
      $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_OPERATOR_REMOVED, $user));
    }
  }

  /**
   * @throws RuntimeError
   * @throws SyntaxError
   * @throws LoaderError
   */
  public function resetPasswordRequest(array $users, $email, $locale): void
  {

    if (empty($users)) {
      return;
    }

    foreach ($users as $user) {
      $user->setConfirmationToken($this->tokenGenerator->generateToken());
      $user->setPasswordRequestedAt(new \DateTime());
      $this->entityManager->persist($user);
    }
    $this->entityManager->flush();

    $tenant = $this->instanceService->getCurrentInstance();
    $message = $this->templating->render('Emails/Security/_reset_password_request.html.twig', [
      'users' => $users,
      'tenant' => $tenant,
      'locale' => $locale,
    ]);

    $callToActions = [];
    foreach ($users as $user) {
      $callToActions[] = [
        'link' => $this->router->generate(
          'reset_password_confirm',
          ['token' => $user->getConfirmationToken()],
          UrlGeneratorInterface::ABSOLUTE_URL
        ),
        'label' => "{$user->getFullName()} - ({$user->getUsername()})",
      ];
    }

    $this->mailerService->dispatchMail(
      $this->defaultSender,
      $tenant->getName(),
      $email,
      '',
      $message,
      $this->translator->trans('user.reset_password.subject') . ' - ' . date('d-m-Y'),
      $this->instanceService->getCurrentInstance(),
      $callToActions,
      [],
      $locale
    );

    $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_RESET_PASSWORD_REQUEST, ['email' => $email]));
  }

  /**
   * @param User $user
   * @param User|null $requester
   * @return void
   */
  public function resetPassword(User $user, User $requester = null): void
  {
    $user->setConfirmationToken($this->tokenGenerator->generateToken());
    $user->setPasswordRequestedAt(new \DateTime());
    $this->entityManager->flush();

    $receiver = $requester ?? $user;
    // Se l'utente per cui è stata reimpostata la password non è un operatore oppure non è un utente di sistema
    // l'email viene sempre inviata all'utente
    if (!$user instanceof OperatoreUser || !$user->isSystemUser()) {
      $receiver = $user;
    }

    $tenantName = $this->instanceService->getCurrentInstance()->getName();
    $this->mailerService->dispatchMail(
      $this->defaultSender,
      $tenantName,
      $receiver->getEmail(),
      $receiver->getFullName(),
      $this->translator->trans('user.reset_password.message', ['%tenant_name%' => $tenantName]),
      $this->translator->trans('user.reset_password.subject') . ' - ' . date('d-m-Y'),
      $this->instanceService->getCurrentInstance(),
      [
        [
          'link' => $this->router->generate(
            'reset_password_confirm',
            ['token' => $user->getConfirmationToken()],
            UrlGeneratorInterface::ABSOLUTE_URL
          ),
          'label' => $this->translator->trans('user.reset_password.btn'),
        ],
      ]
    );

    $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_RESET_PASSWORD_REQUEST, ['email' => $user->getEmail()]));
  }

  /**
   * @param User $user
   * @param $plainPassword
   * @return void
   */
  public function changePassword(User $user, $plainPassword): void
  {
    $encodedPassword = $this->passwordHasher->hashPassword($user, $plainPassword);
    $user->setPassword($encodedPassword);
    $user->setConfirmationToken(null);
    $user->setLastChangePassword();
    $this->entityManager->persist($user);
    $this->entityManager->flush();

    $this->dispatcher->dispatch(new SecurityEvent(SecurityLogInterface::ACTION_USER_RESET_PASSWORD_SUCCESS, ['email' => $user->getEmail()]));
  }
}
