<?php

namespace App\Services\Manager;

use Jose\Component\KeyManagement\JWKFactory;
use Psr\Log\LoggerInterface;

class JwksManager
{

  private ?string $publicKey;
  private string $kid;
  private LoggerInterface $logger;

  public function __construct(?string $publicKey = null, string $kid, LoggerInterface $logger)
  {
    $this->publicKey = $publicKey;
    $this->kid = $kid;
    $this->logger = $logger;
  }

  public function generateJwks(): ?array
  {
    if ($this->publicKey && is_file($this->publicKey) && is_readable($this->publicKey)) {

      try {

        if ($this->isCertificate($this->publicKey)) {
          $jwkData = JWKFactory::createFromCertificateFile($this->publicKey, ['use' => 'sig']);
          return [
            'keys' => [
              [
                'alg' => 'RS256',
                'kid' => $this->kid,
                'kty' => $jwkData->get('kty'),
                'n' => $jwkData->get('n'),
                'e' => $jwkData->get('e'),
                'use' => $jwkData->get('use'),
              ],
            ],
          ];
        }

        if ($this->isPublicKey($this->publicKey)) {
          $jwkData = $this->createFromPublicKeyFile($this->publicKey);
          return [
            'keys' => [
              [
                'alg' => 'RS256',
                'kid' => $this->kid,
                'kty' => $jwkData['kty'],
                'n' => $jwkData['n'],
                'e' => $jwkData['e'],
                'use' => 'sig',
              ],
            ],
          ];
        }
      } catch (\Exception $e) {
        $this->logger->error('Error generating JWK keys', [
          'exception' => $e
        ]);
      }
    }
    return [];
  }

  private function isCertificate($filePath): bool
  {
    // Read the first few bytes of the file
    $fileContents = file_get_contents($filePath, false, null, 0, 1024);

    // Check if it contains the BEGIN CERTIFICATE marker
    return strpos($fileContents, '-----BEGIN CERTIFICATE-----') !== false;
  }

  private function isPublicKey($filePath): bool
  {
    // Read the first few bytes of the file
    $fileContents = file_get_contents($filePath, false, null, 0, 1024);

    // Check if it contains the BEGIN PUBLIC KEY marker
    return strpos($fileContents, '-----BEGIN PUBLIC KEY-----') !== false;
  }

  private function createFromPublicKeyFile($filePath): array
  {
    $publicKey = file_get_contents($filePath);

    // Get details of the public key
    $keyDetails = openssl_pkey_get_details(openssl_pkey_get_public($publicKey));

    return [
      'kty' => 'RSA',
      'n'   => rtrim(strtr(base64_encode($keyDetails['rsa']['n']), '+/', '-_'), '='),
      'e'   => rtrim(strtr(base64_encode($keyDetails['rsa']['e']), '+/', '-_'), '='),
    ];
  }
}
