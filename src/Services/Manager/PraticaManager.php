<?php


namespace App\Services\Manager;

use App\Dto\ApplicationOutcome;
use App\Entity\AdminUser;
use App\Entity\Allegato;
use App\Entity\AllegatoMessaggio;
use App\Entity\CPSUser;
use App\Entity\FormIO;
use App\Entity\GeographicArea;
use App\Entity\Message;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\PraticaRepository;
use App\Entity\RichiestaIntegrazione;
use App\Entity\RichiestaIntegrazioneDTO;
use App\Entity\RispostaIntegrazione;
use App\Entity\RispostaIntegrazioneRepository;
use App\Entity\ScheduledAction;
use App\Entity\Servizio;
use App\Entity\StatusChange;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Event\GenerateApplicationReceiptEvent;
use App\Event\KafkaEvent;
use App\Exception\ApplicationException;
use App\FormIO\ExpressionValidator;
use App\FormIO\Mapper\FormIOBeneficiaryToBeneficiaryDataMapper;
use App\FormIO\Schema;
use App\FormIO\SchemaFactoryInterface;
use App\Logging\LogConstants;
use App\Model\Service\AssignmentConfig;
use App\Model\Transition;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use App\Protocollo\Exception\AlreadySentException;
use App\Protocollo\Exception\InvalidStatusException;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\TacitOutcomeService;
use App\Services\BackOfficeCollection;
use App\Services\CPSUserProvider;
use App\Services\GeoService;
use App\Services\InstanceService;
use App\Services\ModuloPdfBuilderService;
use App\Services\PaymentService;
use App\Services\PraticaStatusService;
use App\Utils\FiscalCodeUtils;
use App\Utils\FormIOUtils;
use App\Utils\FormUtils;
use App\Utils\UploadedBase64File;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Flagception\Manager\FeatureManagerInterface;
use Gotenberg\Exceptions\GotenbergApiErroed;
use Http\Discovery\Exception\NotFoundException;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class PraticaManager
{

  // Mappa una chiave "human redable" su quelle pessime usate da formio
  public const APPLICANT_KEYS = [
    'name' => 'applicant.data.completename.data.name',
    'surname' => 'applicant.data.completename.data.surname',
    'email' => 'applicant.data.email_address',
    'fiscal_code' => 'applicant.data.fiscal_code.data.fiscal_code',
  ];

  // Serve per mappare i dati dello schema con quelli dell'utente
  public const APPLICATION_USER_MAP = [
    'applicant.completename.name' => 'getNome',
    'applicant.completename.surname' => 'getCognome',
    'applicant.Born.natoAIl' => 'getDataNascita',
    'applicant.Born.place_of_birth' => 'getLuogoNascita',
    'applicant.fiscal_code.fiscal_code' => 'getCodiceFiscale',
    'applicant.address.address' => 'getIndirizzoResidenza',
    'applicant.address.house_number' => 'getCivicoResidenza',
    'applicant.address.municipality' => 'getCittaResidenza',
    'applicant.address.postal_code' => 'getCapResidenza',
    'applicant.address.county' => 'getProvinciaResidenza',
    'applicant.address.country' => 'getStatoResidenza',
    'applicant.email_address' => 'getEmail',
    'applicant.email_repeat' => 'getEmail',
    'applicant.cell_number' => 'getCellulare',
    'applicant.phone_number' => 'getTelefono',
    'applicant.gender.gender' => 'getSessoAsString',
    'cell_number' => 'getCellulare',
  ];

  /** @var */
  private $schema = null;

  private $flatSchema = null;

  private ModuloPdfBuilderService $moduloPdfBuilderService;

  private PraticaStatusService $praticaStatusService;

  private LoggerInterface $logger;

  private EntityManagerInterface $entityManager;

  private InstanceService $is;

  private TranslatorInterface $translator;

  private SchemaFactoryInterface $schemaFactory;

  private MessageManager $messageManager;
  private PaymentService $paymentService;

  private ExpressionValidator $expressionValidator;

  private UserManager $userManager;
  private GeoService $geoService;
  private ProfileBlockManager $profileBlockManager;
  private CPSUserProvider $cpsUserProvider;
  private EventDispatcherInterface $dispatcher;
  private PdndManager $pdndManager;
  private RouterInterface $router;
  /**
   * @var BackOfficeCollection
   */
  private $backOfficeCollection;
  private FeatureManagerInterface $featureManager;
  private TacitOutcomeService $tacitOutcomeService;

  /**
   * PraticaManagerService constructor.
   * @param EntityManagerInterface $entityManager
   * @param InstanceService $instanceService
   * @param ModuloPdfBuilderService $moduloPdfBuilderService
   * @param PraticaStatusService $praticaStatusService
   * @param TranslatorInterface $translator
   * @param LoggerInterface $logger
   * @param SchemaFactoryInterface $schemaFactory
   * @param MessageManager $messageManager ,
   * @param PaymentService $paymentService ,
   * @param ExpressionValidator $expressionValidator
   * @param UserManager $userManager
   * @param GeoService $geoService
   * @param ProfileBlockManager $profileBlockManager
   * @param CPSUserProvider $cpsUserProvider
   * @param EventDispatcherInterface $dispatcher
   * @param PdndManager $pdndManager
   * @param BackOfficeCollection $backOfficeCollection
   * @param RouterInterface $routerInterface
   * @param FeatureManagerInterface $featureManager
   * @param TacitOutcomeService $tacitOutcomeService
   */
  public function __construct(
    EntityManagerInterface   $entityManager,
    InstanceService          $instanceService,
    ModuloPdfBuilderService  $moduloPdfBuilderService,
    PraticaStatusService     $praticaStatusService,
    TranslatorInterface      $translator,
    LoggerInterface          $logger,
    SchemaFactoryInterface   $schemaFactory,
    MessageManager           $messageManager,
    PaymentService           $paymentService,
    ExpressionValidator      $expressionValidator,
    UserManager              $userManager,
    GeoService               $geoService,
    ProfileBlockManager      $profileBlockManager,
    CPSUserProvider          $cpsUserProvider,
    EventDispatcherInterface $dispatcher,
    PdndManager              $pdndManager,
    BackOfficeCollection     $backOfficeCollection,
    RouterInterface          $routerInterface,
    FeatureManagerInterface  $featureManager,
    TacitOutcomeService      $tacitOutcomeService
  )
  {
    $this->moduloPdfBuilderService = $moduloPdfBuilderService;
    $this->praticaStatusService = $praticaStatusService;
    $this->logger = $logger;
    $this->entityManager = $entityManager;
    $this->is = $instanceService;
    $this->translator = $translator;
    $this->schemaFactory = $schemaFactory;
    $this->messageManager = $messageManager;
    $this->paymentService = $paymentService;
    $this->expressionValidator = $expressionValidator;
    $this->userManager = $userManager;
    $this->geoService = $geoService;
    $this->profileBlockManager = $profileBlockManager;
    $this->cpsUserProvider = $cpsUserProvider;
    $this->dispatcher = $dispatcher;
    $this->pdndManager = $pdndManager;
    $this->backOfficeCollection = $backOfficeCollection;
    $this->router = $routerInterface;
    $this->featureManager = $featureManager;
    $this->tacitOutcomeService = $tacitOutcomeService;
  }

  /**
   * @return mixed
   */
  public function getSchema()
  {
    return $this->schema;
  }

  /**
   * @param mixed $schema
   */
  public function setSchema($schema): void
  {
    $this->schema = $schema;
  }

  /**
   * @return null
   */
  public function getFlatSchema()
  {
    return $this->flatSchema;
  }

  /**
   * @param null $flatSchema
   */
  public function setFlatSchema($flatSchema): void
  {
    $this->flatSchema = $flatSchema;
  }


  /**
   * @return ProfileBlockManager
   */
  public function getProfileBlockManager(): ProfileBlockManager
  {
    return $this->profileBlockManager;
  }

  public function save(Pratica $pratica): void
  {
    $this->entityManager->persist($pratica);
    $this->entityManager->flush();
  }

  public function saveAndDispatchEvent(Pratica $pratica)
  {
    $this->save($pratica);

    $this->entityManager->refresh($pratica);

    if ($pratica->getStatus() >= Pratica::STATUS_DRAFT) {
      $this->dispatcher->dispatch(new KafkaEvent($pratica), KafkaEvent::NAME);
    }
  }

  /**
   * @param Pratica $pratica
   * @throws Exception
   */
  public function finalizeSubmission(Pratica $pratica): void
  {

    /** @var PraticaRepository $repo */
    $repo = $this->entityManager->getRepository(Pratica::class);

    if ($pratica->getFolderId() === null) {
      if ($pratica->isFormIOType()) {
        $schema = $this->schemaFactory->createFromService($pratica->getServizio());
        if (!empty($pratica->getDematerializedForms()['data'])) {
          $data = $schema->getDataBuilder()->setDataFromArray($pratica->getDematerializedForms()['data'])->toFullFilledFlatArray();
          if (isset($data['related_applications']) && $data['related_applications']) {
            $parentId = trim($data['related_applications']);
            $parent = $repo->find($parentId);
            if ($parent instanceof Pratica) {
              $pratica->setParent($parent);
              $pratica->setServiceGroup($parent->getServizio()->getServiceGroup());
              $pratica->setFolderId($parent->getFolderId());
            }
          }
        }
      } else {
        $pratica->setServiceGroup($pratica->getServizio()->getServiceGroup());
        $pratica->setFolderId($repo->getFolderForApplication($pratica));
      }
    }

    if (
      $pratica->getStatus() == Pratica::STATUS_DRAFT
      || $pratica->getStatus() == Pratica::STATUS_STAMPS_PAYMENT_PENDING
      || ($pratica->getStatus() == Pratica::STATUS_PAYMENT_PENDING && $pratica->getEsito() === null)
    ) {

      $pratica->setSubmissionTime(time());
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_PRE_SUBMIT);

      // Emette l'evento per la generazione del pdf
      $this->dispatcher->dispatch(new GenerateApplicationReceiptEvent($pratica), GenerateApplicationReceiptEvent::NAME);
      $this->tacitOutcomeService->createAsyncTask($pratica);

    } elseif ($pratica->getStatus() == Pratica::STATUS_DRAFT_FOR_INTEGRATION) {

      // Creo il file principale per le integrazioni
      $integrationsAnswer = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRispostaIntegrazione($pratica);
      $pratica->addAllegato($integrationsAnswer);
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION);
    }
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param null $message
   * @throws Exception
   */
  public function finalizePaymentCompleteSubmission(Pratica $pratica, User $user, $message = null): void
  {

    $statusChange = new StatusChange();
    $statusChange->setEvento('Pagamento completato');
    $statusChange->setOperatore($user->getFullName());
    $this->praticaStatusService->setNewStatus(
      $pratica,
      Pratica::STATUS_PAYMENT_SUCCESS,
      $statusChange
    );

    if ($message['message'] !== null) {
      $this->generateStatusMessage($pratica, $message['message'], $message['subject'], [], $message['visibility']);
    }

    // Stiamo forzando lo stato della pratica in pagata, dobbiamo eliminare i pagamenti pendenti sul proxy di pagamento se presenti
    $this->paymentService->cancelApplicationPendingPayment($pratica);

  }

  public function autoAssign(Pratica $pratica): void
  {
    $assignmentConfig = $pratica->getServizio()->getAssignmentConfig();
    if (!$assignmentConfig instanceof AssignmentConfig || empty($assignmentConfig->getField())) {
      return;
    }

    $formData = $pratica->getDematerializedForms()['data'] ?? [];
    $assignmentFieldData = FormIOUtils::getValueFromDotNotation($formData, $assignmentConfig->getField());
    $assignmentFieldValue = null;
    // Se non diversamente specificato le select con datasource url salvano un oggetto del tipo:
    // [
    //   "label" => "string",
    //   "value" => "string",
    // ]
    if (is_array($assignmentFieldData) && isset($assignmentFieldData['value'])) {
      $assignmentFieldValue = $assignmentFieldData['value'];
    }
    // Se invece il datasource è data viene salvata una stringa
    if (is_string($assignmentFieldData)) {
      $assignmentFieldValue = $assignmentFieldData;
    }

    $userGroupIDToAssign = $assignmentConfig->getItem($assignmentFieldValue);
    if ($userGroupIDToAssign) {
      try {
        $userGroupToAssign = $this->entityManager->getRepository(UserGroup::class)->find($userGroupIDToAssign);
        if (!$userGroupToAssign instanceof UserGroup) {
          throw new \Exception("User group with id {$userGroupIDToAssign} not found");
        }

        if ($pratica->getUserGroup() !== $userGroupToAssign) {
          $this->assign($pratica, null, null, $userGroupToAssign);
        }
      } catch (\Exception $e) {
        $this->logger->error('Error during automatic assignment of application', [
          'application_id' => $pratica->getId(),
          'assignment_field' => $assignmentConfig->getField(),
          'assignment_field_data' => $assignmentFieldData,
          'exception_message' => $e->getMessage(),
        ]);
      }
    }

  }

  /**
   * @param Pratica $pratica
   * @param User|null $author
   * @param OperatoreUser|null $assignedUser
   * @param UserGroup|null $userGroup
   * @param string|null $message
   * @param DateTime|null $assignedAt
   */
  public function assign(Pratica $pratica, ?User $author, OperatoreUser $assignedUser = null, UserGroup $userGroup = null, ?string $message = null, ?DateTime $assignedAt = null): void
  {
    // Assegnazione automatica all'autore se non è stato specificato un gruppo o un ufficio
    if (!$assignedUser && !$userGroup && $author) {
      $assignedUser = $author;
    }

    if ($assignedUser && $pratica->getOperatore() && $pratica->getOperatore()->getId() === $assignedUser->getId() && $pratica->getUserGroup() === $userGroup) {
      throw new BadRequestHttpException(
        $this->translator->trans('pratica.already_assigned', ['%operator_fullname%' => $pratica->getOperatore()->getFullName()])
      );
    }

    if ($pratica->getNumeroProtocollo() === null && $pratica->getServizio()->isProtocolRequired()) {
      throw new BadRequestHttpException($this->translator->trans('pratica.no_protocol_number'));
    }

    if ($assignedUser && $userGroup && !$assignedUser->getUserGroups()->contains($userGroup)) {
      throw new BadRequestHttpException($this->translator->trans('operatori.user_not_in_user_group', [
        '%fullname%' => $assignedUser->getFullName(),
        '%user_group%' => $userGroup->getName(),
      ]));
    }

    // Assegnazioni automatiche del gruppo
    if ($assignedUser && !$userGroup) {
      if ($pratica->getUserGroup() && $pratica->getUserGroup()->getUsers()->contains($assignedUser)) {
        // Se non è specificato il gruppo tra i parametri, ma l'operatore appartiene al gruppo a cui è assegnata
        // la pratica mantengo l'assegnazione del gruppo
        $userGroup = $pratica->getUserGroup();
      } else {
        // Ricerco tutti gli uffici a cui appartiene l'operatore
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('user_group')
          ->from('App:UserGroup', 'user_group')
          ->andWhere(':user MEMBER OF user_group.users')
          ->setParameter('user', $assignedUser)
          ->orderBy('user_group.name', 'ASC');

        $userGroups = $qb->getQuery()->getResult();
        $preferredUserGroup = null;

        if (!empty($userGroups)) {
          // Fixme: inserire nell'ordinamento della query precedente: attenzione però perché la pratica può essere associata ad un ufficio che non è incaricato del servizio
          foreach ($userGroups as $u) {
            if (!$preferredUserGroup && $u->getServices()->contains($pratica->getServizio())) {
              // Utilizzo il primo ufficio in ordine alfabetico che sia associato al servizio della pratica
              $preferredUserGroup = $u;
            }
          }

          // Se nessun ufficio gestisce il servizio allora prendo il primo in ordine alfabetico
          $userGroup = $preferredUserGroup ?? reset($userGroups);
        }
      }
    }

    $pratica->setOperatore($assignedUser);
    $pratica->setUserGroup($userGroup);
    $this->entityManager->persist($pratica);
    $this->entityManager->flush();

    $statusChange = new StatusChange();
    $statusChange->setEvento('Presa in carico');
    if ($author) {
      $statusChange->setResponsabile($author->getFullName());
    }

    if ($assignedUser) {
      $statusChange->setOperatore($assignedUser->getFullName());
    }

    if ($userGroup) {
      $statusChange->setUserGroup($userGroup->getName());
    }

    if ($message) {
      $statusChange->setMessage($message);
    }

    if ($assignedAt) {
      $statusChange->setTimestamp($assignedAt->getTimestamp());
    }

    try {
      $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_PENDING);
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_PENDING, $statusChange);
    } catch (\Exception $e) {
      $this->logger->error("Invalid status change for application {$pratica->getId()}: set previous status {$pratica->getStatus()}");
    }
  }


  /**
   * @param Pratica $pratica
   * @param User|null $user
   * @param float|null $paymentAmount
   * @param array $stamps
   * @throws Exception
   */
  public function finalize(Pratica $pratica, User $user, ?ApplicationOutcome $outcome): void
  {
    if ($pratica->getStatus() == Pratica::STATUS_COMPLETE
      || $pratica->getStatus() == Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE
      || $pratica->getStatus() == Pratica::STATUS_CANCELLED
      || $pratica->getStatus() == Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE) {
      throw new BadRequestHttpException($this->translator->trans('pratica.already_processed'));
    }

    if ($pratica->getRispostaOperatore() === null) {
      $signedResponse = $this->moduloPdfBuilderService->createSignedResponseForPratica($pratica);
      $pratica->addRispostaOperatore($signedResponse);
    }

    $protocolloIsRequired = $pratica->getServizio()->isProtocolRequired();
    $statusChange = new StatusChange();
    $statusChange->setOperatore($user->getFullName());

    if ($pratica->getEsito()) {
      $statusChange->setEvento('Approvazione pratica');

      if ($outcome instanceof ApplicationOutcome && (($pratica->getServizio()->needsPayments() && $outcome->getPaymentAmount() > 0) || !empty($outcome->getStamps()) || !empty($outcome->getPaymentConfigs()))) {

        // Se sono stati configurati dei bolli
        if (!empty($outcome->getStamps())) {
          $this->addDeferredStampsPayment($pratica, $outcome->getStamps());
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_STAMPS_PAYMENT_PENDING,
            $statusChange
          );
        }

        // Se è stato configurato un pagamento
        if ($pratica->getServizio()->needsPayments() && $outcome->getPaymentAmount() > 0) {
          $this->addDeferedPayment($pratica, $outcome->getPaymentAmount());
          if ($pratica->getStatus() != Pratica::STATUS_STAMPS_PAYMENT_PENDING) {
            $this->praticaStatusService->setNewStatus(
              $pratica,
              Pratica::STATUS_PAYMENT_PENDING,
              $statusChange
            );
          }
        }

        // Se sono state configurate delle configurazioni di pagamento
        if (!empty($outcome->getPaymentConfigs())) {
          $this->paymentService->createDeferredPayment($pratica, $outcome->getPaymentConfigs());
          if ($pratica->getStatus() != Pratica::STATUS_STAMPS_PAYMENT_PENDING) {
            $this->praticaStatusService->setNewStatus(
              $pratica,
              Pratica::STATUS_PAYMENT_PENDING,
              $statusChange
            );
          }
        }

      } else {
        if ($protocolloIsRequired) {
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE,
            $statusChange
          );
        } else {
          $this->praticaStatusService->setNewStatus(
            $pratica,
            Pratica::STATUS_COMPLETE,
            $statusChange
          );
        }
      }

      $this->logger->info(
        LogConstants::PRATICA_APPROVED,
        [
          'pratica' => $pratica->getId(),
          'user' => $pratica->getUser()->getId(),
        ]
      );
    } else {

      $statusChange->setEvento('Rifiuto pratica');
      if ($protocolloIsRequired) {
        $this->praticaStatusService->setNewStatus(
          $pratica,
          Pratica::STATUS_CANCELLED_WAITALLEGATIOPERATORE,
          $statusChange
        );
      } else {
        $this->praticaStatusService->setNewStatus(
          $pratica,
          Pratica::STATUS_CANCELLED,
          $statusChange
        );
      }

      $this->logger->info(
        LogConstants::PRATICA_CANCELLED,
        [
          'pratica' => $pratica->getId(),
          'user' => $pratica->getUser()->getId(),
        ]
      );
    }
  }

  public function addDeferredStampsPayment(Pratica $pratica, array $stamps): void
  {
    $stamps = array_merge($pratica->getStamps(), $stamps);
    $pratica->setStamps($stamps);
  }

  /**
   * @throws Exception
   */
  public function addDeferedPayment(Pratica $pratica, $paymentAmount): void
  {
    // Seleziono il primo gateway disponibile
    $selectedGateways = $pratica->getServizio()->getPaymentParameters()['gateways'] ?? [];
    if (!$selectedGateways) {
      throw new BadRequestHttpException($this->translator->trans('payment.no_selected_gateways'));
    }
    $identifier = array_keys($selectedGateways)[0];

    // Vecchi gateway, da deprecare
    if (in_array($identifier, [Bollo::IDENTIFIER, MyPay::IDENTIFIER])) {
      $pratica->setPaymentAmount($paymentAmount);
    } else {
      $pratica->setPaymentData($this->paymentService->createDefferedPaymentData($pratica, $paymentAmount));
    }
    $pratica->setPaymentType($identifier);

    $this->entityManager->persist($pratica);
    $this->entityManager->flush();
  }

  public function addDeferedPaymentFromConfigs(Pratica $pratica, $paymentConfigs): void
  {

  }

  /**
   * @param Pratica $pratica
   * @param $data
   * @throws FileExistsException
   * @throws GotenbergApiErroed
   * @throws LoaderError
   * @throws RuntimeError
   * @throws SyntaxError
   * @throws Exception
   */
  public function withdrawApplication(Pratica $pratica, $data): void
  {

    if ($pratica->getStatus() == Pratica::STATUS_WITHDRAW) {
      throw new BadRequestHttpException($this->translator->trans('pratica.already_processed'));
    }

    $withdrawAttachment = $this->moduloPdfBuilderService->createWithdrawForPratica($pratica, $data);
    $pratica->addAllegato($withdrawAttachment);

    $statusChange = new StatusChange();
    $this->praticaStatusService->setNewStatus(
      $pratica,
      Pratica::STATUS_WITHDRAW,
      $statusChange
    );

    $this->logger->info(
      LogConstants::PRATICA_WITHDRAW,
      [
        'pratica' => $pratica->getId(),
        'user' => $pratica->getUser()->getId(),
      ]
    );
  }

  /**
   * @param Pratica $pratica
   * @param UserInterface $user
   * @param $data
   * @throws FileExistsException
   * @throws GotenbergApiErroed
   * @throws LoaderError
   * @throws RuntimeError
   * @throws SyntaxError
   * @throws Exception
   */
  public function revokeApplication(Pratica $pratica, UserInterface $user, $data): void
  {

    if ($pratica->getStatus() == Pratica::STATUS_REVOKED) {
      throw new BadRequestHttpException($this->translator->trans('pratica.already_processed'));
    }

    if ($pratica->getEsito() !== null && $pratica->getMotivazioneEsito() !== null) {
      $this->removeApplicationOutcome($pratica);
    }

    $attachment = $this->moduloPdfBuilderService->createRevocationForApplication($pratica, $data);
    $pratica->addAllegato($attachment);

    $statusChange = new StatusChange();
    $statusChange->setOperatore($user->getFullName());
    $this->praticaStatusService->setNewStatus(
      $pratica,
      Pratica::STATUS_REVOKED,
      $statusChange,
      true
    );

    $this->logger->info(
      LogConstants::PRATICA_REVOKED,
      [
        'pratica' => $pratica->getId(),
        'user' => $pratica->getUser()->getId(),
      ]
    );
  }

  /**
   * @param Pratica $pratica
   * @param UserInterface $user
   * @param string $status
   * @throws Exception
   */
  public function changeStatus(Pratica $pratica, UserInterface $user, string $status): void
  {

    if (!$pratica->getServizio()->isAllowReopening()) {
      throw new PreconditionFailedHttpException('Service settings do not allow change status on application, check settings.');
    }

    if (!in_array($status, $pratica->getAllowedStates())) {
      throw  new BadRequestHttpException('The selected state is not among those allowed for application, allowed states are: ' . implode(', ', $pratica->getAllowedStates()));
    }

    if ($status == Pratica::STATUS_REVOKED) {
      $this->revokeApplication($pratica, $user, []);
    } else {
      if ($pratica->getEsito() !== null && $pratica->getMotivazioneEsito() !== null && $status < Pratica::STATUS_COMPLETE_WAITALLEGATIOPERATORE) {
        $this->removeApplicationOutcome($pratica);
      }

      if (!$pratica->getOperatore() && $status >= Pratica::STATUS_PENDING) {
        $pratica->setOperatore($user);
      }

      $statusChange = new StatusChange();
      $statusChange->setEvento('Cambio stato pratica');
      $statusChange->setOperatore($user->getFullName());

      $this->praticaStatusService->setNewStatus(
        $pratica,
        $status,
        $statusChange,
        true
      );
    }
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $data
   * @throws Exception
   */
  public function requestIntegration(Pratica $pratica, User $user, $data): void
  {

    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);

    $message = new Message();
    $message->setApplication($pratica);
    $message->setProtocolRequired(false);
    $message->setVisibility(Message::VISIBILITY_APPLICANT);
    $message->setMessage($data['message']);
    $message->setSubject($this->translator->trans('pratica.messaggi.oggetto', ['%pratica%' => $message->getApplication()]));
    $message->setAuthor($user);

    $detailUrl = $this->getApplicationDetailUrl($pratica, ['tab' => 'note']);
    $callToActions = [
      ['label' => 'operatori.reply_integration_request', 'link' => $detailUrl],
    ];

    $message->setCallToAction($callToActions);

    $requestAttachmentsIds = $requestAttachments = [];
    foreach ($data['attachments'] as $attachment) {
      $base64Content = $attachment->getFile();
      $file = new UploadedBase64File($base64Content, $attachment->getMimeType(), $attachment->getName());
      $allegato = new AllegatoMessaggio();
      $allegato->setFile($file);
      $allegato->setOwner($pratica->getUser());
      $allegato->setDescription('Allegato richiesta integrazione');
      $allegato->setOriginalFilename($attachment->getName());
      //$allegato->setIdRichiestaIntegrazione($integration->getId());
      $this->entityManager->persist($allegato);
      $message->addAttachment($allegato);
      $requestAttachments[] = $allegato;
      $requestAttachmentsIds[] = $allegato->getId();
    }

    // Creo il file di richiesta integrazione
    $richiestaIntegrazione = new RichiestaIntegrazioneDTO([], null, $data['message']);
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION);
    $integration = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRichiestaIntegrazione(
      $pratica,
      $richiestaIntegrazione,
      $requestAttachments
    );
    if (!empty($requestAttachmentsIds)) {
      $integration->setAttachments($requestAttachmentsIds);
      $this->entityManager->persist($integration);
    }
    $pratica->addRichiestaIntegrazione($integration);


    $this->entityManager->persist($pratica);
    $this->entityManager->flush();
    $this->messageManager->save($message);

    $statusChange = new StatusChange();
    $statusChange->setOperatore($user->getFullName());
    $statusChange->setMessageId($message->getId());
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_REQUEST_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @return void
   * @throws FileExistsException
   * @throws Exception
   */
  public function cancelIntegration(Pratica $pratica, User $user): void
  {
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION);
    $integrationsAnswer = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRispostaIntegrazione(
      $pratica,
      [],
      true
    );
    $pratica->addAllegato($integrationsAnswer);

    if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
      $statusChange = new StatusChange();
      $statusChange->setOperatore($user->getFullName());
    }
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $messages
   * @return void
   * @throws FileExistsException
   * @throws Exception
   */
  public function acceptIntegration(Pratica $pratica, User $user, $messages = null): void
  {
    $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION);
    $integrationsAnswer = $this->moduloPdfBuilderService->creaModuloProtocollabilePerRispostaIntegrazione(
      $pratica,
      $messages
    );
    $pratica->addAllegato($integrationsAnswer);

    if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
      $statusChange = new StatusChange();
      $statusChange->setOperatore($user->getFullName());
    }
    $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_SUBMITTED_AFTER_INTEGRATION, $statusChange);
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $data
   * @throws Exception
   */
  public function registerIntegrationRequest(Pratica $pratica, UserInterface $user, $data): void
  {
    $integrationRequest = $pratica->getRichiestaDiIntegrazioneAttiva();
    if (!$integrationRequest instanceof RichiestaIntegrazione) {
      throw new NotFoundException();
    }

    if ($integrationRequest->getNumeroProtocollo() && $integrationRequest->getIdDocumentoProtocollo()) {
      throw new AlreadySentException();
    }

    try {
      $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_DRAFT_FOR_INTEGRATION);
      $integrationRequest->setNumeroProtocollo($data['integration_outbound_protocol_number']);
      $integrationRequest->setIdDocumentoProtocollo($data['integration_outbound_protocol_document_id']);
      $this->entityManager->persist($integrationRequest);
      $this->entityManager->flush();

      if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
        $statusChange = new StatusChange();
        $statusChange->setOperatore($user->getFullName());
      }
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_DRAFT_FOR_INTEGRATION, $statusChange);
    } catch (\Exception $e) {
      throw new InvalidStatusException();
    }
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @param $data
   * @throws Exception
   */
  public function registerIntegrationAnswer(Pratica $pratica, UserInterface $user, $data): void
  {
    $integrationAnswer = $this->getIntegrationAnswer($pratica);
    if (!$integrationAnswer instanceof RispostaIntegrazione) {
      throw new NotFoundException();
    }

    if ($integrationAnswer->getNumeroProtocollo() && $integrationAnswer->getIdDocumentoProtocollo()) {
      throw new AlreadySentException();
    }

    try {
      $this->praticaStatusService->validateChangeStatus($pratica, Pratica::STATUS_REGISTERED_AFTER_INTEGRATION);
      $integrationAnswer->setNumeroProtocollo($data['integration_inbound_protocol_number']);
      $integrationAnswer->setIdDocumentoProtocollo($data['integration_inbound_protocol_document_id']);

      $this->entityManager->persist($integrationAnswer);
      $this->entityManager->flush();

      if ($user instanceof AdminUser || $user instanceof OperatoreUser) {
        $statusChange = new StatusChange();
        $statusChange->setOperatore($user->getFullName());
      }
      $this->praticaStatusService->setNewStatus($pratica, Pratica::STATUS_REGISTERED_AFTER_INTEGRATION, $statusChange);
    } catch (\Exception $e) {
      throw new InvalidStatusException();
    }
  }

  /**
   * @param Pratica $pratica
   * @return RispostaIntegrazione|null
   */
  public function getIntegrationAnswer(Pratica $pratica): ?RispostaIntegrazione
  {

    $integrationRequest = $pratica->getRichiestaDiIntegrazioneAttiva();
    if (!$integrationRequest instanceof RichiestaIntegrazione) {
      return null;
    }

    /** @var RispostaIntegrazioneRepository $integrationAnswerRepo */
    $integrationAnswerRepo = $this->entityManager->getRepository('App\Entity\RispostaIntegrazione');

    $integrationAnswerCollection = $integrationAnswerRepo->findByIntegrationRequest($integrationRequest->getId());

    if (!empty($integrationAnswerCollection)) {
      /** @var RispostaIntegrazione $answer */
      return $integrationAnswerCollection[0];
    }

    return null;
  }

  /**
   * @param Pratica $pratica
   * @param string $text
   * @param string $subject
   * @param array $callToActions
   * @param string|null $visibility
   * @return Message
   */
  public function generateStatusMessage(
    Pratica $pratica,
    string  $text,
    string  $subject,
    array   $callToActions = [],
    ?string $visibility = Message::VISIBILITY_APPLICANT
  ): Message
  {
    $message = new Message();
    $message->setApplication($pratica);
    $message->setProtocolRequired(false);
    $message->setVisibility($visibility);
    $message->setMessage($text);
    $message->setSubject($subject);
    $message->setCallToAction($callToActions);
    $message->setEmail($pratica->getUser()->getEmailContatto());
    $message->setSentAt(time());

    try {
      $this->messageManager->save($message, false);
      $this->entityManager->persist($pratica);
      $this->entityManager->flush();
    } catch (Exception $e) {
      $this->logger->error("Impossible to generate status message for application {$pratica->getId()}: {$e->getMessage()}");
    }
    return $message;
  }

  public function createDraftApplication(Servizio $servizio, CPSUser $user, array $additionalDematerializedData): FormIO
  {
    $pratica = new FormIO();
    $pratica->setUser($user);
    $pratica->setServizio($servizio);
    $pratica->setStatus(Pratica::STATUS_DRAFT);
    $pratica->setEnte($this->is->getCurrentInstance());

    $cpsUserData = [
      'applicant' => [
        'data' => [
          'completename' => [
            'data' => [
              'name' => $user->getNome(),
              'surname' => $user->getCognome(),
            ],
          ],
          'gender' => [
            'data' => [
              'gender' => $user->getSessoAsString(),
            ],
          ],
          'Born' => [
            'data' => [
              'natoAIl' => $user->getDataNascita() ? $user->getDataNascita()->format('d/m/Y') : '',
              'place_of_birth' => $user->getLuogoNascita(),
            ],
          ],
          'fiscal_code' => [
            'data' => [
              'fiscal_code' => $user->getCodiceFiscale(),
            ],
          ],
          'address' => [
            'data' => [
              'address' => $user->getIndirizzoResidenza(),
              'house_number' => '',
              'municipality' => $user->getCittaResidenza(),
              'postal_code' => $user->getCapResidenza(),
              'county' => $user->getProvinciaResidenza(),
            ],
          ],
          'email_address' => $user->getEmail(),
          'email_repeat' => $user->getEmail(),
          'cell_number' => $user->getCellulare(),
          'phone_number' => $user->getTelefono(),
        ],
      ],
      'cell_number' => $user->getCellulare(),
      'phone_number' => $user->getTelefono(),
    ];

    $pratica->setDematerializedForms([
      "data" => array_merge(
        $additionalDematerializedData,
        $cpsUserData
      ),
    ]);

    $this->entityManager->persist($pratica);
    $this->entityManager->flush();

    return $pratica;
  }

  public function flatSchema(array $schema, ?string $prefix = ''): array
  {
    $result = [];
    foreach ($schema as $key => $value) {

      if ($key === 'metadata' || $key === 'state') {
        continue;
      }

      $newKey = $prefix . (empty($prefix) ? '' : '.') . $key;

      if (is_array($value)) {
        $result = array_merge($result, $this->flatSchema($value, $newKey));
      } else {
        $result[$newKey] = $value;
      }
    }
    return $result;
  }


  /**
   * @param $array
   * @param array|null $prefix
   * @return array
   */
  public function arrayFlat($array, ?array $prefix = []): array
  {
    $result = array();
    foreach ($array as $key => $value) {
      $tempPrefix = $prefix;
      if ($key === 'metadata' || $key === 'state') {
        continue;
      }

      $isFile = false;
      $tempPrefix [] = $key;
      // Nel caso dei datagrid lo schema ha sempre e solo l'indice 0 mentre la submission può avere più valori
      // Va confrontato quindi sempre con l'elemento con indice 0 dello schema per capire il tipo
      $checkKeyParts = [];
      foreach ($tempPrefix as $v) {
        if (is_int($v) && $v > 0) {
          $checkKeyParts [] = 0;
          continue;
        }
        $checkKeyParts [] = $v;
      }
      $new_key = implode('.', $tempPrefix);
      $checkKey = implode('.', $checkKeyParts) . '.type';

      if (isset($this->flatSchema[$checkKey]) &&
        ($this->flatSchema[$checkKey] === 'file' || $this->flatSchema[$checkKey] === 'sdcfile' || $this->flatSchema[$checkKey] === 'financial_report')) {
        $isFile = true;
      }

      if (is_array($value) && !$isFile) {
        $result = array_merge($result, $this->arrayFlat($value, $tempPrefix));
      } else {
        $result[$new_key] = $value;
      }
    }
    return $result;
  }

  /**
   * @param array $data
   * @return CPSUser
   */
  public function fetchOrCreateApplicant(array $data): CPSUser
  {

    $cf = $data['flattened']['applicant.data.fiscal_code.data.fiscal_code'] ?? false;

    $userRepo = $this->entityManager->getRepository(CPSUser::class);
    $user = $userRepo->findByTaxCode($cf);

    if (!$user instanceof CPSUser) {
      $user = $this->cpsUserProvider->createAnonymousUser();
    }

    $this->updateUserFromApplicantData($data, $user);
    return $user;
  }

  /**
   * @param Pratica $pratica
   * @return void
   */
  public function fetchOrCreateBeneficiary(Pratica $pratica): void
  {
    try {
      if ($pratica->getUser()->isAnonymous()) {
        throw new RuntimeException('Applicant is anonymous');
      }

      $data = $pratica->getDematerializedForms();
      if (empty($data['data']['beneficiary']['data']['tax_id'])) {
        throw new RuntimeException('Beneficiary tax id is empty');
      }

      $cf = $data['data']['beneficiary']['data']['tax_id'];
      if (!FiscalCodeUtils::isValid($cf)) {
        throw new RuntimeException(sprintf('Fiscal code "%s" is not valid', $cf));
      }

      $userRepo = $this->entityManager->getRepository(CPSUser::class);
      $user = $userRepo->findByTaxCode($cf);
      if (!$user instanceof CPSUser) {
        $user = $this->cpsUserProvider->createPerson(FormIOBeneficiaryToBeneficiaryDataMapper::map($data));
      }

      $pratica->setBeneficiary($user);

    } catch (\Exception $e) {
      $this->logger->error("Beneficiary not added to application {$pratica->getId()} because: " . $e->getMessage());
    }
  }

  /**
   * @param array $data
   * @param CPSUser $user
   */
  public function updateUserFromApplicantData(array $data, CPSUser $user): void
  {
    $email = $this->getUserEmail($data, $user);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $user
        ->setEmail($email)
        ->setEmailContatto($email);
    } else {
      $this->logger->warning("User {$user->getId()} provided an invalid email address {$email}");
    }

    $flattenedData = $data['flattened'];

    if (!empty($flattenedData['applicant.data.completename.data.name']) && $user->getNome() !== $flattenedData['applicant.data.completename.data.name']) {
      $user->setNome($flattenedData['applicant.data.completename.data.name']);
    }

    if (!empty($flattenedData['applicant.data.completename.data.surname']) && $user->getCognome() !== $flattenedData['applicant.data.completename.data.surname']) {
      $user->setCognome($flattenedData['applicant.data.completename.data.surname']);
    }

    if (!empty($flattenedData['applicant.data.gender.data.gender']) && $user->getSessoAsString() !== $flattenedData['applicant.data.gender.data.gender']) {
      $user->setSessoAsString($flattenedData['applicant.data.gender.data.gender']);
    }

    $birthDay = isset($flattenedData['applicant.data.Born.data.natoAIl']) ? DateTime::createFromFormat('d/m/Y', $flattenedData['applicant.data.Born.data.natoAIl']) : null;
    if ($birthDay && $user->getDataNascita() !== $birthDay) {
      $user->setDataNascita($birthDay);
    }

    if (!empty($flattenedData['applicant.data.Born.data.place_of_birth']) && $user->getLuogoNascita() !== $flattenedData['applicant.data.Born.data.place_of_birth']) {
      $user->setLuogoNascita($flattenedData['applicant.data.Born.data.place_of_birth']);
    }

    if (!empty($flattenedData['applicant.data.cell_number'])) {
      $user->setCellulareContatto($flattenedData['applicant.data.cell_number']);
    }
    if (!empty($flattenedData['applicant.data.phone_number'])) {
      $user->setCpsTelefono($flattenedData['applicant.data.phone_number']);
    }

    $user
      ->setSdcIndirizzoResidenza($flattenedData['applicant.data.address.data.address'] ?? '')
      ->setSdcCivicoResidenza($flattenedData['applicant.data.address.data.house_number'] ?? '')
      ->setSdcCittaResidenza($flattenedData['applicant.data.address.data.municipality'] ?? '')
      ->setSdcCapResidenza($flattenedData['applicant.data.address.data.postal_code'] ?? '')
      ->setSdcProvinciaResidenza($flattenedData['applicant.data.address.data.county'] ?? '')
      ->setSdcStatoResidenza($flattenedData['applicant.data.address.data.country'] ?? '');

    $this->userManager->save($user);
  }

  public function updateUserFromBeneficiaryData(array $data, CPSUser $user): void
  {
    $this->userManager->save($user);
  }

  /**
   * @param array $data
   * @param CPSUser $user
   * @param null $applicationId
   * @throws Exception
   */
  public function validateUserData(array $data, CPSUser $user, $applicationId = null): void
  {
    if (strcasecmp($data['applicant.data.fiscal_code.data.fiscal_code'], $user->getCodiceFiscale()) != 0) {
      $this->logger->error("Fiscal code Mismatch", [
          'pratica' => $applicationId ?? '-',
          'cps' => $user->getCodiceFiscale(),
          'form' => $data['applicant.data.fiscal_code.data.fiscal_code'],
        ]
      );
      throw new Exception($this->translator->trans('steps.formio.fiscalcode_violation_message'));
    }
  }

  /**
   * @param array $data
   * @param Pratica $pratica
   * @return void
   */
  public function validateDematerializedData(array $data, Pratica $pratica): void
  {
    // Verifico che la submission non sia vuota
    if (empty($data['data']) || empty($data['flattened'])) {
      $this->logger->error("Received empty dematerialized data");
      throw new ValidatorException($this->translator->trans('steps.formio.empty_data_violation_message'));
    }

    // Verifico che sia disponibile lo schema del form al momento dell'invio
    if (empty($data['schema'])) {
      $this->logger->error("Received empty form schema");
      throw new ValidatorException($this->translator->trans('steps.formio.empty_schema_violation_message'));
    }

    // Check sulla presenza del codice fiscale (per pratiche vuote)
    if (!isset($data['flattened']['applicant.data.fiscal_code.data.fiscal_code'])) {
      $this->logger->error("Dematerialized form not well formed", ['pratica' => $pratica->getId()]);
      throw new ValidatorException($this->translator->trans('steps.formio.generic_violation_message'));
    }

    // Check sulla univocità di un campo
    $errors = $this->expressionValidator->validateData($pratica->getServizio(), json_encode($data['data']));
    if (!empty($errors)) {
      $this->logger->error("Received duplcated unique_id");
      throw new ValidatorException($this->translator->trans('steps.formio.duplicated_unique_id'));
    }

    // Verifica fascicolazione della pratica
    if (isset($data['flattened']['related_applications'])) {
      $parentId = trim($data['flattened']['related_applications']);
      if ($parentId == $pratica->getId()) {
        $this->logger->error("The application {$parentId} cannot be linked to itself");
        throw new ValidatorException($this->translator->trans('steps.formio.parent_id_error'));
      }
    }

    // Verifica campi necessari alle integrazioni
    $integrations = $pratica->getServizio()->getIntegrations();
    if ($integrations) {
      $backOfficeHandler = $this->backOfficeCollection->getBackOffice(reset($integrations));
      if (!$backOfficeHandler->isSubmissionValid($data)) {
        $this->logger->error("The application {$pratica->getId()} data is not valid for integration with the {$backOfficeHandler->getIdentifier()} backoffice");
        throw new ValidatorException($this->translator->trans("steps.formio.{$backOfficeHandler->getIdentifier()}_integration_error"));
      }
    }
  }

  /**
   * @param Pratica $pratica
   * @param array $data
   * @return void
   * @throws \Doctrine\DBAL\Driver\Exception
   * @throws \Doctrine\DBAL\Exception|\JsonException
   */
  public function addApplicationToGeographicAreas(Pratica $pratica, array $data): void
  {
    $currentAreas = $pratica->getGeographicAreas();
    $areaIds = [];
    $areas = $this->validateApplicationCoordinates($data, $pratica);

    foreach ($areas as $area) {
      $pratica->addGeographicArea($area);
      $areaIds[] = $area->getId();
    }

    // Rimuovo le aree che non devono più essere associate alla pratica
    foreach ($currentAreas as $currentArea) {
      if (!in_array($currentArea->getId(), $areaIds)) {
        $pratica->removeGeographicArea($currentArea);
      }
    }
  }

  /**
   * @param array $data
   * @param Pratica $pratica
   * @return array
   * @throws \Doctrine\DBAL\Exception|\JsonException
   */
  private function validateApplicationCoordinates(array $data, Pratica $pratica): array
  {
    $result = [];
    // Se non sono definite aree geografiche per il servizio non devo procedere
    // Se la submission non contiene un campo address o non è valorizzato correttamente non devo procedere
    if (!isset($data['data']['address']) || empty($data['data']['address']['lon']) || empty($data['data']['address']['lat']) ||
      $pratica->getServizio()->getGeographicAreas()->isEmpty()
    ) {
      return $result;
    }

    $lon = $data['data']['address']['lon'];
    $lat = $data['data']['address']['lat'];

    /** @var GeographicArea $area */
    foreach ($pratica->getServizio()->getGeographicAreas() as $area) {
      if ($area->getGeofence() && $this->geoService->geoFenceContainsPoint($lon, $lat, $area->getGeofence())) {
        $result [] = $area;
      }
    }

    if (empty($result)) {
      $this->logger->error("There is no geographic area that contains the specified point", [
        'lon' => $lon,
        'lat' => $lat,
        'service_id' => $pratica->getServizio()->getId(),
      ]);
      throw new ValidatorException($this->translator->trans('steps.formio.geographic_area_violation_message'));
    }
    return $result;
  }

  public function collectProfileBlocks(Pratica $pratica): void
  {
    if ($pratica->getServizio()->isLegacy()) {
      return;
    }

    $schema = $this->schemaFactory->createFromService($pratica->getServizio());

    // Todo: I profile block del servizio non vengono più recuperati dal db, valutare impatto performance ed in caso eliminare salvataggio nel database
    //$profileBlocks = $pratica->getServizio()->getProfileBlocks();
    $profileBlocks = $schema->getProfileBlocks();

    if (empty($profileBlocks)) {
      return;
    }

    $saveApplication = false;
    $data = $pratica->getDematerializedForms();

    $pdndFieldFilterMap = $schema->getPdndFieldComponents();
    $pdndFieldFilterMapFlipped = array_flip($pdndFieldFilterMap);

    foreach ($profileBlocks as $key => $value) {
      // Estrae il contenuto del singolo profile block dall'intera submission passando il path
      $profileBlockSubmission = FormIOUtils::getValueFromDotNotation($data['data'], $value['path']);

      if (!FormUtils::isArrayEmpty($profileBlockSubmission)) {
        try {

          // Verifico sia attivata la pdnd sul blocco
          if (isset($profileBlockSubmission['data'], $profileBlockSubmission['meta']) && !empty($value['eservice'])) {

            // Se attivata ricavo il subset del contenuto cha fa parte della pdnd per inviarlo alla validazione
            $subset = [];
            foreach ($profileBlockSubmission['data'] as $k => $v) {
              $index = $value['path'] . '.' . $k;
              // Check if the current value is an array
              if (is_array($v)) {
                foreach ($v as $subKey => $subValue) {
                  $subset[$k][] = FormIOUtils::processSubmissionData($subValue, $index, $pdndFieldFilterMapFlipped);
                }
              }
              // Check if the index is in the filter map
              if (isset($pdndFieldFilterMapFlipped[$index])) {
                $subset[$k] = $v; // Add to $subset if it passes the filter
              }
            }

            if (!empty($subset)) {
              $dataToValidate = [
                'data' => $subset,
                'meta' => $profileBlockSubmission['meta']
              ];
              $isValid = $this->pdndManager->isValidData($dataToValidate);
              FormIOUtils::setIsValidFromDotNotation($data['data'], $value['path'], $isValid);
              $saveApplication = true;
            }
          }
          $this->profileBlockManager->save($pratica, $key, $profileBlockSubmission, $value['url']);
        } catch (\Throwable $e) {

          $this->logger->error('Unable to save profile block: ' . $e->getMessage(), [
            'application_id' => $pratica->getId(),
            'profile_block_key' => $key,
          ]);
        }
      }
    }

    if ($saveApplication) {
      $pratica->setDematerializedForms($data);
      $this->save($pratica);
    }

  }


  /**
   * @param Schema $schema
   * @param CPSUser $user
   * @return mixed
   */
  public function getMappedFormDataWithUserData(Schema $schema, CPSUser $user)
  {
    $data = $schema->getDataBuilder();
    if ($schema->hasComponents()) {
      foreach (self::APPLICATION_USER_MAP as $schemaFlatName => $userMethod) {
        try {
          if ($schema->hasComponent($schemaFlatName) && method_exists($user, $userMethod)) {
            $component = $schema->getComponent($schemaFlatName);
            $value = $user->{$userMethod}();

            // se il campo è datatime popola con iso8601 altrimenti testo
            if ($value instanceof DateTime) {
              if ($component['form_type'] == DateTimeType::class) {
                $value = $value->format(\DateTimeInterface::W3C);
              } else {
                $value = $value->format('d/m/Y');
              }
            }

            if ($component['form_type'] == ChoiceType::class
              && isset($component['form_options']['choices'])
              && !empty($component['form_options']['choices'])) {

              /*if ($schemaFlatName !== 'applicant.gender.gender') {
                $value = strtoupper($value);
              }*/

              if (!in_array($value, $component['form_options']['choices'])) {
                $value = null;
              }
            }

            if ($value) {
              $data->set($schemaFlatName, $value);
            }
          }
        } catch (\Exception $e) {
          $this->logger->error($e->getMessage());
        }
      }
    }
    return $data->toArray();
  }

  /**
   * @param Pratica $pratica
   * @param $flattenedData
   * @throws Exception
   */
  public function addAttachmentsToApplication(Pratica $pratica, $flattenedData): void
  {
    $repo = $this->entityManager->getRepository(Allegato::class);
    $currentAttachments = $pratica->getAllegati();
    $attachments = [];
    foreach ($flattenedData as $value) {
      if (is_array($value)) {
        foreach ($value as $file) {
          if ((isset($file['mime_type']) || isset($file['type'])) && !empty($file['data']['id'])) {
            $id = $file['data']['id'];
            $attachment = $repo->find($id);
            if ($attachment instanceof Allegato) {
              if (!empty($file['fileType'])) {
                $attachment->setDescription($file['fileType']);
                $this->entityManager->persist($attachment);
              }

              // Imposto il proprietario per gli allegati appena creati se non presente
              if (!$attachment->getOwner() instanceof CPSUser) {
                $attachment->setOwner($pratica->getUser());
                $this->entityManager->persist($attachment);
              }

              $attachments[] = $id;
              $pratica->addAllegato($attachment);
            } else {
              $msg = "The file present in form schema doesn't exist in database";
              $this->logger->error($msg, ['pratica' => $pratica->getId(), 'allegato' => $id]);
              throw new \Exception($this->translator->trans('steps.formio.attachments_violation_message'));
            }
          }
        }
      }
    }

    // Rimuovo gli allegati che non sono più presenti nella pratica
    foreach ($currentAttachments as $attachment) {
      if (!in_array($attachment->getId(), $attachments, true)) {
        $pratica->removeAllegato($attachment);
      }
    }

    // Verifico che il numero degli allegati associati alla pratica sia uguale a quello passato nel form
    if ($pratica->getAllegati()->count() !== count($attachments)) {
      $msg = 'The number of files in form data is not equal to those linked to the application';
      $this->logger->error($msg, ['pratica' => $pratica->getId()]);
      throw new \Exception($this->translator->trans('steps.formio.attachments_violation_message'));
    }
  }

  /**
   * @param Pratica $pratica
   * @return int
   */
  public function countAttachments(Pratica $pratica): int
  {
    $count = 0;
    $count += $pratica->getModuliCompilati()->count();
    // Include sia allegati che risposte a integrazione
    $count += $pratica->getAllegati()->count();

    $count += $pratica->getRichiesteIntegrazione()->count();

    if ($pratica->getRispostaOperatore()) {
      $count++;
    }
    $count += $pratica->getAllegatiOperatore()->count();

    return $count;
  }

  /**
   * @param Pratica $pratica
   * @return array
   */
  public function getGroupedModuleFiles(Pratica $pratica): array
  {
    $files = [];
    if ($pratica->getServizio()->isLegacy()) {
      return $files;
    }
    $attachments = $pratica->getAllegatiWithIndex();
    $schema = $this->schemaFactory->createFromService($pratica->getServizio());
    $filesComponents = $schema->getFileComponents();
    $data = $pratica->getDematerializedForms();
    $flatData = $data['flattened'] ?? [];

    $componentsByName = [];
    foreach ($filesComponents as $component) {
      $componentsByName[$component->getName()] = $component;
    }

    foreach ($flatData as $key => $data) {
      if (!is_array($data) || empty($data)) {
        continue;
      }
      // Todo: lo schema factory crea delle chiavi senza 'data' mentre il flat data lo contiene
      // Todo: Fix veloce bonifichiamo le chiavi del flat data per fare il match, da migliorare
      $simplifiedKey = FormIOUtils::simplifyFlattenedKey($key);
      if (isset($componentsByName[$simplifiedKey])) {
        $component = $componentsByName[$simplifiedKey];
        $componentOptions = $component->getFormOptions();
        $labelParts = explode('/', $componentOptions['label']);
        $page = $labelParts[0];
        unset($labelParts[0]);
        $label = implode(' / ', $labelParts);
        foreach ($data as $f) {
          if (isset($f['mime_type']) || isset($f['type'])) {
            $id = $f['data']['id'];
            $files [$page][$label][] = $attachments[$id];
          }
        }
      }
    }

    return $files;
  }

  public function getUserEmail(array $data, CPSUser $user)
  {
    $emailFromData = $data['flattened']['applicant.data.email_address'] ?? $data['flattened']['email_address']
      ?? $data['flattened']['email'] ?? '';
    // Non sovrascrivo l'email se nella submission il dato è presente ma vuoto
    return empty($emailFromData) ? $user->getEmailContatto() : $emailFromData;
  }

  /**
   * @throws AlreadyScheduledException
   */
  public function regenerateModule(Pratica $pratica): void
  {
    // Programmo la rigenerazione del modulo pdf
    $this->moduloPdfBuilderService->updateForPraticaAsync($pratica);
  }

  /**
   * @param Pratica $pratica
   * @return bool
   */
  public function isModuleUpdating(Pratica $pratica): bool
  {
    // Verifico se c'è una scheduled action per rigenerare il pdf
    $isModuleUpdating = false;
    try {
      $sql = "select id from scheduled_action where type = '" . ModuloPdfBuilderService::SCHEDULED_UPDATE_FOR_PRATICA . "' AND params::text LIKE '%" . $pratica->getId() . "%' and status = '" . ScheduledAction::STATUS_PENDING . "'";
      $stmt = $this->entityManager->getConnection()->prepare($sql);
      $results = $stmt->executeQuery()->rowCount();
      $isModuleUpdating = $results > 0;
    } catch (\Doctrine\DBAL\Driver\Exception $e) {
    } catch (\Doctrine\DBAL\Exception $e) {
      $this->logger->error("Error retrieving scheduled action to regenerate pdf for application {$pratica->getId()}: {$e->getMessage()}");
    }
    return $isModuleUpdating;
  }

  /**
   * @param Pratica $pratica
   * @param bool|null $completeHistory
   * @return array
   */
  public function getApplicationHistory(Pratica $pratica, ?bool $completeHistory = true): array
  {
    $history = [];

    $previous = false;

    foreach ($this->explodeSameTimestampHistory($pratica) as $timestamp => $v) {
      $transition = new Transition();
      $transition->setDate((new DateTime())->setTimestamp($timestamp));

      foreach ($v as $change) {
        // Da verificare, ci sono state delle history senza gli giusti elementi
        if (isset($change[0])) {
          $statusCode = $change[0];
          $statusChange = new StatusChange($change[1] ?? null);
          $transition->setStatusCode($statusCode);
          $transition->setStatusName(strtolower($pratica->getStatusNameByCode($statusCode)));
          $transition->setMessageId($statusChange->getMessageId());

          if ($completeHistory) {
            $transition->setMessage($statusChange->getMessage());
          }

          // Nome del cambio di stato automatico
          $description = $this->translator->trans('pratica.dettaglio.stato_' . $statusCode);

          if ($statusCode === Pratica::STATUS_PENDING) {
            if (!$completeHistory && $statusChange->getUserGroup()) {
              // Vista cittadino: mostro l'assegnatario solo se è un ufficio
              // Ex: Presa in carico da Ufficio
              $description = $this->translator->trans('pratica.dettaglio.iter.auto_assigned_from', ['%assignee%' => $statusChange->getUserGroup()]);
            } elseif ($statusChange->getAssigner()) {
              // Vista operatore: se ho un assegnatario lo mostro
              // Ex: Assegnata a Operatore A (Ufficio) da Operatore B
              $description = $this->translator->trans('pratica.dettaglio.iter.assigned_to_from', ['%assigner%' => $statusChange->getAssigner(), '%assignee%' => $statusChange->getAssignee()]);
            } elseif ($statusChange->getOperatore() && $completeHistory) {
              // Vista operatore: auto assegnazione da parte di un operatore
              $description = $this->translator->trans('pratica.dettaglio.iter.auto_assigned_from', ['%assignee%' => $statusChange->getAssignee()]);
            }
          }
          $transition->setDescription($description);
        }
      }

      if ($completeHistory || (!$previous or $previous !== $transition->getDescription())) {
        // Se la descrizione del cambio di stato è duplicata non l'aggiungo alla storia
        $history[] = $transition;
      }

      $previous = $transition->getDescription();
    }

    return $history;
  }

  /**
   * @throws Exception
   */
  public function selectPaymentGateway(FormIO $application): void
  {
    $service = $application->getServizio();
    $paymentParameters = $service->getPaymentParameters();
    // Gateway abilitato con vecchio sistema di pagamento
    $selectedGateways = $paymentParameters['gateways'] ?? [];
    // Gateway abilitato con pagamenti multipli
    $paymentConfigIdentifier = $service->getPaymentGatewayIdentifier();

    if (empty($selectedGateways) && empty($paymentConfigIdentifier)) {
      throw new \Exception($this->translator->trans('payment.no_selected_gateways'));
    }

    $identifier = reset($selectedGateways)['identifier'] ?? $paymentConfigIdentifier;
    $application->setPaymentType($identifier);
    $this->entityManager->persist($application);
    $this->entityManager->flush();

    if ($identifier === Bollo::IDENTIFIER || $application->getStatus() === Pratica::STATUS_PAYMENT_PENDING) {
      return;
    }

    if ($identifier !== MyPay::IDENTIFIER) {
      if ($this->featureManager->isActive('feature_multiple_payments')) {
        $this->paymentService->createPayment($application);
      } else {
        $application->setPaymentData($this->paymentService->createPaymentData($application));
        $this->entityManager->persist($application);
        $this->entityManager->flush();
      }
    }

    $this->praticaStatusService->setNewStatus($application, Pratica::STATUS_PAYMENT_PENDING);
  }

  public function getApplicationDetailUrl(Pratica $application, $parameters = []): string
  {
    $parameters = array_merge(['pratica' => $application, '_locale' => $application->getLocale()], $parameters);

    $service = $application->getServizio();
    if ($service->getExternalApplicationUrl()) {
      $applicationUrl = str_replace(['%uuid%'], $application->getId(), $service->getExternalApplicationUrl());
    } else {
      $applicationUrl = $this->router->generate('pratica_show_detail', $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    $user = $application->getUser();
    if ($user->isAnonymous()) {
      return $this->router->generate('login_token', ['token' => $user->getConfirmationToken()], UrlGeneratorInterface::ABSOLUTE_URL) . '&return-url=' . $applicationUrl;
    }
    return $applicationUrl;
  }

  private function explodeSameTimestampHistory($pratica): ArrayCollection
  {
    $normalizedArray = [];
    foreach ($pratica->getStoricoStati() as $timestamp => $v) {
      $count = count($v);
      if (count($v) > 1) {
        for ($i = 0; $i < $count; $i++) {
          $normalizedArray[$timestamp + $i][] = $v[$i];
        }
      } else {
        $normalizedArray[$timestamp] = $v;
      }
    }
    return new ArrayCollection($normalizedArray);
  }

  /**
   * @param Pratica $pratica
   * @param OperatoreUser $operatoreUser
   * @param bool|null $strictlyAssigned
   * @return bool
   */
  public static function isOperatorAssigned(Pratica $pratica, OperatoreUser $operatoreUser, ?bool $strictlyAssigned = false): bool
  {
    $isAssigned = $pratica->getOperatore() && $pratica->getOperatore()->getId() === $operatoreUser->getId();

    // Verifico che l'operatore abbia direttamente in carico la pratica
    if ($strictlyAssigned) {
      return $isAssigned;
    }

    // Verifico che la pratica sia assegnata all'ufficio di cui l'operatore fa parte
    $isUserGroupAssigned = $pratica->getUserGroup() && ($operatoreUser->getUserGroups()->contains($pratica->getUserGroup()) || $operatoreUser->getUserGroupsManager()->contains($pratica->getUserGroup()));

    return $isAssigned || $isUserGroupAssigned;
  }

  /**
   * @param Pratica $pratica
   * @param OperatoreUser $operatoreUser
   * @return bool
   */
  public static function isOperatorEnabled(Pratica $pratica, OperatoreUser $operatoreUser): bool
  {
    // Verifico che l'operatore sia di sistema oppure che sia abilitato al servizio
    return $operatoreUser->isSystemUser() || in_array($pratica->getServizio()->getId(), $operatoreUser->getAllEnabledServicesIds());
  }

  /**
   * @param Pratica $pratica
   * @param User $user
   * @return bool
   */
  public static function isOwner(Pratica $pratica, UserInterface $user): bool
  {
    // Verifico che l'utente sia il cittadino che ha inviato la pratica
    return $pratica->getUser()->getId() === $user->getId();
  }

  /** @throws ApplicationException */
  public function delete(Pratica $application): void
  {
    if ($application->getStatus() != Pratica::STATUS_DRAFT) {

      throw ApplicationException::becauseOnlyDraftApplicationCanBeDeleted();
    }

    /* Devo marchiarla come eliminata anche se la rimuovo
     * fisicamente dal DB perché devo propagare il messaggio. */
    $application->delete();

    $this->entityManager->remove($application);
    $this->entityManager->flush();

    try {

      $this->dispatcher->dispatch(new KafkaEvent($application), KafkaEvent::NAME);

    } catch (Throwable $e) {
      $this->logger->error("Unable to propagate application deletion. Message: {$e->getMessage()}");
    }
  }


  /**
   * @param Pratica $pratica
   * @return void
   */
  public function removeApplicationOutcome(Pratica $pratica): void
  {
    $pratica->setEsito(null);
    $pratica->setMotivazioneEsito(null);

    // Eliminazione risposta operatore
    $rispostaOperatore = $pratica->getRispostaOperatore();
    $pratica->removeRispostaOperatore();
    $this->entityManager->remove($rispostaOperatore);
    $this->logger->info("Successfully deleted main outcome {$rispostaOperatore->getName()} for application {$pratica->getId()}");

    // Eliminazione allegati alla risposta operatore
    foreach ($pratica->getAllegatiOperatore() as $allegato) {
      $pratica->removeAllegatoOperatore($allegato);
      $this->entityManager->remove($allegato);
      $this->logger->info("Successfully deleted outcome attachment {$allegato->getName()} for application {$pratica->getId()}");
    }
    $this->entityManager->flush();
  }
}
