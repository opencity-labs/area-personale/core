<?php

namespace App\Services\Manager;

use App\Entity\CodeCounter;
use App\Entity\CodeGenerationStrategy;
use App\Utils\DateTimeUtils;
use Doctrine\ORM\EntityManagerInterface;

class CodeCounterManager
{
  private EntityManagerInterface $entityManager;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->entityManager = $entityManager;
  }

  /**
   * @throws \Exception
   */
  public function getNextCode(CodeGenerationStrategy $cgs, \DateTime $date, $prefix = null, $postfix = null): string
  {

    $counterKey = $this->getCounterKey($cgs, $date);
    $this->entityManager->beginTransaction();
    try {
      $counter = $this->getCounter($cgs, $counterKey);

      $number = $counter->incrementCounter();

      $this->entityManager->persist($counter);
      $this->entityManager->flush();
      $this->entityManager->commit();

      $code = '';
      $code .= $prefix ?? $cgs->getPrefix();
      $code .= str_pad($number, $cgs->getCounterDigitLength(), '0', STR_PAD_LEFT);
      $code .= $postfix ?? $cgs->getPostfix();

      return $code;
    } catch (\Exception $e) {
      $this->entityManager->rollback();
      throw $e;
    }
  }

  private function getCounterKey(CodeGenerationStrategy $cgs, \DateTime $date): string
  {
    switch ($cgs->getTemporalReset()) {
      case DateTimeUtils::PERIOD_DAILY:
        return $date->format('Y-m-d');
      case DateTimeUtils::PERIOD_WEEKLY:
        return $date->format('Y-W');
      case DateTimeUtils::PERIOD_MONTHLY:
        return $date->format('Y-m');
      case DateTimeUtils::PERIOD_YEARLY:
        return $date->format('Y');
      case DateTimeUtils::PERIOD_NONE:
      default:
        return 'general';
    }
  }

  private function getCounter(CodeGenerationStrategy $cgs, string $key): CodeCounter
  {
    $codeRepository = $this->entityManager->getRepository(CodeCounter::class);
    $codeCounter = $codeRepository->findOneBy([
      'codeGenerationStrategy' => $cgs,
      'key' => $key
    ]);

    if (!$codeCounter instanceof CodeCounter) {
      $codeCounter = new CodeCounter();
      $codeCounter
        ->setCodeGenerationStrategy($cgs)
        ->setKey($key);
    }

    return $codeCounter;
  }

}
