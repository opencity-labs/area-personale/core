<?php

namespace App\Services\Manager;

use App\Entity\CPSUser;
use App\Entity\Pratica;
use App\Entity\ProfileBlock;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ProfileBlockManager
{

  private EntityManagerInterface $entityManager;
  private LoggerInterface $logger;

  /**
   * @param EntityManagerInterface $entityManager
   * @param LoggerInterface $logger
   */
  public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
  {
    $this->entityManager = $entityManager;
    $this->logger = $logger;
  }

  /**
   * @throws \JsonException
   */
  public function save(Pratica $application, $key, $value, $subformUrl): void
  {
    // Per adesso non gestiamo l'applicant
    if ($key === 'applicant') {
      return;
    }

    $user = $application->getUser();
    $hash = md5(json_encode($value, JSON_THROW_ON_ERROR));
    $uniqueId = !empty($value['data']['unique_id']) ? $value['data']['unique_id'] : $key;

    $profileBlockRepo = $this->entityManager->getRepository(ProfileBlock::class);
    $profileBlock = $profileBlockRepo->findOneBy([
      'user' => $user,
      'key' => $key,
      'uniqueId' => $uniqueId
    ]) ?? new ProfileBlock();

    $profileBlock
      ->setUser($user)
      ->setKey($key)
      ->setUniqueId($uniqueId)
      ->setValue($value)
      ->setSubformUrl($subformUrl)
      ->setOrigin($this->generateOrigin($application))
      ->setHash($hash);

    $this->entityManager->persist($profileBlock);
    $this->entityManager->flush();

  }


  public function generateSubmissionFromProfileBlocks(CPSUser $user, ?array $profileBlocks): array
  {

    return [];

    $submission = [];
    if (!$profileBlocks) {
      return $submission;
    }
    $profileBlockRepo = $this->entityManager->getRepository(ProfileBlock::class);
    foreach ($profileBlocks as $key => $value) {
      $profileBlock = $profileBlockRepo->findOneBy(['user' => $user, 'key' => $key, "subformUrl" => $value['url']]);
      if ($profileBlock instanceof ProfileBlock) {
        $value = $profileBlock->getValue();
        unset($value['meta']);
        $submission[$key] = $value;
      }
    }

    return $submission;
  }

  private function generateOrigin(Pratica $application): array
  {
    $service = $application->getServizio();
    return [
      'service_id' => $service->getId(),
      'service_name' => $service->getName(),
      'application_id' => $application->getId(),
      'session_id' => $application->getSessionData()->getId()
    ];
  }
}
