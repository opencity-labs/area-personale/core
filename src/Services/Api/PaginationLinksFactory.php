<?php

namespace App\Services\Api;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Model\LinksPagedList;

class PaginationLinksFactory
{
  private UrlGeneratorInterface $urlGenerator;

  public function __construct(UrlGeneratorInterface $urlGenerator)
  {
    $this->urlGenerator = $urlGenerator;
  }

  public function createLinks(string $route, int $offset, int $limit, int $total, array $parameters): LinksPagedList
  {
    $links = new LinksPagedList();

    // Set self link
    $links->setSelf($this->generateUrl($route, $parameters));

    // Set previous link if applicable
    if ($offset > 0) {
      $parameters['offset'] = max(0, $offset - $limit);
      $links->setPrev($this->generateUrl($route, $parameters));
    }

    // Set next link if applicable
    if ($offset + $limit < $total) {
      $parameters['offset'] = $offset + $limit;
      $links->setNext($this->generateUrl($route, $parameters));
    }

    return $links;
  }

  private function generateUrl(string $route, array $parameters): string
  {
    return $this->urlGenerator->generate($route, $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
  }
}
