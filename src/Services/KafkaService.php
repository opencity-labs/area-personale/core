<?php

namespace App\Services;

use App\Applications\Domain\ApplicationLiteDataTransformer;
use App\Dto\Admin as AdminDto;
use App\Dto\ApplicationDto;
use App\Dto\Message as MessageDto;
use App\Dto\Operator as OperatorDto;
use App\Dto\ServiceDto;
use App\Dto\User as UserDto;
use App\Entity\ApplicationLite;
use App\Entity\Calendar;
use App\Entity\CPSUser;
use App\Entity\Meeting;
use App\Entity\Message;
use App\Entity\OperatoreUser;
use App\Entity\Pratica;
use App\Entity\ScheduledAction;
use App\Entity\Servizio;
use App\Entity\User;
use App\Model\Payment;
use App\Model\Security\SecurityLogInterface;
use App\ScheduledAction\Exception\AlreadyScheduledException;
use App\ScheduledAction\ScheduledActionHandlerInterface;
use DateTime;
use DateTimeInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use ReflectionException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class KafkaService implements ScheduledActionHandlerInterface
{

  private const ACTION_PRODUCE_MESSAGE = 'produce_message';

  /**
   * @var ScheduleActionService
   */
  private ScheduleActionService $scheduleActionService;

  /**
   * @var SerializerInterface
   */
  private SerializerInterface $serializer;

  /**
   * @var RouterInterface
   */
  private RouterInterface $router;

  /**
   * @var VersionService
   */
  private VersionService $versionService;

  /**
   * @var LoggerInterface
   */
  private LoggerInterface $logger;

  private $kafkaUrl;

  private array $kafkaEventVersions = [
    'applications' => 2,
    'services' => 2,
    'meetings' => 2,
    'calendars' => 2,
    'security' => 2,
    'users' => 2,
    'messages' => 2,
    'payments' => 1,
  ];

  private int $fallbackKafkaEventVersion = 1;

  private $topics;

  /**
   * @var FormServerApiAdapterService
   */
  private FormServerApiAdapterService $formServerApiAdapterService;

  /**
   * @var ApplicationDto
   */
  private ApplicationDto $applicationDto;

  /**
   * @var ServiceDto
   */
  private ServiceDto $serviceDto;

  /**
   * @var InstanceService
   */
  private InstanceService $instanceService;

  private $kafkaRequestTimeout;
  private ApplicationLiteDataTransformer $applicationLiteDataTransformer;

  /**
   * WebhookService constructor.
   * @param ApplicationLiteDataTransformer $applicationLiteDataTransformer
   * @param ScheduleActionService $scheduleActionService
   * @param SerializerInterface $serializer
   * @param VersionService $versionService
   * @param LoggerInterface $logger
   * @param FormServerApiAdapterService $formServerApiAdapterService
   * @param ApplicationDto $applicationDto
   * @param ServiceDto $serviceDto
   * @param InstanceService $instanceService
   * @param RouterInterface $router
   * @param $kafkaUrl
   * @param $topics
   * @param $kafkaRequestTimeout
   */
  public function __construct(
    ApplicationLiteDataTransformer $applicationLiteDataTransformer,
    ScheduleActionService          $scheduleActionService,
    SerializerInterface            $serializer,
    VersionService                 $versionService,
    LoggerInterface                $logger,
    FormServerApiAdapterService    $formServerApiAdapterService,
    ApplicationDto                 $applicationDto,
    ServiceDto                     $serviceDto,
    InstanceService                $instanceService,
    RouterInterface                $router,
                                   $kafkaUrl,
    $topics,
    $kafkaRequestTimeout
  ) {
    $this->scheduleActionService = $scheduleActionService;
    $this->serializer = $serializer;
    $this->versionService = $versionService;
    $this->logger = $logger;
    $this->kafkaUrl = $kafkaUrl;
    $this->topics = $topics;
    $this->formServerApiAdapterService = $formServerApiAdapterService;
    $this->applicationDto = $applicationDto;
    $this->serviceDto = $serviceDto;
    $this->instanceService = $instanceService;
    $this->kafkaRequestTimeout = $kafkaRequestTimeout;
    $this->router = $router;
    $this->applicationLiteDataTransformer = $applicationLiteDataTransformer;
  }

  /**
   * @param $params
   */
  private function produceMessageAsync($params)
  {
    try {
      $this->scheduleActionService->appendAction(
        'ocsdc.kafka_service',
        self::ACTION_PRODUCE_MESSAGE,
        serialize($params),
      );
    } catch (AlreadyScheduledException $e) {
      $this->logger->error('Kafka message is already scheduled', $params);
    }
  }

  /**
   * @param ScheduledAction $action
   * @throws Exception
   * @throws GuzzleException
   */
  public function executeScheduledAction(ScheduledAction $action)
  {
    $params = unserialize($action->getParams());
    if ($action->getType() == self::ACTION_PRODUCE_MESSAGE) {
      $this->sendMessage($params);
    }
  }

  /**
   * @param $item
   * @param null $eventVersion override default event version
   * @throws GuzzleException
   * @throws ReflectionException
   */
  public function produceMessage($item, $eventVersion = null)
  {
    if (empty($this->kafkaUrl)) {
      return;
    }

    $context = new SerializationContext();
    $context->setSerializeNull(true);

    // Todo: va creato un registry con i mapper delle singole entità
    if ($item instanceof ApplicationLite) {
      $content = $this->applicationLiteDataTransformer->toApplicationEvent($item);
      $topic = $this->topics['applications'];
      $context->setGroups('kafka');
      $eventVersion = $this->kafkaEventVersions['applications'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof Pratica) {
      $content = $this->applicationDto->fromEntity($item, true);
      $topic = $this->topics['applications'];
      // Da riattivare dopo aver inserito un test sul serializer delle application
      //$context->setGroups('kafka');
      $eventVersion = $this->kafkaEventVersions['applications'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof Servizio) {
      $content = $this->serviceDto->fromEntity($item, $this->formServerApiAdapterService->getFormServerPublicUrl());
      $topic = $this->topics['services'];
      $eventVersion = $this->kafkaEventVersions['services'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof Meeting) {
      $context->setGroups('kafka');
      $content = $item;
      $topic = $this->topics['meetings'];
      $eventVersion = $this->kafkaEventVersions['meetings'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof Calendar) {
      $context->setGroups('kafka');
      $content = $item;
      $topic = $this->topics['calendars'];
      $eventVersion = $this->kafkaEventVersions['calendars'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof SecurityLogInterface) {
      $content = $item;
      $topic = $this->topics['security'];
      $eventVersion = $this->kafkaEventVersions['security'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof User) {
      if ($item instanceof CPSUser) {
        $content = UserDto::fromEntity($item);
      } elseif ($item instanceof OperatoreUser) {
        $content = OperatorDto::fromEntity($item);
      } else {
        $content = AdminDto::fromEntity($item);
      }
      $context->setGroups('kafka');
      $topic = $this->topics['users'];
      $eventVersion = $this->kafkaEventVersions['users'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof Message) {
      $context->setGroups('kafka');
      $applicationBaseUrl = $this->router->generate('applications_api_list', [], UrlGeneratorInterface::ABSOLUTE_URL);
      $content = MessageDto::fromEntity($item, $applicationBaseUrl . '/' . $item->getId());
      $topic = $this->topics['messages'];
      $eventVersion = $this->kafkaEventVersions['messages'] ?? $this->fallbackKafkaEventVersion;

    } elseif ($item instanceof Payment) {
      $context->setGroups('kafka');
      $context->setVersion('1');
      $content = $item;
      $topic = $this->topics['payments'];
      // FIXME: Se viene specificata la versione come parametro della funzione sovrascrivo i valori di default per gli eventi inseriti nel topic payments
      $eventVersion = $eventVersion ?? $this->kafkaEventVersions['payments'] ?? $this->fallbackKafkaEventVersion;
    } else {
      $topic = 'default';
      $content = $item;
      $eventVersion = $this->fallbackKafkaEventVersion;
    }

    $data = json_decode($this->serializer->serialize($content, 'json', $context), true);

    // todo: fix veloce, prevedere tenant_id nelle entità per il multi tenant
    if (!isset($data['tenant_id'])) {
      $data['tenant_id'] = $this->instanceService->getCurrentInstance()->getId();
    }

    $data['event_id'] = Uuid::uuid4()->toString();
    $date = new DateTime();
    $data['event_created_at'] = $date->format(DateTimeInterface::W3C);
    $data['event_version'] = $eventVersion;
    $data['app_version'] = $this->versionService->getVersion();
    $data['app_id'] = 'symfony-core:' . $this->versionService->getVersion();

    $data = json_encode($data);

    $params = [
      'topic' => $topic,
      'data' => $data,
    ];

    try {
      $this->sendMessage($params);
    } catch (Exception $e) {
      $this->logger->error($e->getMessage());
      $this->produceMessageAsync($params);
    }
  }

  /**
   * @param $params
   * @throws GuzzleException
   * @throws Exception
   */
  private function sendMessage($params)
  {

    $client = new Client();
    $headers = ['Content-Type' => 'application/json'];

    if (substr($this->kafkaUrl, -1) != '/') {
      $this->kafkaUrl .= '/';
    }
    $url = $this->kafkaUrl . $params['topic'];

    $request = new Request(
      'POST',
      $url,
      $headers,
      $params['data'],
    );

    /** @var GuzzleResponse $response */
    $response = $client->send($request, ['timeout' => $this->kafkaRequestTimeout]);

    $this->logger->debug('KAFKA-EVENT', $params);

    if (!in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED])) {
      throw new Exception("Error sending kafka message: " . $response->getBody());
    }
  }

}
