<?php

namespace App\Repository;

use App\Entity\CodeGenerationStrategy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CodeGenerationStrategy>
 *
 * @method CodeGenerationStrategy|null find($id, $lockMode = null, $lockVersion = null)
 * @method CodeGenerationStrategy|null findOneBy(array $criteria, array $orderBy = null)
 * @method CodeGenerationStrategy[]    findAll()
 * @method CodeGenerationStrategy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CodeGenerationStrategyRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, CodeGenerationStrategy::class);
  }

  /**
   * @throws ORMException
   * @throws OptimisticLockException
   */
  public function add(CodeGenerationStrategy $entity, bool $flush = true): void
  {
    $this->_em->persist($entity);
    if ($flush) {
      $this->_em->flush();
    }
  }

  /**
   * @throws ORMException
   * @throws OptimisticLockException
   */
  public function remove(CodeGenerationStrategy $entity, bool $flush = true): void
  {
    $this->_em->remove($entity);
    if ($flush) {
      $this->_em->flush();
    }
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  public function getStrategies($parameters = [], $onlyCount = false, $order = 'created_at', $sort = 'ASC', $offset = 0, $limit = 10)
  {

    $qb = $this->createQueryBuilder('code_generation_strategy');

    if ($onlyCount) {
      $qb->select('COUNT(code_generation_strategy.id)');
      return $qb->getQuery()->getSingleScalarResult();
    }

    $qb
      ->orderBy('code_generation_strategy.' . $order, $sort)
      ->setFirstResult($offset)
      ->setMaxResults($limit);

    return $qb->getQuery()->execute();
  }
}
