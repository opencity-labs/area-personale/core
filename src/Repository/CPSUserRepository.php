<?php

namespace App\Repository;

use App\Entity\CPSUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

class CPSUserRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, CPSUser::class);
  }

  /**
   * @throws NonUniqueResultException
   */
  public function findByTaxCode(string $taxCode): ?CPSUser
  {
    $qb = $this
      ->createQueryBuilder('u')
      ->andWhere('UPPER(u.codiceFiscale) = :taxCode')
      ->setParameter('taxCode', strtoupper($taxCode));

    return $qb->getQuery()->getOneOrNullResult();
  }
}
