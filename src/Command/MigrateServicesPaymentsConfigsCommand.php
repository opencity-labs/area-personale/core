<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Servizio;
use App\Model\Payment\PaymentPhase;
use App\Model\Service\PaymentConfig;
use App\Payment\Gateway\Bollo;
use App\Payment\Gateway\MyPay;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MigrateServicesPaymentsConfigsCommand extends Command
{
  protected static $defaultName = 'ocsdc:services:migrate-payments-configs';

  protected static $defaultDescription = 'Migrate old payments configurations stored in payment_parameters field to payment_configs field to be compliant with new multiple payments system.';

  private EntityManagerInterface $entityManager;

  protected function configure(): void
  {
    $this->addOption('run', null, InputOption::VALUE_NONE, 'Run option, if passed changes wil be executed');
  }

  public function __construct(EntityManagerInterface $entityManager)
  {
    parent::__construct();
    $this->entityManager = $entityManager;
  }

  /**
   * @throws Exception
   */
  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $io = new SymfonyStyle($input, $output);
    $io->title('Migrating payments configurations');

    $run = $input->getOption('run');

    try {
      $repository = $this->entityManager->getRepository(Servizio::class);
      $qb = $repository->createQueryBuilder('s')
        ->where('s.paymentParameters IS NOT NULL')
        ->andWhere('s.paymentRequired IN (1, 2)')
      ;

      $services = $qb->getQuery()->getResult();

      $servicesToUpdate = 0;
      /** @var Servizio $service */
      foreach ($services as $service) {
        $paymentParameters = $service->getPaymentParameters();
        $selectedGateways = $paymentParameters['gateways'] ?? [];
        $identifier = reset($selectedGateways)['identifier'];

        if (empty($identifier)) {
          $io->error('Gateway identifier not found for service ' . $service->getName() . ' (' . $service->getId() . ')');
          continue;
        }

        if ($identifier === MyPay::IDENTIFIER || $identifier === Bollo::IDENTIFIER) {
          $io->warning('Service ' . $service->getName() . ' (' . $service->getId() . ') will be skipped because has legacy payments configurations!');
          continue;
        }

        if ($service->getPaymentGatewayIdentifier() && $service->getPaymentConfigs() !== []) {
          $io->warning('Service ' . $service->getName() . ' (' . $service->getId() . ') will be skipped because is already configured for multiple payments!');
          continue;
        }

        ++$servicesToUpdate;
        $io->info($service->getName() . ' (' . $service->getId() . ') will be updated');
        $service->setPaymentGatewayIdentifier($identifier);

        $paymentConfig = new PaymentConfig();
        $paymentConfig->setId((string)$service->getId());
        $phase = $service->getPaymentRequired() === Servizio::PAYMENT_REQUIRED ? PaymentPhase::PHASE_REQUEST : PaymentPhase::PHASE_RELEASE;
        $paymentConfig->setPhase($phase);
        $service->setPaymentConfigs([$paymentConfig]);

        if ($run) {
          $this->entityManager->persist($service);
        }
      }

      if ($run) {
        $this->entityManager->flush();
      }

      if ($servicesToUpdate !== 0) {
        if ($run) {
          $io->note($servicesToUpdate . ' service have been updated.');
        } else {
          $io->success('There are  ' . $servicesToUpdate . ' services to update.');
        }
      } else {
        $io->note('There are no services to update.');
      }

    } catch (Exception $exception) {
      $io->error('Error: ' . $exception->getMessage());
      return Command::FAILURE;
    }

    return Command::SUCCESS;
  }

}
